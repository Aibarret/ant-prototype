using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor {
    public SerializedProperty defaultSelectedObject;

    public bool showInputSystem = false;

    // input system
    public SerializedProperty playerSelect;
    public SerializedProperty playerErase;
    public SerializedProperty cancelDraw;
    public SerializedProperty controllerPanCursor;
    public SerializedProperty mousePosition;
    public SerializedProperty useFireSpray;
    public SerializedProperty searchButton;
    public SerializedProperty centerControllerMouse;

    // camera inputs
    public SerializedProperty moveAction;
    public SerializedProperty playerPanCamera;
    public SerializedProperty mouseDelta;
    public SerializedProperty toQueen;

    // buttons
    public SerializedProperty buttonX;
    public SerializedProperty buttonZ;
    public SerializedProperty buttonC;
    public SerializedProperty pauseButton;
    public SerializedProperty debugButton;
    public SerializedProperty exitButton;

    // hotkeys
    public SerializedProperty switchDraw;
    public SerializedProperty switchErase;
    public SerializedProperty switchSearch;
    public SerializedProperty hotkey1;
    public SerializedProperty hotkey2;
    public SerializedProperty hotkey3;
    public SerializedProperty hotkey4;
    public SerializedProperty hotkey5;
    public SerializedProperty hotkey6;
    public SerializedProperty hotkey7;
    public SerializedProperty hotkey8;
    public SerializedProperty hotkey9;
    public SerializedProperty hotkey10;
    public SerializedProperty hotkey11;
    public SerializedProperty hotkey12;

    // idk where else to put these
    public SerializedProperty selectLeft;
    public SerializedProperty selectRight;
    public SerializedProperty selectSwapRow;

    public SerializedProperty gameChecks;

    void OnEnable() {
        EditorGUIUtility.labelWidth = 200;

        defaultSelectedObject = serializedObject.FindProperty("defaultSelectedObject");

        // input system
        playerSelect = serializedObject.FindProperty("playerSelect");
        playerErase = serializedObject.FindProperty("playerErase");
        cancelDraw = serializedObject.FindProperty("cancelDraw");
        controllerPanCursor = serializedObject.FindProperty("controllerPanCursor");
        mousePosition = serializedObject.FindProperty("mousePosition");
        useFireSpray = serializedObject.FindProperty("useFireSpray");
        searchButton = serializedObject.FindProperty("searchButton");
        centerControllerMouse = serializedObject.FindProperty("centerControllerMouse");

        // camera inputs
        moveAction = serializedObject.FindProperty("moveAction");
        playerPanCamera = serializedObject.FindProperty("playerPanCamera");
        mouseDelta = serializedObject.FindProperty("mouseDelta");
        toQueen = serializedObject.FindProperty("toQueen");

        // buttons
        buttonX = serializedObject.FindProperty("buttonX");
        buttonZ = serializedObject.FindProperty("buttonZ");
        buttonC = serializedObject.FindProperty("buttonC");
        switchDraw = serializedObject.FindProperty("switchDraw");
        switchErase = serializedObject.FindProperty("switchErase");
        switchSearch = serializedObject.FindProperty("switchSearch");
        pauseButton = serializedObject.FindProperty("pauseButton");
        debugButton = serializedObject.FindProperty("debugButton");
        exitButton = serializedObject.FindProperty("exitButton");

        // hotkeys
        hotkey1 = serializedObject.FindProperty("hotkey1");
        hotkey2 = serializedObject.FindProperty("hotkey2");
        hotkey3 = serializedObject.FindProperty("hotkey3");
        hotkey4 = serializedObject.FindProperty("hotkey4");
        hotkey5 = serializedObject.FindProperty("hotkey5");
        hotkey6 = serializedObject.FindProperty("hotkey6");
        hotkey7 = serializedObject.FindProperty("hotkey7");
        hotkey8 = serializedObject.FindProperty("hotkey8");
        hotkey9 = serializedObject.FindProperty("hotkey9");
        hotkey10 = serializedObject.FindProperty("hotkey10");
        hotkey11 = serializedObject.FindProperty("hotkey11");
        hotkey12 = serializedObject.FindProperty("hotkey12");

        // idk where else to put these
        selectLeft = serializedObject.FindProperty("selectLeft");
        selectRight = serializedObject.FindProperty("selectRight");
        selectSwapRow = serializedObject.FindProperty("selectSwapRow");
        

        gameChecks = serializedObject.FindProperty("gameChecks");
    }

    override public void OnInspectorGUI() {
        serializedObject.Update();

        EditorGUILayout.PropertyField(defaultSelectedObject);

        showInputSystem = EditorGUILayout.Foldout(showInputSystem, "Input System");

        if (showInputSystem) {

            // input system
            EditorGUILayout.PropertyField(playerSelect);
            EditorGUILayout.PropertyField(playerErase);
            EditorGUILayout.PropertyField(cancelDraw);
            EditorGUILayout.PropertyField(controllerPanCursor);
            EditorGUILayout.PropertyField(mousePosition);
            EditorGUILayout.PropertyField(useFireSpray);
            EditorGUILayout.PropertyField(searchButton);
            EditorGUILayout.PropertyField(centerControllerMouse);

            // camera inputs
            EditorGUILayout.PropertyField(moveAction);
            EditorGUILayout.PropertyField(playerPanCamera);
            EditorGUILayout.PropertyField(mouseDelta);
            EditorGUILayout.PropertyField(toQueen);


            // buttons
            EditorGUILayout.PropertyField(buttonX);
            EditorGUILayout.PropertyField(buttonZ);
            EditorGUILayout.PropertyField(buttonC);
            EditorGUILayout.PropertyField(pauseButton);
            EditorGUILayout.PropertyField(debugButton);
            EditorGUILayout.PropertyField(exitButton);

            // hotkeys
            EditorGUILayout.PropertyField(switchDraw);
            EditorGUILayout.PropertyField(switchErase);
            EditorGUILayout.PropertyField(switchSearch);
            EditorGUILayout.PropertyField(hotkey1);
            EditorGUILayout.PropertyField(hotkey2);
            EditorGUILayout.PropertyField(hotkey3);
            EditorGUILayout.PropertyField(hotkey4);
            EditorGUILayout.PropertyField(hotkey5);
            EditorGUILayout.PropertyField(hotkey6);
            EditorGUILayout.PropertyField(hotkey7);
            EditorGUILayout.PropertyField(hotkey8);
            EditorGUILayout.PropertyField(hotkey9);
            EditorGUILayout.PropertyField(hotkey10);
            EditorGUILayout.PropertyField(hotkey11);
            EditorGUILayout.PropertyField(hotkey12);

            // idk where else to put these
            EditorGUILayout.PropertyField(selectLeft);
            EditorGUILayout.PropertyField(selectRight);
            EditorGUILayout.PropertyField(selectSwapRow);
        }

        EditorGUILayout.PropertyField(gameChecks);

        serializedObject.ApplyModifiedProperties();
    }
}
#endif