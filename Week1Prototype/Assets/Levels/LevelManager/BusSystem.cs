using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;

public class BusSystem : MonoBehaviour
{
    public LevelManager LevelManager;

    private Bus masterBus;
    private Bus musicBus;
    private Bus sfxBus;

    // Start is called before the first frame update
    private void Awake()
    {
        masterBus = RuntimeManager.GetBus("bus:/");
        musicBus = RuntimeManager.GetBus("bus:/Music");
        sfxBus = RuntimeManager.GetBus("bus:/SFX");
    }

    public void UpdateVolume()
    {
        masterBus.setVolume(LevelManager.masterVolume);
        musicBus.setVolume(LevelManager.musicVolume);
        sfxBus.setVolume(LevelManager.sfxVolume); //Mathf.Lerp(0, .25f, LevelManager.sfxVolume / 1f)
    }

}
