using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD;

public class SFXManager : MonoBehaviour
{
    public static SFXManager Instance;
    [Header("Object Refs")]
    public Transform emitterContainer;
    public StudioEventEmitter musicEmitter;

    [Header("Prefabs")]
    public GameObject emitterPrefab;

    [Header("Pool Settings")]
    public int poolAmount;

    private int musicProgressionIndex;

    private List<StudioEventEmitter> emitterPool = new List<StudioEventEmitter>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            GameObject.Destroy(gameObject);
        }

        generatePool();
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Tab))
        {
            //incrementMusic();
            musicEmitter.EventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            playMusic("E2Lvl2");
        }*/
    }


    /*    private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                playSFX("event:/Queen SFX/Corgi/Bark", new Vector3(5000, 5000));
            }
        }*/

    public void playMusic(string eventPath)
    {
        if (eventPath == "")
        {
            return;
        }

        try
        {
            musicEmitter.Stop();
            EventReference newEvent = FMODUnity.RuntimeManager.PathToEventReference("event:/Music/" + eventPath);
            musicEmitter.EventReference = newEvent;
            musicProgressionIndex = 0;
            musicEmitter.Play();
        }
        catch (EventNotFoundException errorCode)
        {
            print("Failed to find sound effect: " + eventPath + " with error: " + errorCode);
        }
    }

    public void stopMusic()
    {
        musicEmitter.Stop();
    }

    public void incrementMusic()
    {
        musicProgressionIndex++;
        if (musicProgressionIndex > 2)
        {
            musicProgressionIndex = 2;
        }
        musicEmitter.SetParameter("Parameter 1", musicProgressionIndex);

        print("Incrementing music to "+ musicProgressionIndex);
    }

    public void setMusicIncrement(int setParameter)
    {
        musicProgressionIndex = setParameter;
        if (musicProgressionIndex > 2)
        {
            musicProgressionIndex = 2;
        }
        musicEmitter.SetParameter("Parameter 1", setParameter);


        print("Setting music to " + musicProgressionIndex);
    }

    public void playSFX(string eventPath)
    {
        try
        {
            FMODUnity.RuntimeManager.PlayOneShot(eventPath);
            /*EventReference newEvent = FMODUnity.RuntimeManager.PathToEventReference(eventPath);
            StudioEventEmitter emitter = getEmitterFromPool();

            if (emitter)
            {
                emitter.EventReference = newEvent;
                emitter.gameObject.SetActive(true);
                emitter.Play();
            }*/
        }
        catch (EventNotFoundException errorCode)
        {
            print("Failed to find sound effect: " + eventPath + " with error " +errorCode);
        } 
    }

    public void playSFX(string eventPath, Vector3 posn)
    {
        try
        {
            if (GlobalVariables.cam != null)
            {
                Vector3 camPosn = GlobalVariables.cam.transform.position;
                float aspect = (float)Screen.width / Screen.height;
                float worldHeight = GlobalVariables.cam.orthographicSize * 2;
                float worldWidth = worldHeight * aspect;
                if (posn.x > camPosn.x + (worldWidth * .5f) || posn.x < camPosn.x - (worldWidth * .5f))
                {
                    return;
                }
                if (posn.y > camPosn.y + (worldHeight * .5f) || posn.y < camPosn.y - (worldHeight * .5f))
                {
                    return;
                }
            }

            FMODUnity.RuntimeManager.PlayOneShot(eventPath);

            /*EventReference newEvent = FMODUnity.RuntimeManager.PathToEventReference(eventPath);
            StudioEventEmitter emitter = getEmitterFromPool();

            if (emitter)
            {
                emitter.EventReference = newEvent;
                emitter.gameObject.SetActive(true);
                emitter.Play();
            }*/
        }
        catch (EventNotFoundException errorCode)
        {
            print("Failed to find sound effect: " + eventPath + " with error " + errorCode);
        }
    }



    #region Pool Controls
    private void generatePool()
    {
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject emitterObject = GameObject.Instantiate(emitterPrefab);
            emitterObject.transform.parent = emitterContainer;
            StudioEventEmitter emitter = emitterObject.GetComponent<StudioEventEmitter>();
            emitterObject.SetActive(false);

            emitterPool.Add(emitter);
        }
    }

    private StudioEventEmitter getEmitterFromPool()
    {
        foreach (StudioEventEmitter emitter in emitterPool)
        {
            if (!emitter.gameObject.activeInHierarchy)
            {
                return emitter;
            }
        }

        print("Audio System Warning: Tried to recieve an emitter from pool but found no available emitters. Consider increasing pool amount");
        return null;
    }
    #endregion
}
