using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class EventDeactivateOnComplete : MonoBehaviour
{
    public StudioEventEmitter emitter;

    private void LateUpdate()
    {
        if (isActiveAndEnabled && !emitter.IsPlaying())
        {
            gameObject.SetActive(false);
            
        }
        
    }
}
