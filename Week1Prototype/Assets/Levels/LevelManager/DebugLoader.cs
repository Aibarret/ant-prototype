using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
public class DebugLoader : MonoBehaviour
{
    public static DebugLoader instance { get; private set; }
    public bool isDebugMode;
    public GameData gameData;

    private bool resetDataOnLoad;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("There is more than one debug loader present! ");
            Destroy(this.gameObject);
        }

        /*if (DataPersistenceManager.instance == null)
        {
            GameObject.Instantiate(Resources.Load("DataPersistenceManager"));
        }*/

        instance = this;

        if (isDebugMode)
        {
            print("DEBUG LOADER IS LOADING DATA ======================================================================================");
            DataPersistenceManager.instance.DebugData(gameData);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetData()
    {
        gameData = new GameData();
    }
}
#endif
