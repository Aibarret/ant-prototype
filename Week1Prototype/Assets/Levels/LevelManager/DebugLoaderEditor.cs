using UnityEngine;
using System;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(DebugLoader))]
public class DebugLoaderEditor : Editor {
    public SerializedProperty isDebugMode;
    public SerializedProperty gameData;

    void OnEnable() {
        EditorGUIUtility.labelWidth = 200;

        isDebugMode = serializedObject.FindProperty("isDebugMode");
        gameData = serializedObject.FindProperty("gameData");
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        EditorGUILayout.PropertyField(isDebugMode);
        // button to call method in inspector
        if (GUILayout.Button("Reset Debug Data")) {
            ((DebugLoader)target).ResetData();
        }
        EditorGUILayout.PropertyField(gameData);

        serializedObject.ApplyModifiedProperties();
    }
}
#endif