using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using Steamworks;

public class LevelManager : MonoBehaviour, IDataPersistence
{
    protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;

    [Header("The button that is selected by default when exiting a menu")]
    public GameObject defaultSelectedObject;
    public GameObject controllerSelectedObject;
    public bool isUsingController;
    public bool isUsingLimitDrawing;

    [Header("Sound Settings")]
    public BusSystem BusSystem;
    public float masterVolume = .5f;
    public float musicVolume = .5f;
    public float sfxVolume = .5f;

    [Header("Video Settings")]
    public int resolution;
    public int quality;
    public bool fullscreen;
    public bool vSync;

    // New Input System
    [Header("Input System")]
    public InputAction playerSelect;  
    public InputAction playerErase;
    public InputAction cancelDraw;
    public InputAction controllerPanCursor;
    public InputAction mousePosition;
    //Needs Saving
    public InputAction useFireSpray;
    //Needs Saving
    public InputAction searchButton;
    //Needs Saving
    public InputAction centerControllerMouse;

    

    [Header("Camera")]
    public InputAction moveAction;
    public InputAction playerPanCamera;
    public InputAction mouseDelta;
    public InputAction toQueen;
     
    [Header("Buttons")]
    public InputAction buttonX;
    public InputAction buttonZ;
    //Needs saving
    public InputAction buttonC;
    public InputAction switchDraw;
    public InputAction switchErase;
    public InputAction switchSearch;
    public InputAction pauseButton;
    public InputAction debugButton;
    public InputAction exitButton;
    
    [Header("Hotkeys")]
    public InputAction hotkey1;
    public InputAction hotkey2;
    public InputAction hotkey3;
    public InputAction hotkey4;
    public InputAction hotkey5;
    public InputAction hotkey6;
    public InputAction hotkey7;
    public InputAction hotkey8;
    public InputAction hotkey9;
    public InputAction hotkey10;
    public InputAction hotkey11;
    public InputAction hotkey12;
    // Probably Needs Saving
    public InputAction selectLeft;
    public InputAction selectRight;
    public InputAction selectSwapRow;

    public static LevelManager instance;

    public List<Check> gameChecks = new List<Check>();
    public Dictionary<string, int> levelHighScores = new Dictionary<string, int>();
    public Dictionary<string, int> levelScoresToBeat = new Dictionary<string, int>();

    [HideInInspector] public float cursorSpeed = 15f;
    [HideInInspector] public float cameraSpeed = 20f;


    private int deathCounter;


    void Awake()
    {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        masterVolume = 1;
        musicVolume = .5f;
        sfxVolume = .5f;

        PopulateChecks();
        QueenIcon.GenerateIconFiles();
    }

    void Start() {
        // Adding checks to list
        //Debug.Log("Adding checks to list");
        
        //TODO: LOAD VOLUME SETTINGS FROM FILE HERE!!!!!!
        BusSystem.UpdateVolume();
        //InputSystem.onEvent


        InputSystem.onActionChange += (obj, change) =>
        {
            if (change == InputActionChange.ActionPerformed)
            {
                
                var inputAction = (InputAction)obj;
                var lastControl = inputAction.activeControl;
                var lastDevice = lastControl.device;
                
                if (inputAction.name == "Navigate")
                {
                    return;
                }

                if (!lastDevice.displayName.Equals("Mouse") && !lastDevice.displayName.Equals("Keyboard"))
                {
                    if (!isUsingController)
                    {
                        isUsingController = true;
                        if (controllerSelectedObject != null)
                        {
                            EventSystem.current.SetSelectedGameObject(controllerSelectedObject, new BaseEventData(EventSystem.current));
                        }
                    }
                    
                }
                else
                {
                    if (isUsingController)
                    {
                        isUsingController = false;
                        EventSystem.current.SetSelectedGameObject(null);
                    }
                }
            }
        };
    }

    

public void inputEventMade()
    {

    }

    public void incrementDeathCounter()
    {
        deathCounter++;
        print("Deaths: " + deathCounter);
    }

    public void resetDeathCounter()
    {
        deathCounter = 0;
    }

    public int getDeathCounter()
    {
        return deathCounter;
    }

    public bool ReturnSingleCheck(string checkName) {
        for (int i = 0; i < gameChecks.Count; i++) {
            if (gameChecks[i].checkName == checkName) {
                return gameChecks[i].checkValue;
            }
        }
        Debug.LogError("ReturnSingleCheck(): Check not found: " + checkName);
        return false;
    }

    public void ToggleSingleCheck(string checkName, bool toggle) {
        for (int i = 0; i < gameChecks.Count; i++) {
            if (gameChecks[i].checkName == checkName) {
          
                gameChecks[i].checkValue = toggle;
                return;
            }
        }
        Debug.LogError("ToggleSingleCheck(): Check not found: " + checkName);
    }

    public void ToggleChecks(bool toggle) {
        for (int i = 0; i < gameChecks.Count; i++) {
            gameChecks[i].checkValue = toggle;
        }
    }

    public void SetDefaultSelectedObject() {
        // Set the default selected object
        EventSystem.current.SetSelectedGameObject(defaultSelectedObject, new BaseEventData(EventSystem.current));
    }

    public void SetControllerSelectedObject(GameObject menuObject)
    {
        controllerSelectedObject = menuObject;

        if (isUsingController)
        {
            EventSystem.current.SetSelectedGameObject(menuObject, new BaseEventData(EventSystem.current));
        }
    }

    public void LoadData(GameData gameData)
    {

        // Checks
        this.gameChecks = gameData.checks;
        if (this.gameChecks.Count == 0) {
            PopulateChecks();
        }

        // Control Settings
        cameraSpeed = gameData.cameraSpeed;
        cursorSpeed = gameData.cursorSpeed;
        isUsingLimitDrawing = gameData.isEasyMode;

        resolution = gameData.resolution;
        quality = gameData.quality;
        fullscreen = gameData.fullscreen;
        vSync = gameData.vSync;

        // Key Bindings
        // clear existing bindings
        moveAction.ChangeBinding(0).Erase();
        moveAction.ChangeBinding(0).Erase();
        controllerPanCursor.ChangeBinding(0).Erase();

        // keyboard
        playerSelect = new InputAction(binding: gameData.playerSelectRebind);
        playerErase = new InputAction(binding: gameData.playerEraseRebind);
        toQueen = new InputAction(binding: gameData.toQueenRebind);
        useFireSpray = new InputAction(binding: gameData.useFireSprayRebind);
        cancelDraw = new InputAction(binding: gameData.cancelDrawRebind);
        buttonX = new InputAction(binding: gameData.buttonXRebind);
        buttonZ = new InputAction(binding: gameData.buttonZRebind);
        buttonC = new InputAction(binding: gameData.buttonCRebind);
        switchDraw = new InputAction(binding: gameData.switchDrawRebind);
        switchErase = new InputAction(binding: gameData.switchEraseRebind);
        switchSearch = new InputAction(binding: gameData.switchSearchRebind);
        pauseButton = new InputAction(binding: gameData.pauseButtonRebind);
        selectLeft = new InputAction(binding: gameData.selectLeftRebind);
        selectRight = new InputAction(binding: gameData.selectRightRebind);

        hotkey1 = new InputAction(binding: gameData.hotkey1Rebind);
        hotkey2 = new InputAction(binding: gameData.hotkey2Rebind);
        hotkey3 = new InputAction(binding: gameData.hotkey3Rebind);
        hotkey4 = new InputAction(binding: gameData.hotkey4Rebind);
        hotkey5 = new InputAction(binding: gameData.hotkey5Rebind);
        hotkey6 = new InputAction(binding: gameData.hotkey6Rebind);
        hotkey7 = new InputAction(binding: gameData.hotkey7Rebind);
        hotkey8 = new InputAction(binding: gameData.hotkey8Rebind);
        hotkey9 = new InputAction(binding: gameData.hotkey9Rebind);
        hotkey10 = new InputAction(binding: gameData.hotkey10Rebind);
        hotkey11 = new InputAction(binding: gameData.hotkey11Rebind);
        hotkey12 = new InputAction(binding: gameData.hotkey12Rebind);

        //create composite binding
        moveAction.AddCompositeBinding("2DVector")
            .With("Up", gameData.cameraUpRebind)
            .With("Down", gameData.cameraDownRebind)
            .With("Left", gameData.cameraLeftRebind)
            .With("Right", gameData.cameraRightRebind);

        toQueen = new InputAction(binding: gameData.toQueenRebind);

        // controller

        controllerPanCursor.AddCompositeBinding("2DVector")
            .With("Up", gameData.controllercursorUpRebind)
            .With("Down", gameData.controllercursorDownRebind)
            .With("Left", gameData.controllercursorLeftRebind)
            .With("Right", gameData.controllercursorRightRebind);

        moveAction.AddCompositeBinding("2DVector")
            .With("Up", gameData.controllercameraUpRebind)
            .With("Down", gameData.controllercameraDownRebind)
            .With("Left", gameData.controllercameraLeftRebind)
            .With("Right", gameData.controllercameraRightRebind);

        playerSelect.AddBinding(gameData.controllerplayerSelectRebind);
        cancelDraw.AddBinding(gameData.controllerCancelDrawRebind);
        useFireSpray.AddBinding(gameData.controllerUseFiresprayRebind);
        centerControllerMouse.AddBinding(gameData.controllerCenterControllerMouseRebind);
        toQueen.AddBinding(gameData.controllerToQueenRebind);
        buttonX.AddBinding(gameData.controllerButtonXRebind);
        buttonZ.AddBinding(gameData.controllerButtonZRebind);
        buttonC.AddBinding(gameData.controllerButtonCRebind);
        switchDraw.AddBinding(gameData.controllerSwitchDraw);
        switchErase.AddBinding(gameData.controllerSwitchErase);
        switchSearch.AddBinding(gameData.controllerSwitchSearch);
        pauseButton.AddBinding(gameData.controllerPauseButton);
        selectLeft.AddBinding(gameData.controllerSelectLeftRebind);
        selectRight.AddBinding(gameData.controllerSelectRightRebind);
        selectSwapRow.AddBinding(gameData.controllerSelectSwapRowRebind);


        // Highscores
        // Dicts are too complicated to save directly so they must be saved as two arrays
        this.levelHighScores = new Dictionary<string, int>();

        //print("Loading Highscores: " + gameData.personalBests.pbKeys.Count);
        for (int i = 0; i < gameData.personalBests.pbKeys.Count; i++)
        {
            //print(i + ": " + gameData.personalBests.pbKeys[i] + " - " + gameData.personalBests.pbValues[i]);
            if (!this.levelHighScores.ContainsKey(gameData.personalBests.pbKeys[i]))
            {
                this.levelHighScores.Add(gameData.personalBests.pbKeys[i], gameData.personalBests.pbValues[i]);
            }
        }

       // print("Finished Loading Highscores with a final key count of " + this.levelHighScores.Keys.Count);

    }

 

    public void SaveData(ref GameData gameData)
    {
        gameData.cameraSpeed = cameraSpeed;
        gameData.cursorSpeed = cursorSpeed;
        gameData.isEasyMode = isUsingLimitDrawing;

        gameData.resolution = resolution;
        gameData.quality = quality;
        gameData.fullscreen = fullscreen;
        gameData.vSync = vSync;

        // Checks
        gameData.checks = this.gameChecks;

        // Key Bindings
        foreach (var binding in controllerPanCursor.bindings) {
            //controllerPanCursor.RemoveBinding(binding);
        }
        foreach (var binding in moveAction.bindings) {
            //moveAction.RemoveBinding(binding);
        }

        #region Saving Key and Controller Bindings

        foreach (var binding in LevelManager.instance.playerSelect.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerplayerSelectRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.playerSelectRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.playerSelectRebind = binding.ToString();
            }
        }

        foreach (var binding in playerErase.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.playerEraseRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.playerEraseRebind = binding.ToString();
            }
        }

        foreach (var binding in switchDraw.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSwitchDraw = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.switchDrawRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.switchDrawRebind = binding.ToString();
            }
        }

        foreach (var binding in toQueen.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.toQueenRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.toQueenRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerToQueenRebind = binding.ToString();
            }
        }

        foreach (var binding in cancelDraw.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerCancelDrawRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.cancelDrawRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.cancelDrawRebind = binding.ToString();
            }
        }

        foreach (var binding in buttonX.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.buttonXRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.buttonXRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerButtonXRebind = binding.ToString();
            }
        }

        foreach (var binding in buttonZ.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.buttonZRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.buttonZRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerButtonZRebind = binding.ToString();
            }
        }

        foreach (var binding in buttonC.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.buttonCRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.buttonCRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerButtonCRebind = binding.ToString();
            }
        }

        foreach (var binding in switchErase.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.switchEraseRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.switchEraseRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSwitchErase = binding.ToString();
            }
        }

        foreach (var binding in switchSearch.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.switchSearchRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.switchSearchRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSwitchSearch = binding.ToString();
            }
        }

        foreach (var binding in pauseButton.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.pauseButtonRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.pauseButtonRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerPauseButton = binding.ToString();
            }
        }

        foreach (var binding in selectLeft.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.selectLeftRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.selectLeftRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSelectLeftRebind = binding.ToString();
            }
        }

        foreach (var binding in selectRight.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.selectRightRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.selectRightRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSelectRightRebind = binding.ToString();
            }
        }

        foreach (var binding in selectSwapRow.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.controllerSelectSwapRowRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.controllerSelectSwapRowRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerSelectSwapRowRebind = binding.ToString();
            }
        }

        foreach (var binding in useFireSpray.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.useFireSprayRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.useFireSprayRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerUseFiresprayRebind = binding.ToString();
            }
        }

        foreach (var binding in centerControllerMouse.bindings)
        {
            if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.controllerCenterControllerMouseRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.controllerCenterControllerMouseRebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.controllerCenterControllerMouseRebind = binding.ToString();
            }
        }
        #endregion

        #region Holy Shit 12 Rebinds

        foreach (var binding in hotkey1.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey1Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey1Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey1Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey2.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey2Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey2Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey2Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey3.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey3Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey3Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey3Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey4.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey4Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey4Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey4Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey5.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey5Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey5Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey5Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey6.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey6Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey6Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey6Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey7.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey7Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey7Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey7Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey8.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey8Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey8Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey8Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey9.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey9Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey9Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey9Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey10.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey10Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey10Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey10Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey11.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey11Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey11Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey11Rebind = binding.ToString();
            }
        }
        foreach (var binding in hotkey12.bindings)
        {
            if (binding.ToString().Contains("<Gamepad>"))
            {
                gameData.hotkey12Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Mouse>"))
            {
                gameData.hotkey12Rebind = binding.ToString();
            }
            else if (binding.ToString().Contains("<Keyboard>"))
            {
                gameData.hotkey12Rebind = binding.ToString();
            }
        }

        #endregion

        #region Directional Rebinds

        gameData.cameraUpRebind = moveAction.bindings[1].path;
        gameData.cameraDownRebind = moveAction.bindings[2].path;
        gameData.cameraLeftRebind = moveAction.bindings[3].path;
        gameData.cameraRightRebind = moveAction.bindings[4].path;

        // controller
        gameData.controllercursorUpRebind = controllerPanCursor.bindings[1].path;
        gameData.controllercursorDownRebind = controllerPanCursor.bindings[2].path;
        gameData.controllercursorLeftRebind = controllerPanCursor.bindings[3].path;
        gameData.controllercursorRightRebind = controllerPanCursor.bindings[4].path;

        gameData.controllercameraUpRebind = moveAction.bindings[6].path;
        gameData.controllercameraDownRebind = moveAction.bindings[7].path;
        gameData.controllercameraLeftRebind = moveAction.bindings[8].path;
        gameData.controllercameraRightRebind = moveAction.bindings[9].path;

        #endregion

        // Highscores
        gameData.playerPersonalBests = this.levelHighScores;
        gameData.personalBests.pbKeys = new List<string>();
        gameData.personalBests.pbValues = new List<int>(); 

        int count = 0;
        foreach (KeyValuePair<string, int> dictEntry in gameData.playerPersonalBests)
        {
            gameData.personalBests.pbKeys.Add(dictEntry.Key);
            gameData.personalBests.pbValues.Add(dictEntry.Value);
        }
    }

    public void toggleLevelChecks(string levelName) {
        TextAsset csvData = Resources.Load<TextAsset>("ChecksToMarkAfterLevelCompletion");
        string[] data = csvData.text.Split(new char[] { '\n' });

        string[] levelNames = data[0].Split(new char[] { '\t' }); // first row of names
        int columnIndex = 0;

        foreach (string level in levelNames) {
            if (level == levelName) {
                break;
            }
            columnIndex++;
        }
        
        foreach (string row in data) {
            string[] rowSplit = row.Split(new char[] { '\t' });
            if (rowSplit[columnIndex] == "TRUE") {
                ToggleSingleCheck(rowSplit[0], true);
            }
        }
    }

    public void PopulateChecks() {
        #region Checks
        gameChecks.Add(new Check("hasBeatenLevelTutorial1", false));
        gameChecks.Add(new Check("hasBeatenLevelTutorial3", false));
        gameChecks.Add(new Check("hasBeatenLevelTutorial5", false));
        gameChecks.Add(new Check("hasBeatenLevelPark1", false));
        gameChecks.Add(new Check("hasBeatenLevelPark2", false));
        gameChecks.Add(new Check("hasBeatenLevelPark3", false));
        gameChecks.Add(new Check("hasBeatenLevelPark4", false));
        gameChecks.Add(new Check("hasBeatenLevelPark5", false));
        gameChecks.Add(new Check("hasBeatenLevelCity1", false));
        gameChecks.Add(new Check("hasBeatenLevelCity2", false));
        gameChecks.Add(new Check("hasBeatenLevelCity3", false));
        gameChecks.Add(new Check("hasBeatenLevelCity5", false));

        gameChecks.Add(new Check("hasUnlockedUnitBase", true));
        gameChecks.Add(new Check("hasUnlockedUnitWall", false));
        gameChecks.Add(new Check("hasUnlockedUnitCracker", false));
        gameChecks.Add(new Check("hasUnlockedUnitCharger", false));
        gameChecks.Add(new Check("hasUnlockedUnitLance", false));
        gameChecks.Add(new Check("hasUnlockedUnitMoving", false));
        gameChecks.Add(new Check("hasUnlockedUnitBumper", false));
        gameChecks.Add(new Check("hasUnlockedUnitPatrol", false));
        gameChecks.Add(new Check("hasUnlockedUnitElectric", false));
        gameChecks.Add(new Check("hasUnlockedUnitBoost", false));
        gameChecks.Add(new Check("hasUnlockedUnitToy", false));
        gameChecks.Add(new Check("hasUnlockedUnitPharaoh", false));
        gameChecks.Add(new Check("hasUnlockedUnitSuper", false));

        gameChecks.Add(new Check("hasSeenEnemySpider", true));
        gameChecks.Add(new Check("hasSeenEnemyBeetle", false));
        gameChecks.Add(new Check("hasSeenEnemyFlea", false));
        gameChecks.Add(new Check("hasSeenEnemyTermite", false));
        gameChecks.Add(new Check("hasSeenEnemyStink", false));
        gameChecks.Add(new Check("hasSeenEnemyBee", false));
        gameChecks.Add(new Check("hasSeenEnemyWasp", false));
        gameChecks.Add(new Check("hasSeenEnemyMantis", false));
        gameChecks.Add(new Check("hasSeenEnemySuicide", false));
        gameChecks.Add(new Check("hasSeenEnemyDragon", false));
        gameChecks.Add(new Check("hasSeenEnemyShield", false));
        gameChecks.Add(new Check("hasSeenEnemyArmor", false));
        gameChecks.Add(new Check("hasSeenEnemyCentipede", false));
        gameChecks.Add(new Check("hasSeenEnemyCricket", false));
        gameChecks.Add(new Check("hasSeenEnemyRoly", false));
        gameChecks.Add(new Check("hasSeenEnemyBombardier", false));

        gameChecks.Add(new Check("hasSeenObstacleBridge", false));
        gameChecks.Add(new Check("hasSeenObstacleBattery", false));
        gameChecks.Add(new Check("hasSeenObstacleBall", false));
        gameChecks.Add(new Check("hasSeenObstacleGrill", false));
        gameChecks.Add(new Check("hasSeenObstaclePie", false));
        gameChecks.Add(new Check("hasSeenObstacleSoldier", false));
        gameChecks.Add(new Check("hasSeenObstacleFan", false));
        gameChecks.Add(new Check("hasSeenObstacleFireSpray", false));
        gameChecks.Add(new Check("hasSeenObstacleTires", false));
        gameChecks.Add(new Check("hasSeenObstacleCar", false));
        
        gameChecks.Add(new Check("hasSeenQueenCorgi", false));
        gameChecks.Add(new Check("hasSeenQueenDucklings", false));

        gameChecks.Add(new Check("hasSeenBossBee", false));
        gameChecks.Add(new Check("hasSeenBossTermites", false));

        #endregion
    }

    public void OnEnable() {
        if (SteamManager.Initialized)
        {
            m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(OnGameOverlayActivated);
        }

        //enable the controls
        playerSelect.Enable();
        centerControllerMouse.Enable();
        playerPanCamera.Enable();
        playerErase.Enable();
        cancelDraw.Enable();
        useFireSpray.Enable();
        searchButton.Enable();
        controllerPanCursor.Enable();
        mouseDelta.Enable();
        mousePosition.Enable();
        hotkey1.Enable();
        hotkey2.Enable();
        hotkey3.Enable();
        hotkey4.Enable();
        hotkey5.Enable();
        hotkey6.Enable();
        hotkey7.Enable();
        hotkey8.Enable();
        hotkey9.Enable();
        hotkey10.Enable();
        hotkey11.Enable();
        hotkey12.Enable();
        selectLeft.Enable();
        selectRight.Enable();
        selectSwapRow.Enable();
        buttonX.Enable();
        buttonZ.Enable();
        buttonC.Enable();
        switchDraw.Enable();
        switchErase.Enable();
        switchSearch.Enable();
        pauseButton.Enable();
        debugButton.Enable();
        exitButton.Enable();
        moveAction.Enable();
        toQueen.Enable();
    }

    private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
    {
        if (pCallback.m_bActive != 0)
        {
            Debug.Log("Steam Overlay has been activated");
            GlobalVariables.toggleGamePause(true);
        }
        else
        {
            Debug.Log("Steam Overlay has been closed");
            GlobalVariables.toggleGamePause(false);
        }
    }

    public void OnDisable() {
        //disable the controls
        playerSelect.Disable();
        //Don't know if these are others need to be in here
        playerPanCamera.Disable();
        cancelDraw.Disable();
        controllerPanCursor.Disable();
        mouseDelta.Disable();
        centerControllerMouse.Disable();
        //So I'm making a note of all of them
        playerErase.Disable();
        mousePosition.Disable();
        useFireSpray.Disable();
        searchButton.Disable();
        hotkey1.Disable();
        hotkey2.Disable();
        hotkey3.Disable();
        hotkey4.Disable();
        hotkey5.Disable();
        hotkey6.Disable();
        hotkey7.Disable();
        hotkey8.Disable();
        hotkey9.Disable();
        hotkey10.Disable();
        hotkey11.Disable();
        hotkey12.Disable();
        selectLeft.Disable();
        selectRight.Disable();
        selectSwapRow.Disable();
        buttonX.Disable();
        buttonZ.Disable();
        buttonC.Disable();
        switchDraw.Disable();
        switchErase.Disable();
        switchSearch.Disable();
        pauseButton.Disable();
        debugButton.Disable();
        exitButton.Disable();
        moveAction.Disable();
        toQueen.Disable();
    }
}

[System.Serializable]
public class Check {
    public string checkName;
    public bool checkValue;

    public Check(string checkName, bool checkValue) {
        this.checkName = checkName;
        this.checkValue = checkValue;
    }

    public Check(string checkName) {
        this.checkName = checkName;
        this.checkValue = false;
    }
}
