using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLevelWorkAround : MonoBehaviour
{
    public QueenPoint point;
    // Start is called before the first frame update

    private float time = 5;
    private float elapsed = 0;
    void Update()
    {
        if (elapsed < time)
        {
            elapsed += Time.deltaTime;
        }
        else
        {
            point.queenWinConReached = false;

        }
    }

   
}
