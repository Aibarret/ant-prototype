using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenManager : MonoBehaviour
{
    /* ===== Singleton ===== */
    public static QueenManager Instance { get; private set; } = null;

    public delegate void pathIndexReached(PathEvent eventType, int queenIndex, int pathIndex, int waypointIndex);
    public static pathIndexReached pathWaypointEventCallList;

    public List<GameObject> queenList = new List<GameObject>();
    private List<Queen> queenScriptList = new List<Queen>();

    [HideInInspector] public int activeQueenIndex = 0;
    [HideInInspector] public List<bool> queenWinCons = new List<bool>();
    [HideInInspector] public Queen queen { get => queenList[activeQueenIndex].GetComponent<Queen>();}

    private void Awake()
    {
        Instance = this;
        GlobalVariables.addToCallList(eventHandler);
        
        GlobalVariables.queen = queen;
    }

    public void Start()
    {
        int count = 0;
        foreach (GameObject queen in queenList)
        {
            queenWinCons.Add(false);
            Queen script = queen.GetComponent<Queen>();
            queenScriptList.Add(script);
            script.queenManager = this;
            script.queenIndex = count;
            count++;
        }

        GlobalVariables.ui.queenHUD.initializeHUD(getQueenArray());
    }

    private void Update()
    {
        if (!GlobalVariables.gamePaused)
        {
            if (LevelManager.instance.buttonZ.WasPressedThisFrame())
            {
                if (activeQueenIndex == 0)
                {
                    GlobalVariables.camController.FocusQueen();
                }
                else
                {
                    setActiveQueen(0);

                }
            }
            else if (LevelManager.instance.buttonX.WasPressedThisFrame())
            {
                if (activeQueenIndex == 1)
                {
                    GlobalVariables.camController.FocusQueen();
                }
                else
                {
                    if (queenList.Count >= 2)
                    {
                        setActiveQueen(1);
                    }
                }
            }
            else if (LevelManager.instance.buttonC.WasPressedThisFrame())
            {
                if (activeQueenIndex == 2)
                {
                    GlobalVariables.camController.FocusQueen();
                }
                else
                {
                    if (queenList.Count >= 3)
                    {
                        setActiveQueen(2);
                    }
                }
                
            }
            
        }
    }

    public Queen getQueen()
    {
        return queenScriptList[activeQueenIndex];
    }

    public Queen getQueenAtIndex(int index)
    {
        return queenScriptList[index];
    }

    public Queen[] getQueenArray()
    {
        Queen[] newQueensList = new Queen[3];
        newQueensList[0] = getQueenAtIndex(activeQueenIndex);

        if (queenList.Count > 1)
        {
            int remainingQueenIndex = activeQueenIndex;

            for (int i = 1; i < 3; i++)
            {
                remainingQueenIndex++;
                if (remainingQueenIndex >= queenList.Count)
                {
                    remainingQueenIndex = 0;
                }
                newQueensList[i] = getQueenAtIndex(remainingQueenIndex);
            }
        }

        return newQueensList;
    }

    public void setActiveQueen(int queenIndex)
    {
        print("Setting Active Queen to " + queenIndex);
        if (activeQueenIndex != queenIndex && queenIndex <= queenList.Count)
        {
            //print("turning off " + getQueen().gameObject.name);
            getQueen().invincible = true;
            getQueen().togglePause(true);

            activeQueenIndex = queenIndex;

            //print("turning on " + getQueen().gameObject.name);
            getQueen().invincible = false;
            getQueen().togglePause(false);

            GlobalVariables.ui.queenHUD.rotateQueens(getQueenArray());
        }
    }

    public void rotateQueen(int amount)
    {
        int newIndex = activeQueenIndex + amount;

        if (newIndex >= queenList.Count)
        {
            newIndex = 0;
        }
        else if (newIndex < 0)
        {
            newIndex = queenList.Count - 1;
        }

        setActiveQueen(newIndex);
    }

    public bool checkForWin()
    {
        foreach (bool winCon in queenWinCons)
        {
            if (!winCon)
            {
                return false;
            }
        }
        foreach (Queen queen in queenScriptList)
        {
            queen.invincible = true;
            queen.paused = true;
        }

        GlobalVariables.OnLevelEnd(true);

        return true;
    }

    public void onQueenDeath()
    {
        queenWinCons[activeQueenIndex] = false;
        GlobalVariables.OnLevelEnd(false);
    }

    public void updateProgress()
    {

    }

    public void eventHandler(WaypointEvent wEvent, int index, int miscIndex)
    {
        if (wEvent == WaypointEvent.queenDied)
        {
            onQueenDeath();
        }
    }

    private void OnDestroy()
    {
        Instance = null;
        pathWaypointEventCallList = null;
    }
}
