using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WaypointEvent
{
    queenTimerPause,
    queenEventPause,
    queenInvincible,
    queenChangeSpeed,
    queenNormalSpeed,
    queenChangeAnimation,
    queenAtXPosition,
    queenDied,
    cutsceneMode,
    endCutsceneMode,
    queenWinConReached,
    changeQueenPath
}

public class QueenPoint : MonoBehaviour
{
    [HideInInspector] public bool isInitialized;
    [Header("Debug - Don't Touch")]
    public int indexNumber;
    public int pathNumber;
    public int pathCount;
    [Header("Path Controls")]
    public bool changeQueenPath = false;
    public Vector2 changeQueenPathTo = new Vector2(-1, -1);
    public bool curvePath = false;
    public Transform arcOfCurve;
    public Transform nextWaypoint;
    public int resolution = 10;
    [Header("Pausing Controls")]
    public bool timerStopAtPoint = false;
    public bool eventStopAtPoint = false;
    [Header("Speed Controls")]
    public bool changeQueenSpeed = false;
    public bool revertQueenSpeed = false;
    [Header("Queen Toggles")]
    public bool setTogglesAtWaypoint = false;
    public bool queenToggleSprite = true;
    public bool queenToggleShadow = true;
    public bool queenToggleViewthrough = true;
    [Header("Misc Controls")]
    public bool changeQueenAnimation = false;
    public bool enterCutsceneMode = false;
    public bool makeQueenInvincible = false;
    public bool queenWinConReached = false;
    [Header("Misc Info")]
    public int gainResourceAtPoint = 0;
    public int miscIndex = -1;
    public bool changeSectionCamera = false;
    public Vector3 sendTo;
    public int gainScore = 0;

    public List<Vector3> curvePositions;

    [HideInInspector] public QueenWaypoints waypointManager;

    private void Start()
    {
        waypointManager = transform.parent.GetComponentInParent<QueenWaypoints>();
    }

    

    public void queenAtPoint()
    {
        //print("QUEEN AT POINT " + indexNumber);
        GlobalVariables.eventCall(WaypointEvent.queenAtXPosition, indexNumber, pathNumber);
        //print("QUEEN " + waypointManager.connectedQueen.queenIndex + " HAS REACHED WAYPOINT " + indexNumber + " OF PATH " + pathNumber);
        QueenManager.pathWaypointEventCallList(PathEvent.atWaypoint, waypointManager.connectedQueen.queenIndex, pathNumber, indexNumber);

        if (timerStopAtPoint)
        {
            GlobalVariables.eventCall(WaypointEvent.queenTimerPause, indexNumber, miscIndex);
        }
        else if (eventStopAtPoint)
        {
            GlobalVariables.eventCall(WaypointEvent.queenEventPause, indexNumber, miscIndex);
        }

        if (makeQueenInvincible)
        {
            GlobalVariables.eventCall(WaypointEvent.queenInvincible, indexNumber, miscIndex);
        }

        if (changeQueenSpeed)
        {
            GlobalVariables.eventCall(WaypointEvent.queenChangeSpeed, indexNumber, miscIndex);
        }

        else if (revertQueenSpeed)
        {
            GlobalVariables.eventCall(WaypointEvent.queenNormalSpeed, indexNumber, miscIndex);
        }

        if (changeQueenAnimation)
        {
            GlobalVariables.eventCall(WaypointEvent.queenChangeAnimation, indexNumber, miscIndex);
        }

        if (enterCutsceneMode)
        {
            GlobalVariables.ui.dialogueManager.loadCutscene(miscIndex);
            //GlobalVariables.eventCall(WaypointEvent.cutsceneMode, indexNumber, miscIndex);
        }

        if (changeSectionCamera)
        {
            GlobalVariables.camController.sendSectionCam(transform.position, sendTo);
        }

        if (changeQueenPath)
        {
            GlobalVariables.eventCall(WaypointEvent.changeQueenPath, (int) changeQueenPathTo.x, (int) changeQueenPathTo.y);
            QueenManager.pathWaypointEventCallList(PathEvent.changePath, waypointManager.connectedQueen.queenIndex, (int)changeQueenPathTo.x, (int)changeQueenPathTo.y);
        }
        if (setTogglesAtWaypoint)
        {
            waypointManager.connectedQueen.toggleShadow(queenToggleShadow);
            waypointManager.connectedQueen.toggleSprite(queenToggleSprite);
            waypointManager.connectedQueen.toggleViewThrough(queenToggleViewthrough);

        }

        if (GlobalVariables.ui && gainScore > 0)
            if (GlobalVariables.ui.score != null)
                GlobalVariables.ui.score.IncreaseScore(gainScore);
    }

    private void OnDrawGizmosSelected()
    {
       /* if (curvePath && resolution > 0)
        {
            drawQuadraticCurve();

            for (int i = 1; i < curvePositions.Count; i++)
            {s
                Gizmos.DrawLine(curvePositions[i - 1], curvePositions[i]);
            }

        }*/
    }

    public void togglePathChange(bool isChangePath)
    {
        changeQueenPath = isChangePath;
        waypointManager.pathWaypointChanged();
    }

    public void drawQuadraticCurve()
    {
        curvePositions = new List<Vector3>();
        
        for (int i = 0; i <= resolution; i++)
        {
            float t = i / (float)resolution;
            curvePositions.Add(CalculateQuadraticBezierPoint(t, transform.position, arcOfCurve.position, nextWaypoint.position));
        }
    }

    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 start, Vector3 center, Vector3 end)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;

        Vector3 p = uu * start;
        p += 2 * u * t * center;
        p += tt * end;

        return p;
    }

    private void OnDrawGizmos()
    {
        
    }
}
