using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DucklingType : QueenType, Clickable
{
    public GameObject queenManager;
    private QueenManager qm;
    public GameObject cursorCollison;
    public GameObject egg;
    private ChildCollision cursorHitbox;

    public override string prefabName => "Ducklings";

    public override void start()
    {
        qm = queenManager.GetComponent<QueenManager>();
        cursorHitbox = cursorCollison.GetComponent<ChildCollision>();

    }

    public override void behavior()
    {
        egg.SetActive(QueenManager.Instance.activeQueenIndex != queen.queenIndex || QueenManager.Instance.queenWinCons[queen.queenIndex]);

        if (qm.queen.gameObject.name == gameObject.name)
        {
            base.behavior();
        }
    }

    public override void checkWinCondition(int waypointIndex = -1)
    {
        if (waypointIndex >= queen.pointContainerScript.getActivePath().Count - 1)
        {
            qm.queenWinCons[qm.activeQueenIndex] = true;
            int count = 0;
            while (count <= qm.queenWinCons.Count)
            {
                if (!qm.queenWinCons[count])
                {
                    break;
                }
                count++;
            }

            if (count == qm.queenWinCons.Count - 1)
            {
                //queen.die(true);
                queen.togglePause(true);
            }
            else
            {
                print("Queen Reached end of path and is pausing");
                queen.paused = true;
                queen.invincible = true;
            }
        }
    }

    public override void onPause(bool isPause)
    {
        base.onPause(isPause);
        //egg.SetActive(isPause);
        
    }
    public void onClick()
    {
        if (!qm.queenWinCons[queen.queenIndex])
        {
            if (qm.activeQueenIndex != queen.queenIndex)
            {
                
                QueenManager.Instance.setActiveQueen(queen.queenIndex);
            }
        }
        

    }
}
