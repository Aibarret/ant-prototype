using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class QueenType : MonoBehaviour
{
    
    public Queen queen;

    [Header("Debug")]
    public int waypointDestination;
    public int pathDestination;

    [Header("Icon")]
    public QueenTypeIcons icon;

    [Header("Queen Stats")]
    public float speed = 5;
    public float defaultSpeed = 5;
    public int health = 250;
    public int maxHealth = 250;

    public abstract string prefabName { get; }

    [HideInInspector]
    public bool frameOne = false;


    public virtual void start()
    {

    }

    public virtual void awake()
    {

    }

    public virtual void behavior()
    {
        if (!queen.paused)
        {

            if (queen.isMoveLinear)
            {
                Vector3 dir = queen.target.position - queen.transform.position;
                queen.transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
            }
            else
            {
                if (queen.curveWaypointIndex < queen.curvePositions.Count)
                {
                    //print("AT WAYPOINT " + queen.curveWaypointIndex + " MOVING IN THE DIRECTION OF WAYPOINT " + (queen.curveWaypointIndex + 1) + " OUT OF " + queen.curvePositions.Count);
                    
                    if (queen.curveWaypointIndex + 1 >= queen.curvePositions.Count)
                    {
                        queen.wavepointIndex++;
                        queen.GetNextWaypoint();
                        queen.isMoveLinear = true;
                    }
                    else
                    {
                        transform.position = Vector3.MoveTowards(transform.position, queen.curvePositions[queen.curveWaypointIndex + 1], Time.deltaTime * speed);

                        if (transform.position == queen.curvePositions[queen.curveWaypointIndex + 1])
                        {
                            queen.curveWaypointIndex++;

                        }
                    }

                }
                else
                {
                    //print("QUEEN IS STUCK SOMEONE COME HELP HER OH MY GOD UNLESS THIS IS THE FIRST FRAME IN WHICH CASE THIS IS AN ACCEPTABLE AMOUNT OF STUCK");
                }
                
            }

            if (Vector3.Distance(queen.transform.position, queen.target.position) <= .4f && frameOne == false)
            {

                frameOne = true;
                
                if (!queen.queenManager.queenWinCons[queen.queenIndex])
                {
                    queen.pointContainerScript.getActivePath()[queen.wavepointIndex].queenAtPoint();

                }

                /*if (queen.pointContainerScript.getActivePath()[queen.wavepointIndex].gainResourceAtPoint > 0)
                {
                    GlobalVariables.drawingManager.addToMaxPheromones(queen.pointContainerScript.getActivePath()[queen.wavepointIndex].gainResourceAtPoint);
                }*/



                queen.GetNextWaypoint(queen.approachingChangePath);


            }
        }
    }

    public virtual void onChangeSpritedirection(bool isLeft)
    {

    }

    public virtual void onPause(bool isPause)
    {
       
    }

    public virtual void atWaypoint(int index)
    {

    }

    public virtual void pathChanged(int pathChangedTo)
    {

    }

    public virtual void checkWinCondition(int waypointIndex = -1)
    {
        if (queen.queenManager.checkForWin())
        {
            //queen.die(true);
            queen.invincible = true;
            queen.togglePause(true);
        }
    }

    public virtual void onTimerPause(int time)
    {
        queen.togglePause(true);
        queen.updateThoughtBubble(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.TIMER), time);
        queen.timer.startTimer(time, onPauseTimeout);
    }

    public virtual void onPauseTimeout()
    {
        queen.togglePause(false);
    }

    public virtual void onEventPause()
    {
        queen.togglePause(!queen.paused);
        
    }

    public virtual void onInvincible()
    {
        if (!queen.invincible)
        {
            queen.invincible = true;
        }
        else
        {
            queen.invincible = false;
        }
    }

    public virtual void changeSpeed(int speed)
    {
        this.speed = speed;
        if (speed == 0)
        {
            queen.animator.SetFloat("Speed", 0);
            queen.animator.SetTrigger("Stop");
        }
        else
        {
            queen.animator.SetFloat("Speed", 1);
        }
    }

    public virtual void revertSpeed()
    {
        speed = defaultSpeed;
        queen.animator.SetFloat("Speed", 1);
    }

    public virtual void changeAnimation(int index)
    {

    }

    public virtual void takeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            queen.die(false);
        }
    }
}
