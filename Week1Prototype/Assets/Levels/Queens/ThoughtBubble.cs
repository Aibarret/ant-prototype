using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThoughtBubble : MonoBehaviour
{
    [Header("Images")]
    public GameObject stopImage;
    public GameObject speedUpImage;
    public GameObject slowDownImage;
    public GameObject revertSpeedImage;
    public GameObject timerImage;
    public GameObject timerTextObject;
    public GameObject Divider;
    public GameObject clockwise;
    public GameObject counterClockwise;

    private TMPro.TextMeshProUGUI timerText;

    [Header("Position Variables")]
    public Vector3 defaultPosition;
    public Vector3 HalfSizedPosn;
    public Vector3 HalfSizedPosn2;

    private ThoughtBubbleItem[] pendingThoughts = null;
    private float timerDuration;
    private float frames;

    private void Awake()
    {
        timerText = timerTextObject.GetComponent<TMPro.TextMeshProUGUI>();

    }

    private void Update()
    {
        if (timerDuration > 0)
        {
            if (frames < timerDuration)
            {
                int seconds = (int)( timerDuration - frames);
                int milliseconds = Mathf.FloorToInt(((((timerDuration - frames) % 100) - Mathf.Floor(timerDuration - frames)) % 100) * 100);

                timerText.text = seconds + ":" + (milliseconds < 10 ? "0" + milliseconds : milliseconds);

                frames += Time.deltaTime;
            }
            else
            {
                timerDuration = 0;
                frames = 0;
                

                if (pendingThoughts != null)
                {
                    displayIcons(pendingThoughts);
                }
                else
                {
                    deactivateThoughtBubble();
                }
            }
        }
        
    }


    public void activateThoughtBubble()
    {
        gameObject.SetActive(true);
    }

    public void deactivateThoughtBubble()
    {
        removeIcons();
        gameObject.SetActive(false);
    }

    public void displayIcons(ThoughtBubbleItem[] thoughts)
    {
        
        if (!gameObject.activeInHierarchy)
        {
            activateThoughtBubble();
        }
        else
        {
            removeIcons();
        }

        pendingThoughts = thoughts;

        if (thoughts[1] != null && thoughts[0].type != thoughts[1].type)
        {
            //print("multiple thoughts received");
            GameObject firstIcon = typeToIcon(thoughts[0]);
            GameObject secondIcon = typeToIcon(thoughts[1]);

            if (firstIcon && secondIcon)
            {
                firstIcon.transform.localScale = new Vector3(.75f, .75f);
                firstIcon.transform.localPosition = HalfSizedPosn;// + new Vector3(4.03873634f, 0.961965561f, 0);

                secondIcon.transform.localScale = new Vector3(.75f, .75f);
                secondIcon.transform.localPosition = HalfSizedPosn2;// + new Vector3(4.03873634f, 0.961965561f, 0));


                firstIcon.SetActive(true);
                secondIcon.SetActive(true);
                Divider.SetActive(true);
            }
            else
            {
                print("QUEEN THOUGHT ICON ERROR: ONE OR MORE ICONS RETURNED NULL. Icon 1: " + firstIcon + " Icon 2: " + secondIcon);
            }

            Divider.SetActive(true);
        }
        else if (thoughts[0] != null)
        {
            //print("just one thought received");
            try
            {
                typeToIcon(thoughts[0]).SetActive(true);
            }
            catch
            {
                print("QUEEN THOUGHT ICON ERROR: ICON RETURNED NULL");
            }
        }
        else
        {
            removeIcons();
            deactivateThoughtBubble();
        }
    }

    public void displayIcon(ThoughtBubbleItem thought, float timerSeconds = 0)
    {
        if (gameObject.activeInHierarchy)
        {
            removeIcons(true);
        }
        else
        {
            activateThoughtBubble();
        }

        try
        {
            typeToIcon(thought).SetActive(true);

            if (timerSeconds > 0)
            {
                timerDuration = timerSeconds;
                frames = 0;
                timerTextObject.SetActive(true) ;
            }
        }
        catch
        {
            print("QUEEN THOUGHT ICON ERROR: ICON RETURNED NULL");
        }

        
    }

    public GameObject typeToIcon(ThoughtBubbleItem item)
    {
        switch (item.type)
        {
            case ThoughtBubbleItem.ThoughtTypes.SPEED_UP:
                return speedUpImage;
            case ThoughtBubbleItem.ThoughtTypes.SLOW_DOWN:
                return slowDownImage;
            case ThoughtBubbleItem.ThoughtTypes.NORMAL_SPEED:
                return revertSpeedImage;
            case ThoughtBubbleItem.ThoughtTypes.STOP:
                return stopImage;
            case ThoughtBubbleItem.ThoughtTypes.TIMER:
                return timerImage;
            case ThoughtBubbleItem.ThoughtTypes.CLOCKWISE:
                return clockwise;
            case ThoughtBubbleItem.ThoughtTypes.COUNTER_CLOCKWISE:
                return counterClockwise;
            default:
                return null;
        }
    }

    public void removeIcons(bool skipPendingThoughts = false)
    {
        if (!skipPendingThoughts)
        {
            pendingThoughts = null;
        }

        //Replace this with animations later pls
        stopImage.SetActive(false);
        stopImage.transform.localScale = new Vector3(1,1,1);
        stopImage.transform.localPosition = defaultPosition;
        speedUpImage.SetActive(false);
        speedUpImage.transform.localScale = new Vector3(1, 1, 1);
        speedUpImage.transform.localPosition = defaultPosition;
        slowDownImage.SetActive(false);
        slowDownImage.transform.localScale = new Vector3(1, 1, 1);
        slowDownImage.transform.localPosition = defaultPosition;
        revertSpeedImage.SetActive(false);
        revertSpeedImage.transform.localScale = new Vector3(1, 1, 1);
        revertSpeedImage.transform.localPosition = defaultPosition;
        timerImage.SetActive(false);
        timerTextObject.SetActive(false);
        Divider.SetActive(false);
        Divider.transform.localScale = new Vector3(1, 1, 1);
        Divider.transform.localPosition = defaultPosition;
        clockwise.SetActive(false);
        clockwise.transform.localScale = new Vector3(1, 1, 1);
        clockwise.transform.localPosition = defaultPosition;
        counterClockwise.SetActive(false);
        counterClockwise.transform.localScale = new Vector3(1, 1, 1);
        counterClockwise.transform.localPosition = defaultPosition;
    }
}
