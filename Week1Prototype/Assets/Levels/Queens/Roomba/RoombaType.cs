using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoombaType : QueenType, Clickable
{
   public enum Direction
    {
        UP, RIGHT, DOWN, LEFT
    }

    public override string prefabName => "Roomba";

    [Header("Roomba Variables")]
    public Rigidbody2D rb;
    public Direction dir;
    public int stuckTime;
    public GameObject goalPosition;
    public float goalRadius;

    private GameObject stickyUnit;
    private int stuckCounter;
    private bool isClockwise;

    public override void start()
    {
        updateQueenThought();
    }

    public override void behavior()
    {
        if (Vector3.Distance(transform.position, goalPosition.transform.position) < goalRadius)
        {
            GlobalVariables.eventCall(WaypointEvent.queenWinConReached, 0, queen.queenIndex);
        }
        else
        {
            Vector3 velocity = Vector3.zero;
            switch (dir)
            {
                case Direction.UP:
                    velocity = Vector3.up;
                    break;
                case Direction.DOWN:
                    velocity = Vector3.down;
                    break;
                case Direction.LEFT:
                    velocity = Vector3.left;
                    break;
                case Direction.RIGHT:
                    velocity = Vector3.right;
                    break;
            }

            rb.MovePosition(transform.position + (velocity * speed * Time.deltaTime));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isClockwise)
        {
            dir += 1;
        }
        else
        {
            dir -= 1;
        }

        if ((int) dir > 3)
        {
            dir = Direction.UP;
        }
        else if ((int) dir < 0)
        {
            dir = Direction.LEFT;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("FormationUnit"))
        {
            if (stickyUnit == null)
            {
                stickyUnit = collision.gameObject;
            }

            if (stickyUnit == collision.gameObject)
            {
                stuckCounter += 1;
            }

            if (stuckCounter >= stuckTime)
            {
                stuckCounter = 0;
                GameObject.Destroy(stickyUnit);
                stickyUnit = null;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject == stickyUnit)
        {
            stuckCounter = 0;
            stickyUnit = null;
        }
    }

    private void updateQueenThought()
    {
        List<ThoughtBubbleItem> list = new List<ThoughtBubbleItem>();
        if (isClockwise)
        {
            list.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.CLOCKWISE));
        }
        else
        {
            list.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.COUNTER_CLOCKWISE));
        }

        queen.updateThoughtBubble(list);
    }

    public void onClick()
    {
        isClockwise = !isClockwise;
        updateQueenThought();
    }

    private void OnDrawGizmos()
    {
        if (goalPosition)
        {
            Gizmos.DrawWireSphere(goalPosition.transform.position, goalRadius);
        }
    }
}
