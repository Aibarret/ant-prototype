using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PathEvent
{
    atWaypoint,
    changePath,
}

public class QueenWaypoints : MonoBehaviour
{
    [Header("Debug Info")]
    [SerializeField] private int lineIndex; //The index of where the queen is in the line positions

    [Header("Debug Controls")]
    public bool generateIndexNumber = false;
    public bool drawQueenPathGizmo = true;
    
    [Header("Object Refs")]
    public LineRenderer lineRenderer;
    public GameObject hiddenPathContainer;
    public GameObject hiddenPathPrefab;
    
    [Header("Path Variables")]
    public Color gizmoColor = Color.white;
    public Color hiddenPathColor = Color.white;
    public int fieldOfView;
    public int cornerDefinition;
    public int thoughtBubbleFOV;
    public List<Transform> queenPaths = new List<Transform>();

    [HideInInspector] public int lineLength;
    [HideInInspector] public int lastKnownPosn;


    [HideInInspector] public List<List<QueenPoint>> qPoints = new List<List<QueenPoint>>();
    [HideInInspector] public int pathIndex = 0;
    [HideInInspector] public Queen connectedQueen;

    // a collection of the lists of pathWaypoints that make up the queen's path:
    // 0 = the line thats being erased
    // 1 = the line that has already been drawn
    // 2 = the line thats still being drawn
    private List<PathWaypoint>[] pathLists = { new List<PathWaypoint>(), new List<PathWaypoint>(), new List<PathWaypoint>() };
    private List<int> pathLengths = new List<int>();

    private class PathWaypoint
    {
        public int pathIndex;
        public int waypointIndex;
        public List<Vector3> positions;
        public Vector3 tweenPosition;
        public int tweenPositionIndex;

        public PathWaypoint(List<Vector3> positions, int pathIndex, int waypointIndex)
        {
            this.positions = positions;
            this.pathIndex = pathIndex;
            this.waypointIndex = waypointIndex;
            tweenPositionIndex = 0;
            tweenPosition = this.positions[0];
        }

        

        public bool isEqual(PathWaypoint comparingWaypoint)
        {
            if (pathIndex == comparingWaypoint.pathIndex && waypointIndex == comparingWaypoint.waypointIndex)
            {
                return true;
            }

            return false;
        }
    }

    private void Awake()
    {
        drawQueenPathGizmo = false;
        //Whenever the scene starts get all the way points and put them in
        //Note all waypoints MUST be children of the game object with the waypoint script

        QueenManager.pathWaypointEventCallList += pathWaypointReached;
        GlobalVariables.callList += eventHandler;

        List<List<QueenPoint>> fullPathList = new List<List<QueenPoint>>();
        qPoints = new List<List<QueenPoint>>();
        int pathCount = 0;
        foreach (Transform pathContainer in queenPaths)
        {
            int totalPathDistance = 0;
            fullPathList.Add(new List<QueenPoint>());
            List<QueenPoint> pointList = new List<QueenPoint>();
            for (int i = 0; i < pathContainer.childCount; i++)
            {
                totalPathDistance++;

                pointList.Add(pathContainer.GetChild(i).GetComponent<QueenPoint>());
                fullPathList[pathCount].Add(pathContainer.GetChild(i).GetComponent<QueenPoint>());
                pointList[i].indexNumber = i;
                pointList[i].pathNumber = pathCount;
                //waypointList.Add(queenPaths[pathIndex].GetChild(i));
            }
            pathLengths.Add(totalPathDistance);
            qPoints.Add(pointList);
            pathCount++;
        }

        foreach (List<QueenPoint> pathPoints in fullPathList)
        {
            GameObject hiddenPathObject = GameObject.Instantiate(hiddenPathPrefab);
            hiddenPathObject.transform.parent = hiddenPathContainer.transform;
            LineRenderer hiddenPath = hiddenPathObject.GetComponent<LineRenderer>();

            hiddenPath.startColor = hiddenPathColor;
            hiddenPath.endColor = hiddenPathColor;

            foreach (QueenPoint point in pathPoints)
            {
                if (point.curvePath)
                {
                    for (int i = 0; i < cornerDefinition; i++)
                    {
                        hiddenPath.positionCount += 1;
                        hiddenPath.SetPosition(hiddenPath.positionCount - 1, point.transform.position);
                    }

                    foreach (Vector3 posn in point.curvePositions)
                    {
                        hiddenPath.positionCount += 1;
                        hiddenPath.SetPosition(hiddenPath.positionCount - 1, posn);
                    }
                }
                else
                {
                    for (int i = 0; i < cornerDefinition; i++)
                    {
                        hiddenPath.positionCount += 1;
                        hiddenPath.SetPosition(hiddenPath.positionCount - 1, point.transform.position);
                    }
                }
            }
        }

        lineRenderer.startColor = gizmoColor;
        lineRenderer.endColor = gizmoColor;
        
    }

    private void Start()
    {
        //connectedQueen.queenManager.
        int pathIndex = 0;
        int waypointIndex = 0;
        int countToEnd = 0;

        while (true)
        {
            if (qPoints[pathIndex][waypointIndex].queenWinConReached || qPoints[pathIndex][waypointIndex].isInitialized)
            {
                break;
            }

            qPoints[pathIndex][waypointIndex].isInitialized = true;

            if (qPoints[pathIndex][waypointIndex].changeQueenPath)
            {
                float newPath = qPoints[pathIndex][waypointIndex].changeQueenPathTo.x;
                float newWaypoint = qPoints[pathIndex][waypointIndex].changeQueenPathTo.y;

                pathIndex = (int)newPath;
                waypointIndex = (int)newWaypoint;
            }
            else
            {
                waypointIndex++;
            }

            countToEnd++;
        }

        lineLength = countToEnd;
    }



    private void LateUpdate()
    {
        // Determine Speed
        // speed will be determined by the number of waypoints in each part's list, with the base point being the queen's speed
        float baseSpeed = connectedQueen.type.speed;
        float pathDrawSpeed;
        float pathEraseSpeed;

        //Determine Erase Speed
        if (pathLists[0].Count >= 3)
        {
            pathEraseSpeed = baseSpeed * 1.5f;
        }
        else
        {
            pathEraseSpeed = baseSpeed;
        }

        //Dertermine Draw Speed
        if (pathLists[2].Count >= 6)
        {
            pathDrawSpeed = baseSpeed * 2f;
        }
        else if (pathLists[2].Count >= 3)
        {
            pathDrawSpeed = baseSpeed * 1.5f;
        }
        else
        {
            pathDrawSpeed = baseSpeed;
        }


        // Tween erase queue
        if (pathLists[0].Count > 0)
        {
            Vector3 startingPosn = Vector3.zero;
            Vector3 endPosn = Vector3.zero;
            if (pathLists[0][0].positions.Count == 1)
            {
                // if the pathWaypoint is just one position, move it's tween position to the next position
                pathLists[0][0].tweenPositionIndex = 0;
                startingPosn = pathLists[0][0].tweenPosition;

                Vector3 targetPosn = Vector3.zero;
                if (pathLists[0].Count > 1)
                {
                    targetPosn = pathLists[0][1].positions[0];
                }
                else if (pathLists[1].Count > 0)
                {
                    targetPosn = pathLists[1][0].positions[0];
                }
                else if (pathLists[2].Count > 0)
                {
                    targetPosn = pathLists[2][0].positions[0];
                }

                pathLists[0][0].tweenPosition = Vector3.MoveTowards(pathLists[0][0].tweenPosition, targetPosn, pathEraseSpeed * Time.deltaTime);

                endPosn = pathLists[0][0].tweenPosition;

                // if the pathWaypoint is at the end of it's path, remove it from the list
                if (pathLists[0][0].tweenPosition == targetPosn)
                {
                    pathLists[0].RemoveAt(0);
                }
            }
            else
            {
                // if there are multiple positions, it get's a bit more complicated
                if (pathLists[0][0].tweenPositionIndex == -1)
                {
                    pathLists[0][0].tweenPositionIndex = 0;
                }

                if (pathLists[0][0].tweenPositionIndex < pathLists[0][0].positions.Count - 1)
                {
                    // tween the position towards the next position in the list
                    startingPosn = pathLists[0][0].tweenPosition;
                    pathLists[0][0].tweenPosition = Vector3.MoveTowards(pathLists[0][0].tweenPosition, pathLists[0][0].positions[pathLists[0][0].tweenPositionIndex + 1], pathEraseSpeed * Time.deltaTime);
                    endPosn = pathLists[0][0].tweenPosition;

                    if (pathLists[0][0].tweenPosition == pathLists[0][0].positions[pathLists[0][0].tweenPositionIndex + 1])
                    {
                        // if the position is at it's destination, increment the tween index

                        pathLists[0][0].tweenPositionIndex++;

                        if (pathLists[0][0].tweenPositionIndex >= pathLists[0][0].positions.Count - 1)
                        {
                            // if the tween index is at the end, then the pathWaypoint is complete and can be safely removed
                            pathLists[0].RemoveAt(0);
                        }
                    }
                }

            }

            // increase the offset of the linerenderer by the distance the line travelled this frame
            if ((Vector3.Distance(startingPosn, endPosn) * .5f) > 0)
            {
                lineRenderer.material.SetFloat("_Offset", (lineRenderer.material.GetFloat("_Offset") + (Vector3.Distance(startingPosn, endPosn) * .5f)));
            }


        }

        // Tween draw queue

        if (pathLists[2].Count > 1)
        {
            if (pathLists[2][0].positions.Count == 1)
            {
                pathLists[2][0].tweenPositionIndex = 0;
                pathLists[2][0].tweenPosition = Vector3.MoveTowards(pathLists[2][0].tweenPosition, pathLists[2][1].positions[0], pathDrawSpeed * Time.deltaTime);

                if (pathLists[2][0].tweenPosition == pathLists[2][1].positions[0])
                {
                    pathLists[2][0].tweenPositionIndex = -1;
                    pathLists[2][0].tweenPosition = pathLists[2][0].positions[0];
                    pathLists[1].Add(pathLists[2][0]);
                    pathLists[2].RemoveAt(0);
                }
            }
            else
            {
                // if there are multiple positions, it get's a bit more complicated
                if (pathLists[2][0].tweenPositionIndex == -1)
                {
                    pathLists[2][0].tweenPositionIndex = 0;
                }

                if (pathLists[2][0].tweenPositionIndex < pathLists[2][0].positions.Count - 1)
                {
                    // tween the position towards the next position in the list
                    pathLists[2][0].tweenPosition = Vector3.MoveTowards(pathLists[2][0].tweenPosition, pathLists[2][0].positions[pathLists[2][0].tweenPositionIndex + 1], pathDrawSpeed * Time.deltaTime);


                    if (pathLists[2][0].tweenPosition == pathLists[2][0].positions[pathLists[2][0].tweenPositionIndex + 1])
                    {
                        // if the position is at it's destination, increment the tween index
                        pathLists[2][0].tweenPositionIndex++;

                        if (pathLists[2][0].tweenPositionIndex >= pathLists[2][0].positions.Count - 1)
                        {
                            // if the tween index is at the end, then the pathWaypoint is complete and can be safely removed
                            pathLists[2][0].tweenPositionIndex = -1;
                            pathLists[2][0].tweenPosition = pathLists[2][0].positions[0];
                            pathLists[1].Add(pathLists[2][0]);
                            pathLists[2].RemoveAt(0);
                        }
                    }
                }
            }
        }

        // Constructs the positions variable of the line renderer from the pathLists
        lineRenderer.positionCount = 0;
        for (int i = 0; i < 3; i++)
        {
            // Loop through each of the pathLists
            foreach (PathWaypoint pw in pathLists[i])
            {
                // check every pathWaypoint in the list, if it has one position, then just add the position with the desired number of corner definitions
                if (pw.positions.Count == 1)
                {
                    if (i == 2)
                    {
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[0]);
                    }
                    lineRenderer.positionCount += 1;
                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.tweenPosition);
                    for (int i2 = 0; i2 < cornerDefinition; i2++)
                    {
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.tweenPosition);
                    }
                }
                else
                {
                    // if there are multiple positions in the pathWaypoint, then check if it's currently being tweened through
                    if (pw.tweenPositionIndex >= 0 && i != 1)
                    {
                        // if being tweened, iterate through, depending on if we're drawing or erasing the line
                        if (i < 1)
                        {
                            // if erasing, draw the positions, starting from it's tween index
                            for (int i2 = pw.tweenPositionIndex; i2 < pw.positions.Count; i2++)
                            {
                                if (i2 == pw.tweenPositionIndex)
                                {
                                    lineRenderer.positionCount += 1;
                                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.tweenPosition);
                                }
                                else
                                {
                                    lineRenderer.positionCount += 1;
                                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[i2]);
                                }

                            }
                        }
                        else if (i > 1)
                        {
                            lineRenderer.positionCount += 1;
                            lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[0]);
                            // if drawing, draw the positions, starting from 0 and ending at it's tween index
                            for (int i2 = 0; i2 <= pw.tweenPositionIndex; i2++)
                            {

                                if (i2 == pw.tweenPositionIndex)
                                {
                                    lineRenderer.positionCount += 1;
                                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.tweenPosition);
                                }
                                else
                                {
                                    lineRenderer.positionCount += 1;
                                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[i2]);
                                }
                            }
                        }
                    }
                    else
                    {
                        //if not tweening, then just iterate through the positions adding them to the line renderer.
                        for (int i2 = 0; i2 < pw.positions.Count; i2++)
                        {
                            if (i2 == 0 || i2 == pw.positions.Count - 1)
                            {
                                // if at the beginning or end of the positions, add desired corner definition
                                for (int i3 = 0; i3 < cornerDefinition; i3++)
                                {
                                    lineRenderer.positionCount += 1;
                                    lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[i2]);
                                }
                            }
                            else
                            {
                                lineRenderer.positionCount += 1;
                                lineRenderer.SetPosition(lineRenderer.positionCount - 1, pw.positions[i2]);
                            }
                        }
                    }
                }

                if (i >= 2)
                {
                    break;
                }
            }
        }


    }

    public void pathWaypointReached(PathEvent eventType, int queenIndex, int pathIndex, int waypointIndex)
    {
        if (connectedQueen.queenIndex == queenIndex)
        {
            if (eventType == PathEvent.atWaypoint)
            {
                establishPath(pathIndex, waypointIndex);
                //setVisibility(pathIndex, waypointIndex);
            }
            else if (eventType == PathEvent.changePath)
            {
                changePath(pathIndex, waypointIndex);
            }
        }
    }

    public List<QueenPoint> getActivePath()
    {
        if (qPoints.Count > 0)
        {
            return qPoints[pathIndex];
        }
        else
        {
            return null;
        }
    }

    public void pathWaypointChanged()
    {
        pathLists[2] = new List<PathWaypoint>();
    }

    public void changePath(int changeToPathIndex, int startAt)
    {
        connectedQueen.paused = true;
        print(changeToPathIndex + " - " + startAt + " | " + qPoints[changeToPathIndex][2]);
        print(qPoints[1].Count);
        connectedQueen.target = qPoints[changeToPathIndex][startAt].transform;
        pathIndex = changeToPathIndex;
        connectedQueen.wavepointIndex = startAt;
        connectedQueen.changePath(pathIndex);
        connectedQueen.paused = false;
    }

    public void establishPath(int pathIndex, int waypointIndex)
    {
        // Plan so far is going to be:
        // call the update path at each waypoint,
        // from there the path is reconstructed
        // ???
        // three lists of positions:
        // one for the part of the path being erased,
        // one for the part of the path that is upcoming and complete
        // one for the path that still needs to be drawn

        if (lineRenderer && queenPaths.Count > 0)
        {
            List<PathWaypoint> newWaypoints = new List<PathWaypoint>();

            int path = pathIndex;
            int waypoint = waypointIndex;
            List<ThoughtBubbleItem> thoughts = new List<ThoughtBubbleItem>();

            for (int i = 0; i < fieldOfView && waypoint < qPoints[path].Count; i++)
            {

                // Thought Bubble Setup
                // loop through the ahead waypoints and esablish the thoughtbubbles
                if (i < thoughtBubbleFOV + 1 && i > 0)
                {
                    // checks the current waypoint and checks if it has a signal that needs to update the queen thought bubble
                    if (qPoints[path][waypoint].timerStopAtPoint || qPoints[path][waypoint].eventStopAtPoint)
                    {
                        thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.STOP));
                    }

                    if (qPoints[path][waypoint].changeQueenSpeed)
                    {
                        if (qPoints[path][waypoint].miscIndex > connectedQueen.type.speed)
                        {
                            thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.SPEED_UP));
                        }
                        else if (qPoints[path][waypoint].miscIndex < connectedQueen.type.speed)
                        {
                            thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.SLOW_DOWN));
                        }
                    }
                    else if (qPoints[path][waypoint].revertQueenSpeed)
                    {
                        thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.NORMAL_SPEED));
                    }
                }


                // Path Setup

                //Checks if the path changes at this waypoint
                if (qPoints[path][waypoint].changeQueenPath)
                {
                    List<Vector3> array = new List<Vector3>();
                    array.Add(qPoints[path][waypoint].transform.position);
                    newWaypoints.Add(new PathWaypoint(array, path, waypoint));

                    int newPath = (int)qPoints[path][waypoint].changeQueenPathTo.x;
                    int newWaypoint = (int)qPoints[path][waypoint].changeQueenPathTo.y;
                    path = newPath;
                    waypoint = newWaypoint;

                }
                // If not, checks if the next part of the path needs to curve
                else if (qPoints[path][waypoint].curvePath)
                {
                    List<Vector3> array = new List<Vector3>(qPoints[path][waypoint].curvePositions.ToArray());
                    newWaypoints.Add(new PathWaypoint(array, path, waypoint));
                    waypoint++;
                }
                // if all else fails, add the next part of the path as a single position
                else
                {
                    List<Vector3> array = new List<Vector3>();
                    array.Add(qPoints[path][waypoint].transform.position);
                    newWaypoints.Add(new PathWaypoint(array, path, waypoint));
                    waypoint++;
                }
            }
            // now that the most updated list of pathwaypoints have been generated, we can compare it to the most current version of the list

            // Move the first element of the active line to the erasing line
            if (pathLists[1].Count > 0)
            {
                pathLists[0].Add(pathLists[1][0]);
                pathLists[1].RemoveAt(0);
            }
            

            // now compare the new list to the active list

            int indexToQueueAt = -1;

            for (int i = 0; i < newWaypoints.Count; i++)
            {
                if (i < pathLists[1].Count)
                {
                    if (!newWaypoints[i].isEqual(pathLists[1][i]))
                    {
                        pathLists[1] = pathLists[1].GetRange(0, i);
                        indexToQueueAt = i;
                    }
                    
                }
                else
                {
                    indexToQueueAt = i;
                    break;
                }
            }
            
            // active list has been checked, and an index to go from has been determined, from that point, check the drawing list to see if 
            // there are parts of the list already being queued up, if they're different or there's nothing in the list, clear it out and start again
            //int indexToAddToQueue = -1;
            if (indexToQueueAt >= 0)
            {
                if (pathLists[2].Count > 0)
                {
                    int count = 0;
                    for (int i = indexToQueueAt; i < newWaypoints.Count; i++)
                    {
                        if (count >= pathLists[2].Count)
                        {
                            pathLists[2].Add(newWaypoints[i]);
                        }
                        else if (!newWaypoints[i].isEqual(pathLists[2][count]))
                        {
                            pathLists[2].Add(newWaypoints[i]);
                        }
                        
                        count++;
                    }
                }
                else
                {
                    pathLists[2] = new List<PathWaypoint>(newWaypoints.GetRange(indexToQueueAt, newWaypoints.Count - (indexToQueueAt + 1)));
                }
            }

            // all queues have been complete and now the linerenderer just needs to be updated
            //print("Complete establish visibility with pathLists at " + pathLists[0].Count + " | " + pathLists[1].Count + " | " + pathLists[2].Count);
            connectedQueen.updateThoughtBubble(thoughts);
        }
    }


    public void setVisibility(int pathIndex, int waypointIndex)
    {
        print("SET VISIBILITY CALLED");
        // Fuck me man I should have documented this code holy shit dude what was I thinking. - AMWB
        if (lineRenderer && queenPaths.Count > 0)
        {

            List<ThoughtBubbleItem> thoughts = new List<ThoughtBubbleItem>();
            lineRenderer.positionCount = lineIndex;
            int path = pathIndex;
            int waypoint = waypointIndex;
            int addToIndex = 1;
            for (int i = 0; i < fieldOfView && waypoint < qPoints[path].Count; i++)
            {
                //Pretty sure how this goes is that it uses path and waypoint as the starting point, then uses i, to make sure that it falls in line with the field of view.

                // loop through the ahead waypoints and esablish the thoughtbubbles
                if (i < thoughtBubbleFOV + 1 && i > 0)
                {
                    // checks the current waypoint and checks if it has a signal that needs to update the queen thought bubble
                    if (qPoints[path][waypoint].timerStopAtPoint || qPoints[path][waypoint].eventStopAtPoint)
                    {
                        thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.STOP));
                    }

                    if (qPoints[path][waypoint].changeQueenSpeed)
                    {
                        if (qPoints[path][waypoint].miscIndex > connectedQueen.type.speed)
                        {
                            thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.SPEED_UP));
                        }
                        else if (qPoints[path][waypoint].miscIndex < connectedQueen.type.speed)
                        {
                            thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.SLOW_DOWN));
                        }
                    }
                    else if (qPoints[path][waypoint].revertQueenSpeed)
                    {
                        thoughts.Add(new ThoughtBubbleItem(ThoughtBubbleItem.ThoughtTypes.NORMAL_SPEED));
                    }
                }
                

                if (i == fieldOfView - 1)
                {
                    // loop is at last of the path drawing and needs to start lerping the rest of the line.
                    if (qPoints[path][waypoint].curvePath)
                    {
                        //addToLerpQueue(lineRenderer.GetPosition(lineRenderer.positionCount - 1), qPoints[path][waypoint].transform.position, qPoints[path][waypoint].curvePositions);
                    }
                    else
                    {
                        //addToLerpQueue(lineRenderer.GetPosition(lineRenderer.positionCount - 1), qPoints[path][waypoint].transform.position);
                    }
                }
                else
                {
                    // Main body of the loop

                    //Checks if the path changes at this waypoint
                    if (qPoints[path][waypoint].changeQueenPath)
                    {
                        // Adds extra positions at corners to help avoid rectangle scrunching
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);

                        int newPath = (int)qPoints[path][waypoint].changeQueenPathTo.x;
                        int newWaypoint = (int)qPoints[path][waypoint].changeQueenPathTo.y;
                        path = newPath;
                        waypoint = newWaypoint;

                    }
                    // If not, checks if the next part of the path needs to curve
                    else if (qPoints[path][waypoint].curvePath)
                    {
                        //iterates through all the positions of the curve
                        foreach (Vector3 posn in qPoints[path][waypoint].curvePositions)
                        {
                            lineRenderer.positionCount += 1;
                            lineRenderer.SetPosition(lineRenderer.positionCount - 1, posn);
                        }
                        waypoint++;
                    }
                    // if all else fails, draws a linear line between points
                    else
                    {
                        // Adds extra positions at corners to help avoid rectangle scrunching
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        lineRenderer.positionCount += 1;
                        lineRenderer.SetPosition(lineRenderer.positionCount - 1, qPoints[path][waypoint].transform.position);
                        waypoint++;
                    }
                }


            }
            

            connectedQueen.updateThoughtBubble(thoughts);

            if (waypointIndex < qPoints[pathIndex].Count)
            {
                if (qPoints[pathIndex][waypointIndex].curvePath && !qPoints[pathIndex][waypointIndex].changeQueenPath)
                {
                    addToIndex += qPoints[pathIndex][waypointIndex].curvePositions.Count - 1;
                }
            }

            lineIndex += addToIndex;
        }
    }

    public void eventHandler(WaypointEvent wEvent, int index, int miscIndex)
    {
        if (wEvent == WaypointEvent.queenAtXPosition && QueenManager.Instance.activeQueenIndex == connectedQueen.queenIndex && Boss.BossManager.Instance == null)
        {
            int pathIndex = miscIndex;
            int waypointIndex = index;
            int countToEnd = 0;

            while (true)
            {
                if (qPoints[pathIndex][waypointIndex].queenWinConReached)
                {
                    break;
                }

                if (qPoints[pathIndex][waypointIndex].changeQueenPath)
                {
                    float newPath = qPoints[pathIndex][waypointIndex].changeQueenPathTo.x;
                    float newWaypoint = qPoints[pathIndex][waypointIndex].changeQueenPathTo.y;

                    pathIndex = (int)newPath;
                    waypointIndex = (int)newWaypoint;
                }
                else
                {
                    waypointIndex++;
                }

                countToEnd++;
            }

            lastKnownPosn = lineLength - countToEnd;
            GlobalVariables.ui.queenHUD.updateProgressBar(lineLength - countToEnd, lineLength);
        }
    }

    private void OnDrawGizmos()
    {
        if (drawQueenPathGizmo)
        {
            Gizmos.color = gizmoColor;


            foreach (Transform path in queenPaths)
            {
                for (int i = 0; i < path.childCount - 1; i++)
                {
                    QueenPoint point = path.GetChild(i).GetComponent<QueenPoint>();
                    if (point.curvePath)
                    {
                        point.drawQuadraticCurve();

                        for (int ii = 1; ii < point.curvePositions.Count; ii++)
                        {
                            Gizmos.DrawLine(point.curvePositions[ii - 1], point.curvePositions[ii]);
                        }
                    }
                    else
                    {
                        Gizmos.DrawLine(path.GetChild(i).position, path.GetChild(i + 1).position);

                    }
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (generateIndexNumber)
        {
            generateIndexNumber = false;
            for(int j = 0; j <= queenPaths.Count - 1; j++)
            {
                queenPaths[j].name = "Path " + j;
                for (int i = 0; i <= queenPaths[j].childCount - 1; i++)
                {
                    queenPaths[j].GetChild(i).name = "Waypoint " + j + "-" + i;
                }
            }

        }
    }

}
