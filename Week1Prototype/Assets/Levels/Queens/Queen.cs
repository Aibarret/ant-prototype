using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD;
using SceneTransition;

public class ThoughtBubbleItem
{
    public enum ThoughtTypes
    {
        SPEED_UP,
        NORMAL_SPEED,
        SLOW_DOWN,
        STOP,
        TIMER,
        CLOCKWISE,
        COUNTER_CLOCKWISE
    }

    public ThoughtTypes type;
    public int priority;

    public ThoughtBubbleItem(ThoughtTypes givenType, int priority = 0)
    {
        type = givenType;
        this.priority = priority;

        if (this.priority == 0)
        {
            switch (givenType)
            {
                case ThoughtTypes.STOP:
                    priority = 2;
                    break;
                default:
                    priority = 0;
                    break;
            }
        }
    }
}

public class Queen : MonoBehaviour, DropShadowCastable
{
    public bool invincible = false;
    public bool paused = false;
    public bool approachingChangePath;

    [Header("Object & Component Refs")]
    public QueenType type;
    public GameObject queenPointContainer;
    public GameObject hpbar;
    public ThoughtBubble thoughtBubble;
    public DropshadowController dropShadow;
    public SpriteRenderer sprite;
    public Timer timer;
    public QueenTracker qt;
    public Animator animator;
    public ViewWindowController viewThrough;



    private MovingHealthBar healthBar;
    private float lastFrameXPosn;

    [HideInInspector] public Transform target;
    [HideInInspector] public QueenWaypoints pointContainerScript;
    //[HideInInspector] public int pointIndex = 0;
    [HideInInspector] public int wavepointIndex = 0;
    [HideInInspector] public QueenManager queenManager;
    [HideInInspector] public int queenIndex;
    [HideInInspector] public bool isMoveLinear;
    [HideInInspector] public List<Vector3> curvePositions;
    [HideInInspector] public int curveWaypointIndex;

    public bool CanTakeDamage { get; set; } = true;

    public bool CanBehave { get; set; } = false;

    private void Awake()
    {
        lastFrameXPosn = transform.position.x;
        pointContainerScript = queenPointContainer.GetComponent<QueenWaypoints>();
        type.awake();
        GlobalVariables.addToCallList(eventHandler);
        pointContainerScript.connectedQueen = this;
    }

    void Start()
    {
        SceneTransiter st = SceneTransiter.Instance;
        if (st.IsLoading)
            SceneTransiter.Instance.PushActionAfterSceneTransition(() => CanBehave = true);
        else
            CanBehave = true;
        
        healthBar = hpbar.GetComponent<MovingHealthBar>();
        if (pointContainerScript.getActivePath() != null)
        {
            target = pointContainerScript.getActivePath()[0].transform;
            transform.position = target.position;
        }
       
        type.start();

        SetNextRandomSoundTime();
    }

    void Update()
    {
        if (lastFrameXPosn != transform.position.x)
        {

            if (animator != null)
            {
                if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    animator.SetTrigger("Run");
                }
            }
            
            changeSpriteDirection(lastFrameXPosn < transform.position.x);
        }
        else
        {
            if (animator != null)
            {
                if (animator.GetCurrentAnimatorStateInfo(0).IsName("Running"))
                {
                    animator.SetTrigger("Stop");
                }
            }
            
        }
        lastFrameXPosn = transform.position.x;
        if (CanBehave)
            type.behavior();
        //healthBar.updatePosn();

        

        
    }



    public void eventHandler(WaypointEvent wEvent, int index, int miscIndex)
    {
        switch (wEvent)
        {
            case WaypointEvent.queenAtXPosition:
                type.atWaypoint(index);
                //type.checkWinCondition(index);
                break;
            case WaypointEvent.queenTimerPause:
                type.onTimerPause(miscIndex);
                break;
            case WaypointEvent.queenEventPause:
                type.onEventPause();
                break;
            case WaypointEvent.queenInvincible:
                type.onInvincible();
                break;
            case WaypointEvent.queenChangeSpeed:
                type.changeSpeed(miscIndex);
                break;
            case WaypointEvent.queenNormalSpeed:
                type.revertSpeed();
                break;
            case WaypointEvent.queenChangeAnimation:
                type.changeAnimation(index);
                break;
            case WaypointEvent.queenWinConReached:
                if (miscIndex == queenIndex)
                {
                    queenManager.queenWinCons[queenIndex] = true;
                    queenManager.checkForWin();
                }
                break;
                
        }
    }

    public void changeSpriteDirection(bool isLeft)
    {
        if (type.icon == QueenTypeIcons.CORGI)
        {
            isLeft = !isLeft;
        }
        sprite.flipX = isLeft;
        dropShadow.updateDummySprite();
        type.onChangeSpritedirection(isLeft);
    }

    public void updateThoughtBubble(List<ThoughtBubbleItem> thoughts)
    {
        if (thoughtBubble != null && QueenManager.Instance.getQueen() == this)
        {
            ThoughtBubbleItem[] activeThoughts = new ThoughtBubbleItem[2];
            
            if (thoughts.Count > 0)
            {
                
                activeThoughts[0] = thoughts[0];
                //print("queen updating thought bubble " + activeThoughts[0].type);
                if (thoughts.Count > 1)
                {
                    activeThoughts[1] = thoughts[1];
                    //print("queen updating thought bubble 2" + activeThoughts[1].type);
                }

                thoughtBubble.displayIcons(activeThoughts);
            }
            else
            {
                //print("queen deactivates thought bubble");
                thoughtBubble.deactivateThoughtBubble();
            }
        }
        
    }

    public void updateThoughtBubble(ThoughtBubbleItem thought, float timerSeconds = 0)
    {
        if (QueenManager.Instance.getQueen() == this)
        {
            this.thoughtBubble.displayIcon(thought, timerSeconds);
        }
    }

    public void changePath(int pathChangedTo)
    {
        type.pathChanged(pathChangedTo);
    }

    public void togglePause(bool isPause)
    {
        //print(gameObject.name + " toggling Pause to " + isPause);
        paused = isPause;
        if (isPause)
        {
            animator.SetTrigger("Stop");
        }
        animator.SetBool("Paused", isPause);
        type.onPause(isPause);
    }

    public void takeDamage(int damage)
    {
        if (!CanTakeDamage)
            return;
        
        if (!invincible)
        {
            //Make sure it doesn't break the game!!!
            playSoundEffect("Damage");
            type.takeDamage(damage);
            StartCoroutine(qt.takeDamage());
            healthBar.updateBar(type.health, type.maxHealth);

            GlobalVariables.ui.queenHUD.updateHealthBars(queenManager.getQueenArray());
        }
        type.checkWinCondition();
    }

    public void GetNextWaypoint(bool isAtChangePath = false)
    {
        if (pointContainerScript.getActivePath()[wavepointIndex].queenWinConReached)
        {
            queenManager.queenWinCons[queenIndex] = true;
            togglePause(true);
            queenManager.checkForWin();
            return;
        }

        if (pointContainerScript.getActivePath()[wavepointIndex].curvePath)
        {
            isMoveLinear = false;
            curvePositions = pointContainerScript.getActivePath()[wavepointIndex].curvePositions;
            curveWaypointIndex = 0;
            if (!isAtChangePath)
            {

                wavepointIndex++;
            }
        }
        else
        {
            isMoveLinear = true;
            if (!isAtChangePath)
            {

                wavepointIndex++;
            }
        }

        target = pointContainerScript.getActivePath()[wavepointIndex].transform;

        if (wavepointIndex < pointContainerScript.getActivePath().Count)
        {
            if (pointContainerScript.getActivePath()[wavepointIndex].changeQueenPath)
            {
                approachingChangePath = true;
            }
            else
            {
                approachingChangePath = false;
            }

        }



        //print("Target at index " + wavepointIndex + " named " + target.name);
        //target = pointContainerScript.waypointList[wavepointIndex];
        type.frameOne = false;
    }

    public void toggleHealthVisibility(bool isVisible)
    {
        healthBar.toggleVisible(isVisible);
    }

    public void toggleShadow(bool isActive)
    {
        if (isActive != dropShadow.shadowVisible)
        {
            dropShadow.toggleShadow(isActive);

        }
    }

    public void toggleSprite(bool isActive)
    {
        if (isActive != sprite.enabled)
        {
            sprite.enabled = isActive;
        }
    }

    public void toggleViewThrough(bool isActive)
    {
        viewThrough.toggleEnable(isActive);
    }

    public void die(bool isWin)
    {
        print("Queen Die Called");
        GlobalVariables.ui.score.active = false;
        GlobalVariables.eventCall(WaypointEvent.queenDied, -1, -1);   
    }

    public SpriteRenderer getSpriteRenderer()
    {
        return sprite;
    }

    public void setCollision(FlyingState flyingState)
    {
        if (flyingState == FlyingState.GROUND)
        {
            invincible = false;
        }
        else
        {
            invincible = true;
        }
    }


    public void playSoundEffect(string eventToPlay)
    {
        SFXManager.Instance.playSFX("event:/Queen SFX/" + type.prefabName + "/" + eventToPlay);
    }

    private void SetNextRandomSoundTime()
    {
/*        nextRandomSoundTime = Time.time + Random.Range(minSoundInterval, maxSoundInterval);
        Debug.Log("Next random sound time: " + nextRandomSoundTime);*/
    }

    private void PlayRandomSound()
    {
/*        Debug.Log("Playing random sound at " + Time.time);
        FMODUnity.RuntimeManager.PlayOneShot(randomFMODEvent, transform.position);*/
    }

    
}
