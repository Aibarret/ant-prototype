using System.Collections.Generic;
using UnityEngine;

public class FanType : ObstacleType, Attackable, ButtonActivatable
{
    public Windbox windZone;
    [SerializeField] private bool rotates;
    public bool canBeDestroyed;
    public float rotationSpeed;
    
    private List<GameObject> targetables = new List<GameObject>();
    private float currentRotationTime = 0.0f;
    private Quaternion fanRotation;

    [SerializeField] private float minRotation = 0f;
    [SerializeField] private float maxRotation = 360f;
    
    public void SwitchRotation(bool val) {
        rotates = val;
    }

    public void SwitchFan(bool val) {
        windZone.SwitchWind(val);
    }




    private void Start() {
        health = maxHealth;
        if (vfxManager)
        {
            vfxManager.ManageParticles(15, VFXManager.VFXControl.Play, gameObject);

        }
    }

    public override void behavior() {
        AimAngle();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        string tag = collider.gameObject.tag;
        if (tag == "Obstacle" || tag == "FormationUnit" || tag == "Enemy") {
            targetables.Add(collider.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collider) {
        string tag = collider.gameObject.tag;
        if (tag == "Obstacle" || tag == "FormationUnit" || tag == "Enemy") {
            if (targetables[0] == collider.gameObject) {
                fanRotation = transform.rotation;
                currentRotationTime = 0.0f;
            }
            targetables.Remove(collider.gameObject);
        }
    }

    private void AimAngle() {
        if (!rotates) return;
        if (targetables.Count == 0) {
            currentRotationTime = 0.0f;
            fanRotation = transform.rotation;
            return;
        }
        GameObject target = targetables[0];
        if (target != null) {
            // Debug.Log("Current Target: " + target.name);
            Vector3 targetPos = target.transform.position;
            targetPos.z = 0;
            targetPos.x -= transform.position.x;
            targetPos.y -= transform.position.y;
            
            float min = minRotation;
            NormalizeRotation(ref min);
            float max = maxRotation;
            NormalizeRotation(ref max);
            
            float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
            NormalizeRotation(ref angle);
            angle = Mathf.Clamp(angle, min, max);
            
            Quaternion Facing = Quaternion.Euler(new Vector3(0f, 0f, angle));
            Aim(Facing);
        }

        void NormalizeRotation(ref float rot)
        {
            rot %= 360f;
            if (rot < 0f)
                rot += 360f;
        }
    }


    private void Aim(Quaternion facing) {
        // transform.rotation = facing;
        transform.rotation = Quaternion.Slerp(fanRotation, facing, currentRotationTime);
        currentRotationTime += Time.deltaTime * rotationSpeed;
        // Debug.Log("Current Time: " + currentRotationTime);
    }

    public bool takeDamage(int damage, float? direction = null)
    {
        if (!canBeDestroyed)
            return false;
        return base.takeDamage(damage);
    }

    public void activate()
    {
        throw new System.NotImplementedException();
    }

    public void deactivate()
    {
        throw new System.NotImplementedException();
    }
}
