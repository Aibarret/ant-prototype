using UnityEngine;

public sealed class TrainType : ObstacleType
{
    [SerializeField] private int damage = 10;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Queen queen))
            queen.takeDamage(damage);
        else if (other.TryGetComponent(out Enemy enemy))
            enemy.takeDamage(damage);
        else if (other.TryGetComponent(out FormationUnit unit))
            unit.KillUnit();
        else if (other.TryGetComponent(out Attackable attackable))
            attackable.takeDamage(damage);
    }
}
