using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;

public sealed class TrainGunType : ObstacleType, ButtonActivatable
{
    public bool IsActivated { get; private set; } = false;

    [SerializeField] private Transform spriteTransform;
    [SerializeField] private Transform leftPos;
    [SerializeField] private Transform rightPos;
    
    [SerializeField] private float portalMovementSpeed = 10f;
    [SerializeField] private float trainSpeed = 10f;
    
    private GameObject leftPortal;
    private GameObject rightPortal;
    private GameObject train;

    private void Awake()
    {
        // Instantiate portal and train game objects
        GameObject portalPrefab = Resources.Load<GameObject>("Portal");
        leftPortal = Instantiate(portalPrefab);
        leftPortal.SetActive(false);
        rightPortal = Instantiate(portalPrefab);
        rightPortal.SetActive(false);
        train = Instantiate(Resources.Load<GameObject>("TrainObstacle"));
        train.SetActive(false);
    }

    private Coroutine activateCoroutine = null;
    private Coroutine trainCoroutine = null;
    
    public void activate()
    {
        if (activateCoroutine != null)
            StopCoroutine(activateCoroutine);
        if (trainCoroutine != null)
            StopCoroutine(trainCoroutine);
        activateCoroutine = StartCoroutine(ActivateRoutine());
    }

    public void deactivate()
    {

    }

    private IEnumerator ActivateRoutine()
    {
        IsActivated = true;

        const float rotateSpeed = 300f;
        
        // Aim at left portal position
        Vector2 dirVec2 = (leftPos.position - transform.position).normalized;
        float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;
        dir %= 360f;
        if (dir < 0f)
            dir += 360f;
        Vector3 rot = spriteTransform.rotation.eulerAngles;
        float originalZ = rot.z;
        while (Math.Abs(rot.z - dir) > Mathf.Epsilon)
        {
            rot.z = Mathf.MoveTowards(rot.z, dir, rotateSpeed * Time.deltaTime);
            spriteTransform.rotation = Quaternion.Euler(rot);
            yield return null;
        }
        
        // Shoot out left portal
        leftPortal.SetActive(true);
        leftPortal.transform.position = transform.position;
        Vector2 targetPos = leftPos.position;
        while ((Vector2)(leftPortal.transform.position) != targetPos)
        {
            leftPortal.transform.position = Vector2.MoveTowards(leftPortal.transform.position, targetPos, portalMovementSpeed * Time.deltaTime);
            yield return null;
        }
        
        // Aim at left portal position
        dirVec2 = (rightPos.position - transform.position).normalized;
        dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;
        dir %= 360f;
        if (dir < 0f)
            dir += 360f;
        rot = spriteTransform.rotation.eulerAngles;
        while (Math.Abs(rot.z - dir) > Mathf.Epsilon)
        {
            rot.z = Mathf.MoveTowards(rot.z, dir, rotateSpeed * Time.deltaTime);
            spriteTransform.rotation = Quaternion.Euler(rot);
            yield return null;
        }
        
        // Shoot out right portal
        rightPortal.SetActive(true);
        rightPortal.transform.position = transform.position;
        targetPos = rightPos.position;
        while ((Vector2)(rightPortal.transform.position) != targetPos)
        {
            rightPortal.transform.position = Vector2.MoveTowards(rightPortal.transform.position, targetPos, portalMovementSpeed * Time.deltaTime);
            yield return null;
        }
        
        // Start train routine
        trainCoroutine = StartCoroutine(TrainRoutine());

        // Return the train gun to original rotation
        dir = originalZ;
        dir %= 360f;
        if (dir < 0f)
            dir += 360f;
        rot = spriteTransform.rotation.eulerAngles;
        while (Math.Abs(rot.z - dir) > Mathf.Epsilon)
        {
            rot.z = Mathf.MoveTowards(rot.z, dir, rotateSpeed * Time.deltaTime);
            spriteTransform.rotation = Quaternion.Euler(rot);
            yield return null;
        }
        
        // Wait until train routine is finished before setting 'IsActivated' to false
        yield return trainCoroutine;
        
        IsActivated = false;
        
        leftPortal.SetActive(false);
        rightPortal.SetActive(false);
    }

    private IEnumerator TrainRoutine()
    {
        train.SetActive(true);
        train.transform.position = leftPortal.transform.position;
        Vector3 rot = train.transform.rotation.eulerAngles;
        Vector2 dir = (rightPortal.transform.position - leftPortal.transform.position).normalized;
        rot.z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rot.z %= 360f;
        if (rot.z < 0f)
            rot.z += 360f;
        train.transform.rotation = Quaternion.Euler(rot);
        train.transform.localScale = new Vector3(0f, 1f, 1f);
        
        float distanceBetweenPortals = Vector2.Distance(leftPortal.transform.position, rightPortal.transform.position);
        while (train.transform.localScale.x < distanceBetweenPortals)
        {
            Vector3 scale = train.transform.localScale;
            scale.x = Mathf.MoveTowards(scale.x, distanceBetweenPortals, trainSpeed * Time.deltaTime);
            train.transform.localScale = scale;
            yield return null;
        }
            
        train.SetActive(false);
    }

    

    /*private void Update()
    {
        if (active)
        {
            if (!shooting && !trainMoving && !portalStopped && !trainDestroyed)
            {
                leftPortal = Instantiate(portal, transform.position, Quaternion.identity);
                shooting = true;
                rightPortal = Instantiate(portal, transform.position, Quaternion.identity);
            }
            else if (shooting)
            {
                float leftDistance = Vector3.Distance(leftPortal.transform.position, leftPos.transform.position);
                float rightDistance = Vector3.Distance(rightPortal.transform.position, rightPos.transform.position);

                if (leftDistance < 0.1f && rightDistance < 0.1f)
                {
                    portalStopped = true;
                    shooting = false;
                    portalMovementSpeed = 0;
                }

                Vector3 leftDirection = (leftPos.transform.position - leftPortal.transform.position).normalized;
                leftPortal.GetComponent<Rigidbody2D>().velocity = new Vector3(leftDirection.x, leftDirection.y, 0) * portalMovementSpeed;
                Vector3 rightDirection = (rightPos.transform.position - rightPortal.transform.position).normalized;
                rightPortal.GetComponent<Rigidbody2D>().velocity = new Vector3(rightDirection.x, rightDirection.y, 0) * portalMovementSpeed;
            }
            else if (portalStopped)
            {
                train = Instantiate(trainPrefab, leftPortal.transform.position, Quaternion.identity);
                trainMoving = true;
                portalStopped = false;
            }
            else if (trainMoving)
            {
                Vector3 trainDirection = (rightPortal.transform.position - train.transform.position).normalized;
                train.GetComponent<Rigidbody2D>().velocity = new Vector3(trainDirection.x, trainDirection.y, 0) * trainSpeed;

                float trainDistance = Vector3.Distance(rightPortal.transform.position, train.transform.position);

                if (trainDistance < 0.1f)
                {
                    trainMoving = false;
                    trainDestroyed = true;
                }
            }
            else if(trainDestroyed)
            {
                Destroy(train);
                Destroy(leftPortal);
                Destroy(rightPortal);
                active = false;
                portalMovementSpeed = defaultPortalSpeed;
            }
        }
    }*/
}
