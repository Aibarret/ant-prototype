using System.Collections;
using System.Collections.Generic;
using FlowFieldNavigation;
using UnityEngine;
using UnityEngine.AI;

public class BridgeBuilder : MonoBehaviour
{
    public enum Axis
    {
        Horizonal,
        Verticle
    }

    public GameObject start;
    public GameObject end;
    public Transform plankContainer;
    public NavMeshObstacle navMeshObstacle;
    [SerializeField] private BoxCollider2D flowFieldCollider;

    public GameObject plank;
    public GameObject postPlank;

    public bool bridgeActive = true;
    public Axis bridgeAxis = Axis.Verticle;
    public bool invertDirection;
    public int planks;
    public float distanceFromEdge;
    public float distanceBetweenPlank;

    private bool currentActive = true;
    private Axis currentAxis = Axis.Verticle;
    private int currentPlanks = 0;
    private bool currentDirection;
    private float currentEdgeDistance;
    private float currentPlankDistance;

    private void OnDrawGizmosSelected()
    {
        if (currentAxis != bridgeAxis || currentPlanks != planks || currentDirection != invertDirection || currentEdgeDistance != distanceFromEdge || currentPlankDistance != distanceBetweenPlank)
        {
            drawBridge();
        }

        if (currentActive != bridgeActive)
        {
            currentActive = bridgeActive;
            toggleBridge(bridgeActive);
        }
    }

    public void toggleBridge(bool active)
    {
        for (int i = 0; i < plankContainer.childCount; i++)
        {
            plankContainer.GetChild(i).gameObject.SetActive(active);
        }

        navMeshObstacle.enabled = !active;
        NavigationManager nm = NavigationManager.Instance;
        if (nm != null)
        {
            if (active)
                nm.GroundFlowField.RemoveColliderObstacle(flowFieldCollider);
            else
                nm.GroundFlowField.AddColliderObstacle(flowFieldCollider);
        }
    }

    public void drawBridge()
    {
        currentAxis = bridgeAxis;
        currentPlanks = planks;
        currentDirection = invertDirection;
        currentEdgeDistance = distanceFromEdge;
        currentPlankDistance = distanceBetweenPlank;

        SpriteRenderer startPiece = start.GetComponent<SpriteRenderer>();
        SpriteRenderer endPiece = end.GetComponent<SpriteRenderer>();

        for (int i = plankContainer.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.DestroyImmediate(plankContainer.GetChild(i).gameObject);
        }

        int dir = 1;

        if (invertDirection)
        {
            dir = dir * -1;
            startPiece.flipY = false;
            endPiece.flipY = true;
        }
        else
        {
            startPiece.flipY = true;
            endPiece.flipY = false;
        }

        if (bridgeAxis == Axis.Verticle)
        {
            start.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            start.transform.localRotation = Quaternion.Euler(0, 0, -90);
        }

        start.transform.localPosition = Vector3.zero;
        Vector3 lastPosn = Vector3.zero;
        for (int i = 1; i <= planks; i++)
        {
            GameObject newPlank = GameObject.Instantiate(plank);
            newPlank.transform.parent = plankContainer;
            if (bridgeAxis == Axis.Verticle)
            {
                newPlank.transform.localRotation = Quaternion.Euler(0, 0, 0);
                if (i == 1)
                {
                    newPlank.transform.localPosition = new Vector3(0, distanceFromEdge * dir) + lastPosn;
                    lastPosn = newPlank.transform.localPosition;
                }
                else
                {
                    newPlank.transform.localPosition = new Vector3(0, distanceBetweenPlank * dir) + lastPosn;
                    lastPosn = newPlank.transform.localPosition;
                }
                
            }
            else
            {
                newPlank.transform.localRotation = Quaternion.Euler(0, 0, -90);
                if (i == 1)
                {
                    newPlank.transform.localPosition = new Vector3(distanceFromEdge * dir, 0) + lastPosn;
                    lastPosn = newPlank.transform.localPosition;
                }
                else
                {
                    newPlank.transform.localPosition = new Vector3(distanceBetweenPlank * dir, 0) + lastPosn;
                    lastPosn = newPlank.transform.localPosition;
                }
            }
        }

        if (bridgeAxis == Axis.Verticle)
        {
            end.transform.localRotation = Quaternion.Euler(0, 0, 0);
            end.transform.localPosition = lastPosn + new Vector3(0, distanceFromEdge * dir);
            navMeshObstacle.center = new Vector3(0, (lastPosn.y + (distanceFromEdge * dir)) * .5f);
            navMeshObstacle.size = new Vector3(9, planks * 1.6f);
            NavigationManager nm = NavigationManager.Instance;
            if (nm != null)
            {
                flowFieldCollider.transform.position = new Vector3(0, (lastPosn.y + (distanceFromEdge * dir)) * .5f);
                flowFieldCollider.size = new Vector3(9, planks * 1.6f);
                nm.GroundFlowField.UpdateCachedCostField();
            }
        }
        else
        {
            end.transform.localRotation = Quaternion.Euler(0, 0, -90);
            end.transform.localPosition = lastPosn + new Vector3(distanceFromEdge * dir, 0);
            navMeshObstacle.center = new Vector3((lastPosn.x + (distanceFromEdge * dir)) * .5f, 0);
            navMeshObstacle.size = new Vector3(planks * 1.6f, 9);
            NavigationManager nm = NavigationManager.Instance;
            if (nm != null)
            {
                flowFieldCollider.transform.position = new Vector3((lastPosn.x + (distanceFromEdge * dir)) * .5f, 0);
                flowFieldCollider.size = new Vector3(planks * 1.6f, 9);
                nm.GroundFlowField.UpdateCachedCostField();   
            }
        }



        toggleBridge(bridgeActive);
        
    }
}
