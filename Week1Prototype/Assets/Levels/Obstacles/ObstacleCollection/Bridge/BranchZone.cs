using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchZone : MonoBehaviour
{
    public GameObject bridgeObstacle;
    public Collider2D zone;
    public SpriteRenderer zoneSprite;

    private DamType bridgeController;

    private List<GameObject> sticksCollected = new List<GameObject>();

    private void Awake()
    {
        bridgeController = bridgeObstacle.GetComponent<DamType>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Obstacle") && !sticksCollected.Contains(collision.gameObject))
        {
            BranchObstacle stick = collision.GetComponent<BranchObstacle>();

            if (stick != null)
            {
                sticksCollected.Add(collision.gameObject);
                bridgeController.branchEntered(stick, collision.gameObject);
            }
        }
    }


}
