using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableArrow : MonoBehaviour, Clickable
{
    private ForkInPathType.arrowClicked returnCall;

    public void returnFunction(ForkInPathType.arrowClicked call)
    {
        returnCall = call;
    }

    public void onClick()
    {
        if (returnCall != null)
        {
            print(gameObject.name + " Clicked");
            returnCall();
        }
    }
}
