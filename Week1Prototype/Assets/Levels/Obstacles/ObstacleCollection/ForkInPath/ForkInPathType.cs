using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkInPathType : ObstacleType
{
    [Header("Waypoint Information")]
    public GameObject stopAtWaypoint;
    public GameObject pathChangeWaypoint;

    [Header("Diverging Paths")]
    public GameObject arrowOneObject;
    public GameObject arrowTwoObject;

    private bool isActive;
    private QueenPoint changeWaypoint;
    private QueenPoint stopWaypoint;
    private ClickableArrow arrowOne;
    private ClickableArrow arrowTwo;

    public delegate void arrowClicked();

    private void Awake()
    {
        GlobalVariables.addToCallList(eventReached);
        arrowOne = arrowOneObject.GetComponent<ClickableArrow>();
        arrowTwo = arrowTwoObject.GetComponent<ClickableArrow>();

        arrowOne.returnFunction(arrowOneClicked);
        arrowTwo.returnFunction(arrowTwoClicked);

        stopWaypoint = stopAtWaypoint.GetComponent<QueenPoint>();
        changeWaypoint = pathChangeWaypoint.GetComponent<QueenPoint>();

        stopWaypoint.eventStopAtPoint = true;
    }

    private void Start()
    {
        arrowOneObject.SetActive(false);
        arrowTwoObject.SetActive(false);
    }

    public void eventReached(WaypointEvent wEvent, int waypointIndex, int miscIndex)
    {
        if (wEvent == WaypointEvent.queenEventPause && waypointIndex == stopWaypoint.indexNumber && QueenManager.Instance.queen.pointContainerScript.pathIndex == stopWaypoint.pathNumber)
        {
            
            toggleActive(true);
            
        }
    }

    public void toggleActive(bool active)
    {
        
        isActive = active;
        arrowOneObject.SetActive(true);
        arrowTwoObject.SetActive(true);
    }

    public void arrowOneClicked()
    {
        if (isActive)
        {
            isActive = false;
            stopWaypoint.waypointManager.connectedQueen.togglePause(false);
            changeWaypoint.togglePathChange(false);
            //waypoint.eventStopAtPoint = false;
        }
        
    }

    public void arrowTwoClicked()
    {
        if (isActive)
        {
            isActive = false;
            stopWaypoint.waypointManager.connectedQueen.togglePause(false);
            changeWaypoint.togglePathChange(true);
            //waypoint.eventStopAtPoint = false;
        }
    }
}
