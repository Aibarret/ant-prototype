using System.Collections;
using UnityEngine;

public sealed class TeleporterType : ObstacleType
{
    public TeleporterType dest;
    public int cooldown;

    private bool complete = false;

    private Coroutine resetCooldownCoroutine = null;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.CompareTag("Enemy") || collision.CompareTag("Queen")) && !complete)
        {
            collision.transform.position = dest.transform.position;
            complete = true;
            dest.complete = true;
            if (resetCooldownCoroutine != null)
                StopCoroutine(resetCooldownCoroutine);
            resetCooldownCoroutine = StartCoroutine(ResetCooldown());
        }
    }

    private IEnumerator ResetCooldown()
    {
        yield return new WaitForSeconds(cooldown);
        complete = false;
        dest.complete = false;
    }
}
