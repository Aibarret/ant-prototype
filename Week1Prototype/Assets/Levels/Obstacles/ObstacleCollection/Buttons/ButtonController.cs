using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ButtonActivatable
{
    public void activate();

    public void deactivate();
}

public class ButtonController : MonoBehaviour
{
    public GameObject connectedObject;

    private ButtonActivatable connectedOb;
    private List<ButtonType> connectedButtons = new List<ButtonType>();

    private void Start()
    {
        connectedOb = connectedObject.GetComponent<ButtonActivatable>();

        for (int i = 0; i < transform.childCount; i++)
        {
            ButtonType button = transform.GetChild(i).GetComponent<ButtonType>();
            button.pressCall += checkButtons;
            connectedButtons.Add(button);
        }
    }

    public void checkButtons(ButtonType pressedButton)
    {
        foreach (ButtonType button in connectedButtons)
        {
            if (!button.activated)
            {
                return;
            }
        }

        connectedOb.activate();
    }

}


