using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeashType : ObstacleType
{

    public int healthValue;
    private int maxHealthValue;

    public Rigidbody2D baseRB;

    public GameObject followObject;

    public GameObject handle;


    private void Awake()
    {
        ob.Hitbox = handle.GetComponent<CircleCollider2D>();
        ob.rb = handle.GetComponent<Rigidbody2D>();
    }

    public override void behavior()
    {
        baseRB.MovePosition(new Vector3(followObject.transform.position.x, followObject.transform.position.y));
    }


}
