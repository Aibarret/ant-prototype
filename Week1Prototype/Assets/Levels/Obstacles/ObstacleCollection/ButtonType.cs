using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonType : ObstacleType
{
    public enum ButtonMode
    {
        OneShot,
        Toggle
    }

    public delegate void Pressed(ButtonType pressedButton);
    public Pressed pressCall;

    public bool activated = false;

    [Header("Button Mode")]
    public ButtonMode mode;

    [Header("Timer Settings")]
    public float activeTime = -1;

    private float elapsedFrames;

    private void Update()
    {
        if (activated && activeTime >= 0 && mode == ButtonMode.Toggle)
        {
            if (elapsedFrames < activeTime)
            {
                elapsedFrames += Time.deltaTime;
            }
            else
            {
                elapsedFrames = 0;
                activated = false;
                pressCall(this);
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (mode)
        {
            case ButtonMode.OneShot:
                activated = true;
                pressCall(this);
                break;
            case ButtonMode.Toggle:
                activated = !activated;
                pressCall(this);
                break;
        }
    }
}
