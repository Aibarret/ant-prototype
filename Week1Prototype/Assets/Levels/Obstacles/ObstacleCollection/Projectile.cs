using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int lifetime;
    public float speed;
    public int damage;

    private Vector3 direction;
    private float lifeTimer = 0;
    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3(direction.x, direction.y, 0) * speed;
        lifeTimer += Time.deltaTime;

        if(lifeTimer > lifetime)
        {
            Destroy(gameObject);
        }
    }

    public void SetDirection(Vector3 dir)
    {
        direction = dir.normalized;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            print("Hit");
            collision.gameObject.GetComponent<Enemy>().takeDamage(damage);
            Destroy(gameObject);
        }
        if(collision.CompareTag("FormationUnit"))
        {
            collision.gameObject.GetComponent<FormationUnit>().KillUnit();
            Destroy(gameObject);
        }
    }
}
