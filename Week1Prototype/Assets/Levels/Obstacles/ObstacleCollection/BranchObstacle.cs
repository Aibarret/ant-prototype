using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchObstacle : ObstacleType, DropShadowCastable
{
    public GameObject dropShadowObject;

    private DropshadowController dropShadow;


    private void Awake()
    {
        dropShadow = dropShadowObject.GetComponent<DropshadowController>();
    }

    private void Start()
    {
        ob.updateHealthBar();
    }


    public override void onPickUp()
    {
        ob.Hitbox.enabled = false;
        dropShadow.toggleShadow(false);

    }

    public override void onPutDown()
    {
        ob.Hitbox.enabled = true;
        dropShadow.toggleShadow(true);
    }

    public SpriteRenderer getSpriteRenderer()
    {
        return ob.sprite;
    }

    public void setCollision(FlyingState flyingState)
    {

    }

    
}


