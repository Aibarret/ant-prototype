using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class TirePileType : ObstacleType
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the collision object is an enemy
        if (other.CompareTag("Enemy") || other.CompareTag("FormationUnit"))
        {
            // Check if it's a tire grabber
            if (other.TryGetComponent(out ITireGrabber grabber))
            {
                if (grabber.TireGrabbed)
                    return;
                
                grabber.GrabTire();
            }
        }
    }
}
