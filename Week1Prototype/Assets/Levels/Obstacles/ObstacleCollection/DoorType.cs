using System.Collections;
using System.Collections.Generic;
using FlowFieldNavigation;
using UnityEngine;
using UnityEngine.AI;

public class DoorType : ObstacleType, ButtonActivatable
{
    [SerializeField] private Transform doorTransform;
    [SerializeField] private new Collider2D collider;


    public NavMeshObstacle navMeshObstacle;
    public QueenPoint pathChangeWaypoint;
    public bool IsOpened { get; private set; } = false;

    private const float doorSpeed = 100f;
    private bool buttonActivated;

    //private Coroutine doorCoroutine = null;

    private readonly HashSet<IElectricitySource> sourceSet = new HashSet<IElectricitySource>();

    private void Start()
        => Close();
    
    private void Open()
    {
        if (IsOpened)
            return;

        if (pathChangeWaypoint)
        {
            pathChangeWaypoint.togglePathChange(true);
        }
        IsOpened = true;
        
        navMeshObstacle.enabled = false;
        NavigationManager nm = NavigationManager.Instance;
        if (nm != null)
            nm.GroundFlowField.AddColliderObstacle(collider);
        
        if (vfxManager != null)
            vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
        
        Vector3 rot = doorTransform.eulerAngles;
        rot.y = 85f;
        doorTransform.localRotation = Quaternion.Euler(rot);

        collider.enabled = true;
    }

    private void Close()
    {
        if (!IsOpened)
            return;
        if (pathChangeWaypoint)
        {
            pathChangeWaypoint.togglePathChange(false);
        }
        
        navMeshObstacle.enabled = true;
        NavigationManager nm = NavigationManager.Instance;
        if (nm != null)
            nm.GroundFlowField.RemoveColliderObstacle(collider);
        
        IsOpened = false;
        vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
        Vector3 rot = doorTransform.eulerAngles;
        rot.y = 0f;
        doorTransform.localRotation = Quaternion.Euler(rot);

        collider.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out IElectricitySource source))
            sourceSet.Add(source);
    }

    private void Update()
    {
        bool isPowered = false;

        foreach (IElectricitySource source in sourceSet)
        {
            if (source.HasPower)
            {
                isPowered = true;
                break;
            }
        }
        
        if (isPowered)
            Open();
        else if (!buttonActivated)
            Close();
    }

    private IEnumerator OpenRoutine()
    {
        while (doorTransform.eulerAngles.y % 360f < 85f)
        {
            doorTransform.Rotate(Vector3.up * (Time.deltaTime * doorSpeed), Space.Self);
            yield return null;
        }

        Vector3 rot = doorTransform.eulerAngles;
        rot.y = 85f;
        doorTransform.localRotation = Quaternion.Euler(rot);
    }

    private IEnumerator CloseRoutine()
    {
        while (doorTransform.eulerAngles.y % 360f > 0f)
        {
            doorTransform.Rotate(Vector3.up * (Time.deltaTime * doorSpeed * -1f), Space.Self);   
            yield return null;
        }
        
        Vector3 rot = doorTransform.eulerAngles;
        rot.y = 0f;
        doorTransform.localRotation = Quaternion.Euler(rot);
    }

    public void activate()
    {
        buttonActivated = true;
        Open();
    }

    public void deactivate()
    {
        buttonActivated = false;
        Close();
    }
}
