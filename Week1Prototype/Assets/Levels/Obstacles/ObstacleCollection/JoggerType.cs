using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Experimental.Rendering.RenderGraphModule;
using UnityEngine.UIElements;

public class JoggerType : ObstacleType
{
    public SpriteRenderer sRenderer;

    public int stepDamage;

    public List<Transform> wayPoints;
    public Rigidbody2D rigidBody;

    public float speed = 1;

    private bool fallen = false;
    private bool backwards = false;
    private int phase = 0;


    //todo: change to same waypoint system as queen?
    public override void behavior() {
        if (fallen) return;

        if (wayPoints.Count == 0) {
            Debug.LogError("Jogger needs waypoints to work properly.");
            return;
        }

        Vector3 direction = (wayPoints[phase].transform.position - transform.position).normalized;
        rigidBody.velocity = new Vector2(direction.x, direction.y) * speed;  

        if (Vector3.Distance(wayPoints[phase].transform.position, transform.position) <= 0.1) {
            if (!backwards) {
                phase++;
                if (phase == wayPoints.Count) {
                    backwards = true;
                    phase--;
                }
            } else {
                phase--;
                if (phase == -1) {
                    backwards = false;
                    phase++;
                }
            }
        }

    }

    public void TripOnQueen(Queen queen) {
        fallen = true;
        rigidBody.velocity = new Vector2();
        queen.takeDamage(stepDamage);
    }

    public void Fall() {
        fallen = true;
        rigidBody.velocity = new Vector2();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (fallen) return;
        BallType ball = collider.GetComponent<BallType>();
        if (ball != null) Fall();
        
        Queen queen = collider.GetComponent<Queen>();
        if (queen != null) TripOnQueen(queen);
    }




}
