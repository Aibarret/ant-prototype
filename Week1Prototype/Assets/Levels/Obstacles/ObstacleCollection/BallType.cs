using UnityEngine;

public sealed class BallType : ObstacleType, DropShadowCastable
{
    [Header("Ball Variables")]
    public GameObject dropShadowObject;


    [SerializeField] private int damage;

    private DropshadowController dropShadow;
    private Vector3 startPosition;
    private bool isMoving;

    private void Awake()
    {
        dropShadow = dropShadowObject.GetComponent<DropshadowController>();
    }

    private void Start()
    {
        startPosition = transform.position;
    }

    public override float pushBehavior(Vector3 direction, float initialSpeed)
    {
        
        isMoving = true;
        return base.pushBehavior(direction, initialSpeed);
    }

    public override void behavior()
    {
        isMoving = false;
    }

    public void Respawn()
    {
        transform.position = startPosition;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy") && isMoving)
        {
            collision.gameObject.GetComponent<Enemy>().push((collision.transform.position - transform.position).normalized, knockback);
        }

        
    }

    public override void onTrigger(bool isEnter, Collider2D collider)
    {
        if (isEnter && isMoving)
        {
            if (collider.tag.Equals("Enemy"))
            {
                Enemy enemy = collider.GetComponent<Enemy>();

                enemy.push((collider.transform.position - transform.position).normalized, knockback);
                enemy.takeDamage(damage);
                
            }
        }
    }

    public SpriteRenderer getSpriteRenderer()
    {
        return ob.sprite;
    }

    public void setCollision(FlyingState flyingState)
    {

    }
}
