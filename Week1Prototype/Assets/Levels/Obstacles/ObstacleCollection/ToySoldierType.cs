using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ToySoldierType : ObstacleType, DropShadowCastable, Clickable
{
    //public DrawingManager dm;

    [Header("Toy Soldier Variables")]
    public bool drawNavPath;
    public GameObject dropShadowObject;
    public Transform gun;
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    public Vector3 fireOffset;
    public GameObject bullet;
    public Animator animator;

    [Header("Toy Soldier Stats")]
    public int pheromonesToCharge;
    public int pheromonesPerClick;
    public float attackRange;
    public float attackDuration;
    public int shootDuration;
    public int speed;


    private DropshadowController dropShadow;
    private int currentPheromones = 0;
    private bool charged = false;
    private float lifetime = 0f;
    private float chargedFrames;
    private float searchedFrames;
    private float shootFrames;

    private void Awake()
    {
        dropShadow = dropShadowObject.GetComponent<DropshadowController>();
    }

    private void Start()
    {
        navMeshAgent.updateRotation = false;
        navMeshAgent.updateUpAxis = false;
        navMeshAgent.speed = speed;
    }



    public override void behavior()
    {
        if (pheromonesToCharge < currentPheromones)
        {
            charged = true;
            currentPheromones = 0;
            chargedFrames = 0;
            searchedFrames = 0;
            shootFrames = 0;
            animator.SetBool("IsMoving", true);
            //StartCoroutine(AttackMode());
        }

        if (charged)
        {
            if (chargedFrames < attackDuration)
            {
                GameObject closestEnemy = GameObject.FindGameObjectWithTag("Enemy");

                searchedFrames += Time.deltaTime;

                if (searchedFrames > 1f)
                {
                    closestEnemy = FindNearestEnemy();
                    searchedFrames = 0;
                }

                GameObject targetObject;

                if (closestEnemy == null)
                {
                    targetObject = QueenManager.Instance.getQueen().gameObject;
                }
                else
                {
                    targetObject = closestEnemy;
                }

                Vector3 direction = (targetObject.transform.position - transform.position).normalized;
                //GetComponent<Rigidbody2D>().velocity = new Vector3(direction.x, direction.y, 0) * speed;


                if (targetObject.transform.position.x > transform.position.x)
                {
                    navMeshAgent.SetDestination((targetObject.transform.position + new Vector3(-3, 0)) + (targetObject.transform.position - transform.position).normalized * -3f);
                }
                else
                {
                    navMeshAgent.SetDestination((targetObject.transform.position + new Vector3(3, 0)) + (targetObject.transform.position - transform.position).normalized * -3f);
                }


                if (Vector3.Distance(transform.position, targetObject.transform.position) < attackRange)
                {
                    shootFrames += Time.deltaTime;

                    if (shootFrames > shootDuration)
                    {
                        GameObject newBullet = Instantiate(bullet, fireOffset + transform.position, Quaternion.identity);
                        Projectile projectile = newBullet.GetComponent<Projectile>();
                        projectile.SetDirection((targetObject.transform.position - (transform.position + fireOffset)).normalized);

                        shootFrames = 0;

                    }
                }
            }
            else
            {
                charged = false;
                animator.SetBool("IsMoving", false);
                if (navMeshAgent.destination != transform.position)
                {
                    navMeshAgent.SetDestination(transform.position);

                }
            }
        }

        lifetime += Time.deltaTime;
    }

    private IEnumerator AttackMode()
    {
        float startTime = Time.time + 0.1f;
        int everySecond = 0;
        float lastShotTime = Time.time;

        vfxManager.ManageParticles(11, VFXManager.VFXControl.Play, gameObject);

        while (Time.time - startTime < attackDuration)
        {
            GameObject closestEnemy = GameObject.FindGameObjectWithTag("Enemy");

            if (startTime > everySecond)
            {
                closestEnemy = FindNearestEnemy();
                everySecond++;
            }

            GameObject targetObject;

            if (closestEnemy == null)
            {
                targetObject = QueenManager.Instance.getQueen().gameObject;
            }
            else
            {
                targetObject = closestEnemy;
            }

            Vector3 direction = (targetObject.transform.position - transform.position).normalized;
            //GetComponent<Rigidbody2D>().velocity = new Vector3(direction.x, direction.y, 0) * speed;
            

            if (targetObject.transform.position.x > transform.position.x)
            {
                navMeshAgent.SetDestination((targetObject.transform.position + new Vector3(-3, 0)) + (targetObject.transform.position - transform.position).normalized * -3f);
            }
            else
            {
                navMeshAgent.SetDestination((targetObject.transform.position + new Vector3(3,0 )) + (targetObject.transform.position - transform.position).normalized * -3f);
            }
            
            
            if (Vector3.Distance(transform.position, targetObject.transform.position) < attackRange)
            {
                float timeSinceLastShot = Time.time - lastShotTime;

                if (timeSinceLastShot > shootDuration)
                {
                    GameObject newBullet = Instantiate(bullet, fireOffset + transform.position, Quaternion.identity);
                    Projectile projectile = newBullet.GetComponent<Projectile>();
                    projectile.SetDirection((targetObject.transform.position - (transform.position + fireOffset)).normalized);

                    lastShotTime = Time.time;
                }
            }

            yield return null;
        }

        charged = false;
        if (navMeshAgent.destination != transform.position)
        {
            navMeshAgent.SetDestination(transform.position);

        }
    }

    

    public override void onChangeSpriteDirection(bool isRight)
    {
        print("calling Change Sprite Direction");
        fireOffset = new Vector3(fireOffset.x * -1f, fireOffset.y, 0);
        gun.localScale = new Vector2(gun.localScale.x * -1, gun.localScale.y);
    }

    private GameObject FindNearestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        

        GameObject nearestEnemy = null;
        float closest = Mathf.Infinity;

        foreach (GameObject enemy in enemies)
        {
            if (enemy != null)
            {
                float dist = Vector3.Distance(transform.position, enemy.transform.position);

                if (dist < closest)
                {
                    closest = dist;
                    nearestEnemy = enemy;
                }
            }
        }

        return nearestEnemy;
    }

    public void onClick()
    {


        if (!GlobalVariables.drawingManager.isCostValid(pheromonesPerClick) && !charged)
        {
            GlobalVariables.drawingManager.CurrentPheromones -= pheromonesPerClick;

            currentPheromones += pheromonesPerClick;
            animator.SetTrigger("IsClicked");
            //print("Clicked, valid cost, Current charge at " + currentPheromones);
        }
        else
        {
            print("Invalid Cost");
        }
    }

    public SpriteRenderer getSpriteRenderer()
    {
        return ob.sprite;
    }

    public void setCollision(FlyingState flyingState)
    {

    }


    private void OnDrawGizmos()
    {
        //Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.DrawWireCube(transform.position + fireOffset, new Vector3(.5f,.5f));

        if (drawNavPath)
        {
            if (navMeshAgent.enabled)
            {
                for (int i = 1; i < navMeshAgent.path.corners.Length; i++)
                {
                    Gizmos.DrawLine(navMeshAgent.path.corners[i - 1], navMeshAgent.path.corners[i]);
                }

            }

        }
    }

}
    
