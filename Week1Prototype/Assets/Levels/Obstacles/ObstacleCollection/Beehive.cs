using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beehive : ObstacleType, Attackable
{
    public BeeType bee;
    public float spawnInterval;
    public bool spawnBees;

    public delegate void BeehiveDestroyed();
    public BeehiveDestroyed beehiveCallList;

    private void Start()
    {
        StartCoroutine(SpawnBees());
        health = maxHealth;
    }

    IEnumerator SpawnBees()
    {
        //Needed to be able to turn this off -Omar
        if (spawnBees == true){
            BeeType newBee = Instantiate(bee, transform.position, Quaternion.identity);
            newBee.beehive = this.gameObject;

            yield return new WaitForSeconds(spawnInterval);
        }
    }

    public bool takeDamage(int damage, float? direction = null)
    {
        return base.takeDamage(damage);
    }
}
