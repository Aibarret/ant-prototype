using UnityEngine;

public sealed class PieType : ObstacleType
{

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the collision object is an enemy
        if (other.CompareTag("Enemy") || other.CompareTag("FormationUnit"))
        {
            // Check if it's a pie grabber
            if (other.TryGetComponent(out IPieGrabber grabber))
            {
                if (grabber.PieGrabbed)
                    return;
                grabber.GrabPie();
            }
        }
    }
}
