using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncubatorType : ObstacleType, Clickable
{

    public int healthValue;
    private int maxHealthValue;
    public float attackRadius;
    public GameObject waypointToStopAt;
    public GameObject childCollider;

    private ChildCollision cursorCollider;
    private QueenPoint point;
    private bool isInIncubator = false;


    private void Start()
    {
        point = waypointToStopAt.GetComponent<QueenPoint>();
        QueenManager.pathWaypointEventCallList += eventHandler;
        cursorCollider = childCollider.GetComponent<ChildCollision>();
        
    }

    public void eventHandler(PathEvent eventType, int queenIndex, int pathIndex, int waypointIndex)
    {
        if (eventType == PathEvent.atWaypoint)
        {
            if (queenIndex == point.waypointManager.connectedQueen.queenIndex && pathIndex == point.pathNumber && waypointIndex == point.indexNumber)
            {
                GlobalVariables.eventCall(WaypointEvent.queenEventPause, 0, 0);
                GlobalVariables.eventCall(WaypointEvent.queenInvincible, 0, 0);
                isInIncubator = true;
            }
        }
    }

    public void onClick()
    {
        if (isInIncubator)
        {
            print("unpausing");
            GlobalVariables.callList(WaypointEvent.queenEventPause, 0, 0);
            GlobalVariables.eventCall(WaypointEvent.queenInvincible, 0, 0);
            isInIncubator = false;

        }
    }
}
