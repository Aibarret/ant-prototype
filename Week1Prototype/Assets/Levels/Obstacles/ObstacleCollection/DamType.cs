using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamType : ObstacleType
{
    public List<GameObject> connectedSticks = new List<GameObject>();
    public GameObject branchZoneObject;
    public GameObject bridgeObject;
    public GameObject pathChangePoint;



    private QueenPoint point;
    private BranchZone branchZone;
    private BridgeBuilder bridge;
    private int branchCount;
    private int branchTotal;

    private void Awake()
    {
        branchZone = branchZoneObject.GetComponent<BranchZone>();
        bridge = bridgeObject.GetComponent<BridgeBuilder>();
        branchTotal = connectedSticks.Count;
        if (pathChangePoint)
        {
            point = pathChangePoint.GetComponent<QueenPoint>();

        }
    }

    public override bool takeDamage(int dam, bool isPush = false)
    {
        return false;
    }

    public void branchEntered(BranchObstacle branch, GameObject branchObject)
    {
        if (connectedSticks.Contains(branchObject))
        {
            branchCount += 1;

            if (branchCount >= branchTotal)
            {
                bridgeComplete();
            }
        }
    }

    public void bridgeComplete()
    {
        vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
        bridge.toggleBridge(true);

        branchZone.gameObject.SetActive(false);

        for (int i = connectedSticks.Count - 1; i >= 0; i--)
        {
            GameObject.Destroy(connectedSticks[i]);
        }
        connectedSticks = new List<GameObject>();

        if (point)
        {
            point.togglePathChange(!point.changeQueenPath);
        }
    }


}
