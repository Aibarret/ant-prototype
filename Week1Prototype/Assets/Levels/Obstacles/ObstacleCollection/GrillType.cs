using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrillType : ObstacleType, DropShadowCastable, Attackable
{
    [Header("Grill Stats")]
    public int damage;
    public float attackRate;
    public float attackRange;
    public bool isInstant = false;

    [Header("Grill Object & Component Refs")]
    public DropshadowController dropshadow;
    public Collider2D attackTrigger;
    [SerializeField] private ParticleSystem flameEffect;
    public RangeIndicator rangeIndicator;
    public GameObject fireballObject;
    public EllipseCollider2D hitboxRadiusController;
    public Timer timer;

    private Queen targetQueen;
    //private Transform targetTransform;
    private Attackable attackableTarget;
    private List<Attackable> attackablesInRange = new List<Attackable>();

    private float fballElapsedFrames = 0;
    private Vector3 fireballStartingPosition;
    private float defaultFireballHeight;
    private float lowFireballHeight;


    private void Start()
    {
        health = maxHealth;
        defaultFireballHeight = flameEffect.transform.localPosition.y;
        lowFireballHeight = defaultFireballHeight;

        setAttackRange();
    }

    public override void behavior()
    {
        if (targetQueen)
        {
            if (!timer.isTiming())
            {
                timer.startTimer(attackRate, attackQueen);
                fballElapsedFrames = 0;
            }
            fireballStartingPosition = transform.position;
            fireballBehavior(targetQueen.transform);
        }
        else
        {
            if (timer.isTiming())
            {
                timer.stopTimer();
                resetFireball();
            }
        }        
    }

    public override void carryBehavior(Transform carryingObject, Vector3 carryPosition)
    {
        base.carryBehavior(carryingObject, carryPosition);

        rangeIndicator.transform.position = carryingObject.position;
        //fireballBasePosition = carryingObject.position;
        lowFireballHeight = defaultFireballHeight - carryPosition.y;
        attackTrigger.offset = carryPosition * -1;

        if (attackableTarget == null)
        {
            attackableTarget = getFireballTarget();
        }

        if (attackableTarget != null)
        {
            if (!fireballObject.activeInHierarchy)
            {
                fireballObject.SetActive(true);
            }

            if (!timer.isTiming())
            {
                timer.startTimer(attackRate, attack);
                fireballStartingPosition = carryingObject.position;
            }

            fireballBehavior(attackableTarget.gameObject.transform);
        }
        else
        {
            //fireballObject.SetActive(false);
            fireballObject.transform.position = carryingObject.position;
            if (timer.isTiming())
            {
                timer.stopTimer();
                resetFireball();
            }
        }
    }
    public void fireballBehavior(Transform target)
    {
        if (isInstant)
        {
            fireballObject.transform.position = target.position;
        }
        else
        {
            fireballObject.transform.position = Vector3.Lerp(fireballStartingPosition, target.position, fballElapsedFrames / attackRate);
            if (lowFireballHeight != defaultFireballHeight)
            {
                flameEffect.transform.localPosition = new Vector3(0, Mathf.Lerp(defaultFireballHeight, lowFireballHeight, fballElapsedFrames / attackRate));

            }
            
        }
        
        fballElapsedFrames += Time.deltaTime;
    }

    public override void onTrigger(bool isEnter, Collider2D collider)
    {
        base.onTrigger(isEnter, collider);

        if (collider.tag == "Queen")
        {
            if (isEnter)
            {
                // TODO: Should probably create a list of queens in range but it's a very edge case that we can probably get away with
                if (targetQueen == null)
                {
                    targetQueen = collider.GetComponent<Queen>();
                }
                else if (Vector3.Distance(transform.position, collider.transform.position) < Vector3.Distance(transform.position, targetQueen.transform.position))
                {
                    targetQueen = collider.GetComponent<Queen>();
                }

            }
            else
            {
                if (targetQueen)
                {
                    if (collider.gameObject == targetQueen.gameObject)
                    {
                        targetQueen = null;
                    }

                }
            }
        }
        else if ((collider.tag == "Enemy" || collider.tag == "Obstacle"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (newTarget != null)
            {
                if (isEnter)
                {
                    if (!attackablesInRange.Contains(newTarget))
                    {
                        attackablesInRange.Add(newTarget);
                        if (collider.tag == "Enemy")
                        {
                            collider.GetComponent<Enemy>().EventEnemyKilled += onEnemyDeath;
                        }
                        else
                        {
                            collider.GetComponent<ObstacleType>().EventObstacleKilled += onObstacleDeath;
                        }
                    }
                }
                else
                {
                    if (attackablesInRange.Contains(newTarget))
                    {
                        attackablesInRange.Remove(newTarget);

                        if (collider.tag == "Enemy")
                        {
                            collider.GetComponent<Enemy>().EventEnemyKilled -= onEnemyDeath;
                        }
                        else
                        {
                            collider.GetComponent<ObstacleType>().EventObstacleKilled -= onObstacleDeath;
                        }
                    }
                }
            }
        }

        
    }


    public override void onPickUp()
    {
        dropshadow.toggleShadow(false);
        resetFireball();
        timer.stopTimer();
    }

    public override void onPutDown()
    {
        dropshadow.toggleShadow(true);
        lowFireballHeight = defaultFireballHeight;
        resetFireball();
        timer.stopTimer();
        attackTrigger.offset = Vector2.zero;
    }

    public void onEnemyDeath(Enemy enemy)
    {
        attackablesInRange.Remove(enemy);

        if ((Attackable) enemy == attackableTarget)
        {
            attackableTarget = null;
        }
    }

    public void onObstacleDeath(Obstacle obstacle)
    {
        if ((Attackable)obstacle.type != null)
        {
            attackablesInRange.Remove((Attackable)obstacle.type);

            if ((Attackable) obstacle.type == attackableTarget)
            {
                attackableTarget = null;
            }
        }


    }

    public void resetFireball()
    {
        flameEffect.Stop();
        fireballObject.transform.position = transform.position;
        flameEffect.transform.localPosition = new Vector3(0, defaultFireballHeight);
        flameEffect.Play();
        fballElapsedFrames = 0;
    }

    public Attackable getFireballTarget()
    {
        
        if (attackablesInRange.Count > 0)
        {

            float closestDistance = Vector3.Distance(transform.position, attackablesInRange[0].gameObject.transform.position);
            Attackable closestTarget = attackablesInRange[0];

            foreach (Attackable target in attackablesInRange)
            {
                if (Vector3.Distance(transform.position, target.gameObject.transform.position) < closestDistance)
                {
                    closestDistance = Vector3.Distance(transform.position, target.gameObject.transform.position);
                    closestTarget = target;
                }
            }

            return closestTarget;
        }

        return null;
    }

    public void setAttackRange()
    {
        Vector3 newRadius = RadiusRatios.calculateSkewedCircle(attackRange);

        rangeIndicator.setRange(attackRange * 2);

        hitboxRadiusController.radiusX = newRadius.x;
        hitboxRadiusController.radiusY = newRadius.y;
    }

    public void attackQueen()
    {
        targetQueen.takeDamage(damage);
        resetFireball();
        ob.PlayDirectSFX("event:/Enemies/Fire Ball Attack");
    }

    public void attack()
    {
        attackableTarget.takeDamage(damage * 2);
        attackableTarget = null;
        resetFireball();
        ob.PlayDirectSFX("event:/Enemies/Fire Ball Attack");
    }

    public SpriteRenderer getSpriteRenderer()
    {
        return ob.sprite;
    }

    public void setCollision(FlyingState flyingState)
    {
        
    }

    public bool takeDamage(int damage, float? direction = null)
    {
        return base.takeDamage(damage);
    }

    private void OnDrawGizmosSelected()
    {
        if (attackTrigger != null && hitboxRadiusController != null)
        {
            if (attackRange != hitboxRadiusController.radiusX)
            {
                Vector3 newRadius = RadiusRatios.calculateSkewedCircle(attackRange);

                hitboxRadiusController.radiusX = newRadius.x;
                hitboxRadiusController.radiusY = newRadius.y;
                print("Changing Collider to " + newRadius);
            }
        }
        else if (attackTrigger != null && attackTrigger.GetType().Name == new CircleCollider2D().GetType().Name)
        {
            CircleCollider2D hit = (CircleCollider2D)attackTrigger;
            if (attackRange != hit.radius)
            {
                hit.radius = attackRange;
                print("Changing Circle Collider to radius " + attackRange);
            }
        }
    }
}
