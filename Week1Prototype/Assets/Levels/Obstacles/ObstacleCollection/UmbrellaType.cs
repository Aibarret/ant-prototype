using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UmbrellaType : ObstacleType
{
    public float mouseRadius = 1.0f;
    public SpriteRenderer sRenderer;


    public bool drawGizmos;
    public Color gizmoColor;


    private List<GameObject> targetables = new List<GameObject>();
    private bool isOpen = false;


    // Make this integrated better with unity's systems once the cursor stuff is done so it doesnt spawn unit
    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            // Cast a ray from the mouse position
            Vector2 mouseLocation = (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float distance = Vector2.Distance(transform.position, mouseLocation);

            if (distance <= mouseRadius) {
                // Toggle the umbrella's open state
                isOpen = !isOpen;

                if (isOpen) {
                    UmbrellaOpen();
                } else {
                    UmbrellaClose();
                }
            }
        }
    }


    public void UmbrellaOpen() {
        //Change sprite here
        sRenderer.enabled = true;
        //Change sprite here
    
        foreach (GameObject target in targetables) {
            // Get the rotation of the object
            float rotation = transform.eulerAngles.z * Mathf.Deg2Rad;
            Vector2 direction = new Vector2(Mathf.Cos(rotation), Mathf.Sin(rotation));

            Moveable moveable = target.GetComponent<Moveable>();

            if (moveable != null) {
                // Apply the force to the moveable if exists
                moveable.push(direction, knockback);
            } else {
                // Just move if no moveable, primarily for FormationUnits
                target.transform.position += knockback * (Vector3) direction;
            }
        }
    }

    public void UmbrellaClose() {
        //Change sprite here
        sRenderer.enabled = false;
        //Change sprite here
    }


    private void OnTriggerEnter2D(Collider2D collider) {
        string tag = collider.gameObject.tag;
        if (tag == "Obstacle" || tag == "Enemy") {
            targetables.Add(collider.gameObject);
            Debug.Log(collider.name + " added!");
        }
    }

    private void OnTriggerExit2D(Collider2D collider) {
        string tag = collider.gameObject.tag;
        if (tag == "Obstacle" || tag == "Enemy") {
            targetables.Remove(collider.gameObject);
            Debug.Log(collider.name + " removed.");
        }
    }



    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.color = gizmoColor;
            Gizmos.DrawWireSphere(transform.position, mouseRadius);

        }
    }


}
