using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject followObject;

    private void Update()
    {
        transform.position = new Vector3(followObject.transform.position.x, followObject.transform.position.y, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }
}
