using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Windbox : MonoBehaviour
{
    [SerializeField] private float _windForce = 1f;
    [SerializeField] private bool windEnabled;

    public void SwitchWind(bool val) {
        windEnabled = val;
    }


    private void OnTriggerStay2D(Collider2D collider) {
        if (!windEnabled) return;
        string tag = collider.gameObject.tag;
        if (tag == "Obstacle" || tag == "FormationUnit" || tag == "Enemy") {
            // Get the rotation of the object
            float rotation = transform.eulerAngles.z * Mathf.Deg2Rad;
            Vector2 windDirection = new Vector2(Mathf.Cos(rotation), Mathf.Sin(rotation));

            if (collider.TryGetComponent(out Enemy enemy))
            {
                if (enemy.type.weight < 10)
                    enemy.push(windDirection, (int)_windForce);
            }
            else if (collider.TryGetComponent(out FormationUnit unit))
            {
                if (unit.type.weight < 10)
                    unit.push(windDirection, (int)_windForce);
            }
            
            // Rigidbody2D rb = collider.GetComponent<Rigidbody2D>();
            //
            // if (rb != null) {
            //     // Apply the wind force to the rigidbody if exists
            //     rb.AddForce(0.1f * _windForce * windDirection, ForceMode2D.Impulse);
            // } else {
            //     // Just move if no rigidbody, primarily for FormationUnits
            //     collider.transform.position += 0.1f * _windForce * (Vector3) windDirection;
            // }
        }

    }

}
