using System;
using System.Collections;
using UnityEngine;

public sealed class CarType : ObstacleType
{
    /* ===== Metadata ===== */
    

    
    
    
    /* ===== Car Fields ===== */
    
    [SerializeField] private CarWaypointNode startNode;

    private CarWaypointNode currentNode = null;

    private float speed = 0f;

    private float t = 0f;



    private void Start()
    {
        t = 0f;
        currentNode = startNode;
        print(currentNode.Curve);
        StartCoroutine(BehaviorRoutine());
    }

    private IEnumerator BehaviorRoutine()
    {
        while (true)
        {
            // Terminate if the current waypoint node is null
            if (currentNode == null)
                yield break;
            
            // Wait for action execution to finish
            if (currentNode.StopForSeconds > 0f)
                yield return new WaitForSeconds(currentNode.StopForSeconds);
            speed = currentNode.Speed;
            
            // Move
            if(currentNode.Curve != null)
            {
                while (t < 1f)
                {
                    transform.position = currentNode.Curve.GetPoint(t);
                    const float speedMultiplier = 0.1f;
                    t += speed * Time.deltaTime * speedMultiplier;
                    yield return null;
                }
                transform.position = currentNode.Curve.GetPoint(1f);
            }
            else
            {
                transform.position = currentNode.transform.position;
            }

            // Move to the next node
            t = 0f;
            currentNode = currentNode.Next;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out FormationUnit formationUnit))
            formationUnit.KillUnit();
        else if (other.TryGetComponent(out Queen queen))
            queen.die(false);
        else if (other.TryGetComponent(out Enemy enemy))
            enemy.takeDamage(int.MaxValue);
    }
}
