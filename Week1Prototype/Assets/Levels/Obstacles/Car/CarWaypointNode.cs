﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BezierCurve = Boss.BezierCurve;
using Curve = Boss.BezierCurve;

public sealed class CarWaypointNode : MonoBehaviour
{
    [SerializeField] private CarWaypointNode next;
    public CarWaypointNode Next => next;

    [SerializeField] private List<Vector2> waypoints;

    public Curve Curve { get; private set; }
    
    [SerializeField] private float stopForSeconds = 1f;
    public float StopForSeconds => stopForSeconds;
    
    [SerializeField] private float speed = 10f;
    public float Speed => speed;



    private void Start()
    {
        // Create BezierCurve instance
        List<Vector2> pointList = new List<Vector2>();
        pointList.Add(transform.position);
        pointList.AddRange(waypoints);
        if (next != null)
            pointList.Add(next.transform.position);
        Curve = new Curve(pointList);
    }
    
#if UNITY_EDITOR

    private static readonly Color pathColor = Color.cyan;
    private const int pathResolution = 20;

    // Use cached objects to save GC burden :3
    private List<Vector2> gizmosPointListCache = new List<Vector2>();
    private Curve gizmosCurveCache = new Curve();
    
    private void OnDrawGizmos()
    {
        Gizmos.color = pathColor;
     
        // Reconstruct the list
        gizmosPointListCache.Clear();
        gizmosPointListCache.Add(transform.position);
        gizmosPointListCache.AddRange(waypoints);
        if (next != null)
            gizmosPointListCache.Add(next.transform.position);
        
        // Reconstruct curve object
        gizmosCurveCache.PointList.Clear();
        gizmosCurveCache.PointList.AddRange(gizmosPointListCache);
                
        if (gizmosCurveCache.PointList.Count < 2)
            return;
            
        Vector2 previousPoint = gizmosCurveCache.GetPoint(0);
        Gizmos.DrawSphere(previousPoint, 0.2f);
        for (int i = 1; i <= pathResolution; i++)
        {
            float t = i / (float)pathResolution;
            Vector2 currentPoint = gizmosCurveCache.GetPoint(t);
            Gizmos.DrawLine(previousPoint, currentPoint);
            previousPoint = currentPoint;
        }
    }

#endif
}
