using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public abstract class ObstacleType : MonoBehaviour
{
    public event Action<Obstacle> EventObstacleKilled;

    public Obstacle ob;
    public VFXManager vfxManager;

    [HideInInspector] public int Health;
    [Header("Base Stats")]
    public int MaxHealth;
    public int weight;
    public int knockback;

    public int health { get => Health; set => Health = value; }
    public int maxHealth { get => MaxHealth; set => MaxHealth = value; }

    private void Start()
    {
        health = maxHealth;
        ob.updateHealthBar();

        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
        }
    }

    public virtual void behavior()
    {

    }

    public virtual float pushBehavior(Vector3 direction, float initialSpeed)
    {
        float speedDecay = 0f;

        if (initialSpeed > 0)
        {
            float speed = initialSpeed - (speedDecay * Time.deltaTime);
            ob.rb.MovePosition(transform.position + direction * speed * Time.deltaTime);

            return speed;
        }
        else
        {
            ob.setMoveMode(MoveMode.normal);
            return 0;
        }
    }

    public virtual void pullbehavior(Vector3 direction, float speed)
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    public virtual void carryBehavior(Transform carryingObject, Vector3 carryPosition)
    {
        transform.position = carryingObject.position + carryPosition;
    }

    public virtual void onPickUp()
    {

    }

    public virtual void onPutDown()
    {

    }

    public virtual bool takeDamage(int dam, bool isPush = false)
    {
        
        health = health - dam;
        if (health <= 0)
        {
            die();
            return true;
        }
        else if (health > maxHealth)
        {
            health = maxHealth;
        }

        ob.updateHealthBar();
        return false;
    }

    public virtual void die()
    {
        if (EventObstacleKilled != null)
        {
            EventObstacleKilled.Invoke(ob);
        }
        Destroy(gameObject);
    }

    public virtual void onTrigger(bool isEnter, Collider2D collider)
    {

    }

    public virtual void onCollision(bool isEnter, Collision2D collider)
    {

    }

    public virtual void onChangeSpriteDirection(bool isRight)
    {

    }

}
