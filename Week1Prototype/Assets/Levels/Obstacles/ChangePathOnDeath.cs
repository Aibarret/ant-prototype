using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePathOnDeath : MonoBehaviour
{
    public GameObject waypointToChangePathOf;

    private QueenPoint point;

    private void Start()
    {
        if (waypointToChangePathOf)
        {
            point = waypointToChangePathOf.GetComponent<QueenPoint>();

        }

    }

    private void OnDestroy()
    {
        if (point != null)
        {
            point.togglePathChange(!point.changeQueenPath);
            

        }
    }
}
