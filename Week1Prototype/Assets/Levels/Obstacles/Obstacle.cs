using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, Moveable
{
    public Collider2D Hitbox;
    public Rigidbody2D rb;
    public ObstacleType type;
    public GameObject healthBar;
    public SpriteRenderer sprite;

    private MovingHealthBar movingHealthBar;

    public int health => type.health;
    public int maxHealth => type.maxHealth;

    private bool isColliding = false;

    private Vector3 lastPosn;

    // Push and Pull Variables
    [HideInInspector] public float pushDistance;
    private float pushSpeed;
    [HideInInspector] public GameObject carryObject;
    [HideInInspector] public Vector2 pushDirection;
    private MoveMode moveMode = MoveMode.normal;

    // Start is called before the first frame update
    void Awake()
    {
        lastPosn = transform.position;
        if (healthBar != null)
        {
            movingHealthBar = healthBar.GetComponent<MovingHealthBar>();

        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (sprite != null)
        {
            if (transform.position.x > lastPosn.x && sprite.flipX != true)
            {
                changeSpriteDirection(true);
            }
            else if (transform.position.x < lastPosn.x && sprite.flipX != false)
            {
                changeSpriteDirection(false);
            }
        }

        switch (moveMode)
        {
            case MoveMode.normal:
                type.behavior();
                break;
            case MoveMode.push:
                pushSpeed = type.pushBehavior(pushDirection, pushSpeed);
                break;
            case MoveMode.pull:
                break;
            case MoveMode.carry:
                type.carryBehavior(carryObject.transform, pushDirection);
                break;
        }

        
        isColliding = false;
        if (healthBar != null)
        {
            //movingHealthBar.updatePosn();

        }

        lastPosn = transform.position;
    }



    public void updateHealthBar()
    {
        if (healthBar != null)
        {
            movingHealthBar.updateBar(type.health, type.maxHealth);

        }
    }

    public void changeSpriteDirection(bool isRight)
    {
        type.onChangeSpriteDirection(isRight);
        
        if (sprite != null)
        {
            sprite.flipX = isRight;

        }
    }

    public void push(Vector3 direction, int force)
    {
        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = (force - type.weight);
        
        pushSpeed = appliedForce + 10;
        if (pushSpeed <= 0)
        {
            return;
        }

        pushDirection = direction;
        setMoveMode(MoveMode.push);

    }

    public void pull(Vector3 direction, int force)
    {
        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = force - type.weight;

        if (appliedForce > 0)
        {
            pushSpeed = appliedForce + 7.5f;
        }
        else
        {
            pushSpeed = 3;
        }

        pushDirection = direction;
        setMoveMode(MoveMode.pull);
        type.pullbehavior(pushDirection, pushSpeed);
    }

    public void pickUp(GameObject carryingObject, Vector3 carryPosition)
    {
        if (type.weight == 10)
        {
            return;
        }

        type.onPickUp();
        Hitbox.enabled = false;
        carryObject = carryingObject;
        pushDirection = carryPosition;
        setMoveMode(MoveMode.carry);
    }
    public void putDown(Vector3 placePosition)
    {
        Hitbox.enabled = true;
        type.onPutDown();
        transform.position = placePosition;
        setMoveMode(MoveMode.normal);
    }

    public bool isCarryable()
    {
        return type.weight < 10;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
/*        if (isColliding) return;
        isColliding = true;*/
        
        type.onTrigger(true, collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        type.onTrigger(false, collision);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        type.onCollision(true, collision);
        //print("Collided with " + collision.gameObject.name);
        if (moveMode == MoveMode.push)
        {

            pushDirection = Vector3.Reflect(pushDirection, collision.contacts[0].normal);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        type.onCollision(false, collision);
    }

    public void setPushDistance(float distance)
    {
        pushDistance = distance;
    }

    public void setMoveMode(MoveMode mm)
    {
        moveMode = mm;
    }

    public void PlayDirectSFX(string fullPathName)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName);
        }
    }

    public void PlayDirectSFX(string fullPathName, Vector3 worldSpace)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName, worldSpace);
        }
    }

}
