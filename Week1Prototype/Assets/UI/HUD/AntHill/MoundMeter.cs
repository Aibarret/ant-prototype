using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoundMeter : MonoBehaviour
{
    public LineRenderer line;
    public GameObject[] units;
    public GameObject unit;

    // Start is called before the first frame update
    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();

        //GameObject unit = Resources.Load("Assets/UI/HUD/AntHill/UnitPrefab.prefab", typeof(GameObject)) as GameObject;

        units = new GameObject[line.positionCount];


        // Create an array with instantiated game objects with no data
        for (int x = 0; x < line.positionCount; x++)
        {

            Vector3 curr = line.GetPosition(x);
            units[x] = Instantiate(unit, transform);
            units[x].transform.position = curr;
            
        }
    }

    public void UpdateMound(int currentPheromones, Formation currentFormation)
    {
        //Get current formation type
        GameObject formation = Resources.Load(currentFormation.unitPrefab, typeof(GameObject)) as GameObject;
        Sprite antSprite = formation.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite;


        // Create visual trail of current ant type of length currentPheromones
        for (int x = 0; x < line.positionCount; x++)
        {
            units[x].GetComponent<Image>().enabled = true;
            units[x].GetComponent<Image>().sprite = antSprite;

            if (x > currentPheromones)
            {
                units[x].GetComponent<Image>().enabled = false;
            }
        }

    }
}