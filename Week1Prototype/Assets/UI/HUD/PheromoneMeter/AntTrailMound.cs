using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntTrailMound : MonoBehaviour
{
    public GameObject imageToSpawn;

    public float distanceBetweenPoint;
    private float totalDistance;

    private void Start()
    {
        float distance = 0f;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i < transform.childCount - 1)
            {
                distance += Vector3.Distance(transform.GetChild(i).transform.position, transform.GetChild(i + 1).transform.position);
                //print("Section " + i + ": " + Vector3.Distance(transform.GetChild(i).transform.position, transform.GetChild(i + 1).transform.position));
            }
        }

        totalDistance = distance;

        
    }

    public void drawTrail(int remainingPheromones, int usageRate)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            foreach (Transform child in transform.GetChild(i).transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        int totalUnits = Mathf.FloorToInt(remainingPheromones / usageRate);
        float unitDistance = totalDistance / totalUnits;

        //print("Total Units: " + totalUnits + " unitDistance: " + unitDistance);
        int count = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i < transform.childCount - 1)
            {
                Vector3 direction = (transform.GetChild(i + 1).transform.position - transform.GetChild(i).transform.position).normalized;
                float sectionDistance = Vector3.Distance(transform.GetChild(i).position, transform.GetChild(i + 1).position);
                
                //print("Section Distance: " + sectionDistance + " units per sections: " + sectionDistance / unitDistance);
                for (float x = 0; x < sectionDistance && count < totalUnits; x += unitDistance)
                {
                    GameObject newNode = Instantiate(imageToSpawn, transform.GetChild(i));
                    newNode.transform.position = transform.GetChild(i).position + (direction * (float) x);
                    count++;
                }
            }
        }
        //print(count);

    }



    private void OnDrawGizmos()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (i < transform.childCount - 1)
            {
                Gizmos.DrawLine(transform.GetChild(i).transform.position, transform.GetChild(i + 1).transform.position);
            }
        }
    }
}
