using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Meter : MonoBehaviour
{
    [Header("Object Refs")]
    [SerializeField] private Image fillBar;
    [SerializeField] private Image chipBar;
    [SerializeField] private Image icon;
    [SerializeField] private Timer chipTimer;
    [SerializeField] private Animator animator;

    [Header("Settings")]
    [SerializeField] private bool tweenBar;
    [SerializeField] private float barSpeed = 2f;
    [SerializeField] private bool autoChipBar;
    [SerializeField] private float chipSpeed = 1f;
    [SerializeField] private float chipCooldown = .5f;

    [Header("Animation Settings")]
    [SerializeField] private bool useAnimations = false;
    [SerializeField] private float lowThreashold = .9f;


    public delegate void MeterEmptyEvent();
    public MeterEmptyEvent callOnMeterFinish;

    private float targetFillAmount;

    private bool isFillTweening;
    private bool isChipTweening;

    private void Update()
    {
        if (isFillTweening)
        {
            if (fillBar.fillAmount != targetFillAmount)
            {
                // determines if the bar is lowering or raising
                int isHealMod = 1;

                if (fillBar.fillAmount > targetFillAmount)
                {
                    isHealMod = -1;
                }


                // applies that to the fill amount
                fillBar.fillAmount += Time.deltaTime * barSpeed * (float)isHealMod;

                // A dumb conditional that will correct the bar if it goes above or under the target
                if ((isHealMod < 0 && fillBar.fillAmount < targetFillAmount) || (isHealMod > 0 && fillBar.fillAmount > targetFillAmount) && barSpeed != 0)
                {
                    fillBar.fillAmount = targetFillAmount;
                }

                if (chipBar)
                {
                    // if the bar is filling, it should increase the chip bar with it
                    if (isHealMod > 0 && chipBar.fillAmount < fillBar.fillAmount)
                    {
                        chipBar.fillAmount = fillBar.fillAmount;
                    }
                }
                

                // moves the icon to the correct spot
                if (icon)
                {
                    icon.transform.localPosition = new Vector3(Mathf.Lerp(0f, fillBar.rectTransform.rect.width, fillBar.fillAmount / 1), 0, 0);
                }

            }
            else
            {
                isFillTweening = false;
                if (targetFillAmount == 0)
                {
                    if (callOnMeterFinish != null)
                    {
                        callOnMeterFinish();
                    }
                }
            }
        }

        if (isChipTweening)
        {
            if (chipBar.fillAmount != fillBar.fillAmount)
            {
                // determines if the bar is lowering or raising
                int isHealMod = 1;

                if (chipBar.fillAmount > fillBar.fillAmount)
                {
                    isHealMod = -1;
                }

                // applies that to the fill amount
                chipBar.fillAmount += Time.deltaTime * chipSpeed * (float)isHealMod;

                // A dumb conditional that will correct the bar if it goes above or under the target
                if ((isHealMod < 0 && fillBar.fillAmount < fillBar.fillAmount) || (isHealMod > 0 && chipBar.fillAmount > fillBar.fillAmount) && barSpeed != 0)
                {
                    chipBar.fillAmount = fillBar.fillAmount;
                }

            }
            else
            {
                isChipTweening = false;
            }
        }

        if (animator && useAnimations)
        {
            animator.SetBool("IsLow", fillBar.fillAmount < lowThreashold);

        }
    }

    public void updateMeter(int current, int max)
    {
        targetFillAmount = (float)current / (float)max;

        if (targetFillAmount < fillBar.fillAmount && useAnimations)
        {
            if (animator)
            {
                animator.SetTrigger("Shake");
            }
        }

        if (tweenBar)
        {
            isFillTweening = true;
        }
        else
        {
            fillBar.fillAmount = targetFillAmount;

            if (targetFillAmount > fillBar.fillAmount)
            {
                if (chipBar)
                {
                    chipBar.fillAmount = targetFillAmount;
                }
            }

            if (icon)
            {
                icon.rectTransform.localPosition = new Vector3(Mathf.Lerp(0, fillBar.rectTransform.rect.width, targetFillAmount), 0, 0);
            }
        }

       


        if (autoChipBar && targetFillAmount <= fillBar.fillAmount)
        {
            if (chipTimer)
            {
                chipTimer.startTimer(chipCooldown, chipCooldownComplete);
            }
            else
            {
                isChipTweening = true;
            }
        }
    }

    public void updateMeter(int fillCurrent, int fillMax, int chipCurrent, int chipMax)
    {
        float fillTarget = (float)fillCurrent / (float)fillMax;
        fillBar.fillAmount = fillTarget;

        float chipTarget = (float)chipCurrent / (float)chipMax;
        chipBar.fillAmount = chipTarget;
    }

    public void updateOnlyFillMeter(int fillCurrent, int fillMax)
    {
        targetFillAmount = (float)fillCurrent / (float)fillMax;
        
        

        if (tweenBar)
        {
            isFillTweening = true;
        }
        else
        {
            fillBar.fillAmount = targetFillAmount;

            if (targetFillAmount > fillBar.fillAmount)
            {
                if (chipBar)
                {
                    chipBar.fillAmount = targetFillAmount;
                }
            }
        }
    }

    public void updateOnlyChipMeter(int chipCurrent, int chipMax)
    {
        float targetChip = (float)chipCurrent / (float)chipMax;


        chipBar.fillAmount = targetChip;
    }

    public void tweenChipBar()
    {
        isChipTweening = true;
    }

    public void chipCooldownComplete()
    {
        isChipTweening = true;
    }


    
}
