using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseScore : MonoBehaviour
{
    public int amountToGain;

    private void OnDestroy()
    {
        if (GlobalVariables.ui)
            if (GlobalVariables.ui.score != null)
                GlobalVariables.ui.score.IncreaseScore(amountToGain);
    }
}
