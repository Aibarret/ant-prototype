using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Runtime.CompilerServices;
using UnityEngine.SceneManagement;

public class ScoreResults : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Image star1;
    [SerializeField] private Image star2;
    [SerializeField] private Image star3;
    [SerializeField] private GameObject confettiObject;
    [SerializeField] private float scoreRollTIme;
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button playAgainButton;
    [SerializeField] private Button nextLevelButton;

    private int starValue;
    private int scoreValue;
    private int highscore;
    private LevelManager levelManager;
    public VFXManager vfxManager;
    private bool startScoreReveal = false;
    private float scoreFrameTimer = 0;
    private void Start() {
        //print("Getting score results");
        star1.enabled = false;
        star2.enabled = false;
        star3.enabled = false;
        mainMenuButton.interactable = false;
        playAgainButton.interactable = false;
        nextLevelButton.interactable = false;
        scoreValue = GlobalVariables.ui.score.CalculateFinalScore();
        scoreText.text = scoreValue.ToString();
        levelManager = LevelManager.instance;
        LevelManager.instance.toggleLevelChecks(SceneManager.GetActiveScene().name);

        if (levelManager.levelScoresToBeat.ContainsKey(SceneManager.GetActiveScene().name))
        {
            highscore = levelManager.levelScoresToBeat[SceneManager.GetActiveScene().name];
        }

        startScoreReveal = true;

        //StartCoroutine(scoreRevealRoutine(scoreValue));

        /*CalculateStarValue(scoreValue);
        

        vfxManager = VFXManager.Instance;

        //toggle level complete;
        LevelManager.instance.toggleLevelChecks(SceneManager.GetActiveScene().name);

        // Save the high score
        if (levelManager.levelHighScores.ContainsKey(SceneManager.GetActiveScene().name))
        {
            if (scoreValue > levelManager.levelHighScores[SceneManager.GetActiveScene().name])
            {
                levelManager.levelHighScores[SceneManager.GetActiveScene().name] = scoreValue;
            }

        }
        else
        {
            //print("Adding " + SceneManager.GetActiveScene().name + " - " + scoreValue);
            levelManager.levelHighScores.Add(SceneManager.GetActiveScene().name, scoreValue);
        }

        if (!(GlobalVariables.unitToShowOnVictoryStatic == null))
        {
            //print("calling guidebook to show");
            GlobalVariables.guidebook.OpenOnEntry(GlobalVariables.unitToShowOnVictoryStatic);

        }

        // Save Data
        DataPersistenceManager.instance.SaveGame();*/
    }

    private void Update()
    {
        if (startScoreReveal)
        {

            if (scoreFrameTimer > scoreRollTIme)
            {
                startScoreReveal = false;
                StartCoroutine(scoreRevealRoutine(scoreValue));

                return;
            }
            // Tick up the score we're displaying;
            int displayScore = Mathf.RoundToInt(Mathf.Lerp(0, scoreValue, scoreFrameTimer / scoreRollTIme));
            scoreFrameTimer += Time.unscaledDeltaTime;
            // Display the score

            scoreText.text = displayScore.ToString();

            // Star milestones

            if (displayScore >= highscore * 0.5 && !star1.enabled)
            {
                starValue = 1;
                star1.enabled = true;
                if (displayScore < highscore * 0.75)
                {
                    if (vfxManager)
                        vfxManager.ManageParticles(8, VFXManager.VFXControl.Play, gameObject);
                }
            }
            if (displayScore >= highscore * 0.75 && !star2.enabled)
            {
                starValue = 2;
                star2.enabled = true;
                if (displayScore < highscore)
                {
                    if (vfxManager)
                        vfxManager.ManageParticles(9, VFXManager.VFXControl.Play, gameObject);
                }
            }
            if (displayScore >= highscore && !star3.enabled)
            {
                starValue = 3;
                star3.enabled = true;
                if (vfxManager)
                {
                    vfxManager.ManageParticles(10, VFXManager.VFXControl.Play, gameObject);
                }
            }
        }
    }

    public IEnumerator scoreRevealRoutine(int scoreValue)
    {
        //CalculateStarValue(scoreValue);

        yield return new WaitForSecondsRealtime(.5f);

        vfxManager = VFXManager.Instance;

        //toggle level complete;
        //LevelManager.instance.toggleLevelChecks(SceneManager.GetActiveScene().name);

        // Save the high score
        if (levelManager.levelHighScores.ContainsKey(SceneManager.GetActiveScene().name))
        {
            if (scoreValue > levelManager.levelHighScores[SceneManager.GetActiveScene().name])
            {
                levelManager.levelHighScores[SceneManager.GetActiveScene().name] = scoreValue;
            }

        }
        else
        {
            //print("Adding " + SceneManager.GetActiveScene().name + " - " + scoreValue);
            levelManager.levelHighScores.Add(SceneManager.GetActiveScene().name, scoreValue);
        }

        if (!(GlobalVariables.unitToShowOnVictoryStatic == null))
        {
            //print("calling guidebook to show");
            GlobalVariables.guidebook.OpenOnEntry(GlobalVariables.unitToShowOnVictoryStatic);

        }

        // Save Data
        DataPersistenceManager.instance.SaveGame();
        mainMenuButton.interactable = true;
        playAgainButton.interactable = true;
        nextLevelButton.interactable = true;
    }

    private void CalculateStarValue(int score) {
        starValue = 0;
        int highscore = 0;

        if (levelManager.levelScoresToBeat.ContainsKey(SceneManager.GetActiveScene().name))
        {
            highscore = levelManager.levelScoresToBeat[SceneManager.GetActiveScene().name];

        }

        if (score >= highscore * 0.5) {
            starValue = 1;
            star1.enabled = true;
            if (score < highscore * 0.75)
            {
                if (vfxManager)
                    vfxManager.ManageParticles(8, VFXManager.VFXControl.Play, gameObject);
            }
        }
        if (score >= highscore * 0.75) {
            starValue = 2;
            star2.enabled = true;
            if (score < highscore)
            {
                if (vfxManager)
                    vfxManager.ManageParticles(9, VFXManager.VFXControl.Play, gameObject);
            }
        }
        if (score >= highscore) {
            starValue = 3;
            star3.enabled = true;
            if (vfxManager)
            {
                vfxManager.ManageParticles(10, VFXManager.VFXControl.Play, gameObject);
            }
        }

    }

}