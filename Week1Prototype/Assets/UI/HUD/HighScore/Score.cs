using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    
    //Don't edit score directly, use IncreaseScore()
    public int score;
    public float rollTime;
    private int targetScore;
    private int startScore;
    private float currentLevelTime;
    public GameObject textObject;
    private TMPro.TextMeshProUGUI text;
    public bool active = true;

    private void Awake()
    {
        text = textObject.GetComponent<TMPro.TextMeshProUGUI>();
    }

    private void Update() {
        currentLevelTime += Time.deltaTime;
        text.text = score.ToString();
    }


    public void IncreaseScore(int change)
    {
        targetScore += change;
        startScore = score;
        StopAllCoroutines();
        StartCoroutine(RollScore());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator RollScore() 
    {
        float timeSinceEnabled = 0;
        bool finished = false;

        while (!finished) {
            timeSinceEnabled += Time.deltaTime;
            float currentTime = timeSinceEnabled/rollTime;
            score = Mathf.FloorToInt(Mathf.Lerp(startScore, targetScore, currentTime));
            if (currentTime > 1) {
                finished = true;
            }

            yield return null;
        }
    }


    //Call to calculate the final score at the end of a level
    public int CalculateFinalScore() 
    {
        StopAllCoroutines();
        int bonusTimeScore = Mathf.FloorToInt(GlobalVariables.timeScore - currentLevelTime);
        if (bonusTimeScore > 0) {
            targetScore += 200 * bonusTimeScore;
        }
        int health = GlobalVariables.queen.type.health;
        if (health > 0) {
            targetScore += 100 * health;
        }
        int pheromones = GlobalVariables.drawingManager.CurrentPheromones;
        if (pheromones > 0) {
            targetScore += 80 * pheromones;
        }
        score = targetScore;
        return score;
    }
    
}
