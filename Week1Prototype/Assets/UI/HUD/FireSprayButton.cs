using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireSprayButton : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI text;

    public void onPressed()
    {
        GlobalVariables.drawingManager.fireSprayButtonPressed = true;
        GlobalVariables.ui.toggleFirespray(false);
    }

    public void setState(int state)
    {
        if (state > 0)
        {
            GlobalVariables.ui.toggleFirespray(true);
        }

        switch (state)
        {
            case 1:
                text.text = "Fire Spray:";
                break;
            case 2:
                text.text = "Fire Spray+:";
                break;
            case 3:
                text.text = "Fire Spray++:";
                break;
            case 4:
                text.text = "Fire Spray\nForever:";
                break;
            default:
                text.text = "Fire Spray:";
                break;
        }
    }
}
