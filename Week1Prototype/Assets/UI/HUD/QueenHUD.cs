using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QueenHUD : MonoBehaviour
{
    [Header("Object Refs")]
    [SerializeField] private Animator animator;
    [SerializeField] private ClickableImage focusQueenImage;
    [SerializeField] private ClickableImage changeQueenLeft;
    [SerializeField] private ClickableImage changeQueenRight;

    [Header("Health Bar Refs")]
    [SerializeField] private QueenIcon primaryQueenIcon;
    [SerializeField] private QueenIcon secondaryQueenIcon;
    [SerializeField] private QueenIcon tertiaryQueenIcon;
    [SerializeField] private Meter primaryHealth;
    [SerializeField] private Meter secondaryHealth;
    [SerializeField] private Meter tertiaryHealth;

    [Header("Progress Bar Refs")]
    [SerializeField] private QueenIcon progressIcon;
    [SerializeField] private Meter progressBar;

    [Header("Boss Refs")]
    [SerializeField] private GameObject bossCardObject;
    [SerializeField] private GameObject multiBossObject;
    [SerializeField] private QueenIcon bossIcon;
    [SerializeField] private QueenIcon topBossIcon;
    [SerializeField] private QueenIcon botBossIcon;
    [SerializeField] private Meter botBossHealth;
    [SerializeField] private Meter topBossHealth;
    [SerializeField] private TextMeshProUGUI bossLabel;
    

    private Boss.BossType[] activeBosses = new Boss.BossType[2];

    private bool multiQueenMode = false;
    private bool bossMode = false;
    private Queen[] delayedQueens;

    private void Awake()
    {
        focusQueenImage.addToClickCallList(focusQueen);
        changeQueenLeft.addToClickCallList(changeQueenSecond);
        changeQueenRight.addToClickCallList(changeQueenThird);
    }

    public void initializeHUD(Queen[] queens)
    {
        bossCardObject.SetActive(false);
        multiQueenMode = queens[1] && queens[2];

        secondaryHealth.transform.parent.gameObject.SetActive(multiQueenMode);
        tertiaryHealth.transform.parent.gameObject.SetActive(multiQueenMode);

        animator.SetBool("MultiQueenMode", multiQueenMode);

        updateIcons(queens);
        updateHealthBars(queens);
    }

    public void activateQueenHUD()
    {
        animator.SetBool("Active", true);
    }

    public void addToBossArray(Boss.BossType boss)
    {
        print("Adding to Boss Array");
        if (activeBosses[0] == null)
        {
            activeBosses[0] = boss;
            initializeBossMode(boss);
        }
        else if (activeBosses[1] == null)
        {
            activeBosses[1] = boss;

            enableMultiboss();
        }
    }

    public void initializeBossMode(Boss.BossType boss)
    {
        print("Initializing Boss Mode");
        bossCardObject.SetActive(true);
        bossMode = true;
        bossIcon.setIcon(boss.icon);
        botBossIcon.setIcon(boss.icon);
        bossLabel.text = boss.bossDisplayName;

        animator.SetBool("BossMode", bossMode);
    }

    public void enableMultiboss()
    {
        topBossIcon.setIcon(activeBosses[1].icon);

        bossIcon.gameObject.SetActive(false);
        multiBossObject.SetActive(true);
    }

    public void endBossMode()
    {
        bossMode = false;
        animator.SetBool("BossMode", bossMode);
    }

    public void rotateQueens(Queen[] queens)
    {
        delayedQueens = queens;
        animator.SetTrigger("ChangeQueen");
    }

    public void triggerDelayedQueenUpdate()
    {
        updateHealthBars(delayedQueens);
        updateIcons(delayedQueens);
        updateProgressBar(delayedQueens[0].pointContainerScript.lastKnownPosn, delayedQueens[0].pointContainerScript.lineLength);
    }

    public void updateHealthBars(Queen[] queens)
    {
        if (queens[0] != null)
        {
            primaryHealth.updateMeter(queens[0].type.health, queens[0].type.maxHealth);
        }
        if (queens[1] != null)
        {
            secondaryHealth.updateMeter(queens[1].type.health, queens[1].type.maxHealth);
        }
        if (queens[2] != null)
        {
            secondaryHealth.updateMeter(queens[2].type.health, queens[2].type.maxHealth);
        }
    }

    public void updateIcons(Queen[] queens)
    {
        if (queens[0] != null)
        {
            primaryQueenIcon.setIcon(queens[0].type.icon);
            progressIcon.setIcon(queens[0].type.icon);
        }
        if (queens[1] != null)
        {
            secondaryQueenIcon.setIcon(queens[1].type.icon);
        }
        if (queens[2] != null)
        {
            tertiaryQueenIcon.setIcon(queens[2].type.icon);
        }
    }

    public void updateProgressBar(int current, int max)
    {
        progressBar.updateMeter(current, max);
    }

    public void updateBossHealth(Boss.BossType boss)
    {
        if (boss == activeBosses[0])
        {
            botBossHealth.updateMeter(boss.health, boss.maxHealth);
        }
        else
        {
            topBossHealth.updateMeter(boss.health, boss.maxHealth);
        }
        
    }

    // Clickable Image Functions

    public void focusQueen()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        GlobalVariables.camController.FocusQueen();
    }

    public void changeQueenSecond()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        QueenManager.Instance.rotateQueen(1);
    }

    public void changeQueenThird()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        QueenManager.Instance.rotateQueen(-1);
    }
}
