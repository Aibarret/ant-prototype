using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class AntSelectionButton : MonoBehaviour
{
    public AntSelection container;
    public int index;

    public void buttonPressed(bool value)
    {
        if (value)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
            if (!GlobalVariables.gamePaused || GlobalVariables.drawingManager.listeningForFormChange)
            {
                //print(GlobalVariables.drawingManager.listeningForFormChange);
                GlobalVariables.drawingManager.changeFormation(GlobalVariables.drawingManager.sectionMod(index));
                //ReHighlight(button);
                //FMODUnity.RuntimeManager.PlayOneShot("event:/UI/Ant Select", transform.position);

            }
        }
    }


    // Depricated i think? highlighting stuff is handled by toggle button manager
    public void ReHighlight(int button)
    {
        for (int i = 0; i < container.buttonContainer.transform.childCount; i++)
        {
            GameObject buttonObject = container.buttonContainer.transform.GetChild(i).transform.gameObject;
            if (!buttonObject.transform.Find("CrossOut").GetComponent<Image>().enabled)
            {
                Image selectionVFX = buttonObject.transform.Find("SelectionVFX").GetComponent<Image>();
                if (i == button)
                {
                    selectionVFX.enabled = true;
                }
                else
                {
                    selectionVFX.enabled = false;
                }
            }
        }
    }
}
