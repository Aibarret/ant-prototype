using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AntSelection : MonoBehaviour
{
    public GameObject buttonContainer;
    public PlayerActionButtons actionButtons;
    public ToggleManager antToggles;

    private List<Formation> formationList;
    private int numOfAnts = 12;


    public void updatePaletteFormations(List<Formation> activeFormations)
    {
        formationList = activeFormations;
        
        revealButtons();
        actionButtons.activateToggles();
    }

    

    public void revealButtons()
    {
        int count = 0;
        while ((count < numOfAnts) && (count < formationList.Count))
        {
            GameObject buttonObject = buttonContainer.transform.GetChild(count).gameObject;
            IconButton iconButton = buttonObject.GetComponent<IconButton>();

            iconButton.icon.enabled = true;

            GameObject formation = Resources.Load(formationList[count].unitPrefab, typeof(GameObject)) as GameObject;
            Sprite antSprite = formation.GetComponent<SpriteRenderer>().sprite;

            iconButton.icon.sprite = antSprite;
            iconButton.toggle.interactable = true;
            //GameObject.Destroy(formation);
            count++;
        }

        while (count < numOfAnts)
        {
            GameObject buttonObject = buttonContainer.transform.GetChild(count).gameObject;
            IconButton iconButton = buttonObject.GetComponent<IconButton>();

            iconButton.icon.sprite = null;
            iconButton.icon.enabled = false;
            iconButton.toggle.interactable = false;
            count++;
        }

    }

}
