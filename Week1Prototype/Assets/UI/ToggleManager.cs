using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleManager : MonoBehaviour
{
    [Header("Object Refs")]
    [SerializeField] private List<IconButton> toggleList = new List<IconButton>();
    [Header("Zero Sum Settings")]
    [SerializeField] private bool zeroSumMode;
    [SerializeField] private int defaultButton;

    [SerializeField] private int activeButtonIndex; 

    public void Start()
    {
        foreach (IconButton button in toggleList)
        {
            button.eventCallList += updateToggles;
            button.toggleZeroSumMode(zeroSumMode);
        }

        if (zeroSumMode)
        {
            toggleList[defaultButton].toggle.isOn = true;
            activeButtonIndex = defaultButton;
        }
    }

    public void directToggle(int index, bool isOn)
    {
        toggleList[index].toggle.isOn = isOn;
    }

    public void updateToggles(Toggle button)
    {
        if (button.isOn)
        {
            int count = 0;
            foreach (IconButton toggleButton in toggleList)
            {
                if (toggleButton.toggle == button)
                {
                    activeButtonIndex = count;
                }
                count++;
            }
           
            foreach (IconButton toggleButton in toggleList)
            {
                Toggle toggle = toggleButton.toggle;
                if (toggle != button)
                {
                    //print(toggleButton.name);
                    if (toggle.isOn)
                    {
                        toggle.isOn = false;
                    }
                }
            }
        }
        else
        {
            if (zeroSumMode)
            {
                if (button == toggleList[activeButtonIndex].toggle)
                {
                    button.isOn = true;
                }
            }
        }

    }
}
