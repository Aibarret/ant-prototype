using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{
    public GameObject currentActiveMenu;

    public static MenuManager Instance;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        } else {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void AnimationPreload() {
        // behavior to be executed before transition animation plays

    }

    public void AnimationPostload() {
        // behavior to be executed after transition animation plays
    }

    public bool CheckActiveMenu(GameObject newMenu) {
        if (currentActiveMenu == newMenu) {
            return false;
        }
        
        return true;
    }

    public bool SetActiveMenu(GameObject newMenu, Animator animator) {

        if (!CheckActiveMenu(newMenu)) {
            return false;
        }

        if (currentActiveMenu != null) {
            EnemyAlmanac maybenac = newMenu.GetComponent<EnemyAlmanac>();
            if (maybenac != null)
            {
                LevelManager.instance.SetControllerSelectedObject(maybenac.defaultBackButton);
                print(LevelManager.instance.controllerSelectedObject);
            }
            else
            {
                LevelManager.instance.SetControllerSelectedObject(newMenu.GetComponent<OptionsMenu>().defaultSelectedObject);

            }
            StartCoroutine(LoadAnimation(animator));
        } else {
            AnimationPreload();

            currentActiveMenu = newMenu;
            //EventSystem.current.SetSelectedGameObject(currentActiveMenu, new BaseEventData(EventSystem.current));
            EnemyAlmanac maybenac = newMenu.GetComponent<EnemyAlmanac>();
            if (maybenac != null)
            {
                LevelManager.instance.SetControllerSelectedObject(maybenac.defaultBackButton);
                print(LevelManager.instance.controllerSelectedObject);
            }
            else
            {
                LevelManager.instance.SetControllerSelectedObject(newMenu.GetComponent<OptionsMenu>().defaultSelectedObject);
                
            }
            animator.SetBool("isActive", true);

            AnimationPostload();
        }

        return true;
    }

    public void SetInactiveMenu(Animator animator) {
        StartCoroutine(DeloadAnimation(animator));
    }



    public IEnumerator LoadAnimation(Animator animator) {
        AnimationPreload();

        currentActiveMenu.GetComponent<Animator>().SetBool("isActive", false);

        yield return new WaitUntil(() => currentActiveMenu.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime <= 1.0f);

        print("animator.gameObject: " + animator.gameObject);
        print("currentActiveMenu: " + currentActiveMenu);

        currentActiveMenu = animator.gameObject;
        animator.SetBool("isActive", true);

        print("currentActiveMenu: " + currentActiveMenu);

        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 1.0f);

        AnimationPostload();
    }

    public IEnumerator DeloadAnimation(Animator animator) {
        AnimationPreload();

        animator.SetBool("isActive", false);

        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 1.0f);

        currentActiveMenu = null;

        AnimationPostload();
    }
}
