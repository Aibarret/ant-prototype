using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerScroll : MonoBehaviour
{
    public ScrollRect scroll;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.instance.moveAction.ReadValue<Vector2>().y > 0)
        {
            scroll.velocity = new Vector2(0, 500);
        }
        if (LevelManager.instance.moveAction.ReadValue<Vector2>().y < 0)
        {
            scroll.velocity = new Vector2(0, -500);
        }
    }
}
