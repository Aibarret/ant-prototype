using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Object Refs")]
    public AntSelection palette;
    public Meter pheromoneMeter;
    public Score score;
    public MoundMeter MoundTrail;
    public DialogueManager dialogueManager;
    public ScreenCursor screenCursor;
    public QueenHUD queenHUD;
    public FireSprayButton fireSprayButton;

    [Header("Animator Controls")]
    public Animator animator;


    public void activateHUD()
    {
        animator.SetBool("Active", true);
        queenHUD.activateQueenHUD();
    }

    public void toggleFiresprayvisibility()
    {
        fireSprayButton.gameObject.SetActive(!fireSprayButton.gameObject.activeInHierarchy);
    }

    public void toggleFirespray(bool isAvailable)
    {
        animator.SetBool("FireSprayAvailable", isAvailable);
    }

    public void onPausePressed()
    {
        GlobalVariables.optionsMenu.OnPauseButtonPressed();
    }
}
