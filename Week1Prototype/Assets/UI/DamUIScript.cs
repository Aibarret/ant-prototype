using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamUIScript : MonoBehaviour
{
    public Text amount;
    public Image checkmark;
    public GameObject Dam;
    public GameObject stickUI;
    public Vector3 objectOffset;
    public DamType info;
    public bool inWorld = false;
    private int numOfbranches;
    // Start is called before the first frame update
   /* void Start()
    {
        numOfbranches = info.totalBranches;
    }

    // Update is called once per frame
    void Update()
    {
        if (info.health == 0)
        {
            amount.gameObject.SetActive(false);
            checkmark.gameObject.SetActive(true);
        }
        amount.text = "Remaining: " + info.health + "/" + numOfbranches;

        if (inWorld)
        {
            UpdateDamUI();
        }
    }

    public void UpdateDamUI()
    {
        Vector3 offsettedPosn = Dam.transform.position + objectOffset;
        Vector3 newPosn = new Vector3(Mathf.Round(offsettedPosn.x * 100f) / 100f, Mathf.Round(offsettedPosn.y * 100f) / 100f, 0);


        //print(newPosn);

        Vector3 point = GlobalVariables.cam.WorldToScreenPoint(newPosn);
        //print("WORLD VARIABLE AT : " + point);
        stickUI.transform.position = new Vector3(point.x, point.y);
    }*/
}
