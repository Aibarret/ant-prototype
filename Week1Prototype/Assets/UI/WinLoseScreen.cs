using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLoseScreen : MonoBehaviour
{
    public GlobalVariables global;
    public GameObject defaultSelectedObject;

    public static GameObject defaultSelectedObjectStatic;

    private void Awake()
    {
        global = GameObject.Find("GameManager").GetComponent<GlobalVariables>();
        defaultSelectedObjectStatic = defaultSelectedObject;
    }

    public void MainMenu()
    {
        GlobalVariables.toggleGamePause(false);
        print("MainMenu pressed");
        global.nonStaticToLevelSelect();
    }

    public void ResetLevel()
    {
        GlobalVariables.toggleGamePause(false);
        print("ResetLevel Pressed");
        global.RetryLevel();
    }

    public void NextLevel()
    {
        GlobalVariables.toggleGamePause(false);
        global.ToNextLevel();
    }
}
