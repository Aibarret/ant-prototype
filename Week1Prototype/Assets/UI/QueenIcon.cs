using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum QueenTypeIcons
{
    TUTORIAL,
    CORGI,
    DUCKLING_KING,
    DUCKLING_QUEEN,
    DUCKLING_JOKER,
    BOSS_TERMITE_KING,
    BOSS_TERMITE_QUEEN,
    BOSS_QUEEN_BEE,
    BOSS_WASP_QUEEN
}

public class QueenIcon : MonoBehaviour
{

    // File paths for getting icon sprites
    private static string[] nullPaths = { "QueenPortraits/EmptyPortrait", 
                                            "QueenPortraits/Small Portraits/SmallEmptyPortrait" };
    private static string[] iconPaths = {"QueenPortraits/General", 
                                        "QueenPortraits/Corgi Icon", 
                                        "QueenPortraits/DucklingKing", 
                                        "QueenPortraits/DucklingQueen", 
                                        "QueenPortraits/DucklingJoker", 
                                        "Termite King Placeholder", 
                                        "Termite Queen Placeholder", 
                                        "Queen Bee Placeholder"};
    private static string[] smallIconPaths = {"QueenPortraits/Small Portraits/SmallGeneral",
                                                "QueenPortraits/Small Portraits/SmallCorgi", 
                                                "QueenPortraits/Small Portraits/SmallDucklingKing", 
                                                "QueenPortraits/Small Portraits/SmallDucklingQueen", 
                                                "QueenPortraits/Small Portraits/SmallDucklingJoker",
                                                "QueenPortraits/Small Portraits/TermiteKing",
                                                "QueenPortraits/Small Portraits/TermiteQueen",
                                                "QueenPortraits/Small Portraits/QueenBee"};

    private static Sprite[] nullSprites = new Sprite[2];
    private static Sprite[] iconSprites = new Sprite[8];
    private static Sprite[] smallIconSprites = new Sprite[8];


    [Header("Object Refs")]
    [SerializeField] private Image imageRenderer;
    [SerializeField] private RectTransform rectTransform;

    [Header("Settings")]
    [SerializeField] private bool useSmallIcons;
    [SerializeField] private QueenTypeIcons activeTypeIcon;

    private void Awake()
    {
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }

        if (imageRenderer == null)
        {
            imageRenderer = GetComponent<Image>();
        }
    }

    public static void GenerateIconFiles()
    {
        for (int i = 0; i < iconPaths.Length; i++)
        {
            iconSprites[i] = Resources.Load<Sprite>(iconPaths[i]);
            smallIconSprites[i] = Resources.Load<Sprite>(smallIconPaths[i]);
        }

        nullSprites[0] = Resources.Load<Sprite>(nullPaths[0]);
        nullSprites[1] = Resources.Load<Sprite>(nullPaths[1]);
    }

    public void setIcon(QueenTypeIcons icon)
    {
        activeTypeIcon = icon;
        Vector2 newPivot = Vector2.zero;

        if (useSmallIcons)
        {
            imageRenderer.sprite = smallIconSprites[(int)activeTypeIcon];
        }
        else
        {
            imageRenderer.sprite = iconSprites[(int)activeTypeIcon];
        }

        if (imageRenderer.sprite == null)
        {
            print("Attempted to get sprite at '" + smallIconPaths[(int)activeTypeIcon] + "' but got null");
            if (useSmallIcons)
            {
                imageRenderer.sprite = nullSprites[1];
            }
            else
            {
                imageRenderer.sprite = nullSprites[0];
            }
        }
       
        newPivot = new Vector2(imageRenderer.sprite.pivot.x / imageRenderer.sprite.rect.width,
                                            imageRenderer.sprite.pivot.y / imageRenderer.sprite.rect.height);
        rectTransform.pivot = newPivot;
    }


}
