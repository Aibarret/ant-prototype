using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class IconButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Object Refs")]
    public Button button;
    public Toggle toggle;
    public Image icon;
    public GameObject hoverRing;

    public delegate void ButtonPressedEvent(Toggle button);
    public ButtonPressedEvent eventCallList;

    private bool isZeroSum;
    private bool isActive;

    public void toggleZeroSumMode(bool value)
    {
        isZeroSum = value;
    }

    public void onToggle(bool value)
    {
        if (eventCallList != null)
        {
            eventCallList(toggle);

        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (button)
        {
            if (button.interactable)
            {
                hoverRing.SetActive(true);

            }
        }
        else if (toggle)
        {
            if (toggle.interactable)
            {
                hoverRing.SetActive(true);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (button)
        {
            if (button.interactable)
            {
                hoverRing.SetActive(false);

            }
        }
        else if (toggle)
        {
            if (toggle.interactable)
            {
                hoverRing.SetActive(false);
            }
        }
    }


}
