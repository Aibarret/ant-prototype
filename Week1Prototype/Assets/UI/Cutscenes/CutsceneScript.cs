using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CutsceneScript", menuName = "ScriptableObjects/CutsceneScript", order = 2)]
public class CutsceneScript : ScriptableObject
{
    public TextAsset script;
    
    public List<List<string>> getLines()
    {
        string text = script.ToString();
        //Debug.Log(text);
        string[] lines = text.Split('\n');
        //Debug.Log(lines.Length);

        if (lines.Length > 0)
        {
            List<List<string>> final = new List<List<string>>();
            int cutsceneIndex = -1;
            foreach (string line in lines)
            {
                //Debug.Log(line);
                if (line.Split(" ")[0].ToUpper().Equals("CUTSCENE"))
                {
                    cutsceneIndex++;
                    final.Add(new List<string>());
                }
                else
                {
                    final[cutsceneIndex].Add(line);
                }
            }

            return final;

        }
        else
        {
            Debug.Log("CUTSCENE ERROR");
            return null;
        }
    }
}
