using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CutsceneEvent : MonoBehaviour
{
    public DialogueManager dm;

    public void deleteEvent()
    {
        dm.cutsceneEventsToDelete.Remove(gameObject);
        GameObject.Destroy(gameObject);
    }

    public virtual void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        dm = manager;
        if (waitOnText)
        {
            dm.eventSignalToContinue = true;
        }
        else
        {
            dm.continueCutscene();
        }
    }
}
