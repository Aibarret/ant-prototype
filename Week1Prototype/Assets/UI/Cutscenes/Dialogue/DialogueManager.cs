using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{
    const int DEFAULT_TEXTSPEED = 25;

    [Header("Object & Component Refs")]
    public GameObject textObject;
    public GameObject dialogueBox;
    public GameObject speakerObject;
    public GameObject fadeObject;
    
    [Header("The script for all cutscenes")]
    public CutsceneScript script;

    [Header("Settings")]
    public bool dialogueOn;
    public bool paused;
    public List<string> debugDialogue = new List<string>();
    public List<string> cutsceneTotal = new List<string>();
    public float clickCooldownDuration;
    public int textFillDuration;
    public int textSpeed;
    public float fadeDuration;
    [HideInInspector] public List<GameObject> cutsceneEventsToDelete = new List<GameObject>();

    private bool isMovingBox = false;
    public bool isFillingText = false;
    private bool clickToContinue = false;
    private bool eventClickToContinue = false;
    [HideInInspector] public bool eventSignalToContinue = false;

    [Header("Highlight Colors")]
    [SerializeField] private Color queenHighlight;

    private GameObject eventContainer;
    private TMPro.TextMeshProUGUI dialogueText;
    private Animator dialogueTween;
    private Animator speakerTween;
    private Animator fadeTween;
    private float clickCooldownTimer = 0f;
    private List<string> activeCutscene = new List<string>();
    private int cutsceneIndex = 0;

    private int tagIndexMod = 0;
    private List<string> fillTags;
    private List<int> tagIndexes;
    private float fillTextTimer;
    private string fillText;
    private string fillTo;

    private Vector3 lastDialoguePosn;

    private bool test = true;

    private bool isPlayerInputFirstFrame;
    private bool readValueNot0;
    private void Update()
    {
        if (!paused)
        {
            if (dialogueOn)
            {
                if((LevelManager.instance.playerSelect.ReadValue<float>() > 0 || LevelManager.instance.useFireSpray.ReadValue<float>() > 0)  
                    && isPlayerInputFirstFrame)
                {
                    isPlayerInputFirstFrame = false;
                }
                else if((LevelManager.instance.playerSelect.ReadValue<float>() > 0 || LevelManager.instance.useFireSpray.ReadValue<float>() > 0) 
                    && !readValueNot0)
                {
                    isPlayerInputFirstFrame = true;
                    readValueNot0 = true; ;
                }
        
                if(LevelManager.instance.playerSelect.ReadValue<float>() == 0 || LevelManager.instance.useFireSpray.ReadValue<float>() > 0)
                {
                    readValueNot0 = false;
                }
        
                // this timer stops the player from entering a cutscene like the moment they go to click and skipping dialogue
                if (clickCooldownTimer < clickCooldownDuration)
                {
                    clickCooldownTimer += Time.unscaledDeltaTime;
                }

                // This is by far the dumbest solution to a problem I have ever encountered.
                if (lastDialoguePosn != transform.position)
                {
                    isMovingBox = true;
                    lastDialoguePosn = transform.position;
                }
                else
                {
                    isMovingBox = false;
                }


                if (!isMovingBox)
                {
                    // Text Scroll
                    if (isFillingText)
                    {
                        if (fillTextTimer * textSpeed / fillTo.Length <= 1)
                        {
                            fillTextTimer += Time.unscaledDeltaTime;
                            int indexToCutAt = (int)Mathf.Lerp(0, fillTo.Length, fillTextTimer * textSpeed / fillTo.Length);
                            fillText = fillTo.Substring(0, indexToCutAt);
                            if (tagIndexes != null && fillTags != null)
                            {
                                int count = 0;
                                foreach(int index in tagIndexes)
                                {
                                    if (fillText.Length > index)
                                    {
                                        fillText = fillText.Insert(index, fillTags[count]);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    count++;
                                }
                            }
                            changeText(fillText);
                        }
                        else
                        {
                            isFillingText = false;
                        }
                    }

                    // Input controls
                    if ((LevelManager.instance.playerSelect.WasPressedThisFrame() || LevelManager.instance.useFireSpray.WasPressedThisFrame()) 
                        && clickCooldownTimer >= clickCooldownDuration)
                    {
                        if (isFillingText)
                        {
                            isFillingText = false;
                            fillText = fillTo;
                            int count = 0;
                            foreach (int index in tagIndexes)
                            {
                                if (fillText.Length > index)
                                {
                                    fillText = fillText.Insert(index, fillTags[count]);
                                }
                                else
                                {
                                    break;
                                }
                                count++;
                            }
                            changeText(fillText);
                        }
                        else if (clickToContinue)
                        {
                            clickToContinue = false;
                            continueCutscene();
                        }
                        else if (eventClickToContinue && eventSignalToContinue)
                        {
                            eventClickToContinue = false;
                            eventSignalToContinue = false;
                            continueCutscene();
                        }
                    }
                }

            }

        }


       
    }

    private void Awake()
    {
        eventContainer = GameObject.Find("CutsceneEventContainer");
        dialogueText = textObject.GetComponent<TMPro.TextMeshProUGUI>();
        dialogueTween = dialogueBox.GetComponent<Animator>();
        speakerTween = speakerObject.GetComponent<Animator>();
        fadeTween = fadeObject.GetComponent<Animator>();
    }

    private void Start()
    {
        //dialogueTween.Play("Open");
        //dialogueTween.SetTrigger("TrigOpen");
        //loadCutscene(0);
    }
    
    public void togglePause(bool isPause)
    {
        paused = isPause;
    }

    public void loadCutscene(int cutsceneIndex)
    {
        if (debugDialogue.Count > 0)
        {
            activeCutscene = debugDialogue;
        }
        else
        {
            activeCutscene = script.getLines()[cutsceneIndex];
            cutsceneTotal = script.getLines()[cutsceneIndex];
            if (cutsceneIndex == 34 || cutsceneIndex == 35)
            {
                print(cutsceneTotal[0]);
            }
        }

        // Enter cutscene mode
        GlobalVariables.toggleGamePause(true);
        GlobalVariables.camController.toggleCutsceneMode(true);
        cutsceneIndex = 0;
        clickCooldownTimer = 0f;

        if (activeCutscene.Count > 0)
        {
            parseCutscene(activeCutscene[0]);

        }
        else
        {
            print("ATTEMPTED TO PARSE AN EMPTY CUTSCENE LIST");
        }

        dialogueOn = true;
    }

    public void parseCutscene(string cutsceneElement)
    {
        cutsceneElement = cutsceneElement.Replace("</QUEEN>", "<color=#" + ColorUtility.ToHtmlStringRGB(dialogueText.color) + ">");
        cutsceneElement = cutsceneElement.Replace("<QUEEN>", "<color=#" + ColorUtility.ToHtmlStringRGB(queenHighlight) + ">");
        string[] cutscene = cutsceneElement.Split("|");

        switch (cutscene[0])
        {
            case "t":
                //print("Playing Text " + GlobalVariables.tutorialDrawing);
                readLine(cutscene[1]);
                break;
            case "e":
               // print("Playing Event " + GlobalVariables.tutorialDrawing);
                playEvent(cutscene[1]);
                break;
            case "et":
                //print("Playing Event Text " + GlobalVariables.tutorialDrawing);
                playEvent(cutscene[1], true);
                readLine(cutscene[2], true);
                break;
            default:
                continueCutscene();
                break;
        }
    }

    public void readLine(string text, bool isPlayingEvent = false)
    {

        string[] line = text.Split("\\");
        string speech;
        if (line.Length > 1)
        {
            speech = line[0];
            if (int.TryParse(line[1], out int speed))
            {
                textSpeed = speed;
            }
            else
            {
                print("DIALOGUE ERROR: COULD NOT PARSE GIVEN TEXTSPEED AT: " + text);
            }
        }
        else
        {
            speech = text;
        }

        if (checkState("Close") || checkState("Base"))
        {
            toggleDialogueWindow(true);
        }

        List<int> startIndexes = new List<int>();
        //List<int> endIndexes = new List<int>();
        List<string> storedTags = new List<string>();
        int differenceMod = 0;
        while (speech.Contains('<'))
        {
            int startIndex = speech.IndexOf('<');
            int count = startIndex;
            while (true)
            {
                if (speech[count] != '>')
                {
                    count++;
                }
                else
                {
                    startIndexes.Add(startIndex + differenceMod);
                    //endIndexes.Add(count + differenceMod);
                    differenceMod += count - startIndex + 1;
                    storedTags.Add(speech.Substring(startIndex, count - startIndex + 1));
                    speech = speech.Remove(startIndex, count - startIndex + 1);
                    break;
                }
            }
        }

        fillTags = storedTags;
        tagIndexes = startIndexes;
        fillText = "";
        fillTo = speech;
        isFillingText = true;

        if (!isPlayingEvent)
        {
            clickToContinue = true;
        }
        
    }

    public void playEvent(string eventName, bool waitOnText = false)
    {
        GameObject cutsceneEvent = GameObject.Instantiate(Resources.Load(eventName) as GameObject);
        if (eventContainer)
        {
            cutsceneEvent.transform.parent = eventContainer.transform;
        }
        cutsceneEventsToDelete.Add(cutsceneEvent);
        cutsceneEvent.GetComponent<CutsceneEvent>().playEvent(this, waitOnText);
        eventClickToContinue = waitOnText;
    }

    public void changeText(string text)
    {
        dialogueText.text = text;
        
    }

    public void continueCutscene()
    {
        cutsceneIndex++;
        if (cutsceneIndex < activeCutscene.Count)
        {
            parseCutscene(activeCutscene[cutsceneIndex]);
        }
        else
        {
            endCutscene();
        }

        fillTextTimer = 0;
        //changeText("");
        
        //GlobalVariables.camController.toggleCutsceneMode(false);
        //gameObject.SetActive(false);
        //clickCooldownTimer = 0;
    }

    public void endCutscene()
    {
        while (cutsceneEventsToDelete.Count > 0)
        {
            GameObject.Destroy(cutsceneEventsToDelete[0]);
            cutsceneEventsToDelete.RemoveAt(0);
        }
        cutsceneIndex = 0;
        toggleDialogueWindow(false);
        dialogueOn = false;
        clickToContinue = false;
        clickCooldownTimer = 0;
        GlobalVariables.drawingManager.waitForDrawButtonRelease = true;
        GlobalVariables.toggleGamePause(false);
        GlobalVariables.camController.toggleCutsceneMode(false);
    }

    public void fadeOut()
    {
        fadeTween.SetTrigger("FadeOut");
    }

    public void dimScreen()
    {
        fadeTween.SetTrigger("FadeToDim");
    }

    public void fadeIn()
    {
        fadeTween.SetTrigger("FadeIn");
    }

    public void toggleHighOpenWindow(bool active)
    {
        if (active)
        {
            dialogueTween.SetTrigger("TrigHighOpen");
        }
        else
        {
            dialogueTween.SetTrigger("TrigOpen");
        }
    }

    public void toggleDialogueWindow(bool open)
    {
        if (open)
        {
            dialogueTween.SetTrigger("TrigOpen");
            speakerTween.SetTrigger("TrigOpen");
        }
        else
        {
            dialogueTween.SetTrigger("TrigClose");
            speakerTween.SetTrigger("TrigClose");
        }
    }

    public bool checkState(string stateName)
    {
        return dialogueTween.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
}
