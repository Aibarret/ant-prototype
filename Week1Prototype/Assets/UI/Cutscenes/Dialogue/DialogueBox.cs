using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour
{
    [HideInInspector]
    public TMPro.TextMeshProUGUI dialogueText;
    public GameObject textObject;

    public float elapsedFrames = 0f;

    private void Update()
    {
        if (isActiveAndEnabled)
        {
            elapsedFrames += Time.unscaledTime;

            if (Input.GetMouseButtonDown(0) && elapsedFrames > 1)
            {
                onOkay();
            }
        }
    }

    private void Awake()
    {
        dialogueText = textObject.GetComponent<TMPro.TextMeshProUGUI>();
    }

    public void changeText(string text)
    {
        dialogueText.text = text;
    }

    public void onOkay()
    {
        GlobalVariables.camController.toggleCutsceneMode(false);
        gameObject.SetActive(false);
        elapsedFrames = 0;
    }
}
