using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WASDdemonstration : CutsceneEvent
{
    public float cameraMoveDuration;
    public override void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        GlobalVariables.camController.sendCutsceneCamToPoint(transform.position, cameraMoveDuration, camMoveDone);

        base.playEvent(manager, waitOnText);
    }

    public void camMoveDone()
    {
        GlobalVariables.drawingManager.paused = false;
    }
}
