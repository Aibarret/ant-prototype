using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamToLocation : CutsceneEvent
{
    public float cameraMoveDuration;
    public override void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        print("Calling move camera");
        GlobalVariables.camController.sendCutsceneCamToPoint(transform.position, cameraMoveDuration, camMoveDone);

        base.playEvent(manager, waitOnText);
    }

    public void camMoveDone()
    {
        GlobalVariables.drawingManager.paused = false;
    }
}
