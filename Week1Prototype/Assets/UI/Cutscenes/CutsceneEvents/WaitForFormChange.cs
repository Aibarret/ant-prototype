using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForFormChange : CutsceneEvent
{
    public string formationToLookFor;

    private void Start()
    {
        print("AHHHHHHHHHHHHHHHHHHHHHH");
    }

    public override void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        print("Waiting for wall units to be selected");
        dm = manager;

        dm.toggleHighOpenWindow(true);
        GlobalVariables.drawingManager.formationChangeCallList += formationChanged;
        GlobalVariables.drawingManager.listeningForFormChange = true;
        print("Cutscene thing is happening");
    }

    public void formationChanged(Formation form)
    {
        if (form.unitPrefab.Equals(formationToLookFor))
        {
            GlobalVariables.drawingManager.listeningForFormChange = false;
            GlobalVariables.drawingManager.formationChangeCallList -= formationChanged;
            //dm.eventSignalToContinue = true;
            dm.toggleHighOpenWindow(false);
            dm.continueCutscene();
        }
    }
}
