using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : CutsceneEvent
{
    public override void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        dm = manager;
        StartCoroutine(play(waitOnText));
    }

    public IEnumerator play(bool waitOnText)
    {
        dm.fadeOut();

        if (waitOnText)
        {
            dm.eventSignalToContinue = true;
        }
        else
        {
            dm.continueCutscene();
        }

        yield return new WaitForSecondsRealtime(0f);
    }
}
