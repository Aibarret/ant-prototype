using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawScriptNode : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public CircleCollider2D circleCollider;

    [HideInInspector] public bool isActive = false;

    private void Update()
    {
        if (!isActive)
        {
            foreach (Collider2D coll in Physics2D.OverlapCircleAll(transform.position, transform.localScale.x * .5f, LayerMask.GetMask("Cursor")))
            {
                if (coll.tag.Equals("FormationUnit"))
                {
                    isActive = true;
                    spriteRenderer.color = Color.green;
                    transform.GetComponentInParent<DrawShape>().nodeSetActive();
                }
            }  
        }
    }


}
