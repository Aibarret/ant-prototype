using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawShape : CutsceneEvent
{
    public float cameraMoveDuration;
    
    private bool waitOnText;
    private bool isFinsihed;

    public override void playEvent(DialogueManager manager, bool waitOnText = false)
    {
        dm = manager;
        this.waitOnText = waitOnText;
        GlobalVariables.camController.sendCutsceneCamToPoint(transform.position, cameraMoveDuration, camMoveDone);
    }

   public void camMoveDone()
    {
        if (!isFinsihed)
        {
            GlobalVariables.tutorialDrawing = true;

        }
    }

    public void nodeSetActive()
    {
        int count = 0;
        foreach (Transform drawShapeNode in transform)
        {
            if (drawShapeNode.GetComponent<DrawScriptNode>().isActive)
            {
                count++;
            }
        }

        if (count == transform.childCount && ! isFinsihed)
        {
            isFinsihed = true;
            endEvent();
        }
    }

    public void endEvent()
    {
        GlobalVariables.tutorialDrawing = false;

        if (waitOnText && dm.isFillingText)
        {
            dm.eventSignalToContinue = true;
        }
        else
        {
            dm.continueCutscene();
        }
    }

   
}
