using SceneTransition;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public sealed class LevelSelect : MonoBehaviour
{
    public enum LevelType
    {
        Tutorial,
        Park,
        City,
    }

    public LevelType currentLevelContainer;
    public GameObject activeLevelContainer;
    private int currentContainerIndex = 1;
    public TextMeshProUGUI LevelTypeLabel;
    public GameObject tutorialContainer;
    public GameObject parkContainer;
    public GameObject cityContainer;
    public LevelData[] levels;
    public LevelData currentLevel;
    public Dictionary<GameObject, string> levelDictionary = new Dictionary<GameObject, string>();
    public Image LevelPicture;
    //private LevelManager levelManager;
    public TextMeshProUGUI LevelDescription;

    void Start()
    {

        UpdateAllLevelData();

        currentLevelContainer = LevelType.Park;
        UpdateCurrentLevelContainer(levels[3], LevelType.Park);

        PopulateLevelGrid();

        foreach (LevelData level in levels) {
            if (!LevelManager.instance.levelScoresToBeat.ContainsKey(level.sceneName))
            {
                //print("Adding " + level.sceneName + " to high score list");
                LevelManager.instance.levelScoresToBeat.Add(level.sceneName, level.highScore);
                
            }
            /*if (!LevelManager.instance.levelHighScores.ContainsKey(level.sceneName))
            {
                LevelManager.instance.levelHighScores.Add(level.sceneName, level.score);
            }*/
        }
    }

    void Update() {

        UpdateAllLevelData();
        UpdateButtonData();
    }

    void UpdateCurrentLevelContainer(LevelData level, LevelType type) {
        onLevelButtonPressed(level);

        switch (type)
        {
            case LevelType.Tutorial:
                LevelTypeLabel.text = "Tutorial";
                tutorialContainer.transform.position = activeLevelContainer.transform.position;
                parkContainer.transform.position = new Vector3(5000, 5000, 1000);
                cityContainer.transform.position = new Vector3(5000, 5000, 1000);
                break;
            case LevelType.Park:
                LevelTypeLabel.text = "Park";
                parkContainer.transform.position = activeLevelContainer.transform.position;
                tutorialContainer.transform.position = new Vector3(5000, 5000, 1000);
                cityContainer.transform.position = new Vector3(5000, 5000, 1000);
                break;
            case LevelType.City:
                LevelTypeLabel.text = "City";
                cityContainer.transform.position = activeLevelContainer.transform.position;
                tutorialContainer.transform.position = new Vector3(5000, 5000, 1000);
                parkContainer.transform.position = new Vector3(5000, 5000, 1000);
                break;
            default:
                break;
        }
    }

    public void Select()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        if (currentLevel.locked) { return; }
        
        if (int.TryParse(currentLevel.buildPath, out int result)) {
            SceneTransiter.Instance.TransitSceneN(result);
        }
        SFXManager.Instance.stopMusic();
        SceneTransiter.Instance.TransitScene(currentLevel.buildPath);
    }

    public void UpdateLevelData() {
        currentLevel.updateFlags(LevelManager.instance);

        string description = "";

        description += currentLevel.displayName + "\n";
        //If the player has yet to beat the highscore, show it versus their score
        if(currentLevel.score < currentLevel.highScore)
        {
            description += "Your Score: " + currentLevel.score + "\n";
            description += "High Score: " + currentLevel.highScore + "\n";
        }
        //If they have, show their highest score
        else
        {
            description += "Best Score: " + currentLevel.score + "\n";
        }
        //description += "Locked: " + currentLevel.locked + "\n";
        //description += "Completed: " + currentLevel.completed + "\n";
        description += "Description: " + currentLevel.description + "\n";

        LevelDescription.text = description;

        //Update picture here
        LevelPicture.sprite = currentLevel.levelPicture;
    }

    public void UpdateAllLevelData() {
        foreach (var level in levels)
        {
            level.updateFlags(LevelManager.instance);
        }
    }

    public void UpdateButtonData() {
        foreach (GameObject button in levelDictionary.Keys) {
            Button levelEntryButton = button.GetComponent<Button>();
            LevelData level = FindLevel(levelDictionary[button]);
            levelEntryButton.interactable = !level.locked;
            levelEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = level.locked ? "Locked" : level.displayName;
        }
    }

    public void PopulateLevelGrid() {

        // Clear the grid
        foreach (GameObject button in levelDictionary.Keys) {
            Destroy(button);
        }

        foreach (var level in levels)
        {
            GameObject levelContainer = null;
            switch (level.type)
            {
                case LevelData.LevelType.Tutorial:
                    levelContainer = tutorialContainer;
                    break;
                case LevelData.LevelType.Park:
                    levelContainer = parkContainer;
                    break;
                case LevelData.LevelType.City:
                    levelContainer = cityContainer;
                    break;
                default:
                    break;
            }

            GameObject levelEntry = Instantiate(Resources.Load<GameObject>("LevelEntryButton"), levelContainer.transform);

            Button levelEntryButton = levelEntry.GetComponent<Button>();
            //Debug.Log(level.name + ": " + level.locked);
            levelEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = level.locked ? "Locked" : level.displayName;
            levelEntryButton.onClick.AddListener(delegate { onLevelButtonPressed(level); });
            levelEntryButton.interactable = !level.locked;
            levelDictionary.Add(levelEntry, level.name);
        }
    }

    private LevelData FindLevel(string name) {
        foreach (var level in levels)
        {
            if (level.name == name) {
                return level;
            }
        }
        return null;
    }

    public void onLevelButtonPressed(LevelData level) {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        currentLevel = level;
        UpdateLevelData();
    }

    public void onNextContainerButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        currentContainerIndex++;
        if (currentContainerIndex >= 3) {
            currentContainerIndex = 0;
        }

        switch (currentContainerIndex)
        {
            case 0:
                UpdateCurrentLevelContainer(levels[0], LevelType.Tutorial);
                break;
            case 1:
                UpdateCurrentLevelContainer(levels[3], LevelType.Park);
                break;
            case 2:
                try
                {
                    UpdateCurrentLevelContainer(levels[8], LevelType.City);
                }
                catch (System.Exception)
                {
                    Debug.LogError("City levels not found, have they been added to the levels array?");
                }
                break;
            default:
                break;
        }
    }

    public void onPreviousContainerButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        currentContainerIndex--;
        if (currentContainerIndex < 0) {
            currentContainerIndex = 2;
        }

        switch (currentContainerIndex)
        {
            case 0:
                UpdateCurrentLevelContainer(levels[0], LevelType.Tutorial);
                break;
            case 1:
                UpdateCurrentLevelContainer(levels[3], LevelType.Park);
                break;
            case 2:
                try
                {
                    UpdateCurrentLevelContainer(levels[8], LevelType.City);
                }
                catch (System.Exception)
                {
                    Debug.LogError("City levels not found, have they been added to the levels array?");
                }
                break;
            default:
                break;
        }
    }

    public void OnEnable()
    {
        UpdateLevelData();

        UpdateCurrentLevelContainer(levels[3], LevelType.Park);
    }
}

[System.Serializable]
public class LevelData {
    [Header("This is what will be displayed in the level select menu")]
    public string displayName;
    [Header("This is the name of the level in file")]
    public string name;
    [Header("Type of level (tutorial, park, city, lab)")]
    public LevelType type;
    [Header("Both number and path work")]
    public string buildPath;
    [Header("Name of the scene to load")]
    public string sceneName;
    [Header("Name of the check(s) required to unlock this level")]
    public string[] requiredChecks;
    [Header("Image to display in the level select menu")]
    public Sprite levelPicture;
    public int score; // personal best
    public int highScore; // score to beat
    public bool locked;
    public bool completed;
    [TextAreaAttribute]
    public string description;
    public bool[] objectives; // maybe?

    public enum LevelType {
        Tutorial,
        Park,
        City,
    }

    public LevelData(string displayName, string name, LevelType type, string buildPath, string sceneName, string[] requiredChecks, int score, int highScore, bool locked, bool completed, string description, bool[] objectives) {
        this.displayName = displayName;
        this.name = name;
        this.type = type;
        this.buildPath = buildPath;
        this.sceneName = sceneName;
        this.requiredChecks = requiredChecks;
        this.score = score;
        this.highScore = highScore;
        this.locked = locked;
        this.completed = completed;
        this.description = description;
        this.objectives = objectives;
    }

    public void updateFlags(LevelManager levelManager) 
    {
        // Update the flags
        bool allRequiredChecksMet = true;
        foreach (var item in requiredChecks)
        {
            if (!levelManager.ReturnSingleCheck(item)) {
                allRequiredChecksMet = false;
                break;
            }
        }
        locked = !allRequiredChecksMet;

        // Update the score
        if (levelManager.levelHighScores.ContainsKey(sceneName))
        {
            score = levelManager.levelHighScores[sceneName];
        }
        else
        {
            score = 0;
            //levelManager.levelHighScores.Add(sceneName, 0);
        }
        // Update the high score
        // I don't think we need to do anything with this?

        // Update the completed flag

        if (name == "") {
            return;
        }

        completed = levelManager.ReturnSingleCheck("hasBeatenLevel" + name);
    }
}
