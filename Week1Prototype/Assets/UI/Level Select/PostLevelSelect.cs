using SceneTransition;
using UnityEngine;

public sealed class PostLevelSelect : MonoBehaviour
{
    public GameObject preLevelMenu;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    public void Back()
    {
        SFXManager.Instance.stopMusic();
        SceneTransiter.Instance.TransitScene("LevelSelect");
    }

    public void StartGame()
    {
        preLevelMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
