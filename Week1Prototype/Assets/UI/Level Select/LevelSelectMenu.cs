using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelSelectMenu : MonoBehaviour
{

    public Sprite[] levelSprites;
    public string[] levelNames;
    public bool[] unlockedLevels;
    public int currentLevel = 0;
    public GameObject gamePreviewImage;
    public GameObject lockedIcon;
    public TextMeshProUGUI levelNameText;

    // Start is called before the first frame update
    void Start()
    {
        unlockedLevels[0] = true;
        ChangeLevel(currentLevel);

        // replace this to populate unlockedLevels from a save file
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NextLevel() { 
        currentLevel++;
        ChangeLevel(currentLevel);
    }

    public void PreviousLevel() {
        currentLevel--;
        ChangeLevel(currentLevel);
    }

    public void PlayLevel() {

        if (unlockedLevels[currentLevel]) {
            //SceneManager.LoadScene(levelNames[currentLevel]);
        } else {
            Debug.Log("Level is locked");
        }
    }

    public void ChangeLevel(int currentLevelIndex) {

        if (currentLevelIndex < 0) {
            currentLevelIndex = levelSprites.Length - 1;
        } else if (currentLevelIndex >= levelSprites.Length) {
            currentLevelIndex = 0;
        }

        currentLevel = currentLevelIndex;
        gamePreviewImage.GetComponent<Image>().sprite = levelSprites[currentLevel];

        if (unlockedLevels[currentLevel]) {
            levelNameText.text = "";
        } else {
            levelNameText.text = "Locked!";
        }
        
    }
}
