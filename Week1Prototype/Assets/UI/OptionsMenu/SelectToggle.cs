using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectToggle : Toggle
{
    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        transform.localScale = new Vector3(1, 1, 1);

    }
}
