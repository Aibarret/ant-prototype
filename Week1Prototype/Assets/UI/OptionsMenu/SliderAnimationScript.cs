using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SliderAnimationScript : MonoBehaviour
{
    public Slider slider;
    public GameObject handle;
    //private float smooth = 15f;
    private float prevSliderValue = .5f;
    private float currentSliderValue = .5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentSliderValue = slider.value;
        if(currentSliderValue > prevSliderValue)
        {
            handle.transform.rotation = Quaternion.Euler(0, 0, -30f);
        }
        else if(prevSliderValue > currentSliderValue)
        {
            handle.transform.rotation = Quaternion.Euler(0, 0, 30f);
        }
        else
        {
            handle.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        prevSliderValue = currentSliderValue;
    }
}
