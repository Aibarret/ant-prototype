using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectSlider : Slider
{
    private bool isSelected;
    private float increment = .1f;

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        isSelected = true;
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        transform.localScale = new Vector3(1, 1, 1);
        isSelected = false;
    }

    private void Update()
    {
        base.Update();
        if (isSelected && LevelManager.instance.selectLeft.WasPressedThisFrame())
        {
            value -= increment;
        }
        else if (isSelected && LevelManager.instance.selectRight.WasPressedThisFrame())
        {
            value += increment;
        }
    }

    public override Selectable FindSelectableOnLeft()
    {
        return base.FindSelectableOnLeft();
    }

    public override Selectable FindSelectableOnRight()
    {
        return base.FindSelectableOnRight();
    }


}
