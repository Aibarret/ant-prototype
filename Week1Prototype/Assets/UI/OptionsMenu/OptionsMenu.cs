using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.InputSystem;
using System;
using JetBrains.Annotations;
using UnityEngine.EventSystems;
using SceneTransition;

public class OptionsMenu : MonoBehaviour, IDataPersistence
{
    //private LevelManager LevelManager.instance = LevelManager.instance;
    public GameObject defaultSelectedObject;
    public bool isPaused = false;
    public bool isGameAlreadyPaused = false;
    public int screenWidth;
    public int screenHeight;
    public Slider slider;
    public Slider sfxSlider;
    public Slider cameraSpeedSlider;
    public Slider masterVolumeSlider;
    public AudioSource source;

    [Header("Easy Mode variables")]
    public float easyModeGameTime = 0.8f;
    public bool isEasyMode = false;

    [Header("Cursor variables")]
    public Slider cursorSpeedSlider;
    public float cursorSpeed = 1f;

    [Header("Volume variables")]
    public bool showSoundOptions = true;

    FMOD.Studio.VCA vcaController;
    public string VcaName;
    public float vcaVolume;

    public float musicVolume = 1f;
    public float sfxVolume = 1f;
    public float masterVolume = 1f;
    public float musicVolumeCap = 10f;
    public float sfxVolumeCap = 10f;
    public float masterVolumeCap = 10f;

    [Header("Resolution variables")]
    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;
    public Vector2[] resolutions;
    public bool isFullscreen;
    public SelectToggle fullscreenToggle;
    public SelectToggle vSyncToggle;
    public SelectToggle limitDrawingToggle;

    [Header("VSync variables")]
    public bool isVSync = false;

    [Header("Options Menu Animator")]
    public Animator optionsMenuAnimator;

    [Header("Almanac variables")]
    public Animator almanacAnimator;
    public bool isShowingAlmanac = false;

    [Header("Rebind References")]
    public GameObject controllerView;
    public GameObject keyboardView;
    public Transform keyboardButtons;
    public Transform controllerButtons;

    private bool editingControllerInputs = false;

    // Start is called before the first frame update
    void Start()
    {
        // Set up input system
        //LevelManager.instance.pauseButton.performed += OnPauseButton;
        //LevelManager.instance.exitButton.performed += OnExitButton;
        LevelManager.instance.debugButton.performed += OnDebug;

        //get screen resolution
        resolutions = new Vector2[4];
        resolutions[0] = new Vector2(1280, 720);
        resolutions[1] = new Vector2(1920, 1080);
        resolutions[2] = new Vector2(2560, 1440);
        resolutions[3] = new Vector2(3840, 2160);

        //set resolution dropdown to current resolution
        /*for (int i = 0; i < resolutions.Length; i++)
        {
            if (Screen.width == (int)resolutions[i].x && Screen.height == (int)resolutions[i].y)
            {
                resolutionDropdown.value = i;
            }
        }*/

        // Set video settings
        resolutionDropdown.value = LevelManager.instance.resolution;
        qualityDropdown.value = LevelManager.instance.quality;
        isFullscreen = LevelManager.instance.fullscreen;
        fullscreenToggle.isOn = !isFullscreen;
        vSyncToggle.isOn = LevelManager.instance.vSync;
        limitDrawingToggle.isOn = LevelManager.instance.isUsingLimitDrawing;

        Screen.fullScreen = isFullscreen;

        almanacAnimator = GameObject.Find("EnemyAlmanac").GetComponent<Animator>();

        // Set up sliders
        Slider slider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        //slider.onValueChanged.AddListener(delegate { OnSliderChanged(slider.value); });
        slider.value = LevelManager.instance.musicVolume;

        slider = GameObject.Find("SFXSlider").GetComponent<Slider>();
        //slider.onValueChanged.AddListener(delegate { OnSFXSliderChanged(slider.value); });
        slider.value = LevelManager.instance.sfxVolume;

        // something's wrong with the master volume I'm ignoring it for now
        slider = GameObject.Find("MasterVolumeSlider").GetComponent<Slider>();
        //slider.onValueChanged.AddListener(delegate { OnMasterVolumeSliderChanged(slider.value); });
        slider.value = LevelManager.instance.masterVolume;

        slider = GameObject.Find("CameraSpeedSlider").GetComponent<Slider>();
        slider.value = LevelManager.instance.cameraSpeed / 40f;
        slider = GameObject.Find("CursorSpeedSlider").GetComponent<Slider>();
        slider.value = LevelManager.instance.cursorSpeed / 30f;
    }

    private void Update()
    {
        //print(LevelManager.instance.moveAction.enabled);
        if (LevelManager.instance.pauseButton.WasPressedThisFrame())
        {
            OnPauseButtonPressed();
        }

        if (LevelManager.instance.exitButton.WasPressedThisFrame())
        {
            OnExitButtonPressed();
        }
    }

    public Formation[] GetValidFormationUnits() {
        // Returns a list of all the formation units that have been unlocked
        // hacky way to adapt this shit
        bool hasUnlockedUnitBase = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitBase");
        bool hasUnlockedUnitWall = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitWall");
        bool hasUnlockedUnitCracker = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitCracker");
        bool hasUnlockedUnitCharger = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitCharger");
        bool hasUnlockedUnitLance = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitLance");
        bool hasUnlockedUnitMoving = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitMoving");
        bool hasUnlockedUnitBumper = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitBumper");
        bool hasUnlockedUnitPatrol = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitPatrol");
        
        bool hasUnlockedUnitElectric = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitElectric");
        bool hasUnlockedUnitBoost = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitBoost");
        bool hasUnlockedUnitToy = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitToy");
        bool hasUnlockedUnitPharaoh = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitPharaoh");
        bool hasUnlockedUnitSuper = LevelManager.instance.ReturnSingleCheck("hasUnlockedUnitSuper");

        bool[] formationUnitsUnlocked = {hasUnlockedUnitBase, hasUnlockedUnitWall, hasUnlockedUnitCracker, hasUnlockedUnitCharger, hasUnlockedUnitLance, hasUnlockedUnitMoving, hasUnlockedUnitBumper, hasUnlockedUnitPatrol, hasUnlockedUnitElectric, hasUnlockedUnitBoost, hasUnlockedUnitToy, hasUnlockedUnitPharaoh, hasUnlockedUnitSuper};
        string[] formationUnitNames = {"Base", "Wall", "Cracker", "Charger", "Lance", "Moving", "Bumper", "Patrol", "Electric", "Boost", "Toy", "Pharaoh", "Super"};
        List<Formation> validFormationUnits = new List<Formation>();

        for (int i = 0; i < formationUnitsUnlocked.Length; i++) {
            if (formationUnitsUnlocked[i]) {
                if (formationUnitNames[i] == "Toy")
                {
                    continue;
                }
                Formation form = null;
                //validFormationUnits.Add(new BaseForm())
                //print("Adding availiable unit " + formationUnitNames[i]);
                // This is so dumb but it should work
                switch (formationUnitNames[i])
                {
                    case "Base":
                        form = new BaseForm();
                        break;
                    case "Wall":
                        form = new WallForm();
                        break;
                    case "Cracker":
                        form = new CrackerForm();
                        break;
                    case "Charger":
                        form = new ChargerForm();
                        break;
                    case "Patrol":
                        form = new PatrolForm();
                        break;
                    case "Moving":
                        form = new MovingForm();
                        break;
                    case "Lance":
                        form = new LanceForm();
                        break;
                    case "Bumper":
                        form = new BumperForm();
                        break;
                    case "Electric":
                        form = new ElectricForm();
                        break;
                    case "Boost":
                        form = new BoostForm();
                        break;
                    case "Toy":
                        form = new ToyForm();
                        break;
                    case "Pharaoh":
                        form = new PharaohForm();
                        break;
                    case "Super":
                        form = new SuperForm();
                        break;
                }

                /*formRef = Instantiate<GameObject>(Resources.Load<GameObject>(formationUnitNames[i] + "FormRef") as GameObject);
                formRef.transform.parent = GlobalVariables.drawingManager.overwriteFormationList.transform;*/
                validFormationUnits.Add(form);
            }
        }
        return validFormationUnits.ToArray();
    }

    public void OnDropDownChanged(TMP_Dropdown change)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        Screen.SetResolution((int)resolutions[change.value].x, (int)resolutions[change.value].y, Screen.fullScreen);
        LevelManager.instance.resolution = change.value;
    }

    public void OnQualityDropDownChanged(TMP_Dropdown change)
    {   
        QualitySettings.SetQualityLevel(change.value);
        LevelManager.instance.quality = change.value;
    }

    public void OnMasterVolumeSliderChanged(float volume) {
        LevelManager.instance.masterVolume = volume;
        masterVolume = volume;
        LevelManager.instance.BusSystem.UpdateVolume();
    }

    //This is the one labelled "Music Volume" in the options menu
    public void OnSliderChanged(float volume) {
        LevelManager.instance.musicVolume = volume;
        musicVolume = volume;
        LevelManager.instance.BusSystem.UpdateVolume();
    }

    public void OnSFXSliderChanged(float volume) {
        LevelManager.instance.sfxVolume = volume;
        sfxVolume = volume;
        LevelManager.instance.BusSystem.UpdateVolume();
    }

    public void OnCameraSpeedSliderChanged(float speed) {
        LevelManager.instance.cameraSpeed = 40f * speed;
    }

    public void OnCursorSpeedSliderChanged(float speed) {
        LevelManager.instance.cursorSpeed = 30f * speed;
    }

    public void OnVSynctoggle(bool toggleBool) {
        isVSync = toggleBool;
        QualitySettings.vSyncCount = isVSync ? 1 : 0;
        LevelManager.instance.vSync = toggleBool;
    }


    public void OnAlmanacButtonPressed()
    {
        print("almanac button pressed");
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        isShowingAlmanac = true;
        MenuManager.Instance.SetActiveMenu(almanacAnimator.gameObject, almanacAnimator);
    }

    #region Rebind Hell

    public void switchControlType(TextMeshProUGUI buttonText)
    {
        editingControllerInputs = !editingControllerInputs;

        if (editingControllerInputs)
        {
            buttonText.text = "Controller";

            controllerView.SetActive(true);
            keyboardView.SetActive(false);
        }
        else
        {
            buttonText.text = "Keyboard\n&\nMouse";

            controllerView.SetActive(false);
            keyboardView.SetActive(true);
        }
        //EventSystem.current.SetSelectedGameObject(buttonText.transform.parent.gameObject, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject( buttonText.transform.parent.gameObject);
        loadRebindButtons();
    }

    public void loadRebindButtons(bool isFromMenu = false)
    {
        if (isFromMenu)
        {
            editingControllerInputs = false;
            controllerView.SetActive(false);
            keyboardView.SetActive(true);
        }

        Transform buttonList;

        if (editingControllerInputs)
        {
            buttonList = controllerButtons;
        }
        else
        {
            buttonList = keyboardButtons;
        }

        foreach (Button button in buttonList.GetComponentsInChildren<Button>())
        {
            InputAction value = (InputAction)LevelManager.instance.GetType().GetField(button.gameObject.name).GetValue(LevelManager.instance);

            if (value != null)
            {
                int x = 0;
                if (editingControllerInputs && value.bindings.Count > 1)
                {
                    x = 1;
                }

                button.GetComponentInChildren<TextMeshProUGUI>().text = 
                    value.bindings[x].effectivePath.ToString().Split("/")[value.bindings[x].effectivePath.ToString().Split("/").Length - 1].ToUpper();
            }
            else
            {
                print("no value found at " + button.gameObject.name);
            }
        }

        
    }

    public void OnPlaceAntRebind(TextMeshProUGUI text) {
        LevelManager.instance.playerSelect.Disable();
        var rebindOperation = LevelManager.instance.playerSelect.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.playerSelect.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.playerSelect.Enable();
        });
    }

    public void OnEraseAntRebind(TextMeshProUGUI text) {
        LevelManager.instance.playerErase.Disable();
        var rebindOperation = LevelManager.instance.playerErase.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.playerErase.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.playerErase.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.playerErase.Enable();
        });
    }

    public void PanCameraRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.playerPanCamera.Disable();
        var rebindOperation = LevelManager.instance.playerPanCamera.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.playerPanCamera.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.playerPanCamera.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.playerPanCamera.Enable();
        });
    }

    public void FireSprayRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.useFireSpray.Disable();
        var rebindOperation = LevelManager.instance.useFireSpray.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.useFireSpray.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.useFireSpray.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.useFireSpray.Enable();
        });
    }

    public void SearchButtonRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.searchButton.Disable();
        var rebindOperation = LevelManager.instance.searchButton.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.searchButton.Enable();
        });
    }

    public void CancelDrawRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.cancelDraw.Disable();
        var rebindOperation = LevelManager.instance.cancelDraw.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.cancelDraw.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.cancelDraw.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.cancelDraw.Enable();
        });
    }

    public void CenterControllerMouseRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.centerControllerMouse.Disable();
        var rebindOperation = LevelManager.instance.centerControllerMouse.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.centerControllerMouse.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.centerControllerMouse.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.centerControllerMouse.Enable();
        });
    }

    // WASD for camera movement
    public void CameraUpRebind(TextMeshProUGUI text) {
        OnCameraRebind(text, direction.up);
    }

    public void CameraDownRebind(TextMeshProUGUI text) {
        OnCameraRebind(text, direction.down);
    }

    public void CameraLeftRebind(TextMeshProUGUI text) {
        OnCameraRebind(text, direction.left);
    }

    public void CameraRightRebind(TextMeshProUGUI text) {
        OnCameraRebind(text, direction.right);
    }
    public void OnCameraRebind(TextMeshProUGUI text, direction direction) {
        switch (direction) {
            case direction.up:
                LevelManager.instance.moveAction.Disable();
                var rebindOperation = LevelManager.instance.moveAction.PerformInteractiveRebinding()
                   .WithTargetBinding(1)
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
                text.text = "Press a key...";
                rebindOperation.OnComplete(operation => {
                    text.text = operation.action.bindings[1].effectivePath.ToString();
                    LevelManager.instance.moveAction.Enable();
                });
                break;
            case direction.down:
                LevelManager.instance.moveAction.Disable();
                var rebindOperation2 = LevelManager.instance.moveAction.PerformInteractiveRebinding()
                   .WithTargetBinding(2)
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
                text.text = "Press a key...";
                rebindOperation2.OnComplete(operation => {
                    text.text = operation.action.bindings[2].effectivePath.ToString();
                    LevelManager.instance.moveAction.Enable();
                });
                break;
            case direction.left:
                LevelManager.instance.moveAction.Disable();
                var rebindOperation3 = LevelManager.instance.moveAction.PerformInteractiveRebinding()
                   .WithTargetBinding(3)
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
                text.text = "Press a key...";
                rebindOperation3.OnComplete(operation => {
                    text.text = operation.action.bindings[3].effectivePath.ToString();
                    LevelManager.instance.moveAction.Enable();
                });
                break;
            case direction.right:
                LevelManager.instance.moveAction.Disable();
                var rebindOperation4 = LevelManager.instance.moveAction.PerformInteractiveRebinding()
                   .WithTargetBinding(4)
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
                text.text = "Press a key...";
                rebindOperation4.OnComplete(operation => {
                    text.text = operation.action.bindings[4].effectivePath.ToString();
                    LevelManager.instance.moveAction.Enable();
                });
                break;
            default:
                LevelManager.instance.moveAction.Disable();
                var rebindOperation5 = LevelManager.instance.moveAction.PerformInteractiveRebinding()
                   .WithTargetBinding(1)
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
                text.text = "Press a key...";
                rebindOperation5.OnComplete(operation => {
                    text.text = operation.action.bindings[1].effectivePath.ToString();
                    LevelManager.instance.moveAction.Enable();
                });
                break;
        }
    }
    // To queen button
    public void OnToQueenRebind(TextMeshProUGUI text) {
        LevelManager.instance.toQueen.Disable();
        var rebindOperation = LevelManager.instance.toQueen.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.toQueen.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.toQueen.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.toQueen.Enable();
        });
    }

    public void OnToQueenCRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.buttonC.Disable();
        var rebindOperation = LevelManager.instance.buttonC.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.buttonC.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.buttonC.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.buttonC.Enable();
        });
    }

    public void OnToQueenXRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.buttonX.Disable();
        var rebindOperation = LevelManager.instance.buttonX.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.buttonX.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.buttonX.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.buttonX.Enable();
        });
    }

    public void OnToQueenZRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.buttonZ.Disable();
        var rebindOperation = LevelManager.instance.buttonZ.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.buttonZ.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.buttonZ.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.buttonZ.Enable();
        });
    }


    // 1-12 hotkeys
    #region 12 fucking Hotkey Rebinds

    public void OnHotKey1Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey1.Disable();
        var rebindOperation = LevelManager.instance.hotkey1.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.hotkey1.Enable();
        });
    }

    public void OnHotKey2Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey2.Disable();
        var rebindOperation = LevelManager.instance.hotkey2.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.hotkey2.Enable();
        });
    }

    public void OnHotKey3Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey3.Disable();
        var rebindOperation = LevelManager.instance.hotkey3.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.hotkey3.Enable();
        });
    }

    public void OnHotKey4Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey4.Disable();
        var rebindOperation = LevelManager.instance.hotkey4.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.hotkey4.Enable();
        });
    }

    public void OnHotKey5Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey5.Disable();
        var rebindOperation = LevelManager.instance.hotkey5.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.hotkey5.Enable();
        });
    }

    public void OnHotKey6Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey6.Disable();
        var rebindOperation = LevelManager.instance.hotkey6.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey6.Enable();
        });
    }

    public void OnHotKey7Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey7.Disable();
        var rebindOperation = LevelManager.instance.hotkey7.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey7.Enable();
        });
    }

    public void OnHotKey8Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey8.Disable();
        var rebindOperation = LevelManager.instance.hotkey8.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey8.Enable();
        });
    }

    public void OnHotKey9Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey9.Disable();
        var rebindOperation = LevelManager.instance.hotkey9.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey9.Enable();
        });
    }

    public void OnHotKey10Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey10.Disable();
        var rebindOperation = LevelManager.instance.hotkey10.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey10.Enable();
        });
    }

    public void OnHotKey11Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey11.Disable();
        var rebindOperation = LevelManager.instance.hotkey11.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey11.Enable();
        });
    }

    public void OnHotKey12Rebind(TextMeshProUGUI text) {
        LevelManager.instance.hotkey12.Disable();
        var rebindOperation = LevelManager.instance.hotkey12.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse") 
                   .OnMatchWaitForAnother(0.1f)
                   .Start(); 
        text.text = "Press a key...";
        rebindOperation.OnComplete(operation => {
            text.text = operation.action.bindings[0].effectivePath.ToString().Split("/")[1].ToUpper(); 
            LevelManager.instance.hotkey12.Enable();
        });
    }

    #endregion

    // select left and right
    // button x and button Z??
    // switch draw
    public void OnSwitchDrawRebind(TextMeshProUGUI text) {
        LevelManager.instance.switchDraw.Disable();
        var rebindOperation = LevelManager.instance.switchDraw.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                    //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.switchDraw.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.switchDraw.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.switchDraw.Enable();
        });
    }

    public void OnSwitchEraseRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.switchErase.Disable();
        var rebindOperation = LevelManager.instance.switchErase.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.switchErase.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.switchErase.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.switchErase.Enable();
        });
    }

    public void OnSwitchSearchRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.switchSearch.Disable();
        var rebindOperation = LevelManager.instance.switchSearch.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.switchSearch.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.switchSearch.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.switchSearch.Enable();
        });
    }

    public void SelectLeftRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.selectLeft.Disable();
        var rebindOperation = LevelManager.instance.selectLeft.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        if (editingControllerInputs && LevelManager.instance.selectLeft.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.selectLeft.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }
        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.selectLeft.Enable();
        });
    }

    public void SelectRightRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.selectRight.Disable();
        var rebindOperation = LevelManager.instance.selectRight.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        if (editingControllerInputs && LevelManager.instance.selectRight.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.selectRight.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }
        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.selectRight.Enable();
        });
    }

    public void OnSwapRowRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.selectSwapRow.Disable();
        var rebindOperation = LevelManager.instance.selectSwapRow.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";

        if (editingControllerInputs && LevelManager.instance.selectSwapRow.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.selectSwapRow.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }

        rebindOperation.OnComplete(operation => {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.selectSwapRow.Enable();
        });
    }

    public void PauseButtonRebind(TextMeshProUGUI text)
    {
        LevelManager.instance.pauseButton.Disable();
        var rebindOperation = LevelManager.instance.pauseButton.PerformInteractiveRebinding()
                   // To avoid accidental input from mouse motion only but allow it on press of the mouse button, we need to
                   //.WithControlsExcluding("Mouse")
                   .OnMatchWaitForAnother(0.1f)
                   .Start();
        text.text = "Press a key...";
        if (editingControllerInputs && LevelManager.instance.pauseButton.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(1);
        }
        else if (LevelManager.instance.pauseButton.bindings.Count > 1)
        {
            rebindOperation.WithTargetBinding(0);
        }
        rebindOperation.OnComplete(operation =>
        {
            int x = 0;
            if (editingControllerInputs && LevelManager.instance.playerSelect.bindings.Count > 1)
            {
                x = 1;
            }
            text.text = operation.action.bindings[x].effectivePath.ToString().Split("/")[1].ToUpper();
            LevelManager.instance.pauseButton.Enable();
        });
    }

    #endregion


    public void OnPauseButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        isPaused = !isPaused;

        if (isPaused) 
        {
            MenuManager.Instance.SetActiveMenu(gameObject, optionsMenuAnimator);
            isGameAlreadyPaused = GlobalVariables.gamePaused;
            if (GlobalVariables.ui)
            {
                if (GlobalVariables.ui.dialogueManager)
                {
                    GlobalVariables.ui.dialogueManager.togglePause(true);

                }
                if (!isGameAlreadyPaused)
                {
                    GlobalVariables.toggleGamePause(true);
                }

                GlobalVariables.drawingManager.optionsMenuOpen = true;
            }
        } 
        else 
        {
            MenuManager.Instance.SetInactiveMenu(optionsMenuAnimator);
            if (GlobalVariables.ui)
            {
                GlobalVariables.ui.dialogueManager.togglePause(false);
                if (!isGameAlreadyPaused)
                {
                    GlobalVariables.toggleGamePause(false);
                }

                GlobalVariables.drawingManager.optionsMenuOpen = false;
            }
        }

        //EventSystem.current.SetSelectedGameObject(defaultSelectedObject, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject( defaultSelectedObject);



    }

    public void OnExitButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        MenuManager.Instance.SetInactiveMenu(optionsMenuAnimator);

        isPaused = false;

        LevelManager.instance.SetDefaultSelectedObject();

        if (GlobalVariables.ui)
        {
            GlobalVariables.ui.dialogueManager.togglePause(false);
            if (!isGameAlreadyPaused)
            {
                GlobalVariables.toggleGamePause(false);
            }

        }
    }

    public void OnExitToMainMenuButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        GlobalVariables.ToLevelSelect();
        GlobalVariables.toggleGamePause(false);
        //SceneTransiter.Instance.TransitScene("MainMenu");
    }

    public void OnLevelRetry()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        GlobalVariables.toggleGamePause(false);
        LevelManager.instance.incrementDeathCounter();
        SFXManager.Instance.stopMusic();
        SceneTransiter.Instance.TransitScene(GlobalVariables.currentSceneName);
        // TODO: Add a reload level to global variables
    }

    public void ViewSwitch(GameObject nextView) {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        if (optionsMenuAnimator.GetBool("isKey") == true)
        {
            optionsMenuAnimator.SetBool("isKey", false);
        }
        else if (optionsMenuAnimator.GetBool("isKey") == false && optionsMenuAnimator.GetBool("isActive") == true)
        {
            optionsMenuAnimator.SetBool("isKey", true);
        }

        //EventSystem.current.SetSelectedGameObject(nextView, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject(nextView);
        //nextView.SetActive(nextView.activeSelf ? false : true);
    }

    private void OnDebug(InputAction.CallbackContext context) {
        LevelManager.instance.ToggleChecks(true);
    }

    private void OnEnable() {
        // Error handling for almanac animator that doesn't work
        Button almanacButton = GameObject.Find("GuideBook").GetComponent<Button>();
        if (almanacAnimator == null) {
            Debug.LogError("Almanac animator not found, making almanac button uninteractable.");
            almanacButton.interactable = false;
            almanacButton.enabled = false;
        } else {
            almanacButton.interactable = true;
            almanacButton.enabled = true;
        }
    }

    public void EasyModeToggle(bool togglebool) 
    {
        /*GlobalVariables.realSpeed = easyModeGameTime;
        isEasyMode = togglebool;*/
        LevelManager.instance.isUsingLimitDrawing = togglebool;
    }

    public void FullScreenToggle(bool togglebool) {
        isFullscreen = !togglebool;
        Screen.fullScreen = isFullscreen;

        LevelManager.instance.fullscreen = !togglebool;

        //Debug.Log("Fullscreen is now " + isFullscreen);

    }

    public void LoadData(GameData gameData)
    {
        // Difficulty settings
        this.isEasyMode = gameData.isEasyMode;
        this.vSyncToggle.isOn = gameData.vSync;

        // Sound settings
        LevelManager.instance.musicVolume = gameData.musicVolume;
        LevelManager.instance.sfxVolume = gameData.sfxVolume;
        LevelManager.instance.masterVolume = gameData.masterVolume;

        // Video Settings
        LevelManager.instance.resolution = gameData.resolution;
        LevelManager.instance.quality = gameData.quality;
        LevelManager.instance.fullscreen = gameData.fullscreen;
    }

    public void SaveData(ref GameData gameData)
    {
        // Difficulty settings
        gameData.isEasyMode = this.isEasyMode;

    // Sound settings
        gameData.musicVolume = LevelManager.instance.musicVolume;
        gameData.sfxVolume = LevelManager.instance.sfxVolume;
        gameData.masterVolume = LevelManager.instance.masterVolume;

        // Video Settings
        gameData.resolution = LevelManager.instance.resolution;
        gameData.quality = LevelManager.instance.quality;
        gameData.fullscreen = LevelManager.instance.fullscreen;
    }
}

public enum direction {
    up,
    down,
    left,
    right
}