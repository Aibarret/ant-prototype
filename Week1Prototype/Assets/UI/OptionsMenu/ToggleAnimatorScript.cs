using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAnimatorScript : MonoBehaviour
{
    public Animator toggleAnimation;

    private void Start()
    {
        toggleAnimation.SetBool("On", GetComponent<SelectToggle>().isOn);
        toggleAnimation.SetTrigger("UpdateBox");
    }

    public void toggleClick(bool value)
    {
        /*if(toggleAnimation.GetBool("On") == false)
        {
            toggleAnimation.SetBool("On", true);
        }
        else
        {
            toggleAnimation.SetBool("On", false);
     
        }*/

        toggleAnimation.SetBool("On", value);
        toggleAnimation.SetTrigger("UpdateBox");
    }
}
