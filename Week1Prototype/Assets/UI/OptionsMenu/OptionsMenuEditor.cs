using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(OptionsMenu))]
public class MyScriptEditor : Editor
{

    SerializedProperty screenWidth;
    SerializedProperty screenHeight;
    public SerializedProperty defaultSelectedObject;
    public SerializedProperty slider;
    public SerializedProperty source;
    public SerializedProperty easyModeGameTime;
    public SerializedProperty limitDrawingToggle;
    public SerializedProperty easyModeText;
    public SerializedProperty isEasyMode;
    public SerializedProperty cameraSpeedSlider;

    //Volume variables
    public SerializedProperty showSoundOptions;
    public SerializedProperty musicVolume;
    public SerializedProperty sfxVolume;
    public SerializedProperty masterVolume;
    public SerializedProperty musicVolumeCap;
    public SerializedProperty sfxVolumeCap;
    public SerializedProperty masterVolumeCap;
    public SerializedProperty sfxSlider;
    public SerializedProperty masterVolumeSlider;

    // Resolution variables
    public SerializedProperty resolutionDropdown;
    public SerializedProperty qualityDropdown;
    public SerializedProperty fullscreenToggle;
    public SerializedProperty vSyncToggle;
    public SerializedProperty resolutions;
    public SerializedProperty isFullscreen;
    public SerializedProperty fullscreenText;

    // VSync variables
    public SerializedProperty isVSync;
    public SerializedProperty vSyncText;

    // Options Menu Animator
    public SerializedProperty optionsMenuAnimator;

    // Almanac variables
    public SerializedProperty almanacAnimator;
    public SerializedProperty isShowingAlmanac;

    public SerializedProperty controllerView;
    public SerializedProperty keyboardView;
    public SerializedProperty keyboardButtons;
    public SerializedProperty controllerButtons;


    void OnEnable() {
        EditorGUIUtility.labelWidth = 200;

        // Fetch the objects from the GameObject script to display in the inspector
        defaultSelectedObject = serializedObject.FindProperty("defaultSelectedObject");
        screenWidth = serializedObject.FindProperty("screenWidth");
        screenHeight = serializedObject.FindProperty("screenHeight");
        slider = serializedObject.FindProperty("slider");
        source = serializedObject.FindProperty("source");

        // Easy Mode
        easyModeGameTime = serializedObject.FindProperty("easyModeGameTime");
        limitDrawingToggle = serializedObject.FindProperty("limitDrawingToggle");
        isEasyMode = serializedObject.FindProperty("isEasyMode");

        // Screen Settings
        cameraSpeedSlider = serializedObject.FindProperty("cameraSpeedSlider");
        resolutionDropdown = serializedObject.FindProperty("resolutionDropdown");
        qualityDropdown = serializedObject.FindProperty("qualityDropdown");
        fullscreenToggle = serializedObject.FindProperty("fullscreenToggle");

        resolutions = serializedObject.FindProperty("resolutions");
        isFullscreen = serializedObject.FindProperty("isFullscreen");
        vSyncToggle = serializedObject.FindProperty("vSyncToggle");

        // VSync
        isVSync = serializedObject.FindProperty("isVSync");

        optionsMenuAnimator = serializedObject.FindProperty("optionsMenuAnimator");
        almanacAnimator = serializedObject.FindProperty("almanacAnimator");
        isShowingAlmanac = serializedObject.FindProperty("isShowingAlmanac");

        //Volume
        showSoundOptions = serializedObject.FindProperty("showSoundOptions");

        musicVolume = serializedObject.FindProperty("musicVolume");
        sfxVolume = serializedObject.FindProperty("sfxVolume");
        masterVolume = serializedObject.FindProperty("masterVolume");
        musicVolumeCap = serializedObject.FindProperty("musicVolumeCap");
        sfxVolumeCap = serializedObject.FindProperty("sfxVolumeCap");
        masterVolumeCap = serializedObject.FindProperty("masterVolumeCap");
        sfxSlider = serializedObject.FindProperty("sfxSlider");
        masterVolumeSlider = serializedObject.FindProperty("masterVolumeSlider");
        controllerView = serializedObject.FindProperty("controllerView");
        keyboardView = serializedObject.FindProperty("keyboardView");
        keyboardButtons = serializedObject.FindProperty("keyboardButtons");
        controllerButtons = serializedObject.FindProperty("controllerButtons");
    }

    override public void OnInspectorGUI()
    {
        // fetch current values from the real instance into the serialized "clone"
        serializedObject.Update();

        // Draw fields
        EditorGUILayout.PropertyField(defaultSelectedObject);
        EditorGUILayout.PropertyField(screenWidth);
        EditorGUILayout.PropertyField(screenHeight);
        EditorGUILayout.PropertyField(slider);
        EditorGUILayout.PropertyField(source);

        // Easy Mode
        EditorGUILayout.PropertyField(easyModeGameTime);
        EditorGUILayout.PropertyField(isEasyMode);
        EditorGUILayout.PropertyField(limitDrawingToggle);

        // Screen Settings
        EditorGUILayout.PropertyField(cameraSpeedSlider);
        EditorGUILayout.PropertyField(resolutionDropdown);
        EditorGUILayout.PropertyField(qualityDropdown);
        EditorGUILayout.PropertyField(fullscreenToggle);
    EditorGUILayout.PropertyField(resolutions);
        EditorGUILayout.PropertyField(isFullscreen);
        EditorGUILayout.PropertyField(vSyncToggle);


        // VSync
        EditorGUILayout.PropertyField(isVSync);

        EditorGUILayout.PropertyField(optionsMenuAnimator);
        EditorGUILayout.PropertyField(almanacAnimator);
        EditorGUILayout.PropertyField(isShowingAlmanac);

        //Volume
        EditorGUILayout.PropertyField(showSoundOptions);
        EditorGUILayout.PropertyField(controllerView);
        EditorGUILayout.PropertyField(keyboardView);
        EditorGUILayout.PropertyField(keyboardButtons);
        EditorGUILayout.PropertyField(controllerButtons);


        if (showSoundOptions.boolValue) {
            EditorGUILayout.LabelField("Sound Options", EditorStyles.boldLabel);

            EditorGUI.indentLevel++;

            EditorGUIUtility.labelWidth = 200;

            EditorGUILayout.PropertyField(musicVolume);
            EditorGUILayout.PropertyField(sfxVolume);
            EditorGUILayout.PropertyField(masterVolume);
            EditorGUILayout.PropertyField(musicVolumeCap);
            EditorGUILayout.PropertyField(sfxVolumeCap);
            EditorGUILayout.PropertyField(masterVolumeCap);
            EditorGUILayout.PropertyField(sfxSlider);
            EditorGUILayout.PropertyField(masterVolumeSlider);

            EditorGUIUtility.labelWidth = 0;

            EditorGUI.indentLevel--;
        }

        // write back serialized values to the real instance
        // automatically handles all marking dirty and undo/redo
        serializedObject.ApplyModifiedProperties();
    }
}
#endif