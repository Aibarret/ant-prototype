using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public enum AlmanacCategory {
    UNIT,
    ENEMY,
    OBSTACLE,
    SPECIAL,
    DEFAULT
}

public enum AlmanacCategoryNumber {
    NAME,
    CATEGORY,
    PICTUREREF,
    HEALTH,
    SPEED,
    DAMAGE,
    EFFECT,
    COOLDOWN,
    KNOCKBACK,
    WEIGHT,
    RANGE,
    EXPLOSIVEAOE,
    MOVETYPE,
    SPAWNDISTANCE,
    COST,
    LINELENGTH,
    DESCRIPTION,
    APPLICABLECHECKS
}

public class EnemyAlmanac : MonoBehaviour
{

    public AlmanacCategory currentCategory = AlmanacCategory.DEFAULT;

    public Animator almanacAnimator;
    public Animator optionsMenuAnimator;

    public AlmanacData currentEntry;
    public List<Button> currentEntries;
    public GameObject currentEntryView;
    public GameObject backButton;
    public GameObject defaultBackButton;

    public string[,] rawData;

    [Header("All unsorted entries read from the TSV file")]
    [SerializeField] public List<AlmanacData> almanacEntries;
    [Header("Entries sorted by category")]
    public List<AlmanacData> unitEntries;
    public List<AlmanacData> enemyEntries;
    public List<AlmanacData> obstacleEntries;
    public List<AlmanacData> specialEntries;
    [Header("Entries that don't fit into any category")]
    [Tooltip("If your entry is in this list, check the Category column in the TSV file")]
    public List<AlmanacData> defaultEntries;

    //public TextAsset textAssetData; // Used in readCSV()
    public string filePath;

    public GameObject almanacSelectView;
    public GameObject almanacDetailView;
    public GridLayoutGroup entryGrid;

    public TextMeshProUGUI currentSection;

    // Current entry view
    public TextMeshProUGUI currentEntryName;
    public TextMeshProUGUI currentEntryDescription;
    public Image currentEntryImage;

    [Header("Show error messages for missing images")]
    public bool showErrors = false;

    void Start()
    {
        optionsMenuAnimator = GameObject.Find("OptionMenu").GetComponent<Animator>();
        LoadTSVData(filePath);
    }

    // For testing only
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.T)) {
            Debug.Log("Opening Ant");
            OpenOnEntry("Wasp");
        }
        */
        
    }

    public void OnBackButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        if (GlobalVariables.gameOver == true)
        {
            almanacAnimator.SetBool("isKey", false);
            almanacAnimator.SetBool("isActive", false);
            //EventSystem.current.SetSelectedGameObject(WinLoseScreen.defaultSelectedObjectStatic, new BaseEventData(EventSystem.current));
            LevelManager.instance.SetControllerSelectedObject( WinLoseScreen.defaultSelectedObjectStatic);
        }
        else
        {
            almanacAnimator.SetBool("isKey", false);
            currentCategory = AlmanacCategory.DEFAULT;
            LevelManager.instance.SetControllerSelectedObject(defaultBackButton);
            ViewChange(currentCategory);
        }
    }

    /** [DEPRECATED] Do not use this function, but admire my work :)
    public void ReadCSV() {

        string[] rawData = textAssetData.text.Split(new string[] { "\n" }, StringSplitOptions.None);

        almanacEntries = new List<AlmanacData>();

        for (int i = 3; i < rawData.Length; i += 3) {
            almanacEntries.Add(new AlmanacData(rawData[i], rawData[i + 1], rawData[i + 2]));
        }

        foreach (AlmanacData entry in almanacEntries) {
            switch (entry.category) {
                case AlmanacCategory.UNIT:
                    unitEntries.Add(entry);
                    break;
                case AlmanacCategory.ENEMY:
                    enemyEntries.Add(entry);
                    break;
                case AlmanacCategory.OBSTACLE:
                    obstacleEntries.Add(entry);
                    break;
                case AlmanacCategory.SPECIAL:
                    specialEntries.Add(entry);
                    break;
                default:
                    defaultEntries.Add(entry);
                    break;
            }
        }

    }
    */

    void LoadTSVData(string path)
    {
        TextAsset file = Resources.Load("AlmanacData") as TextAsset;

        if (file != null)
        {
            //string[] newlines = file.text.Split(Environment.NewLine);
            string[] lines = file.text.Split(Environment.NewLine);
            //string[] lines = File.ReadAllLines("Assets/UI/Almanac/Resources/AlmanacData.txt");

            /*string[] testoldentries = lines[1].Split('\t');
            string[] testnewentries = newlines[1].Split('\t');*/

            //print(testoldentries[17].Equals(testnewentries[17]));

            /*string debug_text = "";
            print(lines[0].Length + " / " + newlines[0].Length);
            for (int i = 0; i < lines[0].Length; i++)
            {
                if (lines[0][i] == newlines[0][i])
                {
                    debug_text += newlines[0][i];
                }
                else
                {
                    print("Break Char: new-" + newlines[0][i] + " old-" + lines[0][i]);
                    break;
                }
            }

            print("Equal Line: " + debug_text.Equals(lines));*/


            // Assuming all rows have the same number of columns
            int numRows = lines.Length;
            int numCols = lines[0].Split('\t').Length;

            rawData = new string[numRows, numCols];

            for (int i = 0; i < numRows; i++)
            {
                string[] entries = lines[i].Split('\t');
                for (int j = 0; j < numCols; j++)
                {
                    rawData[i, j] = (j < entries.Length) ? entries[j] : ""; // Handles empty entries
                }
            }
        }
        else
        {
            Debug.LogError("File not found: " + path);
        }

        almanacEntries = new List<AlmanacData>();

        for (int i = 1; i < rawData.GetLength(0); i += 1) {
            almanacEntries.Add(new AlmanacData(rawData[i, (int)AlmanacCategoryNumber.NAME], rawData[i, (int)AlmanacCategoryNumber.CATEGORY], rawData[i, (int)AlmanacCategoryNumber.PICTUREREF], rawData[i, (int)AlmanacCategoryNumber.HEALTH], rawData[i, (int)AlmanacCategoryNumber.SPEED], rawData[i, (int)AlmanacCategoryNumber.DAMAGE], rawData[i, (int)AlmanacCategoryNumber.EFFECT], rawData[i, (int)AlmanacCategoryNumber.COOLDOWN], rawData[i, (int)AlmanacCategoryNumber.KNOCKBACK], rawData[i, (int)AlmanacCategoryNumber.WEIGHT], rawData[i, (int)AlmanacCategoryNumber.RANGE], rawData[i, (int)AlmanacCategoryNumber.EXPLOSIVEAOE], rawData[i, (int)AlmanacCategoryNumber.MOVETYPE], rawData[i, (int)AlmanacCategoryNumber.SPAWNDISTANCE], rawData[i, (int)AlmanacCategoryNumber.COST], rawData[i, (int)AlmanacCategoryNumber.LINELENGTH], rawData[i, (int)AlmanacCategoryNumber.DESCRIPTION], rawData[i, (int)AlmanacCategoryNumber.APPLICABLECHECKS].Split(',')));
        }

        foreach (AlmanacData entry in almanacEntries) {
            switch (entry.category) {
                case AlmanacCategory.UNIT:
                    unitEntries.Add(entry);
                    break;
                case AlmanacCategory.ENEMY:
                    enemyEntries.Add(entry);
                    break;
                case AlmanacCategory.OBSTACLE:
                    obstacleEntries.Add(entry);
                    break;
                case AlmanacCategory.SPECIAL:
                    specialEntries.Add(entry);
                    break;
                default:
                    defaultEntries.Add(entry);
                    break;
            }
        }

    }

    public void LoadEntries() {
        // Clear the current entries
        foreach (Button entry in currentEntries) {
            Destroy(entry.gameObject);
        }

        currentEntries.Clear();

        bool firstEntry = true;
        // Load the new entries
        switch (currentCategory) {
            case AlmanacCategory.UNIT:
                foreach (AlmanacData entry in unitEntries) {
                    GameObject newEntry = Instantiate(Resources.Load<GameObject>("AlmanacEntryButton"), currentEntryView.transform);
                    Button newEntryButton = newEntry.GetComponent<Button>();
                    newEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = entry.name;
                    newEntryButton.onClick.AddListener(delegate { OnEntryButtonPressed(entry); });
                    currentEntries.Add(newEntryButton);
                }
                currentEntry = unitEntries[0];
                break;
            case AlmanacCategory.ENEMY:
                foreach (AlmanacData entry in enemyEntries) {
                    GameObject newEntry = Instantiate(Resources.Load<GameObject>("AlmanacEntryButton"), currentEntryView.transform);
                    Button newEntryButton = newEntry.GetComponent<Button>();
                    newEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = entry.name;
                    newEntryButton.onClick.AddListener(delegate { OnEntryButtonPressed(entry); });
                    currentEntries.Add(newEntryButton);
                }
                currentEntry = enemyEntries[0];
                break;
            case AlmanacCategory.OBSTACLE:
                foreach (AlmanacData entry in obstacleEntries) {
                    GameObject newEntry = Instantiate(Resources.Load<GameObject>("AlmanacEntryButton"), currentEntryView.transform);
                    Button newEntryButton = newEntry.GetComponent<Button>();
                    newEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = entry.name;
                    newEntryButton.onClick.AddListener(delegate { OnEntryButtonPressed(entry); });
                    currentEntries.Add(newEntryButton);
                }
                currentEntry = obstacleEntries[0];
                break;
            case AlmanacCategory.SPECIAL:
                foreach (AlmanacData entry in specialEntries) {
                    GameObject newEntry = Instantiate(Resources.Load<GameObject>("AlmanacEntryButton"), currentEntryView.transform);
                    Button newEntryButton = newEntry.GetComponent<Button>();
                    newEntryButton.GetComponentInChildren<TextMeshProUGUI>().text = entry.name;
                    newEntryButton.onClick.AddListener(delegate { OnEntryButtonPressed(entry); });
                    currentEntries.Add(newEntryButton);
                }
                currentEntry = specialEntries[0];
                break;
            default:
                foreach (AlmanacData entry in defaultEntries) {
                    Button newEntry = Instantiate(Resources.Load<Button>("AlmanacEntryButton"), currentEntryView.transform);

                    newEntry.GetComponentInChildren<TextMeshProUGUI>().text = entry.name;
                    newEntry.onClick.AddListener(delegate { OnEntryButtonPressed(entry); });
                    currentEntries.Add(newEntry);
                }
                currentEntry = defaultEntries[0];
                Debug.LogError("No entries found for category " + currentCategory + ". Using default entry instead.");
                break;
        }

        if (currentEntries.Count > 0)
        {
            firstEntry = false;
            Navigation newNav = new Navigation();
            newNav.mode = Navigation.Mode.Explicit;
            newNav.selectOnRight = currentEntries[0];
            newNav.selectOnUp = currentEntries[0];
            backButton.GetComponent<Button>().navigation = newNav;
            Button backButtonComponent = backButton.GetComponent<Button>();

            int gridMod = entryGrid.constraintCount;
            int rowCount = 0;
            // Entire Alogrithm for determining controller navigation on generated buttons
            for (int i = 0; i < currentEntries.Count; i++)
            {
                print("Generating Navigation for button " + i);
                newNav = new Navigation();
                newNav.mode = Navigation.Mode.Explicit;
                Button[] directionArray = new Button[4]; // UP DOWN LEFT RIGHT
                if (i == gridMod * (rowCount + 1))
                {
                    rowCount++;
                }

                // Determine select Left
                if (i == gridMod * rowCount)
                {
                    newNav.selectOnLeft = backButtonComponent;
                }
                else
                {
                    newNav.selectOnLeft = currentEntries[i - 1];
                }

                // Determine select Right
                if ((i == (gridMod * rowCount) - 1) || i == currentEntries.Count - 1)
                {
                    newNav.selectOnRight = backButtonComponent;
                }
                else if (i < currentEntries.Count - 1)
                {
                    newNav.selectOnRight = currentEntries[i + 1];
                }

                // Determine select Up
                if (rowCount == 0)
                {
                    if (i + (gridMod * Mathf.CeilToInt(currentEntries.Count / gridMod)) < currentEntries.Count)
                    {
                        newNav.selectOnUp = currentEntries[i + (gridMod * Mathf.CeilToInt(currentEntries.Count / gridMod))];
                    }
                    else
                    {
                        newNav.selectOnUp = currentEntries[currentEntries.Count - 1];
                    }
                }
                else
                {
                    newNav.selectOnUp = currentEntries[i - gridMod];
                }

                // Detrermine select Down
                if (rowCount == Mathf.CeilToInt(currentEntries.Count / gridMod))
                {
                    newNav.selectOnDown = currentEntries[i - (gridMod * Mathf.CeilToInt(currentEntries.Count / gridMod))];
                }
                else
                {
                    if (i + gridMod < currentEntries.Count)
                    {
                        newNav.selectOnDown = currentEntries[i + gridMod];
                    }
                    else
                    {
                        newNav.selectOnDown = currentEntries[i - (gridMod * rowCount)];
                    }
                }

                currentEntries[i].navigation = newNav;
            }
        }

        OnEntryButtonPressed(currentEntry);
    }
    // Category buttons

    public void OnUnitButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        almanacAnimator.SetBool("isKey", true);
        print("clicked");
        currentCategory = AlmanacCategory.UNIT;
        currentSection.text = "GuideBook - Units";
        LevelManager.instance.SetControllerSelectedObject(backButton);
        ViewChange(currentCategory);
    }

    public void OnEnemyButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        almanacAnimator.SetBool("isKey", true);
        currentCategory = AlmanacCategory.ENEMY;
        currentSection.text = "GuideBook - Enemies";
        LevelManager.instance.SetControllerSelectedObject(backButton);
        ViewChange(currentCategory);
    }

    public void OnObstacleButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        almanacAnimator.SetBool("isKey", true);
        currentCategory = AlmanacCategory.OBSTACLE;
        currentSection.text = "GuideBook - Obstacles";
        LevelManager.instance.SetControllerSelectedObject(backButton);
        ViewChange(currentCategory);
    }

    public void OnSpecialButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        almanacAnimator.SetBool("isKey", true);
        currentCategory = AlmanacCategory.SPECIAL;
        currentSection.text = "GuideBook - LeaderShip";
        LevelManager.instance.SetControllerSelectedObject(backButton);
        ViewChange(currentCategory);
    }

    // Entry buttons
    public void OnEntryButtonPressed(AlmanacData entry) {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        currentEntry = entry;
        currentEntryName.text = entry.name;
        currentEntryDescription.text = entry.BuildEntry();
        currentEntryImage.sprite = entry.image;
        if (!entry.isViewable) {
            currentEntryImage.color = new Color(0f, 0f, 0f, 1f);
        } else {
            currentEntryImage.color = new Color(1f, 1f, 1f, 1f);}
    }

    public void OnBackButtonPressedFromDetail() {
        print("Detail");
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        MenuManager.Instance.SetActiveMenu(optionsMenuAnimator.gameObject, optionsMenuAnimator);

        //LevelManager.instance.SetControllerSelectedObject(defaultBackButton);
    }

    public void ViewChange(AlmanacCategory view) {
        if (view == AlmanacCategory.DEFAULT) {
            //almanacSelectView.SetActive(true);
            //almanacDetailView.SetActive(false);
        } else {
            //almanacSelectView.SetActive(false);
            //almanacDetailView.SetActive(true);
            LoadEntries();
        }
    }

    public void OpenOnEntry(string name) {
        foreach (AlmanacData entry in almanacEntries) {
            if (entry.name == name) {
                entry.isViewable = entry.CheckIsViewable();
                currentEntry = entry;
                currentCategory = entry.category;
                almanacAnimator.SetBool("isActive", true);
                almanacAnimator.SetBool("isKey", true);
                currentSection.text = "GuideBook - Units";
                OnEntryButtonPressed(currentEntry);
                //EventSystem.current.SetSelectedGameObject(backButton, new BaseEventData(EventSystem.current));
                LevelManager.instance.SetControllerSelectedObject(backButton);
                //almanacAnimator.SetBool("isShowingAlmanac", true);
                break;
            }
        }
    }
}

[System.Serializable]
public class AlmanacData
{
    public string name;
    public AlmanacCategory category;
    public string pictureRef;
    public string health;
    public string speed;
    public string damage;
    public string effect;
    public string cooldown;
    public string knockback;
    public string weight;
    public string range;
    public string explosiveAOE;
    public string moveType;
    public string spawnDistance;
    public string cost;
    public string lineLength;
    public string description;
    public bool isViewable;
    public string[] applicableChecks;
    public Sprite image;

    public AlmanacData(string name, string category, string description) {
        this.name = name;
        this.description = description;

        switch (category.ToUpper()) {
            case "UNIT":
                this.category = AlmanacCategory.UNIT;
                break;
            case "ENEMY":
                this.category = AlmanacCategory.ENEMY;
                break;
            case "OBSTACLE":
                this.category = AlmanacCategory.OBSTACLE;
                break;
            case "QUEEN":
                this.category = AlmanacCategory.SPECIAL;
                break;
            default:
                this.category = AlmanacCategory.DEFAULT;
                break;
        }

        isViewable = false;
    }

    // Overload for full data
    public AlmanacData(string name, string category, string pictureRef, string health, string speed, string damage, string effect, string cooldown, string knockback, string weight, string range, string explosiveAOE, string moveType, string spawnDistance, string cost, string lineLength, string description, string[] applicableChecks) {
        this.name = name;
        this.description = description;
        this.pictureRef = pictureRef;
        this.health = health;
        this.speed = speed;
        this.damage = damage;
        this.effect = effect;
        this.cooldown = cooldown;
        this.knockback = knockback;
        this.weight = weight;
        this.range = range;
        this.explosiveAOE = explosiveAOE;
        this.moveType = moveType;
        this.spawnDistance = spawnDistance;
        this.cost = cost;
        this.lineLength = lineLength;
        this.applicableChecks = applicableChecks;


        if (pictureRef != "") {
            this.image = Resources.Load<Sprite>("AlmanacEntriesPictures/" + pictureRef);
        } else {
            this.image = Resources.Load<Sprite>("AlmanacEntriesPictures/Default");
            if (GameObject.Find("EnemyAlmanac").GetComponent<EnemyAlmanac>().showErrors) {
                Debug.LogError("Error loading image for " + name + ": " + pictureRef + "\n" + "Using default image instead.");
            }
        }

        switch (category.ToUpper()) {
            case "UNIT":
                this.category = AlmanacCategory.UNIT;
                break;
            case "ENEMY":
                this.category = AlmanacCategory.ENEMY;
                break;
            case "OBSTACLE":
                this.category = AlmanacCategory.OBSTACLE;
                break;
            case "QUEEN":
                this.category = AlmanacCategory.SPECIAL;
                break;
            default:
                this.category = AlmanacCategory.DEFAULT;
                break;
        }

        isViewable = CheckIsViewable();
    }

    public bool CheckIsViewable() {
        LevelManager levelManager = LevelManager.instance;//GameObject.Find("LevelManager").GetComponent<LevelManager>();

        bool isViewable = true;

        foreach (string check in applicableChecks) {
            if (check != "" && levelManager) {
                if (!levelManager.ReturnSingleCheck(check)) {
                    isViewable = false;
                }
            }
        }

        return isViewable;
    }

    public string BuildEntry() {
        string entry = "";

        if (!isViewable) {
            entry = "???";
            return entry;
        }

        entry += name + "\n\n";

        entry += description + "\n\n";

        entry += BuildEntryStats();

        return entry;
    }

    private string BuildEntryStats() {
        string entry = "";

        if (health != "") {
            entry += "Health: " + health + "\n";
        }

        if (speed != "") {
            entry += "Speed: " + speed + "\n";
        }

        if (damage != "") {
            entry += "Damage: " + damage + "\n";
        }

        if (effect != "") {
            entry += "Effect: " + effect + "\n";
        }

        if (cooldown != "") {
            entry += "Cooldown: " + cooldown + "\n";
        }

        if (knockback != "") {
            entry += "Knockback: " + knockback + "\n";
        }

        if (weight != "") {
            entry += "Weight: " + weight + "\n";
        }

        if (range != "") {
            entry += "Range: " + range + "\n";
        }

        if (explosiveAOE != "") {
            entry += "Explosive AOE: " + explosiveAOE + "\n";
        }

        if (moveType != "") {
            entry += "Move Type: " + moveType + "\n";
        }

        if (spawnDistance != "") {
            entry += "Spawn Distance: " + spawnDistance + "\n";
        }

        if (cost != "") {
            entry += "Cost: " + cost + "\n";
        }

        if (lineLength != "") {
            entry += "Line Length: " + lineLength + "\n";
        }

        return entry;
    }
}