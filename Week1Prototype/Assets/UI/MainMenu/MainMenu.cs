using SceneTransition;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public sealed class MainMenu : MonoBehaviour
{
    public bool skipAutoTutorial;

    public string musicName;

    [Header("Menu Refs")]
    public GameObject mainView;
    public GameObject introVideoView;
    public GameObject creditsView;
    public GameObject levelSelectView;
    public GameObject tutorialSelectView;
    public GameObject currentView;
    public GameObject creditBackButton;
    public GameObject creditButton;

    [Header("Default Objects")]
    public GameObject defaultSelectedObject;
    public GameObject levelSelectDefaultSelectedObject;

    [Header("Component Refs")]
    public OptionsMenu pauseMenu;
    public EnemyAlmanac almanacMenu;
    public InputAction ExitToMainViewButton;
    //private LevelManager levelManager;
    public string[] levels;

    [Header("Prefabs")]
    public GameObject levelManagerPrefab;

    private void Awake()
    {
        if (LevelManager.instance == null)
        {
            GameObject.Instantiate(levelManagerPrefab);
        }
    }

    private void Start() {
        currentView = mainView;

        SFXManager.Instance.playMusic(musicName);
        LevelManager.instance.OnEnable();
        //levelManager = LevelManager.instance;
    }

    public void OnPlayButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        LevelManager.instance.ToggleSingleCheck("hasUnlockedUnitBase", true);
        LevelManager.instance.ToggleSingleCheck("hasSeenEnemySpider", true);

        if (!skipAutoTutorial)
        {
            
            if (!LevelManager.instance.ReturnSingleCheck("hasBeatenLevelTutorial1"))
            {
                SFXManager.Instance.stopMusic();
                SceneTransiter.Instance.TransitScene("TutorialLevel1");
                return;
            }

            if (!LevelManager.instance.ReturnSingleCheck("hasBeatenLevelTutorial3"))
            {
                SFXManager.Instance.stopMusic();
                SceneTransiter.Instance.TransitScene("TutorialLevel3");
                return;
            }

            if (!LevelManager.instance.ReturnSingleCheck("hasBeatenLevelTutorial5"))
            {
                SFXManager.Instance.stopMusic();
                SceneTransiter.Instance.TransitScene("TutorialLevel5");
                return;
            }
        }
        else
        {
            LevelManager.instance.toggleLevelChecks("TutorialLevel1");
            LevelManager.instance.toggleLevelChecks("TutorialLevel3");
            LevelManager.instance.toggleLevelChecks("TutorialLevel5");
        }

        switchView(levelSelectView);
        //pauseMenu.gameObject.SetActive(false);
        almanacMenu.gameObject.SetActive(false);
        //EventSystem.current.SetSelectedGameObject(levelSelectDefaultSelectedObject, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject(levelSelectDefaultSelectedObject);
    }

    public void OnIntroVideoButtonPressed() {
        switchView(introVideoView);
    }

    public void OnCreditsButtonPressed() {
        switchView(creditsView);
        //EventSystem.current.SetSelectedGameObject(creditBackButton, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject(creditBackButton);
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
    }

    public void OnOptionsPressed()
    {

    }

    public void OnBackButtonPressed() {
        switchView(mainView);
        //EventSystem.current.SetSelectedGameObject(creditButton, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject(creditButton);
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
    }

    public void OnTutorialButtonPressed() {
        switchView(tutorialSelectView);
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
    }

    public void OnBackToLevelSelectButtonPressed() {
        // EventSystem.current.SetSelectedGameObject(levelSelectDefaultSelectedObject, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject(levelSelectDefaultSelectedObject);
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        switchView(levelSelectView);
    }

    public void OnSkipTurorialToggled(bool value)
    {
        skipAutoTutorial = value;
    }

    public void switchView(GameObject view) {
        view.SetActive(true);

        currentView.SetActive(false);

        currentView = view;
    }

    private void ExitToMainView(InputAction.CallbackContext context) {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        if (!pauseMenu.isPaused || currentView != mainView) { switchView(mainView); }
    }

    public void OnQuitButtonPressed() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
        Application.Quit();
    }

    public void OnEnable() {
        ExitToMainViewButton.Enable();

        ExitToMainViewButton.started += ExitToMainView;

        //LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        LevelManager.instance.defaultSelectedObject = this.defaultSelectedObject;
        //EventSystem.current.SetSelectedGameObject(defaultSelectedObject, new BaseEventData(EventSystem.current));
        LevelManager.instance.SetControllerSelectedObject( defaultSelectedObject);
    }

    public void OnDisable() {
        ExitToMainViewButton.Disable();
    }
}
