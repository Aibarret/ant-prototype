using SceneTransition;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class TutorialSelect : MonoBehaviour
{
    private LevelManager levelManager;
    public GameObject tutorial1Button;
    public GameObject tutorial2Button;
    public GameObject tutorial3Button;
    void Start()
    {
        levelManager = LevelManager.instance;
    }

    public void playTutorial(int entry)
    {
        if (entry == 0)
        {
            SFXManager.Instance.stopMusic();
            SceneTransiter.Instance.TransitScene("TutorialLevel1");
        }
        else if (entry == 1)
        {
            SFXManager.Instance.stopMusic();
            SceneTransiter.Instance.TransitScene("TutorialLevel3");
        }
        else if (entry == 2)
        {
            SFXManager.Instance.stopMusic();
            SceneTransiter.Instance.TransitScene("TutorialLevel5");
        }
    }
}
