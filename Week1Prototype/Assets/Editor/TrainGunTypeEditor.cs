using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TrainGunType))]
public sealed class TrainGunTypeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Activate"))
        {
            if (!EditorApplication.isPlaying)
            {
                Debug.LogError("'TrainGunType.Activate' doesn't work in editor mode.");
                return;
            }
            
            TrainGunType trainGun = target as TrainGunType;
            trainGun!.activate();
        }
    }
}
