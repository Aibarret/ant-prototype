using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : MonoBehaviour
{
    public float speed;
    public GameObject target;

    void Update()
    {
        if(target != null)
        {
            Vector3 direction = (target.transform.position - transform.position).normalized;
            transform.position += direction * speed * Time.deltaTime;

            float distance = Vector3.Distance(transform.position, target.transform.position);
            if(distance < .1f)
            {
                Destroy(gameObject);
            }
        } else {
            Destroy(gameObject);
        }

        
    }
}