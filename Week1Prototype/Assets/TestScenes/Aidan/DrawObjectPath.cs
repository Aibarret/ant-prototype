using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawObjectPath : MonoBehaviour
{
    //public Transform followObject;

    public Transform start;
    public Transform end;
    public Transform centerObject;

    public int resolution;
    public float arcMultiplier;

    public List<Vector3> path;

    // Update is called once per frame
    void Update()
    {
        //path.Add(followObject.position);
    }

   /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(start.position, end.position);

    }*/

    private void OnDrawGizmos()
    {
        /*print("Beginning:");
        multiplierMod(2, 0, 40);
        print("Middle:");
        multiplierMod(2, 20, 40);
        print("End:");
        multiplierMod(2, 40, 40);*/

        path = new List<Vector3>();

        //Vector3 center = (start.position + end.position) * 0.5F;
        Vector3 center = centerObject.position;

        center -= new Vector3(0, 1, 0);

        Vector3 riseRelCenter = start.position - center;
        Vector3 setRelCenter = end.position - center;

        for (int i = 0; i <= resolution; i++)
        {
            float fracComplete = (float) i / (float) resolution;
            Vector3 slerpResult = Vector3.Slerp(riseRelCenter, setRelCenter, fracComplete);// * multiplierMod(arcMultiplier, i, resolution);
            //slerpResult.y = slerpResult.y * arcMultiplier;
            path.Add(slerpResult + center);
        }

        Gizmos.color = Color.green;
        for (int i = 1; i < path.Count; i++)
        {
            Gizmos.DrawLine(path[i - 1], path[i]);
        }

    }

    public float multiplierMod(float multiplier, int iteration, int total)
    {
        float midpoint = (total * .5f);

        float modifier;
        if (iteration <= midpoint)
        {
            modifier = (float) iteration / (float) midpoint;
        }
        else
        {
            modifier = Mathf.Abs((float) total - (float) iteration) / (float) midpoint;
        }

        float result = multiplier * modifier;
        result = (float) Mathf.Clamp(result, 1, multiplier);

        //print("Modifier: " + modifier);
        print("Final Result: " + result);
        return result;
    }

    private void Start()
    {
        print("Multiplier Result: " + multiplierMod(2, 0, 40));
    }
}
