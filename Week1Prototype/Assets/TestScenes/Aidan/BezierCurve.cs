using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve : MonoBehaviour
{

    public Transform start, end, center;
    public int resolution;
    public List<Vector3> positions = new List<Vector3>();

    private void Start()
    {
        drawQuadraticCurve();
    }

    private void OnDrawGizmos()
    {
        
        drawQuadraticCurve();

        Gizmos.color = Color.red;
        Gizmos.DrawLine(start.position, end.position);

        Gizmos.color = Color.green;
        for (int i = 1; i < positions.Count; i++)
        {
            Gizmos.DrawLine(positions[i - 1], positions[i]);
        }

    }

    private void OnDrawGizmosSelected()
    {
        
    }


    public void drawQuadraticCurve()
    {
        positions = new List<Vector3>();

        for (int i = 0; i <= resolution; i++)
        {
            float t = i / (float)resolution;
            positions.Add(CalculateQuadraticBezierPoint(t, start.position, center.position, end.position));
        }
    }

    private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 start, Vector3 center, Vector3 end)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;

        Vector3 p = uu * start;
        p += 2 * u * t * center;
        p += tt * end;

        return p;
    }
    
}
