using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideTest : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Trigger works");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("Collide works");
    }
}
