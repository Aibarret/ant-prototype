using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererTest : MonoBehaviour
{
    public LineRenderer lr;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            print("doot");
            lr.SetPosition(0, new Vector3(5, 0));

            lr.material.SetFloat("_Offset", Vector3.Distance(Vector3.zero, lr.GetPosition(0)) * .5f);
        }
    }
}
