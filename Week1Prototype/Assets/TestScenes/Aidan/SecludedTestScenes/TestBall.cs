using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBall : MonoBehaviour
{
    public bool useAddForce;
    public Rigidbody2D rb;
    public float initialSpeed;
    public float speedDecay;
    public Vector3 direction = new Vector3(1, 0);

    private bool active;
    private bool doot = true;

    private Vector3 lastPosn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            active = true;

            if (useAddForce && doot)
            {
                doot = false;
                rb.AddForce(direction * initialSpeed);

                //Vector3.Reflect()
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        direction = Vector3.Reflect(direction, collision.contacts[0].normal);
        print(direction);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        

        if (useAddForce && !doot)
        {
            print(rb.velocity.magnitude);
            if (rb.velocity.magnitude <= 1f)
            {
                rb.velocity = new Vector3(0,0);
            }
        }

        if (!useAddForce)
        {
            

            if (active)
            {
                //float speedDecay = 1f;

                if (initialSpeed > 0)
                {
                    float speed = initialSpeed - (speedDecay * Time.deltaTime);
                    rb.MovePosition(transform.position + direction * speed * Time.deltaTime);

                    initialSpeed = speed;
                    //return speed;
                }
                else
                {

                    //enemy.setMoveMode(MoveMode.normal);
                    //return 0;
                }

            }
        }
    }


}
