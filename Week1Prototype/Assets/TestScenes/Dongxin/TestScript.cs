using System.Collections;
using UnityEngine;

namespace SceneTransition
{
    public sealed class TestScript : MonoBehaviour
    {
        [SerializeField] private string to;
        
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(1f);

            SceneTransiter sceneTransiter = SceneTransiter.Instance;
            // if (!sceneTransiter.TransiterExist("Wipe"))
            // {
            //     WipeTransiter wipeTransiter = Instantiate(Resources.Load<WipeTransiter>("WipeTransiter"));
            //     sceneTransiter.RegisterTransiter("Wipe", wipeTransiter);   
            // }
            sceneTransiter.TransitScene(to);
        }
    }   
}
