﻿using System;
using System.Runtime.CompilerServices;
using Object = UnityEngine.Object;

public static class NullUtilities
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNullArgument(object arg, string argName)
    {
        if (arg is null)
            throw new ArgumentNullException(argName);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNullArgument(Object arg, string argName)
    {
        if (arg == null)
            throw new ArgumentNullException(argName);
    }
}
