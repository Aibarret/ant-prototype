using UnityEngine;

public sealed class ProgressBar : MonoBehaviour
{
    private QueenManager qm;

    public int QueenIndex { get; set; }

    private void Start()
        => qm = QueenManager.Instance;

    public void SetActiveQueen()
    {
        qm.setActiveQueen(QueenIndex);
        
        Transform queen = qm.queenList[QueenIndex].transform;
        Camera cam = Camera.main!;
        cam.transform.position = new Vector3(queen.position.x, queen.position.y, cam.transform.position.z);
    }
}
