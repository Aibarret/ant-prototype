﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneTransition
{
    public sealed class SceneTransiter : MonoBehaviour, ISingleton
    {
        /* ===== Singleton ===== */
        
        private static SceneTransiter instance = null;
        public static SceneTransiter Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Instantiate(Resources.Load<SceneTransiter>("SceneTransiter"));
                    instance.name = "SceneTransiter";
                    instance.defaultTransiter.gameObject.SetActive(false);
                    DontDestroyOnLoad(instance.gameObject);
                }
                return instance;
            }
        }
        
        
        
        /* ===== Operations ===== */

        [SerializeField] private TransiterBase defaultTransiter;

        private Dictionary<string, TransiterBase> transiterDict = new Dictionary<string, TransiterBase>();

        public bool IsLoading { get; private set; } = false;

        /// <summary>
        /// Note: the life cycle of registered transiters are managed by the SceneTransiter singleton instance.
        /// </summary>
        public void RegisterTransiter(string tag, TransiterBase transiter)
        {
            if (transiterDict.ContainsKey(tag))
                throw new ArgumentException($"Transiter with tag, '{tag}', already existed in the SceneTransiter.", nameof(tag));
            
            DontDestroyOnLoad(transiter.gameObject);
            transiter.transform.SetParent(transform);
            transiter.gameObject.SetActive(false);
            transiterDict.Add(tag, transiter);
        }

        public void DeregisterTransiter(string tag)
        {
            TransiterBase transiter = transiterDict[tag];
            Destroy(transiter.gameObject);
            transiterDict.Remove(tag);
        }

        public bool TransiterExist(string tag)
            => transiterDict.ContainsKey(tag);

        public bool TryGetTransiter(string tag, out TransiterBase transiter)
        {
            if (transiterDict.ContainsKey(tag))
            {
                transiter = transiterDict[tag];
                return true;
            }
            transiter = null;
            return false;
        }

        public TransiterBase GetTransiter(string tag)
        {
            if (!transiterDict.ContainsKey(tag))
                throw new ArgumentException($"Transiter with tag, '{tag}', is not registered in the SceneTransiter.", nameof(tag));
            return transiterDict[tag];
        }
        
        public void TransitScene(string sceneName, string tag = null)
            => TransitSceneF(() => SceneManager.LoadSceneAsync(sceneName), tag);

        public void TransitSceneN(int sceneIndex, string tag = null)
            => TransitSceneF(() => SceneManager.LoadSceneAsync(sceneIndex), tag);

        private void TransitSceneF(Func<AsyncOperation> loadSceneAsyncFunc, string tag = null)
        {
            if (IsLoading)
                return;
            IsLoading = true;
            
            TransiterBase transiter = tag is null ? defaultTransiter : transiterDict[tag];

            transiter.gameObject.SetActive(true);
            transiter.TransitionIn(() => {
                AsyncOperation op = loadSceneAsyncFunc();
                op.completed += (_) => transiter.TransitionOut(() => {
                    transiter.gameObject.SetActive(false);
                    IsLoading = false;
                    
                    while (actionAfterSceneTransitionQueue.Count > 0)
                        actionAfterSceneTransitionQueue.Dequeue()?.Invoke();
                });
            });
        }

        private readonly Queue<Action> actionAfterSceneTransitionQueue = new Queue<Action>();
        
        public void PushActionAfterSceneTransition(Action action)
            => actionAfterSceneTransitionQueue.Enqueue(action);
    }
}
