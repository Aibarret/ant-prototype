﻿using DG.Tweening;
using System;
using UnityEngine;

namespace SceneTransition
{
    public sealed class WipeTransiter : TransiterBase
    {
        [SerializeField] private RectTransform backdrop;
        
        [SerializeField] private Ease inEase = Ease.InOutQuad;
        [SerializeField] private float inDuration = 1f;
        [SerializeField] private float delayBeforeOut = 1f;

        [SerializeField] private Ease outEase = Ease.InOutQuad;
        [SerializeField] private float outDuration = 1f;
        
        private Tween inTween = null;
        private Tween outTween = null;

        private float width;
        
        private void Awake()
            => width = backdrop.rect.width;
        
        public override void TransitionIn(Action callback)
        {
            inTween?.Kill();
            
            Vector2 pos = backdrop.anchoredPosition;
            pos.x = width;
            backdrop.anchoredPosition = pos;
            
            inTween = backdrop.DOAnchorPosX(0f, inDuration)
                              .SetEase(inEase)
                              .OnComplete(() => DOVirtual.DelayedCall(delayBeforeOut, () => callback?.Invoke()));
            loadAnimation.SetBool("isLoading", true);
        }

        public override void TransitionOut(Action callback)
        {
            loadAnimation.SetBool("isLoading", false);
            outTween?.Kill();
            outTween = backdrop.DOAnchorPosX(-width, outDuration)
                               .SetEase(outEase)
                               .OnComplete(() => callback?.Invoke());
        }
    }
}
