﻿using DG.Tweening;
using System;
using UnityEngine;

namespace SceneTransition
{
    public sealed class CircleTransiter : TransiterBase
    {
        [SerializeField] private RectTransform circle;

        [SerializeField] private Ease inEase = Ease.InOutQuad;
        [SerializeField] private float inDuration = 1f;
        [SerializeField] private float delayBeforeOut = 1f;

        [SerializeField] private Ease outEase = Ease.InOutQuad;
        [SerializeField] private float outDuration = 1f;

        private Tween inTween = null;
        private Tween outTween = null;

        
        private void Awake()
            => circle.transform.localScale = Vector3.one;

        public override void TransitionIn(Action callback)
        {
            inTween?.Kill();
            circle.localScale = Vector3.one;
            inTween = circle.DOScale(Vector3.zero, inDuration)
                            .SetEase(inEase)
                            .OnComplete(() =>
                                DOVirtual.DelayedCall(delayBeforeOut, () => callback?.Invoke()));
            loadAnimation.SetBool("isLoading", true);
        }

        public override void TransitionOut(Action callback)
        {
            outTween?.Kill();
            loadAnimation.SetBool("isLoading", false);
            outTween = circle.DOScale(Vector3.one, outDuration)
                             .SetEase(outEase)
                             .OnComplete(() => callback?.Invoke());
        }

        public void SetCenter(Vector2 anchoredPosition)
            => circle.anchoredPosition = anchoredPosition;
    }
}
