using System;
using UnityEngine;

namespace SceneTransition
{
    public abstract class TransiterBase : MonoBehaviour
    {
        public abstract void TransitionIn(Action callback);
        public abstract void TransitionOut(Action callback);

        public Animator loadAnimation;
    }
}
