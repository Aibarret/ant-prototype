using UnityEngine;

public sealed class QueenCanvas : MonoBehaviour
{
    [SerializeField] private GameObject healthBarPrefab;
    [SerializeField] private GameObject progressBarPrefab;

    private RectTransform[] healthBars;
    private RectTransform[] progressBarRtfs;
    private ProgressBar[] progressBars;

    private QueenManager qm;

    private int previousActiveIndex;
    
    private void Start()
    {
        qm = QueenManager.Instance;

        int queenCount = qm.queenList.Count;
        healthBars = new RectTransform[queenCount];
        progressBarRtfs = new RectTransform[queenCount];
        progressBars = new ProgressBar[queenCount];
        
        Vector2 hbPos = new Vector2(625f, 325f);
        Vector2 pbPos = new Vector2(-625f, 325f);
        
        for (int i = 0; i < queenCount; i++)
        {
            RectTransform healthBar = Instantiate(healthBarPrefab, transform).GetComponent<RectTransform>();
            healthBar.anchoredPosition = hbPos;
            hbPos.y -= 40f;
            RectTransform progressBarRtf = Instantiate(progressBarPrefab, transform).GetComponent<RectTransform>();
            progressBarRtf.anchoredPosition = pbPos;
            pbPos.y -= 40f;
            ProgressBar progressBar = progressBarRtf.GetComponent<ProgressBar>();
            progressBar.QueenIndex = i;

            if (i == qm.activeQueenIndex)
                healthBar.localScale = progressBarRtf.localScale = Vector3.one;
            else
                healthBar.localScale = progressBarRtf.localScale = new Vector3(0.5f, 0.5f, 1f);
            
            healthBar.gameObject.name = $"Health Bar {i + 1}";
            progressBarRtf.gameObject.name = $"Progress Bar {i + 1}";

            healthBars[i] = healthBar;
            progressBarRtfs[i] = progressBarRtf;
        }

        previousActiveIndex = qm.activeQueenIndex;
    }

    private void LateUpdate()
    {
        int queenCount = qm.queenList.Count;
        
        if (previousActiveIndex != qm.activeQueenIndex)
        {
            for (int i = 0; i < queenCount; i++)
            {
                RectTransform healthBar = healthBars[i];
                RectTransform progressBar = progressBarRtfs[i];
                
                if (i == qm.activeQueenIndex)
                    healthBar.localScale = progressBar.localScale = Vector3.one;
                else
                    healthBar.localScale = progressBar.localScale = new Vector3(0.5f, 0.5f, 1f);   
            }
            previousActiveIndex = qm.activeQueenIndex;
        }
    }
}
