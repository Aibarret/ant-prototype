using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class QueenTracker : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;
    [SerializeField] private Canvas canvas;
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private RectTransform frame;

    private Camera mainCamera = null;
    private QueenManager qm;
    
    public bool IsVisible { get; private set; }

    private void Start()
    {
        qm = QueenManager.Instance;
        mainCamera = Camera.main;
        UpdateUI();
        canvasGroup.alpha = IsVisible ? 1f : 0f;
        StartCoroutine(AnimationRoutine());
    }

    private void LateUpdate()
        => UpdateUI();

    private void UpdateUI()
    {
        Transform queenTransform = qm.queenList[qm.activeQueenIndex].transform;
        
        // Ignore if the Queen is null (destroyed or null reference)
        if (queenTransform == null)
        {
            IsVisible = false;
            return;
        }
        
        // Check if the Queen is "visible"
        Vector3 viewportPos = mainCamera.WorldToViewportPoint(queenTransform.position);
        if ((viewportPos.x is > 0.15f and < 0.85f) && (viewportPos.y is > 0.2f and < 0.8f))
        {
            IsVisible = false;
            return;
        }
        
        IsVisible = true;
        
        // Set tracker position
        Vector2 clamped = viewportPos;
        clamped.x = Mathf.Clamp(clamped.x, 0.15f, 0.88f);
        clamped.y = Mathf.Clamp(clamped.y, 0.25f, 0.8f);
        Vector3 screenPosition = mainCamera.ViewportToScreenPoint(clamped);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, screenPosition, null, out Vector2 canvasPosition);
        rectTransform.anchoredPosition = canvasPosition;
        
        // Set tracker frame rotation
        Vector3 rot = frame.transform.rotation.eulerAngles;
        Vector2 direction = (Vector2)(viewportPos) - clamped;
        rot.z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        frame.transform.rotation = Quaternion.Euler(rot);
    }

    private IEnumerator AnimationRoutine()
    {
        const float speed = 10f;
        
        while (true)
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, IsVisible ? 1f : 0f, speed * Time.deltaTime);
            
            yield return null;
        }
    }

    public IEnumerator takeDamage()
    {
        if (IsVisible)
        {
            transform.Find("Frame").gameObject.GetComponent<Image>().color = new Color(1f, 0f, 0f, 1f);
            yield return new WaitForSeconds(0.5f);
            transform.Find("Frame").gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        }
    }
}
