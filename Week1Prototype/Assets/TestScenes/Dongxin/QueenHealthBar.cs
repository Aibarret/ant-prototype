using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class QueenHealthBar : MonoBehaviour
{
    [SerializeField] private RectTransform iconRectTransform;
    [SerializeField] private Image fillImage;
    
    private float healthPercentage = 0f;
    public float HealthPercentage
    {
        get => healthPercentage;
        set
        {
            healthPercentage = value switch {
                < 0f => 0,
                > 1f => 1f,
                _ => value
            };
            UpdateUI();   
        }
    }

    public Queen Queen { get; set; } = null;
    
    private const float iconXWhenNoProgress = -350f;
    private const float iconXWhenFullProgress = 350f;
    
    private void Start()
    {
        Queen = QueenManager.Instance.queen;

        healthPercentage = Queen.type.health / (float)Queen.type.maxHealth;
        UpdateUI();
        StartCoroutine(UIRoutine());
    }

    /// <summary>
    /// Call this method when you want to manually update the UIs.
    /// </summary>
    public void UpdateUI()
    {
        Vector2 pos = iconRectTransform.localPosition;
        pos.x = iconXWhenNoProgress + (healthPercentage * (iconXWhenFullProgress - iconXWhenNoProgress));
        iconRectTransform.localPosition = pos;
        
        fillImage.fillAmount = healthPercentage;
    }
    
    private IEnumerator UIRoutine()
    {
        const float speed = 100f;
        
        while (true)
        {
            healthPercentage = Queen.type.health / (float)Queen.type.maxHealth;
            healthPercentage = Mathf.Clamp(healthPercentage, 0f, 1f);
            
            float maxDelta = speed * Time.deltaTime;
            
            Vector2 pos = iconRectTransform.localPosition;
            pos.x = Mathf.MoveTowards(pos.x, iconXWhenNoProgress + (healthPercentage * (iconXWhenFullProgress - iconXWhenNoProgress)), maxDelta * 10f);
            iconRectTransform.localPosition = pos;
            
            fillImage.fillAmount = Mathf.MoveTowards(fillImage.fillAmount, healthPercentage, maxDelta / 100f);
            
            yield return null;
        }
    }
}
