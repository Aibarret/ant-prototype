using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class QueenProgressBar : MonoBehaviour
{
    [SerializeField] private RectTransform crownRectTransform;
    [SerializeField] private Image fillImage;

    private float progress = 0f;
    public float Progress
    {
        get => progress;
        set
        {
            progress = value switch {
                < 0f => 0,
                > 1f => 1f,
                _ => value
            };
            UpdateUI();
        }
    }

    public Queen Queen { get; set; } = null;
    
    private const float crownXWhenNoProgress = -350f;
    private const float crownXWhenFullProgress = 350f;

    private void Start()
    {
        Queen = QueenManager.Instance.queen;
        
        UpdateUI();
        StartCoroutine(UIRoutine());
    }

    /// <summary>
    /// Call this method when you want to manually update the UIs.
    /// </summary>
    public void UpdateUI()
    {
        Vector2 pos = crownRectTransform.localPosition;
        pos.x = crownXWhenNoProgress + (Progress * (crownXWhenFullProgress - crownXWhenNoProgress));
        crownRectTransform.localPosition = pos;
        
        fillImage.fillAmount = Progress;
    }

    private IEnumerator UIRoutine()
    {
        const float speed = 10f;
        
        while (true && Queen.pointContainerScript.qPoints.Count > 0)
        {
            progress = Queen.wavepointIndex / (float)Queen.pointContainerScript.qPoints[0].Count;
            
            float maxDelta = speed * Time.deltaTime;
            
            Vector2 pos = crownRectTransform.localPosition;
            pos.x = Mathf.MoveTowards(pos.x, crownXWhenNoProgress + (Progress * (crownXWhenFullProgress - crownXWhenNoProgress)), maxDelta);
            crownRectTransform.localPosition = pos;
            
            fillImage.fillAmount = Mathf.MoveTowards(fillImage.fillAmount, Progress, maxDelta);
            
            yield return null;
        }
    }
}
