using UnityEngine;

namespace FlowFieldNavigation
{
    public sealed class NavigationObstacle : MonoBehaviour
    {
        [SerializeField] private FlowFieldType type = FlowFieldType.Ground;
        public FlowFieldType Type => type;
    }
}
