namespace FlowFieldNavigation
{
    public enum FlowFieldType : byte
    {
        Ground = 0x00,
        Sky = 0x01
    }
}