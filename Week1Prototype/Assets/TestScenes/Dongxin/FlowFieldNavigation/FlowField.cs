﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FlowFieldNavigation
{
    public sealed class FlowField : MonoBehaviour
    {
        [SerializeField] private Vector2 cellSize = Vector2.one;
        public Vector2 CellSize => cellSize;

        [SerializeField] private int width = 10;
        [SerializeField] private int height = 10;

        public int Width => width;
        public int Height => height;

        public Transform altTarget;

        [SerializeField, HideInInspector]
        private List<Vector2Int> obstaclePositionList = new List<Vector2Int>();
        public List<Vector2Int> ObstaclePositionList => obstaclePositionList;

        private readonly HashSet<Collider2D> colliderObstacleSet = new HashSet<Collider2D>();
        
        private byte[][] cachedCostField = null;
        
        public Cell[][] Cells { get; private set; } = null;

        [SerializeField] private FlowFieldType type = FlowFieldType.Ground;

        private byte[][] initialCostField;
        
        private void Awake()
        {
            initialCostField = new byte[width][];
            for (int i = 0; i < width; i++)
                initialCostField[i] = new byte[height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    initialCostField[i][j] = 1;
            }

            foreach (Vector2Int cell in obstaclePositionList)
                initialCostField[cell.x][cell.y] = byte.MaxValue;
            
            UpdateCachedCostField();
        }

        private static readonly Vector2Int[] integrationDirections = {
            Vector2Int.up,
            Vector2Int.down,
            Vector2Int.left,
            Vector2Int.right
        };
        
        private static readonly Vector2Int[] flowDirections = {
            Vector2Int.up,
            new Vector2Int(1, 1),
            Vector2Int.right,
            new Vector2Int(1, -1),
            Vector2Int.down,
            new Vector2Int(-1, -1),
            Vector2Int.left,
            new Vector2Int(-1, 1)
        };

        public void UpdateCachedCostField()
        {
            // Create a copy of the initial cost field
            cachedCostField = new byte[width][];
            for (int i = 0; i < width; i++)
                cachedCostField[i] = new byte[height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    cachedCostField[i][j] = initialCostField[i][j];
            }
            
            // Update based on collider obstacle set
            foreach (Collider2D obstacle in colliderObstacleSet)
            {
                Bounds obstacleBounds = obstacle.bounds;
                
                Vector2Int bottomLeftCell = WorldToCell(new Vector3(obstacleBounds.min.x, obstacleBounds.min.y, 0));
                Vector2Int topRightCell = WorldToCell(new Vector3(obstacleBounds.max.x, obstacleBounds.max.y, 0));
                
                bottomLeftCell.x = Mathf.Clamp(bottomLeftCell.x, 0, width - 1);
                bottomLeftCell.y = Mathf.Clamp(bottomLeftCell.y, 0, height - 1);
                topRightCell.x = Mathf.Clamp(topRightCell.x, 0, width - 1);
                topRightCell.y = Mathf.Clamp(topRightCell.y, 0, height - 1);
                
                for (int i = bottomLeftCell.x; i <= topRightCell.x; i++)
                {
                    for (int j = bottomLeftCell.y; j <= topRightCell.y; j++)
                        cachedCostField[i][j] = byte.MaxValue;
                }
            }

        }
        
        public void UpdateFlow(Vector2 goal)
        {
            // Convert goal to a cell
            Vector2Int goalCell = WorldToCell(goal);
            
            // Use cached cost field
            byte originalValueAtGoalCell = cachedCostField[goalCell.x][goalCell.y];
            cachedCostField[goalCell.x][goalCell.y] = 0;
            
            // Generate integration field
            ushort[][] integrationField = new ushort[width][];
            for (int i = 0; i < width; i++)
                integrationField[i] = new ushort[height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    integrationField[i][j] = ushort.MaxValue;
            }
            integrationField[goalCell.x][goalCell.y] = 0;
            Queue<Vector2Int> queue = new Queue<Vector2Int>();
            queue.Enqueue(goalCell);
            while (queue.Count > 0)
            {
                Vector2Int current = queue.Dequeue();

                foreach (Vector2Int dir in integrationDirections)
                {
                    Vector2Int next = current + dir;
                    
                    if (next.x < 0 || next.x >= width || next.y < 0 || next.y >= height)
                        continue;
                    
                    ushort newCost = (ushort)(integrationField[current.x][current.y] + cachedCostField[next.x][next.y]);
                    
                    if (newCost < integrationField[next.x][next.y])
                    {
                        integrationField[next.x][next.y] = newCost;
                        queue.Enqueue(next);
                    }
                }
            }
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (cachedCostField[i][j] == byte.MaxValue)
                        integrationField[i][j] = ushort.MaxValue;
                }
            }
            
            // Restore the cached cost field
            cachedCostField[goalCell.x][goalCell.y] = originalValueAtGoalCell;

            // Generate flow field
            Cell[][] cells = new Cell[width][];
            for (int i = 0; i < width; i++)
            {
                cells[i] = new Cell[height];
                for (int j = 0; j < height; j++)
                    cells[i][j] = Cell.Up;
            }
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (integrationField[i][j] == ushort.MaxValue)
                        cells[i][j] = Cell.Obstacle;
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (cells[i][j] == Cell.Goal || cells[i][j] == Cell.Obstacle)
                        continue;

                    ushort lowestCost = ushort.MaxValue;
                    Vector2Int bestDirection = Vector2Int.zero;

                    foreach (Vector2Int dir in flowDirections)
                    {
                        Vector2Int next = new Vector2Int(i + dir.x, j + dir.y);
                        
                        if (next.x >= 0 && next.x < width && next.y >= 0 && next.y < height)
                        {
                            if (integrationField[next.x][next.y] < lowestCost)
                            {
                                lowestCost = integrationField[next.x][next.y];
                                bestDirection = dir;
                            }
                        }
                    }
                    
                    if (bestDirection != Vector2Int.zero)
                        cells[i][j] = DirectionToCell(bestDirection);
                }
            }
            cells[goalCell.x][goalCell.y] = Cell.Goal;
            Cells = cells;

            return;
            
            Cell DirectionToCell(Vector2Int direction)
            {
                return direction.x switch {
                    0 when direction.y == 1 => Cell.Up,
                    1 when direction.y == 1 => Cell.UpRight,
                    1 when direction.y == 0 => Cell.Right,
                    1 when direction.y == -1 => Cell.DownRight,
                    0 when direction.y == -1 => Cell.Down,
                    -1 when direction.y == -1 => Cell.DownLeft,
                    -1 when direction.y == 0 => Cell.Left,
                    -1 when direction.y == 1 => Cell.UpLeft,
                    _ => throw new InvalidOperationException("Invalid direction.")
                };
            }
        }
        
        public Vector2Int WorldToCell(Vector3 worldPosition)
        {
            Vector3 gridCenterOffset = new Vector3((width * cellSize.x) * 0.5f, (height * cellSize.y) * 0.5f, 0);
            Vector3 adjustedPosition = worldPosition - (transform.position - gridCenterOffset);
            
            int cellX = Mathf.FloorToInt(adjustedPosition.x / cellSize.x);
            int cellY = Mathf.FloorToInt(adjustedPosition.y / cellSize.y);
            
            cellX = Mathf.Clamp(cellX, 0, width - 1);
            cellY = Mathf.Clamp(cellY, 0, height - 1);

            return new Vector2Int(cellX, cellY);
        }

        public Vector2 CellToWorld(Vector2Int cellPosition)
        {
            Vector3 gridCenterOffset = new Vector3((width * cellSize.x) * 0.5f, (height * cellSize.y) * 0.5f, 0);
            Vector3 basePosition = transform.position - gridCenterOffset;
    
            float worldX = (cellPosition.x * cellSize.x) + cellSize.x * 0.5f;
            float worldY = (cellPosition.y * cellSize.y) + cellSize.y * 0.5f;
            Vector2 worldPosition = new Vector3(worldX, worldY) + basePosition;
    
            return worldPosition;
        }

        public void ClearObstacles()
        {
            obstaclePositionList.Clear();
#if UNITY_EDITOR
            if (EditorApplication.isPlaying)
                colliderObstacleSet.Clear();
#else
            colliderObstacleSet.Clear();
#endif
        }

        public void AddColliderObstacle(Collider2D collider)
        {
            colliderObstacleSet.Add(collider);
            
            Bounds obstacleBounds = collider.bounds;
            
            Vector2Int bottomLeftCell = WorldToCell(new Vector3(obstacleBounds.min.x, obstacleBounds.min.y, 0));
            Vector2Int topRightCell = WorldToCell(new Vector3(obstacleBounds.max.x, obstacleBounds.max.y, 0));
                
            bottomLeftCell.x = Mathf.Clamp(bottomLeftCell.x, 0, width - 1);
            bottomLeftCell.y = Mathf.Clamp(bottomLeftCell.y, 0, height - 1);
            topRightCell.x = Mathf.Clamp(topRightCell.x, 0, width - 1);
            topRightCell.y = Mathf.Clamp(topRightCell.y, 0, height - 1);
                
            for (int i = bottomLeftCell.x; i <= topRightCell.x; i++)
            {
                for (int j = bottomLeftCell.y; j <= topRightCell.y; j++)
                    cachedCostField[i][j] = byte.MaxValue;
            }
        }

        public void RemoveColliderObstacle(Collider2D collider)
        {
            if (colliderObstacleSet.Contains(collider))
            {
                colliderObstacleSet.Remove(collider);
                UpdateCachedCostField();   
            }
        }

        public void DetectColliderObstacles()
        {
            NavigationObstacle[] obstacles = FindObjectsOfType<NavigationObstacle>();
            foreach (NavigationObstacle obstacle in obstacles)
            {
                if (obstacle.Type != type)
                    return;
                
                Collider2D[] colliders = obstacle.GetComponentsInChildren<Collider2D>();
                foreach (Collider2D collider in colliders)
                {
                    Bounds obstacleBounds = collider.bounds;
            
                    Vector2Int bottomLeftCell = WorldToCell(new Vector3(obstacleBounds.min.x, obstacleBounds.min.y, 0));
                    Vector2Int topRightCell = WorldToCell(new Vector3(obstacleBounds.max.x, obstacleBounds.max.y, 0));
                
                    bottomLeftCell.x = Mathf.Clamp(bottomLeftCell.x, 0, width - 1);
                    bottomLeftCell.y = Mathf.Clamp(bottomLeftCell.y, 0, height - 1);
                    topRightCell.x = Mathf.Clamp(topRightCell.x, 0, width - 1);
                    topRightCell.y = Mathf.Clamp(topRightCell.y, 0, height - 1);
                
                    for (int i = bottomLeftCell.x; i <= topRightCell.x; i++)
                    {
                        for (int j = bottomLeftCell.y; j <= topRightCell.y; j++)
                        {
                            Vector2Int cell = new Vector2Int(i, j);
                            if (!obstaclePositionList.Contains(cell))
                                obstaclePositionList.Add(cell);   
                        }
                    }
                }
            }
        }
        
#if UNITY_EDITOR
        [SerializeField] private bool drawGizmosOnSelected = true;
        
        [SerializeField, Range(0f, 5f)] private float gizmosLineThickness = 0f;

        [SerializeField] private bool drawDirections = false;
        
        [SerializeField, Range(1, 5)] private int obstacleDrawingPenSize = 1;
        public int ObstacleDrawingPenSize => obstacleDrawingPenSize;

        private void OnDrawGizmosSelected()
        {
            if (drawGizmosOnSelected)
                DrawGizmos();
        }

        private void OnDrawGizmos()
        {
            if (!drawGizmosOnSelected)
                DrawGizmos();
        }

        private void DrawGizmos()
        {
            Handles.color = Color.white;
            
            Vector2 gridSize = new Vector2(width * cellSize.x, height * cellSize.y);
            Vector3 offset = new Vector3(-gridSize.x / 2f, -gridSize.y / 2f, 0f);
            
            for (int i = 0; i <= width; i++)
            {
                Vector3 startPos = transform.position + offset + new Vector3(i * cellSize.x, 0f, 0f);
                Vector3 endPos = startPos + new Vector3(0f, height * cellSize.y, 0f);
                Handles.DrawLine(startPos, endPos, gizmosLineThickness);
            }
            
            for (int i = 0; i <= height; i++)
            {
                Vector3 startPos = transform.position + offset + new Vector3(0f, i * cellSize.y, 0f);
                Vector3 endPos = startPos + new Vector3(width * cellSize.x, 0f, 0f);
                Handles.DrawLine(startPos, endPos, gizmosLineThickness);
            }

            if (!EditorApplication.isPlaying)
            {
                foreach (Vector2Int cell in obstaclePositionList)
                {
                    Vector2 wPos = CellToWorld(cell) + new Vector2(-cellSize.x / 2f, -cellSize.y / 2f);
                    Color color = Color.red;
                    color.a = 0.5f;
                    Handles.DrawSolidRectangleWithOutline(new Rect(wPos.x, wPos.y, cellSize.x, cellSize.y), color, color);    
                }
                return;
            }
            
            if (Cells is null)
                return;

            const string basePath = "Assets/TestScenes/Dongxin/FlowFieldNavigation/Gizmos/";
            Vector2 pos = transform.position;
            pos.x -= gridSize.x / 2f;
            pos.x += cellSize.x / 2f;
            pos.y -= gridSize.y / 2f;
            pos.y += cellSize.y / 2f;
            float originalX = pos.x;
            Color cellColor = Color.red;
            cellColor.a = 0.5f;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Cell cell = Cells[j][i];

                    if (drawDirections)
                    {
                        Gizmos.DrawIcon(pos, basePath + cell switch {
                            Cell.Obstacle => "cross",
                            Cell.Goal => "circle",
                            Cell.Up => "arrow-up",
                            Cell.UpRight => "arrow-upright",
                            Cell.Right => "arrow-right",
                            Cell.DownRight => "arrow-downright",
                            Cell.Down => "arrow-down",
                            Cell.DownLeft => "arrow-downleft",
                            Cell.Left => "arrow-left",
                            Cell.UpLeft => "arrow-upleft",
                            _ => throw new InvalidOperationException("Should never happen.")
                        } + ".png", true);
                    }
                    else if (cell == Cell.Obstacle)
                    {
                        Vector2 p = pos;
                        p.x -= cellSize.x / 2f;
                        p.y -= cellSize.y / 2f;
                        Handles.DrawSolidRectangleWithOutline(new Rect(p.x, p.y, cellSize.x, cellSize.y), 
                                                              cellColor, 
                                                              cellColor);   
                    }

                    pos.x += cellSize.x;
                }

                pos.x = originalX;
                pos.y += cellSize.y;
            }
        }
#endif
    }
}
