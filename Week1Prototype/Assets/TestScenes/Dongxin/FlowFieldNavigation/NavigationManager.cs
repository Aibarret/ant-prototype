using System;
using UnityEngine;

namespace FlowFieldNavigation
{
    public class NavigationManager : MonoBehaviour, ISingleton
    {
        /* ===== Singleton ===== */
        
        public static NavigationManager Instance { get; private set; } = null;

        private void Awake()
            => Instance = this;

        private void OnDestroy()
            => Instance = null;

        
        
        /* ===== Components ===== */
        
        [SerializeField] private FlowField groundFlowField;
        public FlowField GroundFlowField => groundFlowField;
        
        [SerializeField] private FlowField skyFlowField;
        public FlowField SkyFlowField => skyFlowField;

        // I'm so sorry
        [SerializeField] private FlowField b_groundFlowField;
        public FlowField b_GroundFlowField => b_groundFlowField;

        [SerializeField] private FlowField b_skyFlowField;
        public FlowField b_SkyFlowField => b_skyFlowField;

        [SerializeField] private FlowField c_groundFlowField;
        public FlowField c_GroundFlowField => c_groundFlowField;

        [SerializeField] private FlowField c_skyFlowField;
        public FlowField c_SkyFlowField => c_skyFlowField;

        [SerializeField] private Transform target;
        [SerializeField] private QueenManager qm;


        public bool multiTarget;
        public Transform Target
        {
            get => target;
            set => target = value;
        }

        
        
        private Vector2Int previousTargetGroundCell;
        private Vector2Int previousTargetSkyCell;
        private Vector2Int b_previousTargetGroundCell;
        private Vector2Int b_previousTargetSkyCell;
        private Vector2Int c_previousTargetGroundCell;
        private Vector2Int c_previousTargetSkyCell;
        
        private void Start()
        {
            Vector2 pos = target.position;
            groundFlowField.UpdateFlow(pos);
            skyFlowField.UpdateFlow(pos);

            if (b_GroundFlowField)
            {
                b_groundFlowField.UpdateFlow(b_groundFlowField.altTarget.position);
                b_skyFlowField.UpdateFlow(b_skyFlowField.altTarget.position);
            }

            if (c_groundFlowField)
            {
                c_groundFlowField.UpdateFlow(c_groundFlowField.altTarget.position);
                c_skyFlowField.UpdateFlow(c_skyFlowField.altTarget.position);
            }
            
            previousTargetGroundCell = groundFlowField.WorldToCell(pos);
            previousTargetSkyCell = skyFlowField.WorldToCell(pos);

            if (b_GroundFlowField)
            {
                pos = b_groundFlowField.altTarget.position;

                b_previousTargetGroundCell = b_groundFlowField.WorldToCell(pos);
                b_previousTargetSkyCell = b_skyFlowField.WorldToCell(pos);

            }
            
            if (c_groundFlowField)
            {
                pos = c_groundFlowField.altTarget.position;

                c_previousTargetGroundCell = c_groundFlowField.WorldToCell(pos);
                c_previousTargetSkyCell = c_skyFlowField.WorldToCell(pos);
            }


        }

        private void Update()
        {
            Vector2 pos = target.position;
            Vector2Int targetGroundCell = groundFlowField.WorldToCell(pos);
            Vector2Int targetSkyCell = skyFlowField.WorldToCell(pos);

            if (targetGroundCell != previousTargetGroundCell)
            {
                previousTargetGroundCell = targetGroundCell;
                groundFlowField.UpdateFlow(pos);
            }

            if (targetSkyCell != previousTargetSkyCell)
            {
                previousTargetSkyCell = targetSkyCell;
                skyFlowField.UpdateFlow(pos);
            }

            if (b_groundFlowField)
            {
                pos = b_groundFlowField.altTarget.position;
                targetGroundCell = b_groundFlowField.WorldToCell(pos);
                targetSkyCell = b_skyFlowField.WorldToCell(pos);

                if (targetGroundCell != b_previousTargetGroundCell)
                {
                    b_previousTargetGroundCell = targetGroundCell;
                    b_groundFlowField.UpdateFlow(pos);
                }

                if (targetSkyCell != b_previousTargetSkyCell)
                {
                    b_previousTargetSkyCell = targetSkyCell;
                    b_skyFlowField.UpdateFlow(pos);
                }
            }

            if (c_groundFlowField)
            {
                pos = c_groundFlowField.altTarget.position;
                targetGroundCell = c_groundFlowField.WorldToCell(pos);
                targetSkyCell = c_skyFlowField.WorldToCell(pos);

                if (targetGroundCell != c_previousTargetGroundCell)
                {
                    c_previousTargetGroundCell = targetGroundCell;
                    c_groundFlowField.UpdateFlow(pos);
                }

                if (targetSkyCell != c_previousTargetSkyCell)
                {
                    c_previousTargetSkyCell = targetSkyCell;
                    c_skyFlowField.UpdateFlow(pos);
                }
            }
        }
        
        public Vector2? GetDirection(Vector2 position, bool isOnGround, int queenIndex = 0)
        {
            if (target == null)
                return null;

            FlowField flowField;

            switch (queenIndex)
            {
                case 1:
                    flowField = isOnGround ? b_groundFlowField : b_skyFlowField;
                    break;
                case 2:
                    flowField = isOnGround ? c_groundFlowField : c_skyFlowField;
                    break;
                default:
                    flowField = isOnGround ? groundFlowField : skyFlowField;
                    break;
            }




            Vector2Int cellPoint = flowField.WorldToCell(position);
            Cell cell = flowField.Cells[cellPoint.x][cellPoint.y];
            return cell switch {
                Cell.Obstacle => null,
                Cell.Goal => Vector2.zero,
                Cell.Up => Vector2.up,
                Cell.UpRight => new Vector2(1f, 1f).normalized,
                Cell.Right => Vector2.right,
                Cell.DownRight => new Vector2(1f, -1f).normalized,
                Cell.Down => Vector2.down,
                Cell.DownLeft => new Vector2(-1f, -1f).normalized,
                Cell.Left => Vector2.left,
                Cell.UpLeft => new Vector2(-1f, 1f).normalized,
                _ => throw new InvalidOperationException("Should never happen.")
            };
        }
    }   
}
