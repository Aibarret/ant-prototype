﻿using UnityEngine;

namespace FlowFieldNavigation
{
    public sealed class NavigationAgent : MonoBehaviour
    {
        [SerializeField] private DropshadowController dropshadow;
        [SerializeField] private EnemyType enemyType;

        [SerializeField] private float turningSpeed = 5f;

        public bool IsUsingGroundFlowField => dropshadow.State == FlyingState.GROUND;

        public float Speed => enemyType.speed;
        
        private NavigationManager navigationManager;

        private Vector2 direction = Vector2.zero;

        private Vector2 prevPos;

        private void Start()
        {
            navigationManager = NavigationManager.Instance;
            Vector2? optDir = navigationManager.GetDirection(transform.position, IsUsingGroundFlowField, enemyType.enemy.assignedQueenIndex);
            if (optDir.HasValue)
            {
                direction = optDir.Value;
                prevPos = transform.position;
            }
        }

        private void FixedUpdate()
        {
            Vector2? optDir = navigationManager.GetDirection(transform.position, IsUsingGroundFlowField, enemyType.enemy.assignedQueenIndex);
            Vector2 dir;
            if (optDir.HasValue)
            {
                dir = optDir.Value;
                prevPos = transform.position;
            }
            else
                dir = (prevPos - (Vector2)(transform.position)).normalized;
            direction = Vector2.MoveTowards(direction, dir, turningSpeed * Time.deltaTime);
            transform.Translate(Speed * Time.deltaTime * direction);
            
            
        }
    }
}
