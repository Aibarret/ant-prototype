﻿namespace FlowFieldNavigation
{
    public enum Cell : byte
    {
        Obstacle,
        Goal,
        Up, UpRight, Right, DownRight, Down, DownLeft, Left, UpLeft,
    }
}
