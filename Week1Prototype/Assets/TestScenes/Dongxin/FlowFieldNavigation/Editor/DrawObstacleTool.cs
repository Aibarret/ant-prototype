using UnityEditor;
using UnityEditor.EditorTools;
using UnityEngine;

namespace FlowFieldNavigation
{
    [EditorTool("Draw Obstacle Tool", typeof(FlowField))]
    public class DrawObstacleTool : EditorTool
    {
        private enum ToolStatus
        {
            None,
            Drawing,
            Erasing
        }

        private ToolStatus toolStatus;

        public override void OnActivated()
            => toolStatus = ToolStatus.None;

        public override void OnWillBeDeactivated()
            => toolStatus = ToolStatus.None;

        public override void OnToolGUI(EditorWindow window)
        {
            // Take full control of the editor
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Keyboard));

            // Get flow field
            FlowField flowField = target as FlowField;
            if (flowField == null)
                return;
            
            // Check mouse inputs
            Event e = Event.current;
            if (e.type == EventType.MouseDown)
            {
                if (toolStatus == ToolStatus.None)
                {
                    if (e.button == 0)
                    {
                        toolStatus = ToolStatus.Drawing;
                        e.Use();
                        Undo.RecordObject(target, "Drawing");
                    }
                    else if (e.button == 1)
                    {
                        toolStatus = ToolStatus.Erasing;
                        e.Use();
                        Undo.RecordObject(target, "Erasing");
                    }   
                }
            }
            else if (e.type == EventType.MouseUp)
            {
                if (e.button is 0 or 1)
                {
                    toolStatus = ToolStatus.None;
                    e.Use();
                    EditorUtility.SetDirty(target);
                }
            }

            // Drawing / erasing
            if (toolStatus != ToolStatus.None)
            {
                Ray worldRay = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                Vector3 worldPos = worldRay.origin;

                Vector2Int centerCell = flowField.WorldToCell(worldPos);

                int penSize = flowField.ObstacleDrawingPenSize - 1;
                for (int x = -penSize; x <= penSize; x++)
                {
                    for (int y = -penSize; y <= penSize; y++)
                    {
                        Vector2Int cell = new Vector2Int(centerCell.x + x, centerCell.y + y);
                        if (cell.x < 0 || cell.x >= flowField.Width || cell.y < 0 || cell.y >= flowField.Height)
                            continue;
                        if (x * x + y * y <= penSize * penSize)
                        {
                            if (toolStatus == ToolStatus.Drawing)
                            {
                                if (!flowField.ObstaclePositionList.Contains(cell))
                                    flowField.ObstaclePositionList.Add(cell);
                            }
                            else if (toolStatus == ToolStatus.Erasing)
                            {
                                if (flowField.ObstaclePositionList.Contains(cell))
                                    flowField.ObstaclePositionList.Remove(cell);
                            }
                        }
                    }
                }
                
                window.Repaint();
            }
        }
    }   
}
