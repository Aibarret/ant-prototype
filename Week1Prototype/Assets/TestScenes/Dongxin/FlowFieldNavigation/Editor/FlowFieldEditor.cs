﻿using System;
using UnityEditor;
using UnityEngine;

namespace FlowFieldNavigation
{
    [CustomEditor(typeof(FlowField))]
    public sealed class FlowFieldEditor : Editor
    {
        private SerializedProperty cellSizeProp;
        private SerializedProperty widthProp;
        private SerializedProperty heightProp;
        private SerializedProperty typeProp;
        private SerializedProperty altTarget;

        private SerializedProperty drawGizmosOnSelectedProp;
        private SerializedProperty gizmosLineThicknessProp;
        private SerializedProperty drawDirectionsProp;
        private SerializedProperty obstacleDrawingPenSizeProp;

        private void OnEnable()
        {
            cellSizeProp = serializedObject.FindProperty("cellSize");
            widthProp = serializedObject.FindProperty("width");
            heightProp = serializedObject.FindProperty("height");
            typeProp = serializedObject.FindProperty("type");
            altTarget = serializedObject.FindProperty("altTarget");
            
            drawGizmosOnSelectedProp = serializedObject.FindProperty("drawGizmosOnSelected");
            gizmosLineThicknessProp = serializedObject.FindProperty("gizmosLineThickness");
            drawDirectionsProp = serializedObject.FindProperty("drawDirections");
            obstacleDrawingPenSizeProp = serializedObject.FindProperty("obstacleDrawingPenSize");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);
            EditorGUILayout.PropertyField(cellSizeProp);
            EditorGUILayout.PropertyField(widthProp);
            EditorGUILayout.PropertyField(heightProp);
            EditorGUILayout.PropertyField(typeProp);
            EditorGUILayout.PropertyField(altTarget);

            EditorGUI.EndDisabledGroup();
            
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Gizmos", EditorStyles.boldLabel);
            
            EditorGUILayout.PropertyField(drawGizmosOnSelectedProp);
            EditorGUILayout.PropertyField(gizmosLineThicknessProp);
            EditorGUILayout.PropertyField(drawDirectionsProp);
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);
            EditorGUILayout.PropertyField(obstacleDrawingPenSizeProp);
            EditorGUI.EndDisabledGroup();
            
            FlowField flowField = target as FlowField;
            if (flowField == null)
                return;
            
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);
            if (GUILayout.Button("Clear Obstacles"))
            {
                Undo.RecordObject(target, "Clear Obstacles");
                flowField.ClearObstacles();
                EditorUtility.SetDirty(flowField);
            }
            
            if (GUILayout.Button("Detect Collider Obstacles"))
            {
                Undo.RecordObject(target, "Detect Collider Obstacles");
                flowField.DetectColliderObstacles();
                EditorUtility.SetDirty(flowField);
            }
            EditorGUI.EndDisabledGroup();
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}
