using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    [Header("Object Refs")]
    public GameObject topLeftBoundry;
    public GameObject bottomRightBoundry;
    [Header("Stats")]
    public CameraState currentState;
    public int panSpeed;
    public int followSpeed;

    [Header("Input System")]
    public Vector2 playerInput;
    public Vector2 cursorInput;
    public Vector2 mousePosition;
    public float focusQueenInput;
    public bool focusQueenCalled;
    public bool isDragging;
    public float holdPan;
    public Vector3 originPosition;


    [HideInInspector]
    public bool CutscenePan = false;
    [HideInInspector] public Vector3 cutscenePosn;
    [HideInInspector] public Vector3 returnPosn;
    [HideInInspector] public Vector3 originPosn;
    [HideInInspector] public float cutsceneSpeed = 0f;
    [HideInInspector] public float elapsedFrames = 0;
    [HideInInspector] public float elapsedTimerFrames = 0;
    [HideInInspector] public float timerSecs = 0;

    public delegate void timerCall();
    public timerCall timerCallList;

    public delegate void returnCall();
    public returnCall callList;

    private LevelManager levelManager;
    private Camera mainCamera;
    private Camera cullingCamera;
    [SerializeField] private float extraFrustumCullRange = 10;

    private void Awake()
    {
        GlobalVariables.cam = gameObject.GetComponent<Camera>();
        mainCamera = GetComponent<Camera>();
        SetupCullingCamera();
    }

    private void Start()
    {
        levelManager = LevelManager.instance;
        changeState(new PanState());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Get the player's input from the Level Manager
        playerInput = levelManager.moveAction.ReadValue<Vector2>();
        mousePosition = levelManager.mousePosition.ReadValue<Vector2>();
        holdPan = levelManager.playerPanCamera.ReadValue<float>();
        focusQueenInput = levelManager.toQueen.ReadValue<float>();

        if (holdPan > 0 && !isDragging)
        {
            originPosition = Camera.main.ScreenToWorldPoint(levelManager.mousePosition.ReadValue<Vector2>());
            isDragging = true;
        }
        else if (holdPan == 0)
        {
            isDragging = false;
        }

        // if queen follow it toggled and there are no other directional input, change state to the follow state
        if (((focusQueenInput > 0 || focusQueenCalled) && playerInput == Vector2.zero && holdPan == 0 && currentState.typeName != "Follow") && !GlobalVariables.gamePaused)
        {
            focusQueenCalled = false;
            changeState(new FollowState());
        }
        else if (((playerInput != Vector2.zero || holdPan > 0) && currentState.typeName != "Pan") && !GlobalVariables.gamePaused)
        {
            changeState(new PanState());
        }

        
        if (currentState != null)
        {
            currentState.executeState(this);
        }


        // Enforce Boundries

        if (transform.position.x > bottomRightBoundry.transform.position.x)
        {
            transform.position = new Vector3(bottomRightBoundry.transform.position.x, transform.position.y, transform.position.z);
        }
        if (transform.position.x < topLeftBoundry.transform.position.x)
        {
            transform.position = new Vector3(topLeftBoundry.transform.position.x, transform.position.y, transform.position.z);
        }
        if (transform.position.y < bottomRightBoundry.transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, bottomRightBoundry.transform.position.y, transform.position.z);
        }
        if (transform.position.y > topLeftBoundry.transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, topLeftBoundry.transform.position.y, transform.position.z);
        }

        // Adjust field of view for frustum culling matrix to give some extra space so billboard sprites dont get culled while on screen
        cullingCamera.fieldOfView = mainCamera.fieldOfView + extraFrustumCullRange;
        mainCamera.cullingMatrix = cullingCamera.cullingMatrix;
    }

    public void FocusQueen()
    {
        focusQueenCalled = true;
    }

    public void cursorPushingCamera(Vector3 direction)
    {
        cursorInput = direction;
    }

    private void SetupCullingCamera()
    {
        var cullingCamGo = new GameObject("CullingCamera");
        cullingCamGo.SetActive(false);
        cullingCamGo.transform.SetParent(transform, false);
        cullingCamera = cullingCamGo.AddComponent<Camera>();
    }

    public void changeState(CameraState state)
    {
        if (currentState != null)
        {
            currentState.endState(this);
        }

        currentState = state;
        currentState.startState(this);
    }
    

    public void toggleCutsceneMode(bool isOn)
    {
        if (isOn)
        {
            changeState(new CutsceneState());
        }
        else
        {
            changeState(new PanState());
        }
    }

    public void sendSectionCam(Vector2 startPosn, Vector2 endPosn)
    {
        /*if (DEBUG_setToSectionMode)
        {
            //secStartPosn = new Vector3(startPosn.x, startPosn.y, -10);
            secTargetPosn = new Vector3(endPosn.x, endPosn.y, -10);
        }*/
    }

    public void sendCutsceneCamToPoint(Vector3 posn, float time, returnCall function, timerCall timerFunction = null, float timerSecs = 0)
    {
        timerCallList = timerFunction;
        this.timerSecs = timerSecs;
        callList = function;
        returnPosn = transform.position;
        
        elapsedFrames = 0f;
        cutscenePosn = new Vector3(posn.x, posn.y, transform.position.z);
        cutsceneSpeed = time;
        CutscenePan = true;
    }

    public void returnCutsceneCam()
    {
        callList = endCutscene;
        timerCallList = null;
        cutscenePosn = originPosn;
        returnPosn = transform.position;
        

        elapsedFrames = 0f;
        cutsceneSpeed = .5f;
        CutscenePan = true;
    }

    public void cutsceneCamComplete()
    {
        if (timerCallList == null)
        {
            CutscenePan = false;
            elapsedFrames = 0;
        }
        if (callList != null)
        {
            callList();

        }
    }

    public void cutSceneTimerComplete()
    {
        CutscenePan = false;
        elapsedTimerFrames = 0;
        timerCallList();
    }

    public void endCutscene()
    {
        CutscenePan = false;
        elapsedFrames = 0;

        changeState(new PanState());
        
    }
    
}

public abstract class CameraState
{
    public abstract string typeName { get; }

    public abstract void executeState(CameraController sm);
    public abstract void startState(CameraController sm);
    public abstract void endState(CameraController sm);

}

public class CutsceneState : CameraState
{
    public override string typeName => "Cutscene";

    public override void endState(CameraController sm)
    {
        GlobalVariables.eventCall(WaypointEvent.endCutsceneMode, -1);
        sm.cutsceneCamComplete();
    }

    public override void executeState(CameraController sm)
    {
        if (sm.CutscenePan && sm.elapsedFrames < sm.cutsceneSpeed)
        {
            sm.transform.position = Vector3.Lerp(sm.returnPosn, sm.cutscenePosn, sm.elapsedFrames / sm.cutsceneSpeed);
            sm.elapsedFrames += Time.unscaledDeltaTime;
            //Debug.Log("Lerp: " + sm.elapsedFrames / sm.cutsceneSpeed);

            if (sm.elapsedFrames >= sm.cutsceneSpeed)
            {
                sm.cutsceneCamComplete();
            }
        }
    }

    public override void startState(CameraController sm)
    {
        sm.originPosn = sm.transform.position;

    }
}


public class FollowState : CameraState
{
    public override string typeName => "Follow";

    public override void endState(CameraController sm)
    {
        
    }

    public override void executeState(CameraController sm)
    {
        if (sm.transform.position != GlobalVariables.queen.transform.position)
        {
            //Debug.Log("Queen position: " + new Vector3(GlobalVariables.queen.transform.position.x, GlobalVariables.queen.transform.position.y, sm.transform.position.z) + " , current position at: " + sm.transform.position);
            sm.transform.position = Vector3.MoveTowards(sm.transform.position, new Vector3(QueenManager.Instance.getQueen().transform.position.x, QueenManager.Instance.getQueen().transform.position.y, sm.transform.position.z), sm.followSpeed * Time.deltaTime);
        }
    }

    public override void startState(CameraController sm)
    {
        
    }
}

[System.Serializable]
public class PanState : CameraState
{
    public override string typeName => "Pan";

    public override void endState(CameraController sm)
    {
        if (sm.holdPan > 0)
        {
            Vector3 difference = Camera.main.ScreenToWorldPoint(sm.mousePosition) - sm.transform.position;
            sm.transform.position = sm.originPosition - difference;
            //sm.transform.Translate(sm.mouseDelta * (sm.panSpeed * 2) * Time.deltaTime, Space.World);
        }
    }

    public override void executeState(CameraController sm)
    {
        Vector2 playerInput = sm.playerInput;
        bool dragPanMode = sm.holdPan > 0;
        
        if (dragPanMode)
        {
            Vector3 difference = Camera.main.ScreenToWorldPoint(sm.mousePosition) - sm.transform.position;
            sm.transform.position = sm.originPosition - difference;
            //sm.transform.Translate(sm.mouseDelta * (sm.panSpeed * 2) * Time.deltaTime, Space.World);
        }
        else
        {
            if (playerInput.y > 0)
            {
                sm.transform.Translate(Vector2.up * LevelManager.instance.cameraSpeed * Time.deltaTime, Space.World);
            }
            if (playerInput.y < 0)
            {
                sm.transform.Translate(Vector2.down * LevelManager.instance.cameraSpeed * Time.deltaTime, Space.World);
            }
            if (playerInput.x > 0)
            {
                sm.transform.Translate(Vector2.right * LevelManager.instance.cameraSpeed * Time.deltaTime, Space.World);
            }
            if (playerInput.x < 0)
            {
                sm.transform.Translate(Vector2.left * LevelManager.instance.cameraSpeed * Time.deltaTime, Space.World);
            }

            if (sm.cursorInput != Vector2.zero)
            {
                sm.transform.Translate(sm.cursorInput * LevelManager.instance.cameraSpeed * Time.deltaTime, Space.World);
                sm.cursorInput = Vector2.zero;
            }
        }
    }

    public override void startState(CameraController sm)
    {
        /*startPosn = sm.transform.position;
        startSize = sm.GetComponent<Camera>().orthographicSize;
        targetPosn = new Vector3(GlobalVariables.queen.transform.position.x, GlobalVariables.queen.transform.position.y, sm.transform.position.z);

        isZooming = true;*/
    }

    
}






