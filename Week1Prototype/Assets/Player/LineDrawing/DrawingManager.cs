using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class DrawingManager : MonoBehaviour
{
    List<Vector3> waypoints = new List<Vector3>();
    List<Formation> formationList = new List<Formation>();

    public bool limitedDrawMode = true;
    public bool paused = false;
    public bool erasing = false;

    [Header("Containers")]
    public Transform AntContainer;
    public Transform emptyAntContainer;
    public GameObject overwriteFormationList;

    [Header("Object & Component Refs")]
    public GameObject cursor;
    public LineRenderer lineRenderer;
    public Timer refillTimer;
    public Animator optionsMenuAnimator;
    public VFXManager vfxManager;
    public GameObject emptyUnitPrefab;
    public GameObject emptyUnitRangePrefab;
    public AntSelectionButton button;
    public FMODUnity.StudioEventEmitter emitter;

    private LineRenderer invalidZoneRenderer;

    public delegate void FormationWasChanged(Formation form);
    public FormationWasChanged formationChangeCallList;

    [HideInInspector] public int section = 0;
    [HideInInspector] public int totalSections = 1;
    
    [Header("Stats")]
    [SerializeField] private int currentPheromones;
    [SerializeField] private int maxPheromones;
    [SerializeField] private float cursorSpeed;
    [SerializeField] private float cursorPanBuffer;
    [SerializeField] private float drawingWidth;

    public float refillFrequency;
    public int refillAmount;

    [Header("Fire Spray")]
    [SerializeField] private int fireSprayState;
    [SerializeField] private List<int> stateDurations;
    public List<FireSprayStatus> expendedFireSprayStatus = new List<FireSprayStatus>();
    [HideInInspector] public bool fireSprayButtonPressed;
    private float fireSprayTime;
    public bool isUsingFireSpray;
    

    [Header("Unit Pools")]
    [SerializeField] private int emptyUnitPoolAmount; // should be the max amount of units that will be displayed when drawing.

    private Dictionary<string, List<GameObject>> activeUnits = new Dictionary<string, List<GameObject>>();
    private Dictionary<string, List<GameObject>> inactiveUnits = new Dictionary<string, List<GameObject>>();
    private List<EmptyUnit> emptyUnitPool = new List<EmptyUnit>();
    private List<RangeIndicator> emptyUnitRanges = new List<RangeIndicator>();

    private Cursor cursorScript;
    private Vector2 cursorScreenPositionOffset;
    private int activeFormationIndex = 0;
    private int spentPheromones = 0;
    private int currentSelection = 0;
    private float bumperTimer = 0;

    [Header("Drawing Conditions")]
    [HideInInspector] public bool listeningForFormChange = false;
    [HideInInspector] public bool optionsMenuOpen = false;
    [HideInInspector] public bool controllerMode = false;
    private bool isDrawing = false;
    private bool lookingForValidDrawingZone;
    private bool validDrawingZone = true;
    private bool doNotSpawnOnClick = false;
    private bool waitingForDrawButtonUp = false;

    // LevelManager to get KeyBindings
    private LevelManager levelManager;

    public bool waitForDrawButtonRelease
    {
        set { waitingForDrawButtonUp = value; }
    }

    public bool spawningOnClick
    {
        set { doNotSpawnOnClick = value; }
    }

    public bool toggleDrawZone
    {
        set { validDrawingZone = value; }
    }

    public int CurrentPheromones
    {
        get => currentPheromones;
        set
        {
            if (currentPheromones > maxPheromones)
            {
                currentPheromones = maxPheromones;
            }
            else
            {
                currentPheromones = value;
            }
            
            GlobalVariables.ui.pheromoneMeter.updateOnlyFillMeter(CurrentPheromones, MaxPheromones);
        }
    }

    public int MaxPheromones 
    {
        get => maxPheromones;
        set
        {
            maxPheromones = value;
            GlobalVariables.ui.pheromoneMeter.updateOnlyFillMeter(CurrentPheromones, MaxPheromones);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        levelManager = LevelManager.instance;

        cursorScript = cursor.GetComponent<Cursor>();
        invalidZoneRenderer = emptyAntContainer.GetComponent<LineRenderer>();

        refillTimer.startTimer(refillFrequency, onRefillTimeOut, true);
        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
    }

    
    public void setFormRefs()
    {
        /*if (overwriteFormationList.transform.childCount > 0)
        {
            foreach (Formation formation in overwriteFormationList.transform.GetComponentsInChildren<Formation>())
            {
                formationList.Add(formation);
            }
        }
        else
        {
        }*/
        formationList = new List<Formation>(GlobalVariables.optionsMenu.GetValidFormationUnits());
        setUpEmptyUnitPool();
        setUpUnitPool();
        GlobalVariables.ui.palette.updatePaletteFormations(formationList);


    }

    void Update()
    {
        if (!levelManager.playerSelect.enabled)
        {
            levelManager.playerSelect.Enable();
        }

        #region Establish Player Input

        
        float playerSelect = levelManager.playerSelect.ReadValue<float>();
        float playerErase = levelManager.cancelDraw.ReadValue<float>();
        float useFireSrapy = levelManager.useFireSpray.ReadValue<float>();
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(levelManager.mousePosition.ReadValue<Vector2>());
        Vector2 mouseScreenPosition = levelManager.mousePosition.ReadValue<Vector2>();
        Vector2 mouseDelta = levelManager.mouseDelta.ReadValue<Vector2>();
        Vector2 controllerInput = levelManager.controllerPanCursor.ReadValue<Vector2>();
        Vector2 centerPoint = new Vector2(Screen.width * .5f, Screen.height * .5f);

        Vector3 mousePosn;
        #endregion
        #region Cursor Handling 

        // if the player starts moving the camera with a controller, lock the mouse and manually start moving around the cursor object
        // switching over to the controller mode used in the levelmanager


        if (UnityEngine.Cursor.lockState != CursorLockMode.Locked && LevelManager.instance.isUsingController)
        {
            
            // if switching to controller mode, lock the cursor and record the offset where the mouse is to the center of the screen
            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
            GlobalVariables.ui.screenCursor.toggleControllerMode(true);
            cursorScreenPositionOffset = levelManager.mousePosition.ReadValue<Vector2>() - centerPoint;
            controllerMode = true;
        }
        else if (UnityEngine.Cursor.lockState == CursorLockMode.Locked && !LevelManager.instance.isUsingController)
        {
            // if switching off of controller mode, unlock the cursor
            UnityEngine.Cursor.lockState = CursorLockMode.None;
            GlobalVariables.ui.screenCursor.toggleControllerMode(false);
            controllerMode = false;
        }

        // Determine what to do depending on if the cursor is locked or not
        if (LevelManager.instance.isUsingController)
        {
            // if in controller mode, move the cursor offset by the controller input, then move the camera if at the bounds of the screen

            if (LevelManager.instance.centerControllerMouse.WasPressedThisFrame())
            {
                StopDrawing();
                cursorScreenPositionOffset = Vector2.zero;
            }
            else
            {
                if (GlobalVariables.tutorialDrawing && !optionsMenuOpen)
                {
                    cursorScreenPositionOffset += controllerInput * LevelManager.instance.cursorSpeed * 100 * Time.unscaledDeltaTime;
                }
                else
                {
                    cursorScreenPositionOffset += controllerInput * LevelManager.instance.cursorSpeed * 100 * Time.deltaTime;
                }

                print("Controller offset set to " + cursorScreenPositionOffset);
            }

            if (GlobalVariables.tutorialDrawing && GlobalVariables.gamePaused)
            {
                GlobalVariables.ui.screenCursor.toggleCursor(true);
            }

            if (Mathf.Abs(cursorScreenPositionOffset.x) > (Screen.width * .5f) - cursorPanBuffer || Mathf.Abs(cursorScreenPositionOffset.y) > (Screen.height * .5f) - cursorPanBuffer)
            {
                GlobalVariables.camController.cursorPushingCamera(controllerInput);
            }

            // Prevent the cursor from going beyond the edge of the screen

            if (Mathf.Abs(cursorScreenPositionOffset.x) > (Screen.width * .5f) - cursorPanBuffer)
            {
                if (cursorScreenPositionOffset.x > 0)
                {
                    cursorScreenPositionOffset.x = (Screen.width * .5f) - cursorPanBuffer;
                }
                else
                {
                    cursorScreenPositionOffset.x = (Screen.width * -.5f) + cursorPanBuffer;
                }


            }
            if (Mathf.Abs(cursorScreenPositionOffset.y) > (Screen.height * .5f) - cursorPanBuffer)
            {
                if (cursorScreenPositionOffset.y > 0)
                {
                    cursorScreenPositionOffset.y = (Screen.height * .5f) - cursorPanBuffer;
                }
                else
                {
                    cursorScreenPositionOffset.y = (Screen.height * -.5f) + cursorPanBuffer;
                }

            }

            // Set the to-be mouse position to the correct offset
            mousePosn = Camera.main.ScreenToWorldPoint(centerPoint + cursorScreenPositionOffset);
            mousePosn.z = 0;
            print("Mouse Posn set to " + mousePosn);

            if (!GlobalVariables.gamePaused && !GlobalVariables.ui.screenCursor.gameObject.activeInHierarchy)
            {
                GlobalVariables.ui.screenCursor.toggleCursor(true);
            }
        }
        else
        {
            // if not in controller mode, set mouse position to wherever the mouse already is
            mousePosn = new Vector3(mousePosition.x, mousePosition.y, 0);
            if (mouseScreenPosition == Vector2.zero && GlobalVariables.ui.screenCursor.gameObject.activeInHierarchy)
            {
                GlobalVariables.ui.screenCursor.toggleCursor(false);
            }
            else if (mouseScreenPosition != Vector2.zero && !GlobalVariables.ui.screenCursor.gameObject.activeInHierarchy)
            {
                GlobalVariables.ui.screenCursor.toggleCursor(true);
            }

        }

        // Move Cursor to wherever it should be
        cursor.transform.position = mousePosn;
        GlobalVariables.ui.screenCursor.setScreenPosition(centerPoint + cursorScreenPositionOffset);

        #endregion

        // Checks if the game is paused
        if (!GlobalVariables.gamePaused || GlobalVariables.tutorialDrawing)
        {
            // checks if the drawing manager itself is paused
            if (!paused)
            {
                #region Fire Spray Controls
                
                if ((useFireSrapy > 0 || fireSprayButtonPressed) && fireSprayState > 0 && !isUsingFireSpray)
                {
                    print("Applying Fire Spray at level: " + fireSprayState);
                    fireSprayButtonPressed = false;
                    isUsingFireSpray = true;
                    fireSprayTime = 0;

                    foreach (FireSprayStatus status in expendedFireSprayStatus)
                    {
                        status.activate();
                    }
                }
                else
                {
                    fireSprayButtonPressed = false;
                }

                if (isUsingFireSpray)
                {
                    if (fireSprayTime < stateDurations[fireSprayState])
                    {
                        fireSprayTime += Time.deltaTime;
                    }
                    else
                    {
                        isUsingFireSpray = false;
                        foreach (FireSprayStatus status in expendedFireSprayStatus)
                        {
                            status.deactivate();
                        }
                    }
                }
                #endregion

                #region Drawing Logic

                // Represents if the player is safe to start drawing a line of ants
                bool startDrawingSafe = true;


                // Checks if the mouse is over a clickable target OR if there is something that should stop drawing
                if (playerSelect > 0 && !isDrawing)
                {
                    // if the camera is in follow mode and we're good to start drawing, put the camera in pan mode
                    if (GlobalVariables.camController.currentState.typeName == "Follow" && !doNotSpawnOnClick)
                    {
                        GlobalVariables.camController.changeState(new PanState());
                    }

                    if (cursorScript.clickTarget != null || doNotSpawnOnClick)
                    {
                        startDrawingSafe = false;
                    }

                    if (playerErase > 0)
                    {
                        startDrawingSafe = false;
                    }

                    if (!validDrawingZone)
                    {
                        startDrawingSafe = false;
                    }

                    // Check if the player's mouse is not on the screen, don't draw if off window. DON'T CHECK IF IN CONTROLLER MODE
                    if (!controllerMode)
                    {
                        if (mouseScreenPosition.y <= 0 || mouseScreenPosition.y > Screen.height || mouseScreenPosition.x <= 0 || mouseScreenPosition.x > Screen.width)
                        {
                            startDrawingSafe = false;
                        }

                    }
                }

                if (waitingForDrawButtonUp)
                {
                    if (playerSelect > 0)
                    {
                        startDrawingSafe = false;
                    }
                    else
                    {
                        waitingForDrawButtonUp = false;
                    }

                }



                // if the mouse is fresh off a "Do Not Draw Here" zone but is otherwise clear to draw, this line will check to make sure there isn't any more invalid zones between the mouse 
                // and the last posn of the line.
                if (playerSelect > 0 && startDrawingSafe && lookingForValidDrawingZone && formationList[activeFormationIndex].continueDrawingCondition() && (LevelManager.instance.isUsingLimitDrawing ? waypoints.Count < formationList[activeFormationIndex].drawLimit : true))
                {
                    if (lineRenderer.positionCount >= 1)
                    {

                        // send three raycasts out, one directly between the mouse and the last position in the line, and two on either side, helping deal with drawing
                        // around corners
                        RaycastHit2D[] hit = Physics2D.RaycastAll(lineRenderer.GetPosition(lineRenderer.positionCount - 1),
                                                                 (mousePosn - lineRenderer.GetPosition(lineRenderer.positionCount - 1)),
                                                                 Vector3.Distance(lineRenderer.GetPosition(lineRenderer.positionCount - 1), mousePosn),
                                                                 Physics2D.GetLayerCollisionMask(6)
                                                                 );

                        Vector3 perpendicularDir = Vector2.Perpendicular(mousePosn - lineRenderer.GetPosition(lineRenderer.positionCount - 1)).normalized;
                        
                        RaycastHit2D[] side1 = Physics2D.RaycastAll(lineRenderer.GetPosition(lineRenderer.positionCount - 1) + perpendicularDir * drawingWidth,
                                                                 (mousePosn - lineRenderer.GetPosition(lineRenderer.positionCount - 1)),
                                                                 Vector3.Distance(lineRenderer.GetPosition(lineRenderer.positionCount - 1), mousePosn),
                                                                 Physics2D.GetLayerCollisionMask(6)
                                                                 );
                        RaycastHit2D[] side2 = Physics2D.RaycastAll(lineRenderer.GetPosition(lineRenderer.positionCount - 1) + perpendicularDir * drawingWidth * -1,
                                                                 (mousePosn - lineRenderer.GetPosition(lineRenderer.positionCount - 1)),
                                                                 Vector3.Distance(lineRenderer.GetPosition(lineRenderer.positionCount - 1), mousePosn),
                                                                 Physics2D.GetLayerCollisionMask(6)
                                                                 );

                        /*Debug.DrawLine(lineRenderer.GetPosition(lineRenderer.positionCount - 1), mousePosn);
                        Debug.DrawLine(lineRenderer.GetPosition(lineRenderer.positionCount - 1) + perpendicularDir * drawingWidth, mousePosn + perpendicularDir * drawingWidth);
                        Debug.DrawLine(lineRenderer.GetPosition(lineRenderer.positionCount - 1) + perpendicularDir * drawingWidth * -1, mousePosn + perpendicularDir * drawingWidth * -1);
*/

                        RaycastHit2D[] raycastResult = new RaycastHit2D[hit.Length + side1.Length + side2.Length];

                        hit.CopyTo(raycastResult, 0);
                        side1.CopyTo(raycastResult, hit.Length);
                        side2.CopyTo(raycastResult, side1.Length);

                        // Check through the list of hits, if anything has Do Not Draw Here it should stop and return that the path is not clear
                        bool isPathClear = true;
                        foreach (RaycastHit2D collide in raycastResult)
                        {
                            if (collide)
                            {
                                if (collide.collider.gameObject.tag.Equals("Do Not Draw Here"))
                                {
                                    isPathClear = false;
                                    break;
                                }

                            }
                        }

                        if (isPathClear)
                        {
                            lookingForValidDrawingZone = false;
                            invalidZoneRenderer.positionCount = 0;
                        }
                    }


                }

                // If the previous section returned all clears drawing begins
                if (playerSelect > 0 && validDrawingZone && !lookingForValidDrawingZone && startDrawingSafe && formationList[activeFormationIndex].continueDrawingCondition() && (LevelManager.instance.isUsingLimitDrawing ? waypoints.Count < formationList[activeFormationIndex].drawLimit : true))
                {
                    if (!isDrawing)
                    {
                        isDrawing = true;
                        GlobalVariables.ui.screenCursor.toggleDraw(true);
                    }

                    if (!emitter.IsPlaying())
                    {
                        emitter.Play();
                    }

                    float waypointToMouse = Vector3.Distance(waypoints.Count == 0 ? new Vector3(0, 0, 0) : waypoints[waypoints.Count - 1], mousePosn);


                    // checks the distance between the most recent waypoint, if the waypoint is farther than a certain number it'll draw a new waypoint
                    if (waypointToMouse >= formationList[activeFormationIndex].spawnDistance && ((waypoints.Count == 0 ? new Vector3(0, 0, 0) : waypoints[waypoints.Count - 1]) != mousePosn) && waypoints.Count != 0)
                    {
                        // checks if the distance is more than double the minimum distance, if it is it'll need to add extra waypoints
                        if (waypointToMouse >= formationList[activeFormationIndex].spawnDistance * 2)
                        {
                            int numOfPoints = (int)Mathf.Floor(waypointToMouse / formationList[activeFormationIndex].spawnDistance);
                            int count = 0;

                            // spawns a new unit repeatedly until we reach the position that the mouse is at this frame.
                            while (count < numOfPoints)
                            {
                                Vector3 direction = (waypoints[waypoints.Count - 1] - mousePosn).normalized * -1;
                                waypoints.Add(waypoints[waypoints.Count - 1] + (direction * formationList[activeFormationIndex].spawnDistance));

                                if (formationList[activeFormationIndex].isDrawUnitAtPoint(waypoints))
                                {
                                    spawnEmptyUnit(waypoints[waypoints.Count - 1], formationList[activeFormationIndex].getUnit());
                                }

                                if (CurrentPheromones >= formationList[activeFormationIndex].cost && (LevelManager.instance.isUsingLimitDrawing ? waypoints.Count < formationList[activeFormationIndex].drawLimit : true))
                                {
                                    if (!GlobalVariables.tutorialDrawing)
                                    {
                                        CurrentPheromones -= formationList[activeFormationIndex].cost;
                                    }
                                    spentPheromones += formationList[activeFormationIndex].cost;
                                    vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, cursor.gameObject);
                                    count++;
                                }
                                else
                                {
                                    count = numOfPoints;
                                }

                            }

                            lineRenderer.positionCount = waypoints.Count;
                            lineRenderer.SetPositions(waypoints.ToArray());
                        }
                        else
                        {
                            // adds a single position to the waypoints
                            waypoints.Add(mousePosn);

                            if (formationList[activeFormationIndex].isDrawUnitAtPoint(waypoints))
                            {
                                spawnEmptyUnit(mousePosn, formationList[activeFormationIndex].getUnit());
                            }

                            lineRenderer.positionCount = waypoints.Count;
                            lineRenderer.SetPositions(waypoints.ToArray());

                            if (CurrentPheromones >= formationList[activeFormationIndex].cost)
                            {
                                if (!GlobalVariables.tutorialDrawing)
                                {
                                    CurrentPheromones -= formationList[activeFormationIndex].cost;
                                }
                                spentPheromones += formationList[activeFormationIndex].cost;
                                //vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, cursor.gameObject);
                            }
                        }
                    }
                    else if (waypoints.Count == 0)
                    {
                        waypoints.Add(mousePosn);

                        if (formationList[activeFormationIndex].isDrawUnitAtPoint(waypoints))
                        {
                            spawnEmptyUnit(mousePosn, formationList[activeFormationIndex].getUnit());


                        }

                        lineRenderer.positionCount = waypoints.Count;
                        lineRenderer.SetPositions(waypoints.ToArray());

                        if (CurrentPheromones >= formationList[activeFormationIndex].cost)
                        {
                            if (!GlobalVariables.tutorialDrawing)
                            {
                                CurrentPheromones -= formationList[activeFormationIndex].cost;
                            }
                            spentPheromones += formationList[activeFormationIndex].cost;
                            //vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, cursor.gameObject);
                        }
                    }


                }
                else if (playerSelect > 0 && startDrawingSafe && formationList[activeFormationIndex].continueDrawingCondition() && (LevelManager.instance.isUsingLimitDrawing ? waypoints.Count < formationList[activeFormationIndex].drawLimit : true))
                {
                    if (lookingForValidDrawingZone == false)
                    {
                        lookingForValidDrawingZone = true;
                        invalidZoneRenderer.positionCount = 2;
                        invalidZoneRenderer.SetPosition(0, lineRenderer.GetPosition(lineRenderer.positionCount - 1));

                    }

                    invalidZoneRenderer.SetPosition(1, mousePosn);
                }

                // Stop Drawing
                // Occurs when the player releases the mouse button, while still drawing a line, or they try to continue to draw a line while 
                if (isDrawing && (LevelManager.instance.playerSelect.WasReleasedThisFrame() || (false && playerSelect > 0))) //|| !validDrawingZone)) and (formationList[activeFormationIndex].stopDrawingCondition()
                {
                    if (emitter.IsPlaying())
                    {
                        emitter.Stop();
                    }
                    GlobalVariables.ui.pheromoneMeter.tweenChipBar();
                    //StopDrawing();
                    GlobalVariables.ui.screenCursor.toggleDraw(false);
                    isDrawing = false;
                    lookingForValidDrawingZone = false;
                    resetEmptyUnitPool();
                    lineRenderer.positionCount = 0;
                    invalidZoneRenderer.positionCount = 0;
                    spentPheromones = 0;
                    formationList[activeFormationIndex].spawnUnits(waypoints);
                    waypoints = new List<Vector3>();
                }
                else if (isDrawing && (playerErase > 0))
                {
                    // turned this into a function so that it can also be called when changing draw mode and formations
                    StopDrawing();
                    /*isDrawing = false;
                    GlobalVariables.ui.screenCursor.toggleDraw(false);
                    lookingForValidDrawingZone = false;
                    waypoints = new List<Vector3>();
                    resetEmptyUnitPool();
                    lineRenderer.positionCount = 0;
                    invalidZoneRenderer.positionCount = 0;
                    currentPheromones += spentPheromones;
                    spentPheromones = 0;

                    waitingForDrawButtonUp = true;*/
                }

                #endregion
            }

            #region Extra Controls

            // Handle action switching hotkeys
            if (levelManager.switchDraw.WasPressedThisFrame())
            {
                if (isDrawing)
                {
                    StopDrawing();
                }
                GlobalVariables.ui.palette.actionButtons.turnOnDraw();
            }
            if (levelManager.switchErase.WasPressedThisFrame())
            {
                if (isDrawing)
                {
                    StopDrawing();
                }
                GlobalVariables.ui.palette.actionButtons.turnOnErase();
            }
            if (levelManager.switchSearch.WasPressedThisFrame())
            {
                if (isDrawing)
                {
                    StopDrawing();
                }
                GlobalVariables.ui.palette.actionButtons.turnOnSearch();
            }

            // Debug inputs
            if (levelManager.hotkey1.ReadValue<float>() > 0)
            {
                currentSelection = 0;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey2.ReadValue<float>() > 0)
            {
                currentSelection = 1;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey3.ReadValue<float>() > 0)
            {
                currentSelection = 2;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey4.ReadValue<float>() > 0)
            {
                currentSelection = 3;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey5.ReadValue<float>() > 0)
            {
                currentSelection = 4;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey6.ReadValue<float>() > 0)
            {
                currentSelection = 5;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey7.ReadValue<float>() > 0)
            {
                currentSelection = 6;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey8.ReadValue<float>() > 0)
            {
                currentSelection = 7;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey9.ReadValue<float>() > 0)
            {
                currentSelection = 8;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey10.ReadValue<float>() > 0)
            {
                currentSelection = 9;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey11.ReadValue<float>() > 0)
            {
                currentSelection = 10;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey12.ReadValue<float>() > 0)
            {
                currentSelection = 11;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }

            if ((levelManager.selectLeft.WasPressedThisFrame()))
            {
                currentSelection -= 1;
                if (currentSelection < 0)
                {
                    currentSelection = formationList.Count - 1;
                }
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
                //changeFormation(sectionMod(currentSelection));
            }
            else if ((levelManager.selectRight.WasPressedThisFrame()))
            {
                currentSelection += 1;
                if (currentSelection >= formationList.Count)
                {
                    currentSelection = 0;
                }
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
                //changeFormation(sectionMod(currentSelection));
            }
            if (levelManager.selectSwapRow.WasPressedThisFrame())
            {
                if (currentSelection >= 6)
                {
                    currentSelection -= 6;
                }
                else
                {
                    currentSelection += 6;
                }

                if (currentSelection >= formationList.Count)
                {
                    currentSelection = formationList.Count - 1;
                }
                if (currentSelection < 0)
                {
                    currentSelection = 0;
                }

                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }

            #endregion
        }
        else if (GlobalVariables.gamePaused && listeningForFormChange && !optionsMenuOpen)
        {
            #region Tutorial Controls Exception

            if ((levelManager.selectLeft.WasPressedThisFrame()))
            {
                currentSelection -= 1;
                if (currentSelection < 0)
                {
                    currentSelection = formationList.Count - 1;
                }
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if ((levelManager.selectRight.WasPressedThisFrame()))
            {
                currentSelection += 1;
                if (currentSelection >= formationList.Count)
                {
                    currentSelection = 0;
                }
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            if (levelManager.selectSwapRow.WasPressedThisFrame())
            {
                if (currentSelection >= 6)
                {
                    currentSelection -= 6;
                }
                else
                {
                    currentSelection += 6;
                }
                
                if (currentSelection >= formationList.Count)
                {
                    currentSelection = formationList.Count - 1;
                }
                if (currentSelection < 0)
                {
                    currentSelection = 0;
                }

                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }

            if (levelManager.hotkey1.ReadValue<float>() > 0)
            {
                currentSelection = 0;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey2.ReadValue<float>() > 0)
            {
                currentSelection = 1;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey3.ReadValue<float>() > 0)
            {
                currentSelection = 2;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey4.ReadValue<float>() > 0)
            {
                currentSelection = 3;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey5.ReadValue<float>() > 0)
            {
                currentSelection = 4;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey6.ReadValue<float>() > 0)
            {
                currentSelection = 5;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey7.ReadValue<float>() > 0)
            {
                currentSelection = 6;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey8.ReadValue<float>() > 0)
            {
                currentSelection = 7;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey9.ReadValue<float>() > 0)
            {
                currentSelection = 8;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey10.ReadValue<float>() > 0)
            {
                currentSelection = 9;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey11.ReadValue<float>() > 0)
            {
                currentSelection = 10;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }
            else if (levelManager.hotkey12.ReadValue<float>() > 0)
            {
                currentSelection = 11;
                GlobalVariables.ui.palette.antToggles.directToggle(currentSelection, true);
            }

            #endregion
        }


    }

    public void onRefillTimeOut()
    {
        if (!isDrawing)
        {
            CurrentPheromones += refillAmount;

        }
    }

    #region Unit Pooling
    // Empty Unit Pool

    public void spawnEmptyUnit(Vector3 posn, FormationUnit unitReference)
    {
        bool isRight = false;

        if (waypoints.Count > 1)
        {
            isRight = waypoints[waypoints.Count - 2].x < posn.x;
        }


        EmptyUnit newEUnit = null;
        RangeIndicator newRange = null;

        foreach(EmptyUnit unit in emptyUnitPool)
        {
            if (!unit.isActiveAndEnabled)
            {
                newEUnit = unit;
                break;
            }
        }

        foreach (RangeIndicator range in emptyUnitRanges)
        {
            if (!range.isActiveAndEnabled)
            {
                newRange = range;
                break;
            }
        }

        if (newEUnit != null && newRange != null)
        {
            newEUnit.initSprite(posn, unitReference.sprite.sprite, unitReference.sprite.size * 1.25f, unitReference.type.attackRange, isRight);
            newRange.transform.position = posn;
            newRange.gameObject.SetActive(true);
            newRange.setRange(unitReference.type.attackRange * 2, .4f);
        }
        else
        {
            //print("ERROR: DRAWING MANAGER IS TRYING TO GET AN EMPTY UNIT FROM A FULLY USED POOL. MIGHT NEED TO ADJUST POOL AMOUNT IN DRAWING MANAGER PREFAB");
        }
    }

    public void setUpEmptyUnitPool()
    {
        for (int i = 0; i < emptyUnitPoolAmount; i++)
        {
            GameObject newUnit = GameObject.Instantiate(emptyUnitPrefab);
            newUnit.transform.parent = emptyAntContainer;
            newUnit.SetActive(false);
            EmptyUnit unit = newUnit.GetComponent<EmptyUnit>();
            emptyUnitPool.Add(unit);

            GameObject newRange = GameObject.Instantiate(emptyUnitRangePrefab);
            newRange.transform.parent = emptyAntContainer;
            newRange.SetActive(false);
            RangeIndicator range = newRange.GetComponent<RangeIndicator>();
            emptyUnitRanges.Add(range);
        }
    }

    public void resetEmptyUnitPool()
    {
        foreach(EmptyUnit unit in emptyUnitPool)
        {
            if (unit.gameObject.activeInHierarchy)
            {
                unit.despawnUnit();
            }
        }

        foreach(RangeIndicator range in emptyUnitRanges)
        {
            if (range.gameObject.activeInHierarchy)
            {
                range.setRange(0, .3f, true);
            }
        }
    }


    // Unit Pool Controls 

    private void setUpUnitPool()
    {
        foreach (Formation form in formationList)
        {
            //prefabCounts.Add(form.unitPrefab, );
            inactiveUnits.Add(form.unitPrefab, new List<GameObject>());
            activeUnits.Add(form.unitPrefab, new List<GameObject>());

            for (int i = 0; i < ((maxPheromones / (form.cost)) * 10); i++)
            {
                GameObject prefab = Resources.Load(form.unitPrefab, typeof(GameObject)) as GameObject;
                GameObject newUnit = Instantiate(prefab, new Vector3(), Quaternion.identity);
                FireSprayStatus newStatus = new FireSprayStatus(0);
                newUnit.GetComponent<FormationUnit>().applyStatusEffect(newStatus);
                expendedFireSprayStatus.Add(newStatus);
                newUnit.SetActive(false);
                newUnit.transform.parent = AntContainer;
                inactiveUnits[form.unitPrefab].Add(newUnit);
            }
        }
    }

    public void addToInactivePool(string unitPrefab, GameObject unit)
    {
        if (inactiveUnits.ContainsKey(unitPrefab) && activeUnits.ContainsKey(unitPrefab))
        {
            if (activeUnits[unitPrefab].Contains(unit))
            {
                activeUnits[unitPrefab].Remove(unit);
            }
            unit.SetActive(false);
            inactiveUnits[unitPrefab].Add(unit);
        }
        else
        {
            inactiveUnits.Add(unitPrefab, new List<GameObject>());
            activeUnits.Add(unitPrefab, new List<GameObject>());
            unit.SetActive(false);
            inactiveUnits[unitPrefab].Add(unit);
        }
    }

    public void addToActivePool(string unitPrefab, GameObject unit)
    {
        if (inactiveUnits.ContainsKey(unitPrefab) && activeUnits.ContainsKey(unitPrefab))
        {
            if (inactiveUnits[unitPrefab].Contains(unit))
            {
                inactiveUnits[unitPrefab].Remove(unit);
            }

            activeUnits[unitPrefab].Add(unit);
        }
        else
        {
            activeUnits.Add(unitPrefab, new List<GameObject>());
            inactiveUnits.Add(unitPrefab, new List<GameObject>());
            activeUnits[unitPrefab].Add(unit);
        }
    }

    public GameObject getInactiveUnit(string unitPrefab)
    {
        if (inactiveUnits.ContainsKey(unitPrefab))
        {
            foreach (GameObject unitObject in inactiveUnits[unitPrefab])
            {
                if (!unitObject.activeInHierarchy)
                {
                    addToActivePool(unitPrefab, unitObject);
                    return unitObject;
                }
            }

            return null;
        }
        else
        {
            return null;
        }
    }

    #endregion


    public void StopDrawing() 
    {
        if (emitter.IsPlaying())
        {
            emitter.Stop();
        }
        isDrawing = false;
        GlobalVariables.ui.screenCursor.toggleDraw(false);
        lookingForValidDrawingZone = false;
        waypoints = new List<Vector3>();
        resetEmptyUnitPool();
        lineRenderer.positionCount = 0;
        invalidZoneRenderer.positionCount = 0;
        currentPheromones += spentPheromones;
        spentPheromones = 0;

        waitingForDrawButtonUp = true;
    }
    
    public void setFiresprayState(int state)
    {
        fireSprayState = state;
    }
    
    public void toggleDrawButton()
    {
        erasing = false;
        cursorScript.toggleErase(false);
        cursorScript.toggleSearch(false);
        toggleDrawing(false);
        GlobalVariables.ui.screenCursor.setDrawingState(CursorState.DRAW);
    }

    public void toggleEraseButton()
    {
        erasing = true;
        cursorScript.toggleErase(true);
        cursorScript.toggleSearch(false);
        toggleDrawing(true);
        GlobalVariables.ui.screenCursor.setDrawingState(CursorState.ERASE);
    }

    public void toggleSearchButton()
    {
        erasing = false;
        cursorScript.toggleSearch(true);
        toggleDrawing(false);
        GlobalVariables.ui.screenCursor.setDrawingState(CursorState.SEARCH);
    }

    // Control Functions =============================================================================================================================
    public int sectionMod(int index)
    {
        return index + (5 * section);
    }

    public void changeFormation(int index)
    {
        if (index < formationList.Count)
        {
            if (isDrawing)
            {
                StopDrawing();
            }
            activeFormationIndex = index;
            if (listeningForFormChange)
            {
                // TODO: Replace?
                //button.ReHighlight(activeFormationIndex);
                formationChangeCallList(formationList[activeFormationIndex]);
            }
        }
        else
        {
            //print("DRAWING MANAGER ATTEMPTED TO CHANGE TO A FORMATION INDEX THAT DOESN'T EXIST: " + index);
        }
    }

    public void toggleDrawing(bool isPaused)
    {

        if (!isPaused && !erasing)
        {
            paused = isPaused;
        }
        else if (isPaused)
        {
            paused = isPaused;
        }


        if (!isPaused)
        {
            GlobalVariables.ui.screenCursor.toggleDraw(false);
        }
    }

    public void addToMaxPheromones(int value)
    {
        MaxPheromones += value;
        CurrentPheromones += value;
    }

    public bool isCostValid(int pheromoneCost)
    {
        return pheromoneCost >= CurrentPheromones;
    }

    private void OnPauseButton(InputAction.CallbackContext context) {
        GlobalVariables.toggleGamePause(!GlobalVariables.gamePaused);
        optionsMenuAnimator.SetBool("isShowingOptionsMenu", !optionsMenuAnimator.GetBool("isShowingOptionsMenu"));
    }

    #region Unit Spawn Behavior Coroutines

    public IEnumerator defaultSpawnBehavior(List<Vector3> waypointList, string prefab, Formation givenFormation)
    {
        int count = 0;
        givenFormation.unitLines.Add(new List<GameObject>());
        givenFormation.unitPosnLines.Add(new List<Vector3>());

        int lineIndex = givenFormation.unitLines.Count != 0 ? givenFormation.unitLines.Count - 1 : 0;

        while (count < waypointList.Count)
        {
            bool isRight = false;

            if (count == 0 && waypointList.Count > 1)
            {
                isRight = waypointList[0].x < waypointList[1].x;
            }
            else if (waypointList.Count > 1)
            {
                isRight = waypointList[count - 1].x < waypointList[count].x;
            }

            GameObject go = getInactiveUnit(givenFormation.unitPrefab); //Instantiate(Resources.Load<GameObject>(unitPrefab));
            if (go == null)
            {
                go = Instantiate(Resources.Load<GameObject>(givenFormation.unitPrefab));
                addToActivePool(givenFormation.unitPrefab, go);
                go.transform.parent = GlobalVariables.drawingManager.AntContainer;
                FireSprayStatus newStatus = new FireSprayStatus(0);
                go.GetComponent<FormationUnit>().applyStatusEffect(newStatus);
                expendedFireSprayStatus.Add(newStatus);
            }

            FormationUnit formationUnit = go.GetComponent<FormationUnit>();
            formationUnit.controller = givenFormation;
            formationUnit.spawnOrder = count;
            formationUnit.lineIndex = lineIndex;

            // Add the FormationUnit reference to a global accessible list
            GlobalVariables.FormationUnitList.Add(formationUnit);

            go.transform.position = waypointList[count];

            givenFormation.unitLines[lineIndex].Add(go);
            givenFormation.unitPosnLines[lineIndex].Add(waypointList[count]);

            go.SetActive(true);
            formationUnit.spawnUnit(isRight);

            count++;
            yield return new WaitForSecondsRealtime(.01f);
        }
    }

    public void movingUnitSpawnBehavior(List<Vector3> waypointList, string prefab, Formation givenFormation)
    {
        int count = 0;
        givenFormation.unitLines.Add(new List<GameObject>());
        givenFormation.unitPosnLines.Add(new List<Vector3>());
        int lineIndex = givenFormation.unitLines.Count != 0 ? givenFormation.unitLines.Count - 1 : 0;

        GameObject go = getInactiveUnit(givenFormation.unitPrefab); //Instantiate(Resources.Load<GameObject>(unitPrefab));

        if (go == null)
        {
            go = Instantiate(Resources.Load<GameObject>(givenFormation.unitPrefab));
            addToActivePool(givenFormation.unitPrefab, go);
            go.transform.parent = GlobalVariables.drawingManager.AntContainer;
        }

        FormationUnit formationUnit = go.GetComponent<FormationUnit>();

        formationUnit.controller = givenFormation;
        formationUnit.spawnOrder = count;
        formationUnit.lineIndex = lineIndex;

        // Add the FormationUnit reference to a global accessible list
        GlobalVariables.FormationUnitList.Add(formationUnit);

        go.transform.position = waypointList[0];

        givenFormation.unitLines[lineIndex].Add(go);
        givenFormation.unitPosnLines[lineIndex].Add(waypointList[count]);

        MovingType movingType = formationUnit.GetComponent<MovingType>();

        go.SetActive(true);
        int i = 0;
        foreach (Vector3 vector in waypointList)
        {
            //Debug.Log(i + ": " + vector);
            i++;
        }
        movingType.SetWaypoints(waypointList);
        formationUnit.spawnUnit();

        //return null;
    }

    #endregion

    /* private void OnDrawGizmos()
     {
         for (int i = 0; i < lineRenderer.positionCount; i++)
         {
             Gizmos.DrawWireSphere(lineRenderer.GetPosition(i), .5f);
         }
     }*/
}
