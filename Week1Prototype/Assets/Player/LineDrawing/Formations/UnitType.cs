using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitType : MonoBehaviour
{

    public FormationUnit unit;

    [Header("Base Stats")]
    public int damage;
    public float speed;
    public int weight;
    public int knockback;
    public float attackCooldown;
    public float attackRange;

    private float currentLifeSpan = 0f;

    public virtual void onKillUnit(){}

    public virtual void onUnitSpawned() { }

    public virtual void unitBehavior() { }

    public virtual void onChangeAttackRange() { }

    public virtual void unitTimeOut() { }
    public virtual void onChangeSpriteDirection(bool isRight) { }

    public virtual void onCollision(bool isExit, Collision2D collider) { }

    public virtual void onTrigger(bool isExit, Collider2D collider) { }


    private void Update()
    {
        currentLifeSpan += Time.deltaTime;
        checkStatus();
    }

    public virtual void pushBehavior(Vector3 direction, float distanceToTravel)
    {
        if (distanceToTravel > 0)
        {
            Vector3 start = transform.position;
            unit.transform.position += (transform.position + direction);
            float pushDistanceTravelled = Vector3.Distance(start, transform.position + direction);

            unit.pushDistance = distanceToTravel - pushDistanceTravelled;
            print(start + " - " + transform.position + " = " + pushDistanceTravelled);
        }
        else
        {
            unit.setMoveMode(MoveMode.normal);
        }
    }

    public float GetCurrentLifeSpan()
    {
        return currentLifeSpan;
    }

    public virtual void pullbehavior(Vector3 direction, float speed)
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    public virtual void carryBehavior(Transform carryingObject, Vector3 carryPosition)
    {
        transform.position = carryingObject.position + carryPosition;
    }

    public virtual void onPickUp()
    {

    }

    public virtual void onPutDown()
    {

    }

    public void checkStatus()
    {
        if (unit.hasStatusEffectOfType("Fire"))
        {
            unit.vfxManager.ManageParticles(2, VFXManager.VFXControl.Play, gameObject);
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, .4f, .4f, 1);
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
    }
}
