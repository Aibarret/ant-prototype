using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BaseType : UnitType
{
    private Attackable activeTarget;

    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;

    private Vector3 returnPosn;
    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;

    private void Start()
    {
        unit.vfxManager = VFXManager.Instance;

        if(unit.vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
        }
    }

    public override void onKillUnit()
    {
        phase = FormationUnit.AttackPhase.Idle;
        activeTarget = null;
        attackPosn = Vector3.zero;
        unitElapsedFrames = 0f;
    }

    protected readonly HashSet<CentipedeType> centipedeSet = new HashSet<CentipedeType>();
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.TryGetComponent(out CentipedeType centipede))
        {
            if (isExit)
            {
                if (centipedeSet.Contains(centipede))
                    centipedeSet.Remove(centipede);
            }
            else
                centipedeSet.Add(centipede);
        }
        if (collider.gameObject.CompareTag("Obstacle") || collider.gameObject.CompareTag("Enemy"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (!isExit && newTarget != null)
            {
                
                if (newTarget.health > 0)
                {
                    declareTarget(newTarget);
                }
            }
            else
            {
                if (newTarget == activeTarget)
                {
                    activeTarget = null;
                }
            }
        }
    }

    public void declareTarget(Attackable newTarget)
    {
        if (centipedeSet.Count > 0)
        {
            activeTarget = centipedeSet.ElementAt(0).GetComponent<Attackable>();
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
            return;
        }
        
        if (phase != FormationUnit.AttackPhase.Attacking && !unit.isTimerRunning())
        {
            if (activeTarget != null)
            {

                if (activeTarget.health > newTarget.health)
                {
                    activeTarget = newTarget;
                }
            }
            else
            {
                activeTarget = newTarget;
            }
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
        }

        return;

        void StartUnitAttackAnimation()
        {
            attackPosn = activeTarget.gameObject.transform.position;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public override void unitTimeOut()
    {
        if (activeTarget != null)
        {
            //Debug.Log("UNIT HAS TIMED OUT AND TARGET IS " + activeTarget);
            declareTarget(activeTarget);
        }
        else
        {
            // The original target is dead, so now we need to check if there's another valid target in range.

            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.CompareTag("Enemy") || attackablesInRange[count].gameObject.CompareTag("Obstacle"))
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        declareTarget(target);
                    }

                }
                count++;
            }
        }
    }

    public override void unitBehavior()
    {
        if (phase == FormationUnit.AttackPhase.Attacking && activeTarget != null)
        {


            if (!isEndAttack)
            {
                //Update name of variable in FormationUnit
                if (unitElapsedFrames / speed < 1)
                {
                    transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames += Time.deltaTime;
                    
                }
                else
                {
                    attackPosn = transform.position;
                    isEndAttack = true;

                    Vector2 dirVec2 = (unit.truePosition - activeTarget.gameObject.transform.position).normalized;
                    float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;

                    unit.PlaySFX("Attack");

                    if (activeTarget.takeDamage(damage, direction: dir))
                    {
                        //Debug.Log("ATTACKABLE SET TO NULL");
                        activeTarget = null;

                    }

                }
            }
            else
            {
                if (unitElapsedFrames / speed > 0)
                {
                    transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames -= Time.deltaTime;
                }
                else
                {
                    unit.vfxManager.ManageObjects(0, 0.25f, activeTarget.gameObject.transform.position);
                    phase = FormationUnit.AttackPhase.Idle;
                    isEndAttack = false;
                    unit.startUnitTimer(attackCooldown);
                }
            }

        }
        else if (phase == FormationUnit.AttackPhase.Attacking)
        {
            //Debug.Log("Out of Range and return to position");
            if (unitElapsedFrames / speed > 0)
            {
                transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                unitElapsedFrames -= Time.deltaTime;
            }
            else
            {
                phase = FormationUnit.AttackPhase.Idle;
                isEndAttack = false;
                unit.startUnitTimer(attackCooldown);
            }
        }
    }



}
