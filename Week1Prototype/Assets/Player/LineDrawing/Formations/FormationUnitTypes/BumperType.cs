﻿using System;
using System.Collections;
using System.Drawing;
using UnityEngine;

namespace Player.Formations.FormationUnitTypes
{
    public sealed class BumperType : UnitType
    {

        private void Start()
        {
            unit.vfxManager = VFXManager.Instance;

            if (unit.vfxManager == null)
            {
                Debug.Log("There is no VFXManager Instance");
            }
        }

        public override void onCollision(bool isExit, Collision2D collider)
        {
            if (collider.gameObject.CompareTag("Enemy") || collider.gameObject.CompareTag("Obstacle"))
            {
                if (collider.gameObject.TryGetComponent(out Moveable type))
                {
                    Vector3 dir = collider.transform.position - transform.position;
                    type.push(dir.normalized, knockback);
                    unit.vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
                    unit.PlaySFX("Bumper Unit Bounce");
                    /*Vector2 normal = collider.contacts[0].normal;
                    unit.vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
                    StartCoroutine(BumpedRoutine(type, -normal, knockback, 1f));*/

                }
                
            }
            
        }

        public override void onTrigger(bool isExit, Collider2D collider)
        {
            if (collider.gameObject.CompareTag("Enemy"))
            {
                if (collider.gameObject.TryGetComponent(out Enemy type))
                {
                    Vector3 dir = collider.transform.position - transform.position;
                    type.push(dir.normalized, knockback);
                    unit.PlaySFX("Bumper Unit Bounce");
                    unit.vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
                    //Vector2 normal = collider.contacts[0].normal;
                    //StartCoroutine(BumpedRoutine(type, -normal, knockback, 1f));
                    
                }
            }
        }

    }
}