using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChargerType : UnitType, ITireGrabber
{
    [SerializeField] GameObject hit;
    [SerializeField] private Transform tireGrabPoint;

    private Vector3 targetPosn;

    private Attackable attackActiveTarget;
    private GameObject targetGameObject;
    private Moveable moveActiveTarget;

    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;
    
    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;

    [SerializeField] private Transform spriteTransform;

    private readonly HashSet<BallType> nearbyBallSet = new HashSet<BallType>();

    private readonly HashSet<CentipedeType> centipedeSet = new HashSet<CentipedeType>();
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.TryGetComponent(out CentipedeType centipede))
        {
            if (isExit)
            {
                if (centipedeSet.Contains(centipede))
                    centipedeSet.Remove(centipede);
            }
            else
                centipedeSet.Add(centipede);
        }
        
        // Add ball to nearby ball set
        if (collider.CompareTag("Obstacle") && collider.TryGetComponent(out BallType ball))
        {
            if (isExit)
            {
                if (nearbyBallSet.Contains(ball))
                    nearbyBallSet.Remove(ball);
            }
            else
            {
                if (ball != null)
                    nearbyBallSet.Add(ball);   
            }
        }
        
        if (collider.CompareTag("Obstacle") || collider.CompareTag("Enemy"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();
            Moveable newMoveTarget = collider.GetComponent<Moveable>();

            if (newMoveTarget != null && collider.CompareTag("Obstacle"))
            {
                if (collider.GetComponent<ObstacleType>().weight >= 10)
                {
                    newMoveTarget = null;
                }
            }

            // Ignore ball obstacles
            if (!isExit && !collider.TryGetComponent(out BallType _))
            {
                if (newTarget != null)
                {
                    if (newTarget.health > 0)
                        declareTarget(collider.gameObject, newTarget, null);
                }
                else if (newMoveTarget != null)
                {
                    declareTarget(collider.gameObject, null, newMoveTarget);
                }

                //targetGameObject = collider.gameObject;

                
            }
            else
            {
                if (collider.gameObject == targetGameObject)
                {
                    targetGameObject = null;
                    moveActiveTarget = null;
                    attackActiveTarget = null;
                }
            }
        }
    }
    
    public void declareTarget(GameObject newTarget, Attackable newAttack, Moveable newMove)
    {
        if (centipedeSet.Count > 0)
        {
            attackActiveTarget = centipedeSet.ElementAt(0).GetComponent<Attackable>();
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
            return;
        }
        
        if (phase != FormationUnit.AttackPhase.Attacking && !unit.isTimerRunning())
        {
            if (targetGameObject != null)
            {
                if (attackActiveTarget != null && newAttack !=  null)
                {
                    if (attackActiveTarget.health > newAttack.health)
                    {
                        targetGameObject = newTarget;
                        attackActiveTarget = newAttack;
                    }
                }
                
                
            }
            else
            {
                targetGameObject = newTarget;

                if (newAttack != null)
                {
                    attackActiveTarget = newAttack;
                }
            }
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
        }
        
        return;

        void StartUnitAttackAnimation()
        {
            startPos = transform.position;
            Vector3 direction = targetGameObject.transform.position - transform.position;
            unit.vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, gameObject);
            direction.Normalize();
            attackPosn = targetGameObject.transform.position - direction;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public override void unitTimeOut()
    {
        if (targetGameObject != null)
        {
            Debug.Log("UNIT HAS TIMED OUT AND TARGET IS " + targetGameObject);
            declareTarget(targetGameObject, attackActiveTarget, moveActiveTarget);
        }
        else
        {
            // The original target is dead, so now we need to check if there's another valid target in range.

            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle")
                {
                    Attackable newTarget = attackablesInRange[count].GetComponent<Attackable>();
                    Moveable newMoveTarget = attackablesInRange[count].GetComponent<Moveable>();

                    if (newTarget != null)
                    {
                        declareTarget(attackablesInRange[count].gameObject, newTarget, newMoveTarget);
                    }
                    else if (newMoveTarget != null)
                    {
                        if (attackablesInRange[count].GetComponent<ObstacleType>().weight >= 10)
                        {
                            newMoveTarget = null;
                        }
                        else
                        {
                            declareTarget(attackablesInRange[count].gameObject, newTarget, newMoveTarget);
                        }
                    }

                }
                count++;
            }
        }
    }

    private Vector2 startPos;
    
    public override void unitBehavior()
    {
        //Transform transform = spriteTransform;
        if (phase != FormationUnit.AttackPhase.Attacking && nearbyBallSet.Count > 0)
        {
            int enemyLayerMask = 0x01 << LayerMask.NameToLayer("Enemy");
            const float ballRadius = 0.71f;
            const float distance = 10f;
            foreach (BallType ball in nearbyBallSet)
            {
                Vector2 direction = (ball.transform.position - transform.position).normalized;
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, ballRadius, direction, distance, enemyLayerMask);
                if (hit)
                {
                    //Debug.Log("bruh");
                    declareTarget(ball.gameObject, null, ball.GetComponent<Moveable>());
                    break;
                }
            }
        }
        
        if (phase == FormationUnit.AttackPhase.Attacking && targetGameObject != null)
        {
            if (!isEndAttack)
            {
                //Update name of variable in FormationUnit
                if (unitElapsedFrames / speed < 1)
                {
                    transform.position = Vector3.Lerp(startPos, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames += Time.deltaTime;
                    //FMODUnity.RuntimeManager.PlayOneShot("event:/Units/ChargerUnit/Spawn", transform.position);
                }
                else
                {
                    attackPosn = transform.position;
                    isEndAttack = true;
                    
                    Moveable moveable = targetGameObject.GetComponent<Moveable>();

                    if (moveable != null)
                    {
                        Vector3 direction = targetGameObject.transform.position - transform.position;
                        direction.Normalize();

                        moveable.push(direction, knockback);
                    }
                    
                    if (attackActiveTarget != null)
                    {
                        Vector2 dirVec2 = (unit.truePosition - attackActiveTarget.gameObject.transform.position).normalized;
                        float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;
                        
                        
                        if (attackActiveTarget.takeDamage(damage, direction: dir))
                        {
                            //Debug.Log("ATTACKABLE SET TO NULL");
                            targetGameObject = null;
                            attackActiveTarget = null;
                            moveActiveTarget = null;
                        }
                    }

                    unit.PlaySFX("Attack");

                }
            }
            else
            {
                if (unitElapsedFrames / speed > 0)
                {
                    transform.position = Vector3.Lerp(startPos, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames -= Time.deltaTime;
                }
                else
                {

                    phase = FormationUnit.AttackPhase.Idle;
                    isEndAttack = false;
                    unit.startUnitTimer(attackCooldown);
                }
            }
        }
        else if (phase == FormationUnit.AttackPhase.Attacking)
        {
            //Debug.Log("Out of Range and return to position");
            if (unitElapsedFrames / speed > 0)
            {
                transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                unitElapsedFrames -= Time.deltaTime;
            }
            else
            {
                phase = FormationUnit.AttackPhase.Idle;
                isEndAttack = false;
                unit.startUnitTimer(attackCooldown);
            }
        }
    }
    
    public bool TireGrabbed { get; private set; } = false;

    public void GrabTire()
    {
        print("Unit grabbing tire");
        // Set flag
        TireGrabbed = true;

        // Instantiate attached tire game object
        GameObject tire = Instantiate(Resources.Load<GameObject>("AttachedTire"), tireGrabPoint);
        //tire.transform.parent = tireGrabPoint;
        //tire.transform.localPosition = Vector3.zero;//tireGrabPoint.localPosition;

        // Increase knock back by 3
        knockback += 3;
        if (knockback > 10)
            knockback = 10;
    }

    public override void onCollision(bool isExit, Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") && !isExit)
        {
            Moveable moveable = collision.gameObject.GetComponent<Moveable>();

            if (moveable != null)
            {
                Vector3 direction = targetGameObject.transform.position - transform.position;
                direction.Normalize();

                moveable.push(direction, knockback);
            }
        }
    }
}
