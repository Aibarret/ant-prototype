using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PharaohType : UnitType
{
    [Header("Pharoh Stats")]
    public float summonCooldown;
    public int maxSummons;
    public int startingCharges;
    public int summonChargeCost;

    [Header("Prefab Reference")]
    public GameObject undeadAntPrefab;

    //private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;
    private int charges;
    private int summonedCount = 0;
    private float cooldownTime = 0;
    private Formation undeadFormation = new UndeadForm();
    private HashSet<Enemy> radiusSet = new HashSet<Enemy>();

    [SerializeField] GameObject undeadSpawner;
    [SerializeField] GameObject soulContainer;

    public override void onUnitSpawned() {
        charges = startingCharges;
        summonedCount = 0;
        cooldownTime = 0;
        radiusSet = new HashSet<Enemy>();
    }

    public override void unitBehavior() {
        cooldownTime += Time.deltaTime;
        if (charges >= summonChargeCost && summonedCount < maxSummons && cooldownTime >= summonCooldown) {
            charges -= summonChargeCost;
            summonUndeadAnt();
        }
    }

    public void summonUndeadAnt() {
        Vector2 spawnLocation = randomPoint();
        List<Vector3> wayPoints = new List<Vector3>();
        wayPoints.Add((Vector3) spawnLocation);
        undeadFormation.spawnUnits(wayPoints);
        GameObject spawnVFX = Instantiate(undeadSpawner, spawnLocation, Quaternion.identity);
        //spawnVFX.GetComponent<UndeadType>().unit.vfxManager.ManageParticles(19, VFXManager.VFXControl.Play, spawnVFX.gameObject);
        summonedCount++;
        cooldownTime = 0;
    }

    private Vector2 randomPoint() 
    {
        Vector3 rawRandomPoint = new Vector3(Random.Range(attackRange * -1, attackRange), Random.Range((RadiusRatios.HEIGHT * attackRange) * -1, (RadiusRatios.HEIGHT * attackRange)), 0);
        if (!unit.hitbox.OverlapPoint(rawRandomPoint))
        {
            rawRandomPoint.y *= .35f; 
        }

        return rawRandomPoint + transform.position;
/*        Vector2 randomDirection = Random.insideUnitCircle.normalized * Random.Range(0f, attackRange);
        Vector2 randomPoint = (Vector2) transform.position + randomDirection;
        return randomPoint;*/
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag.Equals("Enemy"))
        {
            Enemy enemy = collider.gameObject.GetComponent<Enemy>();
            if (enemy == null) return;
            radiusSet.Add(enemy);
            enemy.EventEnemyKilled += OnEnemyDeath;
        }
        
    }

    private void OnTriggerExit2D(Collider2D collider) {
        if (collider.tag.Equals("Enemy"))
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            if (enemy == null) return;
            radiusSet.Remove(enemy);
            enemy.EventEnemyKilled -= OnEnemyDeath;
        }
        
    }


    private void OnEnemyDeath(Enemy enemy) {
        radiusSet.Remove(enemy);
        GameObject soul = Instantiate(soulContainer, enemy.transform.position, Quaternion.identity);
        unit.vfxManager.ManageParticles(25, VFXManager.VFXControl.Play, soul.gameObject);
        soul.GetComponent<Homing>().target = gameObject;
        charges++;
    }

}
