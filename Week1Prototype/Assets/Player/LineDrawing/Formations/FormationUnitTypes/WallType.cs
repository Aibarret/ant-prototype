using FlowFieldNavigation;
using UnityEngine;

public class WallType : UnitType
{
    //public FormationUnit unit;
    //public int damage;
    //public int weight;
    //public int knockback;
    public int maxHealth;

    [SerializeField] private Collider2D flowFieldObstacleCollider;

    private void Start()
    {
        unit.vfxManager = VFXManager.Instance;

        if (unit.vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
    }


    // Wall damage implementation

    public int currentHealth;

    //Change to OnUnitSpawned() when that gets implemented
    public override void onUnitSpawned()
    {
        currentHealth = maxHealth;
        NavigationManager nm = NavigationManager.Instance;
        if (nm != null)
            nm.GroundFlowField.AddColliderObstacle(flowFieldObstacleCollider);
    }

    public override void onKillUnit()
    {
        NavigationManager nm = NavigationManager.Instance;
        if (nm != null)
            nm.GroundFlowField.RemoveColliderObstacle(flowFieldObstacleCollider);
    }

    public void TakeDamage(int i) {
        currentHealth -= i;
        
        if (currentHealth <= 0) {
            unit.KillUnit();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Play, gameObject);
        }
    }
}
