using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackerType : UnitType, IPieGrabber
{
    [Header("Object Refs")]
    public RangeIndicator explosionRange;

    private Vector3 targetPosn;
    private Attackable activeTarget;
    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;
    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;

    private void Start()
    {
        unit.vfxManager = VFXManager.Instance;

        if (unit.vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        } else
        {
            unit.vfxManager.ManageParticles(5, VFXManager.VFXControl.Play, gameObject);
        }
    }

    public override void onUnitSpawned()
    {
        targetPosn = Vector3.zero;
        phase = FormationUnit.AttackPhase.Idle;
        attackPosn = Vector3.zero;
        isEndAttack = false;
        //outOfRange = false;
        unitElapsedFrames = 0f;

        PieGrabbed = false;
        isThrowingPie = false;
        pieThrowingCoroutine = null;
    }

    public void OnKillUnit()
    {
        if (pieThrowingCoroutine != null)
            StopCoroutine(pieThrowingCoroutine);
        pieThrowingCoroutine = null;
    }
    
    #region Pie Grabber Fields

    private Transform pieTransform;
    
    public bool PieGrabbed { get; private set; }

    public void GrabPie()
    {
        // Set the pie grabbed flag
        PieGrabbed = true;
        
        // Spawn a pie onto this transform
        GameObject piePrefab = Resources.Load<GameObject>("AttachedPie");
        pieTransform = Instantiate(piePrefab, transform).transform;
    }

    private bool isThrowingPie = false;
    private const float pieThrowingInterval = 0.5f;
    private static readonly WaitForSeconds waitForPieThrowing = new(pieThrowingInterval); // This is allocated here to prevent unnecessary GC allocations
    private Coroutine pieThrowingCoroutine = null; // Reference to the current pie throwing coroutine
        
    private IEnumerator ThrowPieRoutine()
    {
        isThrowingPie = true;

        // Find the nearest enemy
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject nearest = null;
        float nearestDistance = 3f;
        foreach (GameObject enemy in enemies)
        {
            float enemyDistance = Vector2.Distance(transform.position, enemy.transform.position);
            if (enemyDistance < nearestDistance)
                nearest = enemy;
        }
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obstacle in obstacles)
        {
            float obstacleDistance = Vector2.Distance(transform.position, obstacle.transform.position);
            if (obstacleDistance < nearestDistance)
                nearest = obstacle;
        }

        if (nearest == null)
        {
            isThrowingPie = false;
            yield break;   
        }
        
        // Get start and end positions
        Vector2 start = pieTransform.position;
        Vector2 end = nearest.transform.position;

        /* Throw the pie along a parabola */
        
        // Declare constants
        const float height = 3f;
        const float initialSpeed = 6f;
        const float gravity = 0.981f;

        // Calculate the horizontal distance
        float distance = Mathf.Abs(end.x - start.x);

        // Calculate the time it will take to complete the horizontal motion
        float flightTime = distance / initialSpeed;

        // Calculate the initial vertical velocity to reach the given height
        float initialVelocityY = Mathf.Sqrt(2f * gravity * height);

        // Calculate the total time needed for the projectile to return to the starting height
        float totalTime = 2f * initialVelocityY / gravity;
        
        float currentTime = 0f;
        while (currentTime <= flightTime)
        {
            float normalizedTime = currentTime / flightTime;
            float t = totalTime * normalizedTime;
            
            // Calculate pie position
            float x = Mathf.Lerp(start.x, end.x, normalizedTime);
            float y = start.y + initialVelocityY * t - 0.5f * gravity * Mathf.Pow(t, 2);

            // Update pie position
            pieTransform.position = new Vector2(x, y);

            // Increment current time
            currentTime += Time.deltaTime;

            // Wait until the next frame
            yield return null;
        }

        // Make sure the object ends up at the exact end position
        pieTransform.position = end;
        
        // Create a damage area
        PieDamageInRange(end, damage * 2);
        
        // Wait for attack interval
        yield return waitForPieThrowing;
        
        isThrowingPie = false;
        pieTransform.position = transform.position;
    }

    #endregion
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.CompareTag("Obstacle") || collider.CompareTag("Enemy"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (!isExit && newTarget != null)
            {
                if (newTarget.health > 0)
                    declareTarget(newTarget);
            }
            else
            {
                if (newTarget == activeTarget)
                {
                    activeTarget = null;
                }
            }
        }
    }
    
    public void declareTarget(Attackable newTarget)
    {
        if (phase != FormationUnit.AttackPhase.Attacking && !unit.isTimerRunning())
        {
            if (activeTarget != null)
            {

                if (activeTarget.health > newTarget.health)
                {
                    activeTarget = newTarget;
                }
            }
            else
            {
                activeTarget = newTarget;
            }
            // Start Unit Attack Animation
            setExplosionRange(activeTarget.gameObject.transform.position);
            attackPosn = activeTarget.gameObject.transform.position;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public void damageInRange(Vector2 position, int damage)
    {
        if (activeTarget is null)
            return;

        unit.vfxManager.ManageParticles(1, VFXManager.VFXControl.Play, gameObject);
        unit.vfxManager.ManageObjects(1, 0.5f, transform.position);

        unit.PlayDirectSFX("event:/FX/Explosion", position);

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(activeTarget.gameObject.transform.position, attackRange);
        int count = 0;
        while (count < hitColliders.Length)
        {
            if (hitColliders[count].CompareTag("Enemy") || hitColliders[count].CompareTag("Obstacle"))
            {
                Attackable target = hitColliders[count].GetComponent<Attackable>();
                if (target is null || target != activeTarget)
                {
                    hitColliders[count].GetComponent<Attackable>().takeDamage(damage);
                }

            }
            count++;
        }
    }

    public void PieDamageInRange(Vector2 position, int damage)
    {
        unit.vfxManager.ManageParticles(1, VFXManager.VFXControl.Play, gameObject);
        unit.vfxManager.ManageObjects(1, 0.5f, transform.position);

        unit.PlayDirectSFX("event:/FX/Explosion", position);

        setExplosionRange(position);

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(position, attackRange);
        foreach (Collider2D collider in hitColliders)
        {
            if (collider.CompareTag("Enemy") || collider.CompareTag("Obstacle"))
            {
                try
                {
                    if (collider.TryGetComponent(out Attackable damager))
                        damager.takeDamage(damage);
                }
                catch (Exception e)
                {
                    // Suppress exceptions from Attackables because they are terminating important function calls
                    Debug.LogException(e);
                }
            }
        }

        unit.vfxManager.ManageParticles(13, VFXManager.VFXControl.Play, gameObject);
    }

    public void setExplosionRange(Vector3 posn)
    {
        explosionRange.updatePosition(posn);
        explosionRange.gameObject.SetActive(true);
        explosionRange.setRange(attackRange, .2f, false, .5f);
    }

    public override void unitTimeOut()
    {
        unit.elapsedFrames = 0;

        if (activeTarget != null)
        {
            declareTarget(activeTarget);
        }
        else
        {

            // The original target is dead, so now we need to check if there's another valid target in range.

            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle")
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        declareTarget(target);
                    }

                }
                count++;
            }
        }

    }

    public override void unitBehavior()
    {
        // Pie attack
        if (PieGrabbed)
        {
            // Skip if is already throwing a pie
            if (isThrowingPie)
                return;

            // Start new pie throwing routine
            if (pieThrowingCoroutine != null)
                StopCoroutine(pieThrowingCoroutine);
            pieThrowingCoroutine = StartCoroutine(ThrowPieRoutine());
            return;
        }
        
        if (phase == FormationUnit.AttackPhase.Attacking && activeTarget != null)
        {
            // Not pie attack
            //else
            {
                if (!isEndAttack)
                {
                    //Update name of variable in FormationUnit
                    if (unitElapsedFrames / speed < 1)
                    {
                        transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                        unitElapsedFrames += Time.deltaTime;
                        
                    }
                    else
                    {
                        attackPosn = transform.position;
                        isEndAttack = true;

                        damageInRange(transform.position, damage);
                        if (activeTarget.takeDamage(damage))
                        {
                            //Debug.Log("ATTACKABLE SET TO NULL");
                            activeTarget = null;
                        }

                    }
                }
                else
                {
                    if (unitElapsedFrames / speed > 0)
                    {
                        transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                        unitElapsedFrames -= Time.deltaTime;
                    }
                    else
                    {

                        phase = FormationUnit.AttackPhase.Idle;
                        isEndAttack = false;
                        unit.startUnitTimer(attackCooldown);
                    }
                }   
            }
        }
        else if (phase == FormationUnit.AttackPhase.Attacking)
        {
            //Debug.Log("Out of Range and return to position");
            if (unitElapsedFrames / speed > 0)
            {
                transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                unitElapsedFrames -= Time.deltaTime;
            }
            else
            {
                phase = FormationUnit.AttackPhase.Idle;
                isEndAttack = false;
                unit.startUnitTimer(attackCooldown);
            }
        }
    }
}
