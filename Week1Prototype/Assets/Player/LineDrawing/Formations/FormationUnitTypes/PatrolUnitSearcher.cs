using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolUnitSearcher : MonoBehaviour
{
    public PatrolType type;
    public Collider2D collision;
    public EllipseCollider2D collController;

    private void Update()
    {
        transform.position = type.unit.truePosition;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (Vector3.Distance(type.unit.truePosition, collision.transform.position) < type.attackRange)
        {
            PatrolType newPatrol = collision.GetComponent<PatrolType>();

            if (newPatrol != null)
            {
                if (Vector3.Distance(type.unit.truePosition, newPatrol.unit.truePosition) < type.attackRange)
                {
                    type.patrolUnitInRange(newPatrol);
                }
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        PatrolType newPatrol = collision.GetComponent<PatrolType>();

        if (newPatrol != null)
        {
            type.patrolUnitOutRange(newPatrol);
        }

    }

    public void updateRange(float range)
    {
        Vector3 newRadius = RadiusRatios.calculateSkewedCircle(range);

        collController.radiusX = newRadius.x;
        collController.radiusY = newRadius.y;

        PolygonCollider2D polyColl = (PolygonCollider2D)collision;
        polyColl.points = collController.getPoints();
    }

    public List<Collider2D> getOverlapping()
    {
        List<Collider2D> list = new List<Collider2D>();
        collision.OverlapCollider(new ContactFilter2D().NoFilter(), list);
        return list;
    }
}
