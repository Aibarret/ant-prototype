using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LanceType : UnitType
{
    [SerializeField] GameObject hit;
    //[SerializeField] private FormationUnit unit;


    //[SerializeField] private int damage = 10;


    //[SerializeField] private int weight = 5;


    //[SerializeField] private int knockback = 0;


    private const float attackInterval = 0.75f;
    private static readonly WaitForSeconds waitForAttackInterval = new WaitForSeconds(attackInterval);
    
    private Coroutine unitBehaviorCoroutine = null;

    [SerializeField] private GameObject lance;
    [SerializeField] private GameObject lanceSprite;

    private readonly HashSet<GameObject> enemyInRangeSet = new HashSet<GameObject>();
    private readonly Stack<GameObject> enemyToRemoveStack = new Stack<GameObject>();

    [SerializeField] private CircleCollider2D attackRangeCollider;

    private void Awake()
    {
        Vector3 scale = lanceSprite.transform.localScale;
        scale.x = attackRangeCollider.radius;
        lanceSprite.transform.localScale = scale;
    }
    
    private void OnEnable()
    {
        if (unitBehaviorCoroutine != null)
            StopCoroutine(unitBehaviorCoroutine);
        unitBehaviorCoroutine = StartCoroutine(UnitBehaviorRoutine());   
    }

    private void OnDisable()
    {
        if (unitBehaviorCoroutine != null)
            StopCoroutine(unitBehaviorCoroutine);
    }

    private IEnumerator UnitBehaviorRoutine()
    {
        while (true)
        {
            // Find nearest enemy in range
            GameObject enemyToAttack = null;
            float minDistance = float.PositiveInfinity;
            foreach (GameObject enemy in enemyInRangeSet)
            {
                // Check if the enemy is destroyed or deactivated
                if (enemy == null || !enemy.activeSelf)
                {
                    enemyToRemoveStack.Push(enemy);
                    continue;
                }
                
                // Check if the enemy is the nearest
                float distance = Vector2.Distance(transform.position, enemy.transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    enemyToAttack = enemy;
                }
            }

            // Retry if not found any enemy
            if (enemyToAttack == null)
            {
                yield return null;
                continue;
            }
            
            // Calculate the lance direction and rotation
            Vector2 direction = (enemyToAttack.transform.position - transform.position).normalized;
            Vector3 rotation = lance.transform.rotation.eulerAngles;
            rotation.z = Mathf.Atan2(-direction.y, -direction.x) * Mathf.Rad2Deg;
            lance.transform.rotation = Quaternion.Euler(rotation);
            const float speed = 10f;
            
            // Move the lance towards the target position
            Vector3 targetScale = lance.transform.localScale;
            targetScale.x = .25f;
            while (lance.transform.localScale.x < targetScale.x)
            {
                lance.transform.localScale = Vector3.MoveTowards(lance.transform.localScale, targetScale, speed * Time.deltaTime);

                // Wait for the next frame
                yield return null;
            }

            // Deal damage
            if (enemyToAttack.TryGetComponent(out Attackable attackable))
            {
                attackable.takeDamage(damage, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
                unit.PlaySFX("Attack");
            }

            // Move the lance back
            targetScale.x = 0f;
            while (lance.transform.localScale.x > 0f)
            {
                lance.transform.localScale = Vector3.MoveTowards(lance.transform.localScale, targetScale, speed * Time.deltaTime);
                
                // Wait for the next frame
                yield return null;
            }

            yield return waitForAttackInterval;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Obstacle"))
            enemyInRangeSet.Add(other.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Obstacle"))
            return;
        
        if (enemyInRangeSet.Contains(other.gameObject))
            enemyInRangeSet.Remove(other.gameObject);
    }
}
