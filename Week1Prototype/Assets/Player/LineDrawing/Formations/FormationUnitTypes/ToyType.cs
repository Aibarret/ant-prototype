using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.VFX;

public class ToyType : UnitType, Clickable
{

    [Header("Toy Variables")]
    public GameObject cursorColl;
    public int durationPerActivation;
    public int pheromonesToActivate;
    public int pheromonesPerClick;
    public int maxActivationPhase;

    private Vector3 targetPosn;
    private Attackable activeTarget;

    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;

    private Vector3 returnPosn;
    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;
    private bool isActive = false;
    private int currentPheromones = 0;
    private float currentDurationTimer = 0f;
    //private bool isMouseOver;
    private DrawingManager dm;

    [SerializeField] GameObject BiteTop;
    [SerializeField] GameObject BiteBottom;

    private void Start()
    {
        GameObject drawingManager = GameObject.Find("DrawingManager");

        if (drawingManager != null)
        {
            dm = drawingManager.GetComponent<DrawingManager>();
        }
        else
        {
            Debug.LogError("DrawingManager GameObject not found in the scene.");
        }
    }

    private void ManageActivation()
    {
        if (pheromonesToActivate <= currentPheromones)
        {
            Debug.Log("Charged");
            currentPheromones -= pheromonesToActivate;
            unit.vfxManager.ManageParticles(11, VFXManager.VFXControl.Play, gameObject);
            isActive = true;
            currentDurationTimer += durationPerActivation;
        }
        if (currentDurationTimer >= 0)
        {
            currentDurationTimer -= Time.deltaTime;
        } else
        {
            isActive = false;
        }
    }

    private readonly HashSet<CentipedeType> centipedeSet = new HashSet<CentipedeType>();
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.TryGetComponent(out CentipedeType centipede))
        {
            if (isExit)
            {
                if (centipedeSet.Contains(centipede))
                    centipedeSet.Remove(centipede);
            }
            else
                centipedeSet.Add(centipede);
        }
        
        if ((collider.gameObject.CompareTag("Obstacle") || collider.gameObject.CompareTag("Enemy")) && isActive)
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (!isExit)
            {
                declareTarget(newTarget);
            }
            else
            {
                if (newTarget == activeTarget)
                {
                    activeTarget = null;
                }
            }
        }
    }

    public void declareTarget(Attackable newTarget)
    {
        if (centipedeSet.Count > 0)
        {
            activeTarget = centipedeSet.ElementAt(0).GetComponent<Attackable>();
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
            return;
        }
        
        if (phase != FormationUnit.AttackPhase.Attacking && !unit.isTimerRunning())
        {
            if (activeTarget != null)
            {

                if (activeTarget.health > newTarget.health && newTarget.health > 0)
                {
                    activeTarget = newTarget;
                }
            }
            else
            {
                activeTarget = newTarget;
            }
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
        }

        return;

        void StartUnitAttackAnimation()
        {
            attackPosn = activeTarget.gameObject.transform.position;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            returnPosn = transform.position;
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public override void unitTimeOut()
    {
        if (activeTarget != null)
        {
            Debug.Log("UNIT HAS TIMED OUT AND TARGET IS " + activeTarget);
            declareTarget(activeTarget);
        }
        else
        {
            // The original target is dead, so now we need to check if there's another valid target in range.

            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle")
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        declareTarget(target);
                    }

                }
                count++;
            }
        }
    }

    public override void unitBehavior()
    {
        ManageActivation();

        if (phase == FormationUnit.AttackPhase.Attacking && activeTarget != null)
        {
            if (!isEndAttack)
            {
                //Update name of variable in FormationUnit
                if (unitElapsedFrames / speed < 1)
                {
                    transform.position = Vector3.Lerp(unit.controller.unitPosnLines[unit.lineIndex][unit.spawnOrder], attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames += Time.deltaTime;
                }
                else
                {
                    attackPosn = transform.position;
                    isEndAttack = true;

                    Vector2 dirVec2 = (returnPosn - activeTarget.gameObject.transform.position).normalized;
                    float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;

                    unit.PlaySFX("Attack");

                    if (activeTarget.takeDamage(damage, direction: dir))
                    {
                        //Debug.Log("ATTACKABLE SET TO NULL");
                        activeTarget = null;
                    }

                }
            }
            else
            {
                if (unitElapsedFrames / speed > 0)
                {
                    transform.position = Vector3.Lerp(unit.controller.unitPosnLines[unit.lineIndex][unit.spawnOrder], attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames -= Time.deltaTime;
                }
                else
                {

                    phase = FormationUnit.AttackPhase.Idle;
                    isEndAttack = false;
                    unit.startUnitTimer(attackCooldown);
                }
            }

        }
        else if (phase == FormationUnit.AttackPhase.Attacking)
        {
            //Debug.Log("Out of Range and return to position");
            if (unitElapsedFrames / speed > 0)
            {
                transform.position = Vector3.Lerp(unit.controller.unitPosnLines[unit.lineIndex][unit.spawnOrder], attackPosn, unitElapsedFrames / speed);
                unitElapsedFrames -= Time.deltaTime;
                unit.vfxManager.ManageParticles(22, VFXManager.VFXControl.Play, gameObject);
                unit.vfxManager.ManageParticles(23, VFXManager.VFXControl.Play, gameObject);
            }
            else
            {
                phase = FormationUnit.AttackPhase.Idle;
                isEndAttack = false;
                unit.startUnitTimer(attackCooldown);
            }
        }
    }

    private void CheckAndHandleRPress()
    {
        /*Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 toyPos = transform.position;

        if (Vector3.Distance(mousePos, toyPos) - gameObject.GetComponent<CircleCollider2D>().radius <= cursorColl.GetComponent<CircleCollider2D>().radius)
        {
            if (currentPheromones <= maxActivationPhase - currentPheromones && !dm.isCostValid(pheromonesPerClick))
            {
                currentPheromones += pheromonesPerClick;
                Debug.Log("Current Pheromones: " + currentPheromones);
                Debug.Log("Pheromones To Activate: " + pheromonesToActivate);
                dm.CurrentPheromones -= pheromonesPerClick;
            }
        }*/
    }

    public void onClick()
    {
        if (currentPheromones <= maxActivationPhase - currentPheromones && !dm.isCostValid(pheromonesPerClick))
        {
            currentPheromones += pheromonesPerClick;
            dm.CurrentPheromones -= pheromonesPerClick;
        }
    }
}