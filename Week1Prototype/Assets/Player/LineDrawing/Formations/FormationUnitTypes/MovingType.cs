using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.VFX;

public class MovingType : UnitType
{
    private enum MovingPhase {
        Idle,
        Retrieving,
        Relocating
    }

    //public FormationUnit unit;

    [Header("Object & Component Refs")]
    public Transform carryPosn;
    public Animator animator;
    public LineRenderer lineRenderer;

    private MovingPhase phase;
    private Moveable activeTarget;
    private Transform targetTransform;
    private List<Vector3> waypointList;
    private Dictionary<Moveable, GameObject> targetsInRange = new Dictionary<Moveable, GameObject>();

    private void Start()
    {
        unit.vfxManager = VFXManager.Instance;

        if (unit.vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
    }

    //Change to OnUnitSpawned() when that gets implemented
    public override void onUnitSpawned() {
        targetsInRange = new Dictionary<Moveable, GameObject>();
        setIdle();
    }

    private void Update() 
    {
        if (waypointList.Count > 0 && waypointList.Count != lineRenderer.positionCount)
        {
            lineRenderer.positionCount = waypointList.Count;
            lineRenderer.SetPositions(waypointList.ToArray());
        }

        switch (phase) 
        {
            case MovingPhase.Idle:
                if (Vector3.Distance(transform.position, waypointList[0]) <= .5)
                {
                    animator.SetFloat("Speed", 0);
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, waypointList[0], (speed) * Time.deltaTime);
                    animator.SetFloat("Speed", speed * .5f);
                }
                break;
            case MovingPhase.Retrieving:
                transform.position = Vector3.MoveTowards(transform.position, targetTransform.position, (speed) * Time.deltaTime);
                if (Vector3.Distance(transform.position, targetTransform.position) <= .5)
                {

                    phase = MovingPhase.Relocating;
                    activeTarget.pickUp(gameObject, carryPosn.localPosition);
                }
                
                break;
            case MovingPhase.Relocating:
                unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Play, gameObject);
                if (waypointList.Count <= 0) 
                {
                    activeTarget.putDown(transform.position);
                    setIdle();
                    unit.KillUnit();
                    break;
                }
                Vector3 currentWaypoint = waypointList[0];
                transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, (speed) * Time.deltaTime);
                if (Vector3.Distance(transform.position, currentWaypoint) <= 0.001) 
                {
                    waypointList.RemoveAt(0);
                }
                break;


        }
    }

    public override void onTrigger(bool isExit, Collider2D collider)
    {

        if ((collider.tag == "Obstacle" || collider.tag == "Enemy") && phase != MovingPhase.Relocating)
        {
            bool isObstacle = collider.tag == "Obstacle";
            Moveable targetToConsider = collider.GetComponent<Moveable>();
            
            if (!targetToConsider.isCarryable())
            {
                return;
            }

            if (!isExit)
            {
                #region On Enter
                // Check if it should be added to the dict
                if (!targetsInRange.ContainsKey(targetToConsider))
                {
                    targetsInRange.Add(targetToConsider, collider.gameObject);

                    if (isObstacle)
                    {
                        //collider.GetComponent<Obstacle>().
                    }
                    else
                    {
                        collider.GetComponent<Enemy>().EventEnemyKilled += onEnemyDeath;
                    }
                }

                // determine if the active target should be changed
                if (activeTarget == null)
                {
                    // if no active target, just set it directly
                    Retrieve(targetToConsider);
                }
                else
                {
                    // if there is an active target, determine if it's an obstacle
                    if (targetsInRange[activeTarget].tag == "Obstacle" && isObstacle)
                    {
                        // if both the target and the new target are obstacles, grab the closer one
                        if (Vector3.Distance(transform.position, collider.transform.position) < Vector3.Distance(transform.position, targetsInRange[activeTarget].transform.position))
                        {
                            Retrieve(targetToConsider);
                        }
                    }
                    else if (targetsInRange[activeTarget].tag == "Enemy")
                    {
                        // if target is an enemy, than eiter set the new target directly if it's an obstacle, or grab the closer one if they're both enemies
                        if (isObstacle)
                        {
                            Retrieve(targetToConsider);
                        }
                        else
                        {
                            if (Vector3.Distance(transform.position, collider.transform.position) < Vector3.Distance(transform.position, targetsInRange[activeTarget].transform.position))
                            {
                                Retrieve(targetToConsider);
                            }
                        }
                    }

                    
                }

                #endregion
            }
            else
            {
                #region On Exit

                if (targetsInRange.ContainsKey(targetToConsider))
                {
                    // Remove the new target from the list. if it's the active target, recalculate target
                    targetsInRange.Remove(targetToConsider);

                    if (targetToConsider == activeTarget)
                    {
                        recalculateTarget();
                    }
                }
                else
                {
                    //print("Moving Type Found a target that was not in range dict but is exiting: " + targetToConsider);
                }

                #endregion
            }
        }

        

 /*       if (collider.gameObject.tag == "Obstacle")
        {
            Moveable newTarget = collider.GetComponent<Moveable>();
            targetTransform = collider.transform;
            if (newTarget == null) { return; }

            if (!isExit)
            {
                if (phase == MovingPhase.Idle)
                {
                    Retrieve(newTarget);
                }
            }
            else
            {
                if (newTarget == activeTarget)
                {
                    //activeTarget = null;
                }
            }
        }
        else if (collider.gameObject.tag == "Enemy")
        {

        }*/
    }

    public void onEnemyDeath(Enemy enemy)
    {
        if (targetsInRange.ContainsKey(enemy))
        {
            targetsInRange.Remove(enemy);

            if ((object) activeTarget == enemy)
            {
                recalculateTarget();
            }
        }
    }

    public void onObstacleDeath(Obstacle obstacle)
    {
        if (targetsInRange.ContainsKey(obstacle))
        {
            targetsInRange.Remove(obstacle);

            if ((object)activeTarget == obstacle)
            {
                recalculateTarget();
            }
        }
    }

    private void recalculateTarget()
    {
        List<Moveable> obstacles = new List<Moveable>();
        List<Moveable> enemies = new List<Moveable>();

        foreach (KeyValuePair<Moveable, GameObject> potentialTarget in targetsInRange)
        {
            if (potentialTarget.Value.tag == "Obstacle")
            {
                obstacles.Add(potentialTarget.Key);
            }
            else
            {
                enemies.Add(potentialTarget.Key);
            }
        }

        float smallestDistance = attackRange;
        Moveable closestObject = null;
        
        if (obstacles.Count > 0)
        {
            smallestDistance = Vector3.Distance(transform.position, targetsInRange[obstacles[0]].transform.position);
            closestObject = obstacles[0];

            foreach (Moveable ob in obstacles)
            {
                if (Vector3.Distance(transform.position, targetsInRange[ob].transform.position) <= smallestDistance)
                {
                    smallestDistance = Vector3.Distance(transform.position, targetsInRange[ob].transform.position);
                    closestObject = ob;
                }
            }

            if (closestObject != null)
            {
                Retrieve(closestObject);
                return;
            }
        }

        if (enemies.Count > 0)
        {
            smallestDistance = Vector3.Distance(transform.position, targetsInRange[enemies[0]].transform.position);
            closestObject = enemies[0];

            foreach (Moveable ob in enemies)
            {
                if (Vector3.Distance(transform.position, targetsInRange[ob].transform.position) <= smallestDistance)
                {
                    smallestDistance = Vector3.Distance(transform.position, targetsInRange[ob].transform.position);
                    closestObject = ob;
                }
            }

            if (closestObject != null)
            {
                Retrieve(closestObject);
                return;
            }
        }

        setIdle();
        
    }

    private void setIdle()
    {
        phase = MovingPhase.Idle;
        activeTarget = null;
        targetTransform = null;
    }

    private void Retrieve(Moveable target) {
        //CancelInvoke();
        activeTarget = target;
        targetTransform = targetsInRange[target].transform;
        phase = MovingPhase.Retrieving;
        animator.SetFloat("Speed", Mathf.Ceil(speed * .5f));
    }

    public void SetWaypoints(List<Vector3> waypoints) {
        waypointList = waypoints;
    }


    
}
