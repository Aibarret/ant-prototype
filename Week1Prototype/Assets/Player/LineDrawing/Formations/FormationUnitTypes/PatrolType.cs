using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PatrolType : UnitType
{
    [Header("Patrol Stats")]
    public int patrolCap;

    [Header("Object Refs")]
    public PatrolUnitSearcher unitSearcher;
    
    private Dictionary<PatrolType, StatusEffect> activeEffects = new Dictionary<PatrolType, StatusEffect>();
    private Dictionary<PatrolType, StatusEffect> benchedEffects = new Dictionary<PatrolType, StatusEffect>();

    private Vector3 targetPosn;
    private Attackable activeTarget;

    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;


    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;
    private List<Collider2D> collidersInTrigger = new List<Collider2D>();


    public override void onKillUnit()
    {
        phase = FormationUnit.AttackPhase.Idle;
        activeTarget = null;
        attackPosn = Vector3.zero;
        unitElapsedFrames = 0f;

        List<PatrolType> patrolList = new List<PatrolType>(activeEffects.Keys);

        for (int i = 0; i < patrolList.Count; i++)
        {
            if (patrolList[i].gameObject.activeInHierarchy)
            {
                patrolList[i].recalculateBuff(gameObject);
        //StartCoroutine(patrolList[i].recalculateBuff());
    }
        }

        patrolList = new List<PatrolType>(benchedEffects.Keys);

        for (int i = 0; i < patrolList.Count; i++)
        {
            if (patrolList[i].gameObject.activeInHierarchy)
            {
                patrolList[i].recalculateBuff(gameObject);
                //StartCoroutine(patrolList[i].recalculateBuff());
            }
        }
    }

    private readonly HashSet<CentipedeType> centipedeSet = new HashSet<CentipedeType>();
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.TryGetComponent(out CentipedeType centipede))
        {
            if (isExit)
            {
                if (centipedeSet.Contains(centipede))
                    centipedeSet.Remove(centipede);
            }
            else
                centipedeSet.Add(centipede);
        }
        
        if (collider.CompareTag("Obstacle") || collider.CompareTag("Enemy"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (newTarget != null)
            {
                if (!isExit)
                {
                    if (newTarget.health > 0)
                        declareTarget(newTarget);
                }
                else
                {
                    if (newTarget == activeTarget)
                    {
                        activeTarget = null;
                    }
                }
            }

            
        }
        /*else if (collider.CompareTag("FormationUnit") && Vector3.Distance(transform.position, collider.transform.position) < attackRange)
        {
            PatrolType newPatrol = collider.GetComponent<PatrolType>();

            if (newPatrol != null)
            {
                if (!isExit)
                {
                    if (!activeEffects.ContainsKey(newPatrol))
                    {
                        if (activeEffects.Count < patrolCap)
                        {
                            PatrolBuffStatus newStatus = new PatrolBuffStatus();
                            unit.applyStatusEffect(newStatus);
                            activeEffects.Add(newPatrol, newStatus);
                        }
                        else if (!benchedEffects.ContainsKey(newPatrol))
                        {
                            PatrolBuffStatus newStatus = new PatrolBuffStatus();
                            benchedEffects.Add(newPatrol, newStatus);
                        }
                    }
                }
                else
                {
                    if (activeEffects.ContainsKey(newPatrol))
                    {
                        unit.removeStatusEffect(activeEffects[newPatrol]);
                        activeEffects.Remove(newPatrol);

                        if (activeEffects.Count < patrolCap)
                        {
                            foreach (KeyValuePair<PatrolType, StatusEffect> pair in benchedEffects)
                            {
                                activeEffects.Add(pair.Key, pair.Value);
                                benchedEffects.Remove(pair.Key);
                                break;
                            }
                        }
                    }
                    else if (benchedEffects.ContainsKey(newPatrol))
                    {
                        benchedEffects.Remove(newPatrol);
                    }


                }
            }
        }*/
    }

    public void patrolUnitInRange(PatrolType patrolType)
    {
        if (!activeEffects.ContainsKey(patrolType))
        {
            if (activeEffects.Count < patrolCap)
            {
                PatrolBuffStatus newStatus = new PatrolBuffStatus();
                unit.applyStatusEffect(newStatus);
                activeEffects.Add(patrolType, newStatus);
            }
            else if (!benchedEffects.ContainsKey(patrolType))
            {
                PatrolBuffStatus newStatus = new PatrolBuffStatus();
                benchedEffects.Add(patrolType, newStatus);
            }
        }
    }

    public void patrolUnitOutRange(PatrolType patrolType)
    {
        if (activeEffects.ContainsKey(patrolType))
        {
            unit.removeStatusEffect(activeEffects[patrolType]);
            activeEffects.Remove(patrolType);

            if (activeEffects.Count < patrolCap)
            {
                foreach (KeyValuePair<PatrolType, StatusEffect> pair in benchedEffects)
                {
                    activeEffects.Add(pair.Key, pair.Value);
                    benchedEffects.Remove(pair.Key);
                    break;
                }
            }
        }
        else if (benchedEffects.ContainsKey(patrolType))
        {
            benchedEffects.Remove(patrolType);
        }
    }

    public override void onChangeAttackRange()
    {
        unitSearcher.updateRange(attackRange);
    }

    public void recalculateBuff(GameObject removingObject)
    {

        foreach(var item in activeEffects)
        {
            item.Value.removeEffect();
        }
        
        activeEffects.Clear();
        benchedEffects.Clear();

        List<Collider2D> array = unitSearcher.getOverlapping(); //Physics2D.OverlapCircleAll(transform.position, attackRange, LayerMask.GetMask("Ground"));

        foreach (Collider2D collider in array)
        {
            if (collider.tag.Equals("FormationUnit") && 
                (collider.gameObject != gameObject) && (collider.gameObject != removingObject) && 
                (Vector3.Distance(transform.position, collider.transform.position) < attackRange))
            {

                PatrolType newPatrol = collider.GetComponent<PatrolType>();

                if (!activeEffects.ContainsKey(newPatrol))
                {
                    if (activeEffects.Count < patrolCap)
                    {
                        PatrolBuffStatus newStatus = new PatrolBuffStatus();
                        unit.applyStatusEffect(newStatus);
                        activeEffects.Add(newPatrol, newStatus);
                    }
                    else if (!benchedEffects.ContainsKey(newPatrol))
                    {
                        PatrolBuffStatus newStatus = new PatrolBuffStatus();
                        benchedEffects.Add(newPatrol, newStatus);
                    }
                }
            }
        }

        
    }

    public void declareTarget(Attackable newTarget)
    {
        if (centipedeSet.Count > 0)
        {
            activeTarget = centipedeSet.ElementAt(0).GetComponent<Attackable>();
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
            return;
        }
        
        if (phase != FormationUnit.AttackPhase.Attacking && !unit.isTimerRunning())
        {
            if (activeTarget != null)
            {

                if (activeTarget.health > newTarget.health)
                {
                    activeTarget = newTarget;
                }
            }
            else
            {
                activeTarget = newTarget;
            }
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
        }

        return;

        void StartUnitAttackAnimation()
        {
            attackPosn = activeTarget.gameObject.transform.position;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public override void unitTimeOut()
    {
        if (activeTarget != null)
        {
            if (activeTarget.health > 0)
                declareTarget(activeTarget);
        }
        else
        {
            // The original target is dead, so now we need to check if there's another valid target in range.

            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle")
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        if (target.health > 0)
                        {
                            declareTarget(target);
                            break;
                        }
                    }

                }
                count++;
            }
        }
    }

    public override void unitBehavior()
    {
        if (phase == FormationUnit.AttackPhase.Attacking && activeTarget != null)
        {

            if (!isEndAttack)
            {
                //Update name of variable in FormationUnit
                if (unitElapsedFrames / speed < 1)
                {
                    transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames += Time.deltaTime;
                }
                else
                {
                    attackPosn = transform.position;
                    isEndAttack = true;

                    Vector2 dirVec2 = (unit.truePosition - activeTarget.gameObject.transform.position).normalized;
                    float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;

                    unit.PlaySFX("Attack");
                    if (activeTarget.takeDamage(damage, direction: dir))
                    {
                        //Debug.Log("ATTACKABLE SET TO NULL");
                        activeTarget = null;
                    }

                }
            }
            else
            {


                if (unitElapsedFrames / speed > 0)
                {
                    transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames -= Time.deltaTime;
                }
                else
                {

                    phase = FormationUnit.AttackPhase.Idle;
                    isEndAttack = false;
                    unit.startUnitTimer(attackCooldown);
                }
            }

        }
        else if (phase == FormationUnit.AttackPhase.Attacking)
        {
            //Debug.Log("Out of Range and return to position");
            if (unitElapsedFrames / speed > 0)
            {
                transform.position = Vector3.Lerp(unit.truePosition, attackPosn, unitElapsedFrames / speed);
                unitElapsedFrames -= Time.deltaTime;
            }
            else
            {
                phase = FormationUnit.AttackPhase.Idle;
                isEndAttack = false;
                unit.startUnitTimer(attackCooldown);
            }
        }
    }

    private void ManagePatrolVariables()
    {
       /* int patrolCount = 0;
        attackCooldown = defaultRate;
        rangeColl.radius = defaultRange;
        collidersInTrigger.Clear();
        Physics2D.OverlapCollider(rangeColl, new ContactFilter2D().NoFilter(), collidersInTrigger);

        for(int i = 0; i < collidersInTrigger.Count; i++)
        {
            if(collidersInTrigger[i].gameObject.name.StartsWith("PatrolUnit"))
            {
                patrolCount++;
            }
            if(patrolCount >= patrolCap)
            {
                break;
            }
        }

        attackCooldown += (rateBoostPerUnit * patrolCount);
        rangeColl.radius += (rangeBoostPerUnit * patrolCount);*/
    }


}
