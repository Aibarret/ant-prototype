using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostType : UnitType
{
    //public FormationUnit unit;
    //public int damage;
    //public int weight;
    //public int knockback;

    /*    public StatusValues positiveBoostValues;
        public StatusValues negativeBoostValues;*/


    [Header("Wizard Variables")]
    public float poisonDamageRate;
    public float poisonDuration;

    private Dictionary<GameObject, StatusEffect> activeBuffs = new Dictionary<GameObject, StatusEffect>();
    private Dictionary<GameObject, PoisonStatus> activePoison = new Dictionary<GameObject, PoisonStatus>();

    //private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;

    public override void onUnitSpawned() {
        
    }

    public override void onKillUnit()
    {
        foreach (GameObject key in activePoison.Keys)
        {
            if (activePoison[key] != null)
            {
                activePoison[key].setTimer(poisonDuration);
            }
            
        }

        foreach (GameObject key in activeBuffs.Keys)
        {
            if (activeBuffs[key] != null)
            {
                activeBuffs[key].removeEffect();
            }

        }

        activeBuffs.Clear();
        activePoison.Clear();


    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //print(collider.name + " entered");
        if (collider.tag.Equals("FormationUnit"))
        {
            if (!activeBuffs.ContainsKey(collider.gameObject))
            {
                StatusEffect buff = new WizardBuffStatus();
                activeBuffs.Add(collider.gameObject, buff);
                unit.PlayDirectSFX("event:/Enemies/Praying Mantis Heal", collider.transform.position);
                collider.GetComponent<StatusEffectable>().applyStatusEffect(buff);

                Transform crackerAOE = collider.transform.Find("CrackerAOE");
                if (crackerAOE != null)
                {
                    crackerAOE.gameObject.SetActive(true);
                }
                collider.GetComponent<FormationUnit>().vfxManager.ManageParticles(18, VFXManager.VFXControl.Play, collider.gameObject);
            }
        }
        else if (collider.tag.Equals("Enemy"))
        {
            StatusEffectable target = collider.GetComponent<StatusEffectable>();
            Enemy enemyScript = collider.GetComponent<Enemy>();

            if (!target.hasStatusEffectOfType("PoisonStatus") && !activePoison.ContainsKey(collider.gameObject))
            {
                PoisonStatus posion = new PoisonStatus(poisonDamageRate);
                activePoison.Add(collider.gameObject, posion);
                target.applyStatusEffect(posion);
                collider.GetComponent<Enemy>().vfxManager.ManageParticles(24, VFXManager.VFXControl.Play, collider.gameObject);
                collider.GetComponent<SpriteRenderer>().color = new Color(.4f, 0, 1, 1);
                enemyScript.EventEnemyKilled += OnEnemyDeath;
            }
            
        }

        /*Enemy enemy = collider.GetComponent<Enemy>();
        if (enemy != null) enemy.ApplyStatus(StatusEnum.BoostNegative, negativeBoostValues.Clone());
        FormationUnit unit = collider.GetComponent<FormationUnit>();
        if (unit != null) enemy.ApplyStatus(StatusEnum.BoostPositive, positiveBoostValues.Clone());*/

    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag.Equals("FormationUnit") && activeBuffs.ContainsKey(collider.gameObject))
        {
            collider.GetComponent<StatusEffectable>().removeStatusEffect(activeBuffs[collider.gameObject]);
            Transform crackerAOE = collider.transform.Find("CrackerAOE");
            if (crackerAOE != null)
            {
                crackerAOE.gameObject.SetActive(false);
            }
            collider.GetComponent<FormationUnit>().vfxManager.ManageParticles(18, VFXManager.VFXControl.Stop, collider.gameObject);
        }
        else if (collider.tag.Equals("Enemy") && activePoison.ContainsKey(collider.gameObject))
        {
            activePoison[collider.gameObject].setTimer(poisonDuration);
            Enemy enemyScript = collider.GetComponent<Enemy>();
            collider.GetComponent<Enemy>().vfxManager.ManageParticles(24, VFXManager.VFXControl.Stop, collider.gameObject);
            collider.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            enemyScript.EventEnemyKilled -= OnEnemyDeath;
        }

        /*Enemy enemy = collider.GetComponent<Enemy>();
        if (enemy != null) enemy.RemoveStatus(StatusEnum.BoostNegative);
        FormationUnit unit = collider.GetComponent<FormationUnit>();
        if (unit != null) enemy.RemoveStatus(StatusEnum.BoostPositive);*/
    }

    private void OnEnemyDeath(Enemy enemy)
    {
        if (activePoison.ContainsKey(enemy.gameObject))
        {
            activePoison.Remove(enemy.gameObject);
        }
    }

}
