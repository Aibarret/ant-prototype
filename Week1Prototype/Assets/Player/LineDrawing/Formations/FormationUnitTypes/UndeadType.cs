using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndeadType : BaseType
{
    //UndeadType is just a weaker BaseType. This script is necessary to differentiate the two for some purposes
    //but functionally- they are the same. So it just extends BaseType.
}
