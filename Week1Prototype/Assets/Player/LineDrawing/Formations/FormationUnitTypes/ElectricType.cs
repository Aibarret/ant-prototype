﻿using System.Collections.Generic;
using UnityEngine;

namespace Player.Formations.FormationUnitTypes
{
    public class ElectricType : UnitType, IElectricitySource
    {
        private readonly HashSet<Attackable> targetInRangeSet = new HashSet<Attackable>();
        private readonly HashSet<IElectricitySource> electricitySourceSet = new HashSet<IElectricitySource>();

        private Dictionary<GameObject, StatusEffect> activeEffects = new Dictionary<GameObject, StatusEffect>();
        private ChargedStatus chargedStatusEffect;
        private float elapsedFrames;

        /// <summary>
        /// Whether this electric type is connected to a battery.
        /// </summary>
        private bool isPoweredUp;

        public bool HasPower => isPoweredUp;


        
        public override void onUnitSpawned()
        {
            isPoweredUp = false;
            
        }

        public override void onTrigger(bool isExit, Collider2D collider)
        {
            if (collider.TryGetComponent(out IElectricitySource source))
            {
                // Enter
                if (!isExit)
                    electricitySourceSet.Add(source);
                // Exit
                else
                    electricitySourceSet.Remove(source);
            }
            else if (collider.CompareTag("Obstacle") || collider.CompareTag("Enemy"))
            {
                Attackable target = collider.GetComponent<Attackable>();
                StatusEffectable targetEffectable = null;
                Enemy enemyScript = null;

                if (collider.CompareTag("Enemy"))
                {
                    targetEffectable = collider.GetComponent<StatusEffectable>();
                    enemyScript = collider.GetComponent<Enemy>();
                }

                if (!isExit)
                {
                    targetInRangeSet.Add(target);

                    if (targetEffectable != null)
                    {
                        StatusEffect newEffect = new ElectrifiedStatus();
                        targetEffectable.applyStatusEffect(newEffect);
                        activeEffects.Add(collider.gameObject, newEffect);

                        if (enemyScript != null)
                        {
                            enemyScript.EventEnemyKilled += OnEnemyDeath;
                        }
                    }
                }
                else
                {
                    targetInRangeSet.Remove(target);

                    if (targetEffectable!= null && activeEffects.ContainsKey(collider.gameObject))
                    {
                        if (activeEffects.ContainsKey(collider.gameObject))
                        {
                            targetEffectable.removeStatusEffect(activeEffects[collider.gameObject]);
                            activeEffects.Remove(collider.gameObject);
                        }

                        if (enemyScript != null)
                        {
                            enemyScript.EventEnemyKilled -= OnEnemyDeath;
                        }
                    }
                }
                    
            }
        }

        public override void unitBehavior()
        {

            bool wasPoweredPreviously = isPoweredUp;

            // Check if connected
            isPoweredUp = false;
            Color c = Color.gray;
            foreach (IElectricitySource source in electricitySourceSet)
            {
                if (source.HasPower)
                {
                    c = Color.yellow;
                    unit.vfxManager.ManageParticles(11, VFXManager.VFXControl.Play, gameObject);
                    
                    isPoweredUp = true;
                    break;
                }
            }
            c.a = 0.5f;

            if (!wasPoweredPreviously && isPoweredUp)
            {
                if (chargedStatusEffect == null)
                {
                    chargedStatusEffect = new ChargedStatus();
                    unit.applyStatusEffect(chargedStatusEffect);

                    foreach (KeyValuePair<GameObject, StatusEffect> keyPair in activeEffects)
                    {
                        if (keyPair.Value is ElectrifiedStatus)
                        {
                            ElectrifiedStatus status = (ElectrifiedStatus) keyPair.Value;
                            status.toggleCharge(true);
                        }
                    }
                }
                
            }

            if (!isPoweredUp && chargedStatusEffect != null)
            {
                unit.removeStatusEffect(chargedStatusEffect);

                foreach (KeyValuePair<GameObject, StatusEffect> keyPair in activeEffects)
                {
                    if (keyPair.Value is ElectrifiedStatus)
                    {
                        ElectrifiedStatus status = (ElectrifiedStatus)keyPair.Value;
                        status.toggleCharge(false);
                    }
                }
            }

            // REWORK COLORS HERE
            
            // Attack behavior
            //attackCooldown -= Time.deltaTime;

            if (elapsedFrames < attackCooldown)
            {
                elapsedFrames += Time.deltaTime;
            }
            else
            {
                // Play sound effect, or visual effect maybe...
                if (targetInRangeSet.Count > 0)
                {
                    unit.PlaySFX("Electric Spark");
                }

                // Deal damage to all enemies and obstacles in range
                foreach (Attackable target in targetInRangeSet)
                {
                    target?.takeDamage(damage);

                }

                elapsedFrames = 0;
            }
        }

        private void OnEnemyDeath(Enemy enemy)
        {
            if (activeEffects.ContainsKey(enemy.gameObject))
            {
                activeEffects.Remove(enemy.gameObject);
            }
        }

        public void OnRandomInterval()
        {
            unit.PlaySFX("Electric Spark");
        }
    }

    
}