using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SuperType : UnitType
{
    //public float maxHeight = 5f;
    //public float defaultHeight;
    //public float flyAttackRange;

    private Attackable activeTarget;

    private FormationUnit.AttackPhase phase = FormationUnit.AttackPhase.Idle;

    private Vector3 returnPosn;
    private Vector3 attackPosn;
    private bool isEndAttack;
    //private bool outOfRange = false;
    private float unitElapsedFrames;
    //private bool flying = false;
    private float currentCooldown = 0.0f;

    [SerializeField] private SpriteRenderer flightShadow;
    [SerializeField] private FlyingHeight flyingHeight;

    private void Start()
    {
        currentCooldown = 0;
        unit.vfxManager = VFXManager.Instance;

        if (unit.vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
    }

    IEnumerator FlyToEnemy(GameObject target)
    {
        if (target != null) {

            unit.vfxManager.ManageParticles(15, VFXManager.VFXControl.Play, gameObject);
            unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Play, gameObject);
            float startTime = Time.time;
            float attackLength = Vector3.Distance(transform.position, target.transform.position);

            while (Time.time - startTime < attackCooldown)
            {
                float distCovered = (Time.time - startTime) * attackLength;
                float otw = distCovered / attackLength;

                transform.position = Vector3.Lerp(transform.position, target.transform.position, otw);

                if (Vector3.Distance(transform.position, target.transform.position) < 1f)
                {
                    // Stop the coroutine if needed
                    yield break;
                }

                yield return null;
            }
        }
    }

    IEnumerator ResetFlight()
    {
        /*unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Play, gameObject);
        float startTime = Time.time;
        float attackLength = Vector3.Distance(transform.position, new Vector3(transform.position.x, transform.position.y - defaultHeight, transform.position.z)); // Use your default height here

        while (Time.time - startTime < attackCooldown)
        {
            float distCovered = (Time.time - startTime) * (attackLength / attackCooldown);
            float fracJourney = distCovered / attackLength;

            float height = Mathf.Lerp(transform.position.y, transform.position.y - defaultHeight, fracJourney);
            transform.position = new Vector3(transform.position.x, height, transform.position.z);

            yield return null;
        }*/
        yield return null;
    }

    public override void onKillUnit()
    {
        phase = FormationUnit.AttackPhase.Idle;
        activeTarget = null;
        attackPosn = Vector3.zero;
        returnPosn = Vector3.zero;
        unitElapsedFrames = 0f;
    }

    private readonly HashSet<CentipedeType> centipedeSet = new HashSet<CentipedeType>();
    
    public override void onTrigger(bool isExit, Collider2D collider)
    {
        if (collider.TryGetComponent(out CentipedeType centipede))
        {
            if (isExit)
            {
                if (centipedeSet.Contains(centipede))
                    centipedeSet.Remove(centipede);
            }
            else
                centipedeSet.Add(centipede);
        }
        
        if (collider.gameObject.CompareTag("Obstacle") || collider.gameObject.CompareTag("Enemy"))
        {
            Attackable newTarget = collider.GetComponent<Attackable>();

            if (currentCooldown <= 0)
            {
                currentCooldown = attackCooldown;
                declareTarget(newTarget);
            }
        }
    }

    public void declareTarget(Attackable newTarget)
    {
        if (centipedeSet.Count > 0)
        {
            activeTarget = centipedeSet.ElementAt(0).GetComponent<Attackable>();
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
            return;
        }
        
        if (phase != FormationUnit.AttackPhase.Attacking)
        {
            if (newTarget == null)
            {
                return;
            }

            if (activeTarget != null)
            {
                if (activeTarget.health > 0)
                {
                    if (activeTarget.health > newTarget.health)
                    {
                        activeTarget = newTarget;
                    }

                }
            }
            else
            {
                activeTarget = newTarget;
            }
            
            // Start Unit Attack Animation
            StartUnitAttackAnimation();
        }

        return;

        void StartUnitAttackAnimation()
        {
            StartCoroutine(FlyToEnemy(activeTarget.gameObject));
            attackPosn = activeTarget.gameObject.transform.position;
            unit.changeSpriteDirection(transform.position.x < attackPosn.x);
            returnPosn = transform.position;
            unit.elapsedFrames = 0;
            phase = FormationUnit.AttackPhase.Attacking;
        }
    }

    public override void unitTimeOut()
    {
        if (activeTarget != null)
        {
            currentCooldown = attackCooldown;
            Debug.Log("UNIT HAS TIMED OUT AND TARGET IS " + activeTarget);
            declareTarget(activeTarget);
        }
        else
        {
            unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Stop, gameObject);
            List<Collider2D> attackablesInRange = new List<Collider2D>(); //Physics2D.OverlapCircleAll(unit.transform.position, attackRange);

            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Count)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle" 
                    && attackablesInRange[count].isActiveAndEnabled)
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        currentCooldown = attackCooldown;
                        declareTarget(target);
                    }

                }
                count++;
            }
        }
    }

    public override void unitBehavior()
    {
        if (phase == FormationUnit.AttackPhase.Attacking && activeTarget != null)
        {
            if (!isEndAttack)
            {
                if (unitElapsedFrames / speed < 1)
                {
                    transform.position = Vector3.Lerp(unit.controller.unitPosnLines[unit.lineIndex][unit.spawnOrder], attackPosn, unitElapsedFrames / speed);
                    unitElapsedFrames += Time.deltaTime;
                }
                else
                {
                    currentCooldown = attackCooldown;
                    attackPosn = transform.position;
                    isEndAttack = true;

                    Vector2 dirVec2 = (returnPosn - activeTarget.gameObject.transform.position).normalized;
                    float dir = Mathf.Atan2(dirVec2.y, dirVec2.x) * Mathf.Rad2Deg;

                    unit.PlaySFX("Attack");

                    if (activeTarget.takeDamage(damage, direction: dir))
                    {
                        //Debug.Log("ATTACKABLE SET TO NULL");
                        phase = FormationUnit.AttackPhase.Idle;
                        activeTarget = null;
                    }
                }
            } else {
                if (currentCooldown > 0)
                {
                    currentCooldown -= Time.deltaTime;
                }
                else if (currentCooldown <= 0 && isEndAttack)
                {
                    isEndAttack = false;
                }
            }
        }
        if(phase != FormationUnit.AttackPhase.Attacking)
        {
            unit.vfxManager.ManageParticles(4, VFXManager.VFXControl.Stop, gameObject);
            Collider2D[] attackablesInRange = Physics2D.OverlapCircleAll(unit.transform.position, attackRange);
            unit.hitbox.OverlapCollider(new ContactFilter2D(), attackablesInRange);

            int count = 0;
            while (count < attackablesInRange.Length)
            {
                if (attackablesInRange[count].gameObject.tag == "Enemy" || attackablesInRange[count].gameObject.tag == "Obstacle")
                {
                    Attackable target = attackablesInRange[count].GetComponent<Attackable>();
                    if (target != activeTarget)
                    {
                        currentCooldown = attackCooldown;
                        declareTarget(target);
                    }

                }
                count++;
            }
        }
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == activeTarget.gameObject)
        {
            StopCoroutine(FlyToEnemy(activeTarget.gameObject));
        }
    }*/
}
