using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostForm : Formation
{
    public override int cost => 6;
    public override float spawnDistance => 4;
    public override string unitPrefab => "BoostUnit";
    public override string lineNode => "BoostNode";
    public override int drawLimit => 2;
}
