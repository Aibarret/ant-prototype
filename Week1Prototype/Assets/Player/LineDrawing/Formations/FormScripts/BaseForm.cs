using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseForm : Formation
{
    public override int cost => 2;
    public override float spawnDistance => 1;
    public override string unitPrefab => "BaseUnit";
    public override string lineNode => "BaseNode";
    public override int drawLimit => 15;
}
