using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndeadForm : Formation
{
    public override int cost => 0;
    public override float spawnDistance => 1;
    public override string unitPrefab => "UndeadUnit";
    public override string lineNode => "UndeadNode";

    public override int drawLimit => 1;
}
