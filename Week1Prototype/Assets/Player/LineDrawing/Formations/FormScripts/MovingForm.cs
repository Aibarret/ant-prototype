using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingForm : Formation 
{
    public override int cost => 1;
    public override float spawnDistance => 1f;
    public override string unitPrefab => "MovingUnit";
    public override string lineNode => "MovingNode";

    //This cannot be 1. Each point after the first one is used to create the path for the unit.
    public override int drawLimit => 1000; 

    public override void spawnUnits(List<Vector3> waypointList) {
        GlobalVariables.drawingManager.movingUnitSpawnBehavior(waypointList, unitPrefab, this);
        /*int count = 0;
        unitLines.Add(new List<GameObject>());
        unitPosnLines.Add(new List<Vector3>());
        int lineIndex = unitLines.Count != 0 ? unitLines.Count - 1 : 0;

        GameObject go = GlobalVariables.drawingManager.getInactiveUnit(unitPrefab); //Instantiate(Resources.Load<GameObject>(unitPrefab));
        
        if (go == null)
        {
            go = Instantiate(Resources.Load<GameObject>(unitPrefab));
            GlobalVariables.drawingManager.addToActivePool(unitPrefab, go);
            go.transform.parent = GlobalVariables.drawingManager.AntContainer;
        }

        FormationUnit formationUnit = go.GetComponent<FormationUnit>();

        formationUnit.controller = this;
        formationUnit.spawnOrder = count;
        formationUnit.lineIndex = lineIndex;

        // Add the FormationUnit reference to a global accessible list
        GlobalVariables.FormationUnitList.Add(formationUnit);

        go.transform.position = waypointList[0];

        unitLines[lineIndex].Add(go);
        unitPosnLines[lineIndex].Add(waypointList[count]);

        MovingType movingType = formationUnit.GetComponent<MovingType>();

        go.SetActive(true);
        int i = 0;
        foreach (Vector3 vector in waypointList) {
            //Debug.Log(i + ": " + vector);
            i++;
        }
        movingType.SetWaypoints(waypointList);
        formationUnit.spawnUnit();

        return null;*/
    }

    public override bool isDrawUnitAtPoint(List<Vector3> list)
    {
        return list.Count == 1;
    }

    public override bool stopDrawingCondition() {
        return GlobalVariables.drawingManager.CurrentPheromones < cost;
    }

    public override bool continueDrawingCondition() {
        return GlobalVariables.drawingManager.CurrentPheromones >= cost;
    }
}