using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyForm : Formation
{
    public override int cost => 2;
    public override float spawnDistance => 1.5F;
    public override string unitPrefab => "ToyUnit";
    public override string lineNode => "ToyNode";

    public override int drawLimit => 12;
}
