using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackerForm : Formation
{
    public override int cost => 3;
    public override float spawnDistance => 1.3f;
    public override string unitPrefab => "CrackerUnit";
    public override string lineNode => "CrackerNode";
    public override int drawLimit => 12;

}
