using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricForm : Formation
{
    public override int cost => 3;
    public override float spawnDistance => 3;
    public override string unitPrefab => "ElectricUnit";
    public override string lineNode => "ElectricNode";
    public override int drawLimit => 5;
}
