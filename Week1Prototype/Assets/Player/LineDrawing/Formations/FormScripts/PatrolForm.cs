using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolForm : Formation
{
    public override int cost => 3;
    public override float spawnDistance => 1.3f;
    public override string unitPrefab => "PatrolUnit";
    public override string lineNode => "PatrolNode";
    public override int drawLimit => 13;
}
