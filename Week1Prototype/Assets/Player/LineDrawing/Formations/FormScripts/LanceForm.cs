using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LanceForm : Formation
{
    public override int cost => 4;
    public override float spawnDistance => 1.5f;
    public override string unitPrefab => "LanceUnit";
    public override string lineNode => "LanceNode";
    public override int drawLimit => 12;
}
