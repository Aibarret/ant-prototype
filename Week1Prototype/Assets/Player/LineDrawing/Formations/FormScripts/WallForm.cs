using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallForm : Formation
{
    public override int cost => 1;
    public override float spawnDistance => 1f;
    public override string lineNode => "WallNode";
    public override string unitPrefab => "WallUnit";
    public override int drawLimit => 18;


/*    public override void spawnUnits(List<Vector3> waypointList)
    {
        defaultSpawnBehavior(waypointList, unitPrefab);
       
    }*/

}
