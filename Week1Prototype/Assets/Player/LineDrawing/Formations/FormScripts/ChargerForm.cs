using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargerForm : Formation {
    public override int cost => 4;
    public override float spawnDistance => 2F;
    public override string unitPrefab => "ChargerUnit";
    public override string lineNode => "ChargerNode";
    public override int drawLimit => 9;
}