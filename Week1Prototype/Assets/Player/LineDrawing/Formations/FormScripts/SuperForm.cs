using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperForm : Formation
{
    public override int cost => 8;
    public override float spawnDistance => 3;
    public override string unitPrefab => "SuperUnit";
    public override string lineNode => "SuperNode";
    public override int drawLimit => 5;
}
