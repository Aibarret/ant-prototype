using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PharaohForm : Formation
{
    public override int cost => 6;
    public override float spawnDistance => 6.25f;
    public override string unitPrefab => "PharaohUnit";
    public override string lineNode => "PharaohNode";

    public override int drawLimit => 3;
}
