using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class BumperForm : Formation
{
    public override int cost => 6;
    public override float spawnDistance => 2.2f;
    public override string lineNode => "BumperNode";
    public override string unitPrefab => "BumperUnit";
    public override int drawLimit => 9;


  /*  public override void spawnUnits(List<Vector3> waypointList)
    {
        defaultSpawnBehavior(waypointList, unitPrefab);
    }*/
}