using System.Collections;
using System.Collections.Generic;
using UnityEngine;



abstract public class Formation
{
    public abstract int cost { get; }
    public abstract string unitPrefab { get; }
    public abstract string lineNode {get;}
    public abstract float spawnDistance { get; }
    public abstract int drawLimit { get; }

    public List<List<GameObject>> unitLines = new List<List<GameObject>>();
    public List<List<Vector3>> unitPosnLines = new List<List<Vector3>>();
    

    public virtual void spawnUnits(List<Vector3> waypointList)
    {
        /*print("Calling spawn behavior with " + unitPrefab);
        StartCoroutine(defaultSpawnBehavior(waypointList, unitPrefab));*/
        GlobalVariables.drawingManager.StartCoroutine(GlobalVariables.drawingManager.defaultSpawnBehavior(waypointList, unitPrefab, this));
        //return null;
    }

    public virtual void killUnit(FormationUnit unit, int lineIndex, int spawnOrder)
    {
        unitLines[lineIndex][spawnOrder] = null;
        GlobalVariables.drawingManager.addToInactivePool(unitPrefab, unit.gameObject);
    }

    public virtual bool stopDrawingCondition()
    {
        return GlobalVariables.drawingManager.CurrentPheromones < cost;
    }

    public virtual bool continueDrawingCondition()
    {
        return GlobalVariables.drawingManager.CurrentPheromones >= cost;
    }

    public virtual bool isDrawUnitAtPoint(List<Vector3> list)
    {
        return true;
    }

    public FormationUnit getUnit()
    {
        return Resources.Load<GameObject>(unitPrefab).GetComponent<FormationUnit>();
    }

    /*public IEnumerator defaultSpawnBehavior(List<Vector3> waypointList, string prefab)
    {
        int count = 0;
        unitLines.Add(new List<GameObject>());
        unitPosnLines.Add(new List<Vector3>());

        int lineIndex = unitLines.Count != 0 ? unitLines.Count - 1 : 0;

        while (count < waypointList.Count)
        {
            bool isRight = false;

            if (count == 0 && waypointList.Count > 1)
            {
                isRight = waypointList[0].x < waypointList[1].x;
            }
            else if (waypointList.Count > 1)
            {
                isRight = waypointList[count - 1].x < waypointList[count].x;
            }

            GameObject go = GlobalVariables.drawingManager.getInactiveUnit(unitPrefab); //Instantiate(Resources.Load<GameObject>(unitPrefab));
            if (go == null)
            {
                go = Instantiate(Resources.Load<GameObject>(unitPrefab));
                GlobalVariables.drawingManager.addToActivePool(unitPrefab, go);
                go.transform.parent = GlobalVariables.drawingManager.AntContainer;
                FireSprayStatus newStatus = new FireSprayStatus(0);
                go.GetComponent<FormationUnit>().applyStatusEffect(newStatus);
                GlobalVariables.drawingManager.expendedFireSprayStatus.Add(newStatus);
            }

            FormationUnit formationUnit = go.GetComponent<FormationUnit>();
            formationUnit.controller = this;
            formationUnit.spawnOrder = count;
            formationUnit.lineIndex = lineIndex;
            
            // Add the FormationUnit reference to a global accessible list
            GlobalVariables.FormationUnitList.Add(formationUnit);
            
            go.transform.position = waypointList[count];

            unitLines[lineIndex].Add(go);
            unitPosnLines[lineIndex].Add(waypointList[count]);

            go.SetActive(true);
            formationUnit.spawnUnit(isRight);

            count++;
            yield return new WaitForSecondsRealtime(.01f);
        }
    }*/

}
