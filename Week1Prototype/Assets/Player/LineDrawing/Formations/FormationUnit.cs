using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using System;
using System.Linq;

public enum MoveMode
{
    normal,
    push,
    pull,
    carry
}

public class FormationUnit : MonoBehaviour, Moveable, DropShadowCastable, StatusEffectable
{
    public enum AttackPhase
    {
        Idle,
        Attacking
    }
    [Header("Object & Component Refs")]
    public StudioEventEmitter sfxEmitter;
    public VFXManager vfxManager;
    public UnitType type;
    public SpriteRenderer sprite;
    public GameObject DropShadowObject;
    public RangeIndicator rangeIndicator;
    public Collider2D hitbox;
    public EllipseCollider2D hitboxRadiusController;

    [HideInInspector] public Formation controller;
    [HideInInspector] public DropshadowController dropShadow;
    public Vector3 truePosition;

    [Header("Formation Information")]
    public int spawnOrder;
    public int waypointPosn;
    public int lineIndex;

    private StatusManager statusEffectManager;
    private bool unitReady = false;

    // Push and Pull Variables
    [HideInInspector] public float pushDistance;
    private float pushSpeed;
    private GameObject carryObject;
    private Vector2 pushDirection;
    private MoveMode moveMode = MoveMode.normal;
    private float pullCooldownTimer;

    private void Awake()
    {
        float[] stats = new float[7];
        stats[0] = 0;
        stats[1] = type.damage;
        stats[2] = type.attackCooldown;
        stats[3] = type.attackRange;
        stats[4] = type.speed;
        stats[5] = type.weight;
        stats[6] = type.knockback;
        statusEffectManager = new UnitStatusManager(this, stats);
        dropShadow = DropShadowObject.GetComponent<DropshadowController>();
    }

    private void Start()
    {
        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
        

        dropShadow.effectCallList += onDropShadowEffect;
    }


    public void spawnUnit(bool isRight = false)
    {
        changeSpriteDirection(isRight);
        changeAttackRange(type.attackRange);
        type.onUnitSpawned();
        truePosition = transform.position;
        if (rangeIndicator)
        {
            rangeIndicator.updatePosition(transform.position);

        }

        if (GlobalVariables.drawingManager.isUsingFireSpray)
        {
            VFXManager.Instance.ManageParticles(2, VFXManager.VFXControl.Play, gameObject);
        }
        
        //FMODUnity.RuntimeManager.PlayOneShot("event:/Units/BaseUnit/Spawn", transform.position);
        //Debug.Log("Sound has emitted.");
    }

    public void onDropShadowEffect(EffectState effect)
    {
        if (effect == EffectState.DROP)
        {
            hitbox.enabled = true;
            unitReady = true;

        }
    }

    private void Update()
    {
        if (unitReady)
        {
            switch (moveMode)
            {
                case MoveMode.normal:
                    type.unitBehavior();
                    hitbox.enabled = true;
                    break;
                case MoveMode.push:
                    type.pushBehavior(pushDirection, pushDistance);
                    truePosition = transform.position;
                    rangeIndicator.updatePosition(truePosition);
                    break;
                case MoveMode.pull:
                    truePosition = transform.position;
                    rangeIndicator.updatePosition(truePosition);
                    pullCooldownTimer += Time.deltaTime;
                    if (pullCooldownTimer > .5f)
                    {
                        setMoveMode(MoveMode.normal);
                    }
                    break;
                case MoveMode.carry:
                    truePosition = transform.position;
                    rangeIndicator.updatePosition(truePosition);
                    break;
            }

            tick();
        }

    }

    private void OnDisable()
    {
        hitbox.enabled = false;
        unitReady = false;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        type.onCollision(false, collision);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        type.onTrigger(false, collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        type.onTrigger(true, collision);
    }


    public void KillUnit()
    {
        type.onKillUnit();
        controller.killUnit(this, lineIndex, spawnOrder);
        vfxManager.ManageObjects(4, .5f, transform.position);
    }

    public void changeSpriteDirection(bool isRight)
    {
        type.onChangeSpriteDirection(isRight);
        sprite.flipX = isRight;
    }

    // PUSH AND PULL SYSTEM =================================================================================================================================

    public void push(Vector3 direction, int force)
    {
        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = force - type.weight;

        if (appliedForce > 0)
        {
            pushDistance = appliedForce + 7.5f;
        }
        else
        {
            pushDistance = 3;
        }

        pushDirection = (direction);
        setMoveMode(MoveMode.push);

    }

    public void pull(Vector3 direction, int force)
    {
        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = force - type.weight;

        if (appliedForce > 0)
        {
            pushSpeed = appliedForce;
        }
        else
        {
            pushSpeed = 3;
        }

        pushDirection = direction;
        setMoveMode(MoveMode.pull);
        type.pullbehavior(pushDirection, pushSpeed);
        pullCooldownTimer = 0;
    }

    public void pickUp(GameObject carryingObject, Vector3 carryPosition)
    {
        if (type.weight == 10)
        {
            return;
        }

        type.onPickUp();
        dropShadow.toggleShadow(false);
        carryObject = carryingObject;
        pushDirection = carryPosition;
        setMoveMode(MoveMode.carry);
    }
    public void putDown(Vector3 placePosition)
    {
        type.onPutDown();
        dropShadow.toggleShadow(true);
        transform.position = placePosition;
        setMoveMode(MoveMode.normal);
    }

    public bool isCarryable()
    {
        return type.weight < 10;
    }

    // DROP SHADOW SYSTEM ==========================================================================================================================================

    public SpriteRenderer getSpriteRenderer()
    {
        return sprite;
    }

    public void setCollision(FlyingState flyingState)
    {
        if (unitReady)
        {
            switch (flyingState)
            {
                case FlyingState.GROUND:
                    hitbox.enabled = true;
                    break;
                case FlyingState.HOVERING:
                    hitbox.enabled = true;
                    break;
                case FlyingState.FLYING:
                    hitbox.enabled = false;
                    break;
            }
        }

    }

    // STATUS EFFECT SYSTEM ==================================================================================================================================

    public void changeAttackRange(float range)
    {
        type.attackRange = range;
        //hitbox.radius = type.attackRange;

        if (hitbox != null && hitboxRadiusController != null)
        {
            Vector3 newRadius = RadiusRatios.calculateSkewedCircle(range);

            hitboxRadiusController.radiusX = newRadius.x;
            hitboxRadiusController.radiusY = newRadius.y;

            PolygonCollider2D polyColl = (PolygonCollider2D)hitbox;
            polyColl.points = hitboxRadiusController.getPoints();
            
        }
        else if (hitbox != null && hitbox.GetType().Name == new CircleCollider2D().GetType().Name)
        {
            CircleCollider2D hit = (CircleCollider2D)hitbox;
            hit.radius = range;
        }

        if (rangeIndicator)
        {
            rangeIndicator.setRange(type.attackRange * 2);

        }
        transform.localScale = new Vector3(.99f, .99f, 1);
        type.onChangeAttackRange();

    }

    public void applyStatusEffect(StatusEffect effect)
    {
        statusEffectManager.applyStatusEffect(effect);
    }

    public void removeStatusEffect(StatusEffect effect)
    {
        statusEffectManager.removeStatusEffect(effect);
    }

    public void removeStatusEffect(string effectName)
    {
        statusEffectManager.removeStatusEffect(effectName);
    }

    public bool hasStatusEffectReference(StatusEffect effect)
    {
        return statusEffectManager.hasStatusEffect(effect);
    }

    public bool hasStatusEffectOfType(string typeName)
    {
        return statusEffectManager.hasSatusEffectOfType(typeName);
    }


    // UNIT TIMER ============================================================================================================================================


    [HideInInspector] public float elapsedFrames;
    [HideInInspector] public float maxSeconds;
    [HideInInspector] public bool isLoop;
    [HideInInspector] public bool isRunning = false;



    public void startUnitTimer(float seconds, bool repeat = false)
    {
        if (!isRunning)
        {
            maxSeconds = UnityEngine.Random.Range(seconds - .5f, seconds + .5f);
            isLoop = repeat;
            elapsedFrames = 0;
            isRunning = true;
        }
    }

    public void tick()
    {
        if (isRunning)
        {
            //print("Timer Running");
            if (elapsedFrames < maxSeconds)
            {
                elapsedFrames += Time.deltaTime;
            }
            else
            {
                isRunning = false;
                type.unitTimeOut();
            }
        }
    }

    public bool isTimerRunning()
    {
        return isRunning;
    }



    // Getters and Setters ========================================================================================


    public void setMoveMode(MoveMode mm)
    {
        if (mm != moveMode)
        {
            switch (mm)
            {
                case MoveMode.normal:
                    rangeIndicator.toggleDisplay(true);
                    break;
                case MoveMode.push:
                    rangeIndicator.toggleDisplay(false);
                    break;
                case MoveMode.pull:
                    rangeIndicator.toggleDisplay(false);
                    break;
                case MoveMode.carry:
                    rangeIndicator.toggleDisplay(false);
                    break;
            }
        }
        
        moveMode = mm;
    }

    private void OnDrawGizmosSelected()
    {
        if (hitbox != null && hitboxRadiusController != null)
        {
            if (type.attackRange != hitboxRadiusController.radiusX)
            {
                Vector3 newRadius = RadiusRatios.calculateSkewedCircle(type.attackRange);

                hitboxRadiusController.radiusX = newRadius.x;
                hitboxRadiusController.radiusY = newRadius.y;
                print("Changing Collider to " + newRadius);
            }
        }
        else if (hitbox != null && hitbox.GetType().Name == new CircleCollider2D().GetType().Name)
        {
            CircleCollider2D hit = (CircleCollider2D)hitbox;
            if (type.attackRange != hit.radius)
            {
                hit.radius = type.attackRange;
                print("Changing Circle Collider to radius " + type.attackRange);
            }
        }
    }

    public void PlaySFX(string effectName, bool useWorldSpace = true)
    {
        if (SFXManager.Instance)
        {
            if (useWorldSpace)
            {
                SFXManager.Instance.playSFX("event:/Units/" + effectName, transform.position);

            }
            else
            {
                SFXManager.Instance.playSFX("event:/Units/" + effectName);
            }
        }
    }

    public void PlayDirectSFX(string fullPathName)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName);
        }
    }

    public void PlayDirectSFX(string fullPathName, Vector3 worldSpace)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName, worldSpace);
        }
    }
}