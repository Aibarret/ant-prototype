using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyUnit : MonoBehaviour
{
    public float scaleSpeed;
    public float rotationRange;

    public RangeIndicator rangeIndicator;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Animator animator;
    private float randomRotation;

    private void OnEnable()
    {
        //transform.rotation = Quaternion.Euler(0, 0, randomDegree);
        randomRotation = Random.Range(rotationRange * -1, rotationRange);
        animator.SetFloat("Speed", scaleSpeed);
        animator.SetTrigger("PlaceSticker");
    }

    private void OnDisable()
    {
        transform.localScale = Vector3.zero;
    }

    private void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, randomRotation));
    }

    public void initSprite(Vector3 posn, Sprite texture, Vector2 size, float attackRadius, bool isRight = false)
    {
        transform.localScale = Vector3.zero;
        transform.position = posn - new Vector3(0,.2f,0);

        sprite.flipX = isRight;
        sprite.sprite = texture;
        sprite.size = size;

        //rangeIndicator.setRange(attackRadius);

        gameObject.SetActive(true);
        
    }

    public void despawnUnit()
    {
        animator.SetTrigger("RemoveSticker");
        //rangeIndicator.setRange(0);
    }

    public void animationComplete(string animationName)
    {
        switch (animationName)
        {
            case "StickerPlace":
                break;
            case "StickerRemove":
                gameObject.SetActive(false);
                break;
        }
        
    }
}
