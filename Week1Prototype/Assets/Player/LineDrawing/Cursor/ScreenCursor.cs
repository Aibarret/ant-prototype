using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CursorState
{
    DRAW,
    ERASE,
    SEARCH
}

public class ScreenCursor : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public CursorState cursorState;
    private bool isController;
    private bool isDrawing;

   

    private void LateUpdate()
    {
        if (!LevelManager.instance.isUsingController)
        {
            gameObject.transform.position = LevelManager.instance.mousePosition.ReadValue<Vector2>();
        }

    }

    public void toggleCursor(bool active)
    {
        gameObject.SetActive(active);
    }

    public void setDrawingState(CursorState state)
    {
        if (cursorState != state)
        {
            cursorState = state;
            isDrawing = false;
            animator.SetInteger("CursorMode", (int)state);
            animator.SetTrigger("ChangeCursor");
        }
    }

    public void toggleDraw(bool isDrawing)
    {
        if (this.isDrawing != isDrawing)
        {
            this.isDrawing = isDrawing;
            animator.SetBool("InUse", isDrawing);
        }
    }

    public void toggleControllerMode(bool isController)
    {
        this.isController = isController;
    }

    public void setScreenPosition(Vector2 screenPosn)
    {
        transform.position = screenPosn;
    }

}
