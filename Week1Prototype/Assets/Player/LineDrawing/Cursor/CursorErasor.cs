using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorErasor : MonoBehaviour
{
    public GameObject cursor;
    private Cursor cursorScript;

    private void Start()
    {
        cursorScript = cursor.GetComponent<Cursor>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("FormationUnit"))
        {
            if (cursorScript.isErasing == true)
            {
                collision.gameObject.GetComponent<FormationUnit>().KillUnit();
            }
        }
    }
}
