using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    [Header("Debug")]
    public bool DEBUG_DrawRadius;
    public Clickable clickTarget;
    /*    public CircleCollider2D coll;
        public GameObject EraseRadius;*/

    [Header("Search Variables")]
    [SerializeField] private Transform searchRange;
    [SerializeField] private float searchRangeGrowTime;

    private Color CursorColor = Color.green;
    private List<Clickable> overlappingTargets = new List<Clickable>();
    private List<string> targetTags = new List<string>();

    [HideInInspector] public bool isErasing = false;
    [HideInInspector] public bool isUsing = false;
    [HideInInspector] public bool isSearching = false;

    private bool isRangeGrowing;
    private bool isRangeShrinking;
    private float elapsedFrames;

    private void Update()
    {
        if (LevelManager.instance.playerSelect.WasPressedThisFrame() && clickTarget != null)
        {
            clickTarget.onClick();
        }

        if (isErasing)
        {
            isUsing = LevelManager.instance.playerSelect.ReadValue<float>() > 0;

            GlobalVariables.ui.screenCursor.toggleDraw(isUsing);
        }
        
        if (isSearching)
        {
            if (isRangeGrowing || isRangeShrinking)
            {
                if (!(elapsedFrames > searchRangeGrowTime) && !(elapsedFrames < 0))
                {
                    float scaleAmount;
                    scaleAmount = Mathf.Lerp(0, 1, elapsedFrames / searchRangeGrowTime);

                    searchRange.localScale = new Vector3(scaleAmount, scaleAmount, scaleAmount);

                    if (isRangeGrowing)
                    {
                        elapsedFrames += Time.deltaTime;
                    }
                    else
                    {
                        elapsedFrames -= Time.deltaTime;
                    }
                }
                else
                {
                    if (elapsedFrames > searchRangeGrowTime)
                    {
                        elapsedFrames = searchRangeGrowTime;
                    }
                    else
                    {
                        elapsedFrames = 0;
                    }

                    isRangeGrowing = false;
                    
                    if (isRangeShrinking)
                    {
                        isRangeShrinking = false;
                        isSearching = false;
                    }
                }
            }
            
        }
    }

    public void toggleErase(bool isErase)
    {
        isErasing = isErase;
    }

    public void toggleSearch(bool isSearch)
    {
        if (isSearch)
        {
            toggleErase(false);
            isRangeGrowing = true;
            isRangeShrinking = false;
            isSearching = true;
        }
        else
        {
            isRangeGrowing = false;
            isRangeShrinking = true;
        }
        
    }

    public Clickable determineTarget()
    {
        Clickable priorityTarget = null;

        for (int i = overlappingTargets.Count - 1; i > 0; i--)
        {
            if (targetTags[i] == "Queen")
            {
                priorityTarget =  overlappingTargets[i];
                break;
            }
            else if (targetTags[i] == "Obstacle")
            {
                priorityTarget = overlappingTargets[i];
                break;
            }
        }

        if (priorityTarget != null)
        {
            return priorityTarget;
        }
        else if (overlappingTargets.Count > 0)
        {
            return overlappingTargets[overlappingTargets.Count - 1];
        }
        else
        {
            return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Do Not Draw Here")
        {
            GlobalVariables.drawingManager.toggleDrawZone = false;
            CursorColor = Color.red;
        }

        Clickable target = collision.gameObject.GetComponentInParent<Clickable>();
        if (target != null)
        {
            overlappingTargets.Add(target);
            targetTags.Add(collision.gameObject.tag);
            clickTarget = determineTarget();
            if (GlobalVariables.ui.screenCursor.cursorState == CursorState.ERASE)
            {
                GlobalVariables.ui.screenCursor.toggleDraw(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Do Not Draw Here")
        {
            GlobalVariables.drawingManager.toggleDrawZone = true;
            CursorColor = Color.green;
        }

        Clickable target = collision.gameObject.GetComponentInParent<Clickable>();
        if (target != null && overlappingTargets.Contains(target))
        {
            overlappingTargets.Remove(target);
            targetTags.Remove(collision.tag);
            clickTarget = determineTarget();

        }

        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //print("COLLIDING WITH: " + collision);
        if (collision.gameObject.tag.Equals("FormationUnit"))
        {
            // Erase units that enter range
            if (isErasing && isUsing)
            {
                FormationUnit unit = collision.gameObject.GetComponentInParent<FormationUnit>();
                if (unit != null)
                {
                    unit.KillUnit();
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Do Not Draw Here")
        {
            GlobalVariables.drawingManager.toggleDrawZone = false;
            CursorColor = Color.red;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Do Not Draw Here")
        {
            GlobalVariables.drawingManager.toggleDrawZone = true;
            CursorColor = Color.green;
        }
    }

    /*private void OnDrawGizmos()
    {
        if (DEBUG_DrawRadius)
        {
            Gizmos.color = CursorColor;
            Gizmos.DrawWireSphere(transform.position, coll.radius);
        }
    }*/
}
