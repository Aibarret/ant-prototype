using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionButtons : MonoBehaviour
{
    [SerializeField] private ToggleManager actionsManager;

    private bool drawingManagerReady;


    public void activateToggles()
    {
        drawingManagerReady = true;
    }

    public void turnOnDraw()
    {
        actionsManager.directToggle(0, true);
    }

    public void turnOnErase()
    {
        actionsManager.directToggle(1, true);
    }

    public void turnOnSearch()
    {
        actionsManager.directToggle(2, true);
    }

    // Button Listeners
    public void toggleDraw(bool value)
    {
        if (value && drawingManagerReady)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
            GlobalVariables.drawingManager.toggleDrawButton();
        }
    }

    public void toggleErase(bool value)
    {
        if (value && drawingManagerReady)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
            GlobalVariables.drawingManager.toggleEraseButton();
        }
    }

    public void toggleSearch(bool value)
    {
        if (value && drawingManagerReady)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/SwapSelect UI SFX");
            GlobalVariables.drawingManager.toggleSearchButton();
        }
    }
}
