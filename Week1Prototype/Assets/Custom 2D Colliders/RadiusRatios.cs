using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RadiusRatios
{
    public const float HEIGHT = .35f;
    public const float WIDTH = 1f;

    public static Vector3 calculateSkewedCircle(float radius)
    {
        float newHeight = radius * (HEIGHT / WIDTH);

        return new Vector3(radius, newHeight);
    }
}
