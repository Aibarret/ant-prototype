using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCamera : MonoBehaviour
{
    private void OnDrawGizmosSelected()
    {
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();

        float halfHeight = camera.orthographicSize;
        float halfWidth = camera.aspect * halfHeight;

        float horizontalMin = -halfWidth;
        float horizontalMax = halfWidth;


        Gizmos.DrawLine(transform.position + new Vector3(horizontalMin, halfHeight), transform.position + new Vector3(horizontalMax, halfHeight));
        Gizmos.DrawLine(transform.position + new Vector3(horizontalMin, -halfHeight), transform.position + new Vector3(horizontalMax, -halfHeight));

        Gizmos.DrawLine(transform.position + new Vector3(horizontalMin, -halfHeight), transform.position + new Vector3(horizontalMin, halfHeight));
        Gizmos.DrawLine(transform.position + new Vector3(horizontalMax, halfHeight), transform.position + new Vector3(horizontalMax, -halfHeight));
    }
}
