using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class ExtensionMethods
{
    public static TComponent copyComponent<TComponent>(this GameObject destination, TComponent originalComponent) where TComponent : Component
    {
        Type componentType = originalComponent.GetType();

        Component copy = destination.AddComponent(componentType);

        

        FieldInfo[] fields = componentType.GetFields();
        Debug.Log(fields.Length);
        foreach (FieldInfo field in fields)
        {
            Debug.Log(field);
            field.SetValue(copy, field.GetValue(originalComponent));
        }

        return copy as TComponent;
    }
}
