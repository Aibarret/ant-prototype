using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{

    public delegate void onTimerEnd();
    public onTimerEnd timeOut;
    public bool debug = false;

    private bool isRunning = false;
    private bool isPaused = false;
    private bool inGamePause = false;
    private float maxTime;

    public bool isRepeat = false;

    private float elapsedFrames = 0;



    // Update is called once per frame
    void Update()
    {
        if (isRunning && !isPaused && !inGamePause)
        {
            elapsedFrames += Time.deltaTime;
            
            if (debug)
            {
                print("Timer: " + elapsedFrames + "/" + maxTime);
            }

            if (elapsedFrames >= maxTime)
            {
                endTimer();
            }
        }
    }

    public void startTimer(float seconds, onTimerEnd onTimerEnd, bool repeat = false)
    {
        if (debug)
        {
            print("Starting Timer at " + seconds + " seconds");
        }
        timeOut = onTimerEnd;
        maxTime = seconds;
        elapsedFrames = 0;
        isRunning = true;
        isRepeat = repeat;
    }

    private void endTimer()
    {
        if (debug)
        {
            print("Timer Timed-out with reset value at " + isRepeat);
        }
        if (isRepeat)
        {
            if (debug)
            {
                print("Timer Resetting");
                
            }
            resetTimer();
        }
        else
        {
            isRunning = false;
        }
        elapsedFrames = 0;
        timeOut();
    }

    public float getCurrentTime()
    {
        return maxTime - elapsedFrames;
    }

    public float getElaspsedTime()
    {
        return elapsedFrames;
    }

    public bool getPaused()
    {
        return isPaused;
    }

    public bool isTiming()
    {
        if (isPaused)
        {
            return false;
        }
        return isRunning;
    }

    public void pause()
    {
        isPaused = !isPaused;
    }

    public void resetTimer()
    {
        elapsedFrames = 0;
    }

    public void stopTimer()
    {
        isRunning = false;
        elapsedFrames = 0;
    }

    public void pauseTimer()
    {
        inGamePause = !inGamePause;
    }
}
