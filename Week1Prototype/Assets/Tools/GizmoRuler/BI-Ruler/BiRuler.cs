using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiRuler : MonoBehaviour
{


    public GameObject startingPosition;
    public GameObject endPosition;

    public bool lockEndPosition = false;

    public Color colorOfGizmo;

    public float distance;
    public bool printDistanceToConsole = false;

    private Vector3 endPosnOffset;
    private bool locked = false;

    private void OnDrawGizmos()
    {
        Gizmos.color = colorOfGizmo;


        if (startingPosition && endPosition)
        {

            if (locked)
            {
                endPosition.transform.position = startingPosition.transform.position + endPosnOffset;
            }

            Gizmos.DrawLine(startingPosition.transform.position, endPosition.transform.position);

            if (printDistanceToConsole)
            {
                distance = Vector3.Distance(startingPosition.transform.position, endPosition.transform.position);
                Debug.Log(distance);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (lockEndPosition && !locked)
        {
            endPosnOffset = endPosition.transform.position - startingPosition.transform.position;
            locked = true;
        }
        else if (!lockEndPosition)
        {
            locked = false;
        }

        if (startingPosition && endPosition)
        {
            distance = Vector3.Distance(startingPosition.transform.position, endPosition.transform.position);
        }
    }
}
