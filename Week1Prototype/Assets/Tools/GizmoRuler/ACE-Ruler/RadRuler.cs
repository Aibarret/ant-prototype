using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadRuler : MonoBehaviour
{
    public float radius;
    public GameObject center;
    public Color color;

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireSphere(center.transform.position, radius);
    }
}
