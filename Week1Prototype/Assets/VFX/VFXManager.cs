using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoBehaviour
{
    public static VFXManager Instance { get; private set; }

    public enum VFXControl { Play = 0, Stop = 1 }

    public ParticleSystem[] particleSystems;
    public GameObject[] sprites;
    public int poolAmount;

    private List<List<GameObject>> effectPool;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else
        {
            Debug.Log("There is more than one instance of VFXManager");
            Destroy(gameObject);
        }

        effectPool = new List<List<GameObject>>();
        int count = 0;
        foreach (GameObject effect in sprites)
        {
            effectPool.Add(new List<GameObject>());
            for (int i = 0; i < poolAmount; i++)
            {
                GameObject obj = Instantiate(effect, Vector3.zero, Quaternion.identity);
                obj.transform.parent = transform;
                obj.SetActive(false);
                VFXLifetime vfxLifetime = obj.AddComponent<VFXLifetime>();
                effectPool[count].Add(obj);
            }
            count++;
        }
    }


    public void ManageParticles(int vfxIndex, VFXControl control, GameObject caller)
    {
        if (particleSystems != null && vfxIndex >= 0 && vfxIndex < particleSystems.Length)
        {
            ParticleSystem[] particleChildren = caller.GetComponentsInChildren<ParticleSystem>();
            if (particleChildren != null && particleChildren.Length > 0)
            {
                foreach (ParticleSystem ps in particleChildren)
                {
                    if (particleSystems[vfxIndex].name == ps.name) {
                        switch (control)
                        {
                            case VFXControl.Play:
                                ps.Play();
                                break;
                            case VFXControl.Stop:
                                ps.Stop();
                                break;
                            default:
                                Debug.Log("Must be either a 0 or 1 to start or stop particles");
                                break;
                        }
                    }
                }
            } else
            {
                //Debug.Log(caller.name + "doesn't have any particlesystems as children");
            }
        } else
        {
            Debug.Log("Check the particle array, something is incorrect");
        }
    }

    public GameObject getFromPool(int spriteIndex)
    {
        foreach (GameObject obj in effectPool[spriteIndex])
        {
            if (obj != null)
            {
                if (!obj.activeInHierarchy)
                {
                    return obj;
                }

            }
        }

        return null;
    }

    public void ManageObjects(int spriteIndex, float duration, Vector3 pos)
    {
        if(sprites != null && spriteIndex >= 0 && spriteIndex < sprites.Length)
        {
            GameObject obj = getFromPool(spriteIndex);
            obj.transform.position = pos;
            obj.SetActive(true);
            VFXLifetime vfxLifetime = obj.GetComponent<VFXLifetime>();
            
            vfxLifetime.SetLifetime(duration);
        } else
        {
            Debug.LogError("Check the sprites array, something is incorrect");
        }
    }
}

public class VFXLifetime : MonoBehaviour
{
    private float lifetime;

    public void SetLifetime(float lifetime)
    {
        this.lifetime = lifetime;
    }

    void Start()
    {
        GetComponent<Animator>().SetTrigger("Activate");
    }

    private void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;

            if (lifetime <= 0)
            {
                gameObject.SetActive(false);
            }
        }
        
    }
}
