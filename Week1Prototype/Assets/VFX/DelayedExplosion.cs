using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class DelayedExplosion : MonoBehaviour
{
    public VFXManager vfxManager;
    private float life = 2f;

    private void Start()
    {
        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
            return;
        }
        else
        {
            vfxManager.ManageParticles(1, VFXManager.VFXControl.Play, gameObject);
        }
    }

    private void Update()
    {
        if(life <= 0f)
        {
            Destroy(gameObject);
        } else
        {
            life -= Time.deltaTime;
        }
    }
}
