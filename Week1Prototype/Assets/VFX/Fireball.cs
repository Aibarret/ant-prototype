using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public VFXManager vfxManager;
    public Vector3 target;
    public float speed;

    private void Start()
    {
        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
        }
        vfxManager.ManageParticles(6, VFXManager.VFXControl.Play, gameObject);
    }

    private void Update()
    {
        Vector3 direction = (target - transform.position).normalized;

        transform.position += direction * speed * Time.deltaTime;

        if (Vector3.Distance(transform.position, target) < 0.1f)
        {
            Destroy(gameObject);
        }
    }
}
