using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for units that can grab a pie obstacle.
/// </summary>
public interface IPieGrabber
{
    /// <summary>
    /// The flag to check if the unit already grabbed a pie.
    /// </summary>
    bool PieGrabbed { get; }
    
    /// <summary>
    /// This method will be called when the unit grabs a pie.
    /// </summary>
    /// <param name="pie">The reference to the grabbed pie.</param>
    void GrabPie();
}
