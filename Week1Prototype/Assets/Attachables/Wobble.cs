using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour
{
    public bool isWobbling = true;
    public float wobbleSpeed = 5f;
    public float wobbleScaleIntensity = 0.05f;
    public float wobbleRotationIntensity = 15f;
    private Vector3 lastFramePosition;
    public WobbleType wobbleType = WobbleType.Static;

    public enum WobbleType {
        MovementBased,
        Static
    }

    void Update()
    {
        if (!isWobbling) {
            return;
        }

        switch (wobbleType) {
            case WobbleType.Static:
                WobbleEffect();
                break;
            case WobbleType.MovementBased:
                MovementWobbleEfftect();
                break;
        }
    }

    void LateUpdate() {
        lastFramePosition = transform.position;
        DropShadowFreezeRotationEffect();
    }

    void WobbleEffect() {
        // rotates the object based on the cosine of time * speed
        transform.rotation = Quaternion.Euler(
            0,
            0,
            Mathf.Cos(Time.time * wobbleSpeed) * wobbleRotationIntensity
        );
        
        // slightly adjusts scale as it rotates
        transform.localScale = new Vector3(
            Mathf.Cos(Time.time * wobbleSpeed) * wobbleScaleIntensity + 1,
            Mathf.Cos(Time.time * wobbleSpeed) * wobbleScaleIntensity + 1,
            1
        );
    }

    private void MovementWobbleEfftect() {
        // with a max of +-15 degrees, rotates the object based on the distance it has moved since the last frame and it rotates in the direction it is moving
        transform.rotation = Quaternion.Euler(
            0,
            0,
            Mathf.Clamp(
                Vector3.Distance(transform.position, lastFramePosition) * 100,
                -wobbleRotationIntensity,
                wobbleRotationIntensity
            ) * Mathf.Sign(transform.position.x - lastFramePosition.x)
        );

        // with a max of +-0.12, scales the object based on the distance it has moved since the last frame
        transform.localScale = new Vector3(
            Mathf.Clamp(
                Vector3.Distance(transform.position, lastFramePosition) * 100,
                -wobbleScaleIntensity,
                wobbleScaleIntensity
            ) + 1,
            Mathf.Clamp(
                Vector3.Distance(transform.position, lastFramePosition) * 100,
                -wobbleScaleIntensity,
                wobbleScaleIntensity
            ) + 1,
            1
        );

    }

    private void DropShadowFreezeRotationEffect() {
        
        GameObject dropShadow = transform.Find("DropShadowController").gameObject;

        dropShadow.transform.rotation = Quaternion.Euler(
            0,
            0,
            0
        );
    }
}
