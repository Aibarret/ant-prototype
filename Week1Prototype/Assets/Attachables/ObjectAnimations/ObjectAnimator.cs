using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAnimator : MonoBehaviour
{
    public GameObject objectToAnimate;
    public Animator animator;


    public void setWobbleSpeed(float speed)
    {
        animator.SetFloat("Speed", speed);
    }
}
