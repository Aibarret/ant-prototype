using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeIndicator : MonoBehaviour
{
    [Header("Object Refs")]
    public IgnoreLocalPosition ignoreLocalscript;
    public Timer timer;

    [Header("Stats")]
    public float speed =.5f;

    [Header("Enable Settings")]
    public bool triggerOnEnable;
    public float sizeOnEnable;
    public float enableSpeed = -1;

    private float oneShotSpeed;
    private bool disableOnCompletion;
    private bool tempNoRange = false;

    private bool islerping;
    private float targetSize;
    private float previousSize;
    private float elapsedFrames;


    private void OnEnable()
    {
        islerping = false;
        transform.localScale = Vector3.zero;

        if (triggerOnEnable)
        {
            setRange(sizeOnEnable, enableSpeed);
        }
    }

    private void OnDisable()
    {
        islerping = false;
        transform.localScale = Vector3.zero;
    }

    private void LateUpdate()
    {
        if (islerping)
        {
            float tweenSpeed = speed;

            if (oneShotSpeed > -1)
            {
                tweenSpeed = oneShotSpeed;
            }

            if (elapsedFrames < tweenSpeed)
            {
                float scale;
                if (tempNoRange)
                {
                    scale = Mathf.Lerp(targetSize, 0, elapsedFrames / tweenSpeed);
                }
                else
                {
                    scale = Mathf.Lerp(previousSize, targetSize, elapsedFrames / tweenSpeed);

                }
                transform.localScale = new Vector3(scale, scale);

                elapsedFrames += Time.deltaTime ;
            }
            else
            {
                if (tempNoRange)
                {
                    transform.localScale = new Vector3(0, 0);
                }
                else
                {
                    transform.localScale = new Vector3(targetSize, targetSize);
                }
                elapsedFrames = 0;
                oneShotSpeed = -1;
                islerping = false;

                if (disableOnCompletion)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }

    public void toggleDisplay(bool displayActive)
    {
        tempNoRange = !displayActive;
        islerping = true;
        elapsedFrames = 0;
    }

    public void setRange(float rangeSize, float oneTimeSpeed = -1, bool disableOnCompletion = false, float oneTimeTimer = -1)
    {
        previousSize = transform.localScale.x;
        targetSize = rangeSize;
        oneShotSpeed = oneTimeSpeed;
        elapsedFrames = 0;
        islerping = true;

        if (oneTimeTimer > 0)
        {
            timer.startTimer(oneTimeTimer, timerComplete);
        }

        this.disableOnCompletion = disableOnCompletion;
    }

    public void updatePosition(Vector3 posn)
    {
        if (ignoreLocalscript)
        {
            ignoreLocalscript.updatePosition(posn);
        }

        transform.position = posn;
    }

    public void timerComplete()
    {
        setRange(0, .2f, true);
    }
}
