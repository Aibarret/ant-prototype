using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreLocalPosition : MonoBehaviour
{
    public bool isActive;

    private Vector3 ogPosition;

    private void OnEnable()
    {
        ogPosition = transform.position;
    }

    private void OnDisable()
    {
        
    }

    private void LateUpdate()
    {
        if (isActive)
        {
            if (transform.position != ogPosition)
            {
                transform.position = ogPosition;
            }

        }
    }

    public void updatePosition(Vector3 posn)
    {
        ogPosition = posn;
    }
}
