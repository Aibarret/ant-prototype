using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FlyingState
{
    GROUND,
    HOVERING,
    FLYING
}

public enum EffectState
{
    DROP,
    JUMP
}

public class DropshadowController : MonoBehaviour
{
    const float GROUND_HEIGHT = 0f;
    const float HOVERING_HEIGHT = 1f;
    const float FLYING_HEIGHT = 3f;

    [Header("Object Refs")]
    public GameObject shadowCastingObject;
    public Transform dummySpriteObject;
    public Transform shadow;
    public List<Transform> objectsToBring = new List<Transform>();

    [Header("Dropshadow Variables")]
    [SerializeField] private FlyingState state = FlyingState.GROUND;
    public FlyingState State => state;
    public bool shadowVisible;
    public float flySpeed = 1;
    public float bobFrequency;
    public float bobDistance;

    [Header("Effect Controller")]
    public bool dropOnEnable;
    public FlyingState dropHeight;
    public float dropSpeed;

    private EffectState effectState;
    private bool effectMode;    

    private SpriteRenderer shadowSprite;
    private Transform movingHealthBarObject;
    private MovingHealthBar healthBar;
    private DropShadowCastable shadowCaster;
    private SpriteRenderer dummySpriteRend;

    private bool isChangingState = false;


    public delegate void onEffectComplete(EffectState effect);

    public onEffectComplete effectCallList;

    public FlyingState FlyHeight
    {
        get => state;
    }

    private void Awake()
    {
        dummySpriteRend = dummySpriteObject.gameObject.GetComponent<SpriteRenderer>();
        shadowCaster = shadowCastingObject.GetComponent<DropShadowCastable>();
        shadowSprite = shadow.GetComponent<SpriteRenderer>();
        
        if (shadowCastingObject.transform.Find("Canvas") != null)
        {
            movingHealthBarObject = shadowCastingObject.transform.Find("Canvas").Find("MovingHealthBar");
        }

        if (movingHealthBarObject != null)
        {
            healthBar = movingHealthBarObject.GetComponent<MovingHealthBar>();
        }
    }

    private void Start()
    {
        if (!dropOnEnable)
        {

            changeState(state, true);

        }
    }

    private void OnEnable()
    {
        if (dropOnEnable)
        {
            if (dropHeight == FlyingState.HOVERING)
            {
                triggerEffect(EffectState.DROP, HOVERING_HEIGHT);
                
            }
            else if (dropHeight == FlyingState.FLYING)
            {
                triggerEffect(EffectState.DROP, FLYING_HEIGHT);
            }

        }
    }

    private void Update()
    {
        if (shadowVisible != shadowSprite.enabled)
        {
            toggleShadow(shadowVisible);
        }

        if (isActiveAndEnabled && isChangingState)
        {
            float heightToMoveTo = 0f;

            switch (state)
            {
                case FlyingState.GROUND:
                    heightToMoveTo = GROUND_HEIGHT;
                    break;
                case FlyingState.HOVERING:
                    heightToMoveTo = HOVERING_HEIGHT;
                    break;
                case FlyingState.FLYING:
                    heightToMoveTo = FLYING_HEIGHT;
                    break;
            }

            // Moves in the direction of the height to move to, sets the hight manually if it passes it
            if (dummySpriteObject.localPosition.y > heightToMoveTo)
            {
                dummySpriteObject.localPosition -= new Vector3(0, flySpeed * Time.deltaTime);


                if (dummySpriteObject.localPosition.y < heightToMoveTo)
                {
                    dummySpriteObject.localPosition = new Vector3(0, heightToMoveTo);
                    isChangingState = false;
                }
                updateCanvasPosition();
                updateTagAlongPositions();
            }
            else if (dummySpriteObject.localPosition.y < heightToMoveTo)
            {
                dummySpriteObject.localPosition += new Vector3(0, flySpeed * Time.deltaTime);

                if (dummySpriteObject.localPosition.y > heightToMoveTo)
                {
                    dummySpriteObject.localPosition = new Vector3(0, heightToMoveTo);
                    isChangingState = false;
                }
                updateCanvasPosition();
                updateTagAlongPositions();
            }
        }
        else if (isActiveAndEnabled && !effectMode)
        {
            switch (state)
            {
                case FlyingState.HOVERING:
                    dummySpriteObject.localPosition = new Vector3(0, HOVERING_HEIGHT + Mathf.Sin(Time.time * bobFrequency) * bobDistance);
                    break;
                case FlyingState.FLYING:
                    dummySpriteObject.localPosition = new Vector3(0, FLYING_HEIGHT + Mathf.Sin(Time.time * bobFrequency) * bobDistance);
                    break;
                default:
                    break;
            }
            updateCanvasPosition();
            updateTagAlongPositions();
        }
        else if (isActiveAndEnabled && effectMode)
        {
            switch (effectState)
            {
                case EffectState.DROP:
                    if (dummySpriteObject.localPosition.y > 0)
                    {
                        dummySpriteObject.localPosition -= new Vector3(0, dropSpeed * Time.unscaledDeltaTime);
                    }
                    else
                    {
                        dummySpriteObject.localPosition = Vector3.zero;
                        effectCallList(EffectState.DROP);
                        FlyingState newState = state;
                        state = FlyingState.GROUND;
                        changeState(newState);
                        effectMode = false;
                    }

                    break;
                case EffectState.JUMP:
                    break;
            }
            updateCanvasPosition();
            updateTagAlongPositions();
        }
    }

    public void toggleShadow(bool active)
    {
        shadowVisible = active;
        shadowSprite.enabled = active;
    }

    private void updateCanvasPosition()
    {
        if (movingHealthBarObject != null)
        {
            movingHealthBarObject.parent.transform.localPosition = dummySpriteObject.localPosition;
        }
    }

    private void updateTagAlongPositions()
    {
        foreach (Transform item in objectsToBring)
        {
            item.localPosition = dummySpriteObject.localPosition;
        }
    }

    public void changeState(FlyingState newState, bool isStart = false)
    {
        isChangingState = state != newState;

        if (state == FlyingState.GROUND || isStart)
        {
            toggleDummySprite(true);
        }

        state = newState;

        if ((state == FlyingState.GROUND))
        {
            toggleDummySprite(false);
        }

        shadowCaster.setCollision(state);

    }

    public void triggerEffect(EffectState eState, float height)
    {
        setDummyHeight(height);
        toggleDummySprite(true);
        effectState = eState;
        effectMode = true;
    }

    public void updateDummySprite()
    {
        if (dummySpriteRend.enabled)
        {
            dummySpriteRend.GetCopyOf(shadowCaster.getSpriteRenderer());
            dummySpriteRend.enabled = true;
        }
        
    }

    private void setDummyHeight(float height)
    {
        dummySpriteObject.localPosition = new Vector3(0, height);
    }

    private void toggleDummySprite(bool active)
    {
        if (active)
        {
            
            dummySpriteRend.GetCopyOf(shadowCaster.getSpriteRenderer());
            dummySpriteRend.enabled = true;
            shadowCaster.getSpriteRenderer().enabled = false;
        }
        else
        {
            dummySpriteRend.enabled = false;
            shadowCaster.getSpriteRenderer().enabled = true;
        }
    }
}
