using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEffector
{
    // This interface is empty. It exists only as a group for all things that can apply status effects. 
    // If something can apply a status effect, use this interface
}
