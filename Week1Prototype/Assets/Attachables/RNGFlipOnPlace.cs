using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RNGFlipOnPlace : MonoBehaviour
{
    // This float will be shown as a slider,
    // with the range of 0.2f to 0.8f in the Inspector
    [Range(0f, 1f)]
    public float percentageToFlip = .4f;
    public bool forceCheck = false;
    [Header("Choose ONE of these")]
    public Transform objectToFlip;
    public SpriteRenderer spriteToFlip;

    private bool hasChecked = false;


    private void OnDrawGizmosSelected()
    {
        if (!hasChecked || forceCheck)
        {
            hasChecked = true;
            forceCheck = false;

            
            float rng = Random.Range(0f, 1f);
            if (rng <= percentageToFlip)
            {
                if (objectToFlip)
                {
                    objectToFlip.rotation = Quaternion.Euler(new Vector3(0, 180));
                }
                else if (spriteToFlip)
                {
                    spriteToFlip.flipX = true;
                }
            }
        }
    }

}
