using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Collider2DExtensions
{
    public static IEnumerator RefreshCollider(Collider2D col)
    {
        // Remember to call this with StartCoroutine
        // This will make a collider that will trigger 
        // it's OnEnterState again.

        col.enabled = false;

        // Wait a frame so the collider can update 
        // it's status to false 
        yield return null;

        // Enable
        col.enabled = true;

        yield return null;

        // Force an update to the collider logic by nudging the 
        // the transform but will ultimately not move the object
        col.transform.localPosition += new Vector3(0.01f, 0, 0);
        col.transform.localPosition += new Vector3(-0.01f, 0, 0);
    }
}
