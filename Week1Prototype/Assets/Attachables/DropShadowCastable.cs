using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface DropShadowCastable
{
    public SpriteRenderer getSpriteRenderer();
    public void setCollision(FlyingState flyingState);

}
