using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface StatusEffectable
{
    public void applyStatusEffect(StatusEffect effect);
    public void removeStatusEffect(StatusEffect effect);
    public void removeStatusEffect(string effectName);
    public bool hasStatusEffectReference(StatusEffect effect);
    public bool hasStatusEffectOfType(string typeName);
}
