using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusEffect
{
    public List<Stat> affectedStats = new List<Stat>();
    public StatusManager manager;
    public float elapsedFrames = 0;
    public float duration;

    public StatusEffect(float effectDuration = 0)
    {
        duration = effectDuration;
    }

    public virtual void applyEffect(StatusManager manager)
    {
        this.manager = manager;
    }

    public virtual void removeEffect(StatusManager manager = null)
    {
        for (int i = affectedStats.Count; i > 0; i--)
        {
            affectedStats[0].removeEffect(this);
            affectedStats.RemoveAt(0);
        }

        if (this.manager != null)
        {
            if (this.manager.effectList.Contains(this))
            {
                this.manager.effectList.Remove(this);
            }
        }

        
        
    }

    public virtual void tick()
    {
        if (duration > 0)
        {
            if (elapsedFrames <= duration)
            {
                elapsedFrames += Time.deltaTime;
            }
            else
            {
                duration = 0;
                removeEffect();
            }
        }
    }

}

public class TestEffect : StatusEffect
{
    public override void applyEffect(StatusManager manager)
    {
        affectedStats.Add(manager.statList[AffectableStat.Speed]);
        manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Subtract, 5, this);
    }

    public TestEffect(float effectDuration = 0) : base(effectDuration)
    {

    }
}
