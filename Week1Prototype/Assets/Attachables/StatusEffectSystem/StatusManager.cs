using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AffectableStat
{
    MaxHealth,
    Damage,
    AttackCooldown,
    AttackRange,
    Speed,
    Weight,
    Knockback
}

public class Stat
{
    public class StatAffector
    {
        public Stat.AffectType type;
        public float value;
        public StatusEffect effectReference;

        public StatAffector(Stat.AffectType type, float value, StatusEffect effectReference)
        {
            this.type = type;
            this.value = value;
            this.effectReference = effectReference;
        }
    }

    public enum AffectType
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }

    public float originalValue;

    private StatusManager manager;
    private AffectableStat type;
    private List<StatAffector> constructOrder = new List<StatAffector>();
    private float currentValue;
    
    public float value
    {
        get => currentValue; 
        set
        {
            currentValue = value;
            manager.changeStat(type, currentValue);
        }
    }

    public Stat(AffectableStat type, float value, StatusManager manager)
    {
        this.manager = manager;
        this.type = type;
        originalValue = value;
        currentValue = originalValue;
    }

    public void addEffect(AffectType typeOfEffect, float value, StatusEffect reference)
    {
        constructOrder.Add(new StatAffector(typeOfEffect, value, reference));
        constructValue();
    }

    public void removeEffect(StatusEffect reference)
    {
        for (int i = constructOrder.Count - 1; i >= 0; i--)
        {
            //Race condition problem
            if (i < constructOrder.Count)
            {
                if (constructOrder[i].effectReference == reference)
                {
                    constructOrder.RemoveAt(i);

                    if (i != 0)
                    {
                        i++;
                    }
                }
            }
            else
            {
                break;
            }
            
        }

        constructValue();
    }

    public void reset()
    {
        constructOrder.Clear();
        value = originalValue;
    }

    public void constructValue()
    {
        List<StatAffector> phase2 = new List<StatAffector>();
        float finalValue = originalValue;

        foreach (StatAffector step in constructOrder)
        {
            if (step.type == AffectType.Add)
            {
                finalValue += step.value;
            }
            else if (step.type == AffectType.Subtract)
            {
                finalValue -= step.value;
            }
            else
            {
                phase2.Add(step);
            }

            if (finalValue < 0)
            {
                finalValue = 0;
            }
        }

        foreach (StatAffector step in phase2)
        {
            if (step.type == AffectType.Multiply)
            {
                finalValue *= step.value;
            }
            else if (step.type == AffectType.Divide)
            {
                finalValue /= step.value;
            }

            if (finalValue < 0)
            {
                finalValue = 0;
            }
        }
        value =  finalValue;
    }
}

public abstract class StatusManager
{
    public Dictionary<AffectableStat, Stat> statList = new Dictionary<AffectableStat, Stat>();
    public List<StatusEffect> effectList = new List<StatusEffect>();

    public StatusManager(float[] baseStats)
    {
        statList.Add(AffectableStat.MaxHealth, new Stat(AffectableStat.MaxHealth, baseStats[0], this));
        statList.Add(AffectableStat.Damage, new Stat(AffectableStat.Damage, baseStats[1], this));
        statList.Add(AffectableStat.AttackCooldown, new Stat(AffectableStat.AttackCooldown, baseStats[2], this));
        statList.Add(AffectableStat.AttackRange, new Stat(AffectableStat.AttackRange, baseStats[3], this));
        statList.Add(AffectableStat.Speed, new Stat(AffectableStat.Speed, baseStats[4], this));
        statList.Add(AffectableStat.Weight, new Stat(AffectableStat.Weight, baseStats[5], this));
        statList.Add(AffectableStat.Knockback, new Stat(AffectableStat.Knockback, baseStats[6], this));
    }

    public abstract void changeStat(AffectableStat stat, float value);
    public abstract void takeDamage(int damageValue = -1);
    public abstract void playVFX(int index, VFXManager.VFXControl controlOption);

    public void applyStatusEffect(StatusEffect effect)
    {
        effect.applyEffect(this);
        effectList.Add(effect);
    }

    public void removeStatusEffect(StatusEffect effect)
    {
        effect.removeEffect(this);
    }

    public void removeStatusEffect(string effectName)
    {
        foreach(StatusEffect effect in effectList)
        {
            if (effect.GetType().Name.Equals(effectName))
            {
                effect.removeEffect();
                break;
            }
            
        }
    }

    public bool hasStatusEffect(StatusEffect effectRef)
    {
        foreach(StatusEffect effect in effectList)
        {
            if (effect == effectRef)
            {
                return true;
            }
        }
        return false;
    }

    public bool hasSatusEffectOfType(string effectName)
    {
        foreach (StatusEffect effect in effectList)
        {
            if (effect.GetType().Name == effectName)
            {
                return true;
            }
        }
        return false;
    }


    public void tick()
    {
        foreach (StatusEffect effect in effectList)
        {
            effect.tick();
        }
    }

    public void reset()
    {
        while (effectList.Count > 0)
        {
            effectList[0].removeEffect(this);
        }
    }
}

public class EnemyStatusManager : StatusManager
{
    private Enemy connectedEnemy;



    public override void changeStat(AffectableStat stat, float value)
    {
        switch (stat)
        {
            case AffectableStat.MaxHealth:
                connectedEnemy.type.maxHealth = (int) value;
                break;
            case AffectableStat.Damage:
                connectedEnemy.type.damage = (int)value;
                break;
            case AffectableStat.AttackCooldown:
                connectedEnemy.type.attackCooldown = value;
                break;
            case AffectableStat.AttackRange:

                connectedEnemy.type.attackCooldown = value;
                break;
            case AffectableStat.Speed:
                connectedEnemy.changeSpeed(value);
                break;
            case AffectableStat.Weight:
                connectedEnemy.type.weight = (int) value;
                break;
            case AffectableStat.Knockback:
                connectedEnemy.type.knockback = (int) value;
                break;
        }
    }

    public override void takeDamage(int damageValue = -1)
    {
        if (damageValue == -1)
        {
            if (!(connectedEnemy.type is Boss.BossType))
            {
                connectedEnemy.takeDamage(Mathf.CeilToInt(connectedEnemy.type.maxHealth * .01f));
            }
        }
        else
        {
            connectedEnemy.takeDamage(damageValue);

        }
    }

    public override void playVFX(int index, VFXManager.VFXControl controlOption)
    {
        VFXManager.Instance.ManageParticles(index, controlOption, connectedEnemy.gameObject);
    }

    public EnemyStatusManager(Enemy controller, float[] baseStats) : base(baseStats)
    {
        connectedEnemy = controller;
    }

}

public class UnitStatusManager : StatusManager
{
    private FormationUnit connectedUnit;

    public UnitStatusManager(FormationUnit controller, float[] baseStats) : base(baseStats)
    {
        connectedUnit = controller;
    }

    public override void changeStat(AffectableStat stat, float value)
    {
        switch (stat)
        {
            case AffectableStat.Damage:
                connectedUnit.type.damage = (int)value;
                break;
            case AffectableStat.AttackCooldown:
                connectedUnit.type.attackCooldown = value;
                break;
            case AffectableStat.AttackRange:
                connectedUnit.changeAttackRange(value);
                break;
            case AffectableStat.Speed:
                connectedUnit.type.speed = value;
                break;
            case AffectableStat.Weight:
                connectedUnit.type.weight = (int)value;
                break;
            case AffectableStat.Knockback:
                connectedUnit.type.knockback = (int)value;
                break;
        }
    }

    public override void takeDamage(int damageValue)
    {
        
    }

    public override void playVFX(int index, VFXManager.VFXControl controlOption)
    {
        VFXManager.Instance.ManageParticles(index, controlOption, connectedUnit.gameObject);
    }
}
