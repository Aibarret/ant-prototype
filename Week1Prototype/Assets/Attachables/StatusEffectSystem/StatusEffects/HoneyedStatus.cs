using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoneyedStatus : StatusEffect
{
    public HoneyedStatus(float effectDuration = 0) : base(effectDuration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        affectedStats.Add(manager.statList[AffectableStat.AttackCooldown]);
        affectedStats.Add(manager.statList[AffectableStat.Speed]);
        manager.statList[AffectableStat.AttackCooldown].addEffect(Stat.AffectType.Multiply, 2, this);
        manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Divide, 2, this);
    }
}
