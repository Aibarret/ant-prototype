using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBuffStatus : StatusEffect
{
    public PatrolBuffStatus(float duration = 0) : base(duration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        base.applyEffect(manager);

        affectedStats.Add(manager.statList[AffectableStat.AttackCooldown]);
        affectedStats.Add(manager.statList[AffectableStat.AttackRange]);
        manager.statList[AffectableStat.AttackCooldown].addEffect(Stat.AffectType.Subtract, .05f, this);
        manager.statList[AffectableStat.AttackRange].addEffect(Stat.AffectType.Add, .5f, this);
    }
}
