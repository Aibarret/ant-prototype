using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedStatus : StatusEffect
{
    public ChargedStatus(float duration = 0) : base(duration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        base.applyEffect(manager);

        affectedStats.Add(manager.statList[AffectableStat.Damage]);
        manager.statList[AffectableStat.Damage].addEffect(Stat.AffectType.Multiply, 1.25f, this);
    }
}
