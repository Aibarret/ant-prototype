using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardBuffStatus : StatusEffect
{
    public bool active = false;

    public WizardBuffStatus(float duration = 0) : base(duration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        base.applyEffect(manager);

        // Won't apply the effect if there are other Wizard Status's active
        foreach (StatusEffect effect in manager.effectList)
        {
            if (effect.GetType().Equals(GetType()))
            {
                return;
            }
        }

        setOffEffect();
    }

    public override void removeEffect(StatusManager manager = null)
    {
        base.removeEffect(manager);

        // Checks through all the status effects and sees if any of the remaining Wizard effects are active
        // if nothing else is active, it sets [0] active
        active = false;

        List<WizardBuffStatus> list = new List<WizardBuffStatus>();

        foreach (StatusEffect effect in manager.effectList)
        {
            if (effect.GetType().Equals(GetType()))
            {
                list.Add((WizardBuffStatus)effect);
            }
        }

        foreach (WizardBuffStatus effect in list)
        {
            if (effect.active)
            {
                return;
            }
        }

        if (list.Count > 0)
        {
            list[0].setOffEffect();
        }



    }

    public void setOffEffect()
    {
        active = true;
        affectedStats.Add(manager.statList[AffectableStat.AttackCooldown]);
        affectedStats.Add(manager.statList[AffectableStat.AttackRange]);
        affectedStats.Add(manager.statList[AffectableStat.Speed]);
        manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Multiply, .66f, this);
        manager.statList[AffectableStat.AttackCooldown].addEffect(Stat.AffectType.Multiply, .66f, this);
        manager.statList[AffectableStat.AttackRange].addEffect(Stat.AffectType.Multiply, 1.33f, this);
    }
}
