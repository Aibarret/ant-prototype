using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectrifiedStatus : StatusEffect
{
    public bool active = false;
    public bool isCharged = false;

    public ElectrifiedStatus(float duration = 0) : base(duration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        base.applyEffect(manager);

        // Won't apply the effect if there are other Electrified Status's active
        foreach(StatusEffect effect in manager.effectList)
        {
            if (effect.GetType().Equals(GetType()))
            {
                return;
            }
        }

        setOffEffect();
    }

    public override void removeEffect(StatusManager manager = null)
    {
        base.removeEffect(manager);

        // Checks through all the status effects and sees if any of the remaining Electrified effects are active
        // if nothing else is active, it sets [0] active
        active = false;

        List<ElectrifiedStatus> list = new List<ElectrifiedStatus>();

        foreach (StatusEffect effect in manager.effectList)
        {
            if (effect.GetType().Equals(GetType()))
            {
                list.Add((ElectrifiedStatus)effect);
            }
        }

        foreach(ElectrifiedStatus effect in list)
        {
            if (effect.active)
            {
                return;
            }
        }

        if (list.Count > 0)
        {
            list[0].setOffEffect();
        }
        

        
    }

    public void setOffEffect()
    {
        active = true;
        affectedStats.Add(manager.statList[AffectableStat.Speed]);
        if (isCharged)
        {
            manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Multiply, .5f, this);
        }
        else
        {
            manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Multiply, .66f, this);
        }
    }

    public void toggleCharge(bool isCharged)
    {
        this.isCharged = isCharged;

        if (isCharged)
        {
            manager.statList[AffectableStat.Speed].removeEffect(this);
            manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Multiply, .5f, this);
        }
        else
        {
            manager.statList[AffectableStat.Speed].removeEffect(this);
            manager.statList[AffectableStat.Speed].addEffect(Stat.AffectType.Multiply, .66f, this);
        }
    }
}
