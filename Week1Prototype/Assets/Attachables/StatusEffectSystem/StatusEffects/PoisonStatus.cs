using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonStatus : StatusEffect
{
    float rate;
    int damage;
    float damageElapsedFrames = 0f;


    public PoisonStatus(float damageRate, int damage = -1, float duration = 0) : base(duration)
    {
        rate = damageRate;
        this.damage = damage;
    }



    public override void tick()
    {
        base.tick();

        if (damageElapsedFrames <= rate)
        {
            damageElapsedFrames += Time.deltaTime;
        }
        else
        {
            damageElapsedFrames = 0;
            manager.takeDamage(damage);
        }
    }

    public void setTimer(float duration)
    {
        elapsedFrames = 0;
        this.duration = duration;
    }
}
