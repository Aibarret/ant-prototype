using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ViewWindowController : MonoBehaviour
{
    public GameObject viewWindowObject;
    public Collider2D spriteMaskCollider;

    private Animator animator;
    private List<SpriteRenderer> otherRenderers = new List<SpriteRenderer>();
    private List<GameObject> coverablesInRange = new List<GameObject>();
    private bool active;
    private bool isEnabled = true;

    private void Awake()
    {
        animator = viewWindowObject.GetComponent<Animator>();
        viewWindowObject.transform.localScale = new Vector3(0, 0);
        viewWindowObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
        //ownedSpriteRenderer = spriteContainingObject.GetComponent<SpriteRenderer>();
    }

    private void OnDisable()
    {
        animator.SetTrigger("Shrink");
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger == false)
        {
            return;
        }

        coverablesInRange.Add(collision.gameObject);

        if (!active)
        {
            active = true;
            toggleWindow(active);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.isTrigger == false)
        {
            return;
        }

        if (coverablesInRange.Contains(collision.gameObject))
        {
            coverablesInRange.Remove(collision.gameObject);
                
        }

        if (coverablesInRange.Count == 0 && active)
        {
            active = false;
            toggleWindow(active);
        }

    }

    public void toggleWindow(bool isActive)
    {
        if (isActive && isEnabled)
        {
            animator.SetTrigger("Grow");
        }
        else
        {
            animator.SetTrigger("Shrink");
        }
    }

    public void toggleEnable(bool active)
    {
        if (active != isEnabled)
        {
            isEnabled = active;
            toggleWindow(active);
        }
    }
}
