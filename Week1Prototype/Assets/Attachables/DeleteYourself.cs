using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DeleteYourself : MonoBehaviour
{
    public float seconds;
    private float elapsedFrames = 0;

    
    private void Update()
    {
        elapsedFrames += Time.deltaTime;
        
        if (elapsedFrames > seconds)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
