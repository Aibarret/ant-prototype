using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Resetable
{
    public void resetObject();
}
