﻿public interface ITireGrabber
{
    bool TireGrabbed { get; }

    void GrabTire();
}
