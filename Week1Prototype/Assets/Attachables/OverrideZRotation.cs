using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverrideZRotation : MonoBehaviour
{

    // Update is called once per frame
    void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, gameObject.transform.rotation.z * -1.0f);
        //transform.localPosition = new Vector3(0, transform.localPosition.y);
    }
}
