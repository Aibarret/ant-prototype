using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIPreventDrawOnClick : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        GlobalVariables.drawingManager.spawningOnClick = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GlobalVariables.drawingManager.spawningOnClick = false;
    }
}
