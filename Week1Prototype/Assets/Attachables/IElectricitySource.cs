﻿public interface IElectricitySource
{
    public bool HasPower { get; }
}
