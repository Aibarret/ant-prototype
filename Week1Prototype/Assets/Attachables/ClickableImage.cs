using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class ClickableImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public delegate void ImageHoverEnter();
    public delegate void ImageHoverExit();
    public delegate void ImageClicked();

    private ImageHoverEnter callHoverEnter;
    private ImageHoverExit callHoverExit;
    private ImageClicked callClick;

    [SerializeField] private bool hovered;

    private void Update()
    {
        // Should probably use the new input system but it only should react to the mouse, so this should be fine?
        
        if (Input.GetMouseButtonDown(0) && hovered)
        {
            if (callClick != null)
            {
                callClick();
            }
        }
    }

    public void addToHoverEnterCallList(ImageHoverEnter func)
    {
        callHoverEnter += func;
    }
    public void addToHoverExitCallList(ImageHoverExit func)
    {
        callHoverExit += func;
    }
    public void addToClickCallList(ImageClicked func)
    {
        callClick += func;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hovered = true;

        if (callHoverEnter != null)
        {
            callHoverEnter();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovered = false;

        if (callHoverExit != null)
        {
            callHoverExit();
        }
    }
}
