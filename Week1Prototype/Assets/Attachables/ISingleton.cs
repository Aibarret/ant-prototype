/// <summary>
/// Implement this interface to indicate a class/record/struct to be a singleton.<br/>
/// Implement this means you will have to create the following field(s):<br/>
/// public static T Instance { get; }
/// </summary>
public interface ISingleton { }
