using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Attackable
{
    public int health { get; }
    public int maxHealth { get; }
    public GameObject gameObject { get; }

    public bool takeDamage(int damage, float? direction = null);
}
