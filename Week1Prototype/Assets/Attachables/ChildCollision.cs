using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildCollision : MonoBehaviour
{
    public Collider2D collision;

    public delegate void triggerEnter(Collider2D collision);
    public delegate void triggerExit(Collider2D collision);
    public delegate void collisionEnter(Collision2D collision);
    public delegate void collisionExit(Collision2D collision);

    [SerializeField]
    private bool drawCollisionZone = false;
    [SerializeField]
    private Color gizmoColor = Color.white;

    public string tagToSearchFor;

    public triggerEnter trigEnt;
    public triggerExit trigExt;
    public collisionEnter collEnt;
    public collisionExit collExt;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (trigEnt != null && collision.tag.Equals(tagToSearchFor))
        {
            trigEnt(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (trigExt != null && collision.tag.Equals(tagToSearchFor))
        {
            trigExt(collision);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collEnt != null && collision.gameObject.tag.Equals(tagToSearchFor))
        {
            collEnt(collision);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collExt != null && collision.gameObject.tag.Equals(tagToSearchFor))
        {
            collExt(collision);
        }
    }

    private void OnDrawGizmos()
    {
        if (drawCollisionZone)
        {
            Gizmos.color = gizmoColor;
            //Gizmos.DrawWireSphere(transform.position, collision.radius);
        }
    }
}
