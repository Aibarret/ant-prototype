using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Moveable
{
    public void push(Vector3 direction, int force);
    public void pull(Vector3 direction, int force);
    public void pickUp(GameObject carryingObject, Vector3 carryPosition);
    public void putDown(Vector3 placePosition);

    public bool isCarryable();
}
