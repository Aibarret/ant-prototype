using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingHealthBar : MonoBehaviour
{
    public GameObject barObject;
    public GameObject outBarObject;
    public GameObject debugTextObject;

    public GameObject followObject;
    
    public bool printValues = false;
    public bool invisibleOnSpawn = true;
    public float barSpeed;

    private TMPro.TextMeshProUGUI debugText;
    private Image bar;
    private CanvasGroup incg;
    private CanvasGroup outcg;
    private float previousFillAmount = 1;
    private float realFillAmount = 1;
    private float elapsedFrames = 0;
    private bool isTweeningHealth = false;
    private bool isVisible = true;

    private void Awake()
    {
        debugText = debugTextObject.GetComponent<TMPro.TextMeshProUGUI>();
        GlobalVariables.addToCallList(eventHandler);
        incg = barObject.GetComponent<CanvasGroup>();
        outcg = outBarObject.GetComponent<CanvasGroup>();
        bar = barObject.GetComponent<Image>();
    }

    private void Start()
    {
        transform.GetComponentInParent<Canvas>().worldCamera = GlobalVariables.cam;
        incg = barObject.GetComponent<CanvasGroup>();
        outcg = outBarObject.GetComponent<CanvasGroup>();
        
    }

    private void OnEnable()
    {
        toggleVisible(!invisibleOnSpawn);
    }

    private void Update()
    {

        if (printValues && !debugTextObject.activeSelf)
        {
            debugTextObject.SetActive(true);
        }
        else if (!printValues && debugTextObject.activeSelf)
        {
            debugTextObject.SetActive(false);
        }

        if (isTweeningHealth)
        {
            if (elapsedFrames <= barSpeed)
            {
                bar.fillAmount = Mathf.Lerp(previousFillAmount, realFillAmount, elapsedFrames / barSpeed);
                elapsedFrames += Time.deltaTime;
            }
            else
            {
                if (realFillAmount == 0)
                {
                    bar.fillAmount = 0;
                }
                isTweeningHealth = false;
                previousFillAmount = realFillAmount;
                elapsedFrames = 0;
            }
        }
    }

    public void toggleVisible(bool isVisible)
    {
        this.isVisible = isVisible;
        if (isVisible)
        {

            incg.alpha = 1;
            outcg.alpha = 1;
        }
        else
        {
            incg.alpha = 0;
            outcg.alpha = 0;
        }

    }

    private void OnDestroy()
    {
        GlobalVariables.removeFromCallList(eventHandler);
    }

    public void updateBar(int currentValue, int maxValue)
    {
        if (invisibleOnSpawn && !isVisible)
        {
            if (currentValue != maxValue)
            {
                toggleVisible(true);
            }
        }

        realFillAmount = (float)currentValue / (float)maxValue;
        
        if (!isTweeningHealth)
        {
            isTweeningHealth = true;
            previousFillAmount = bar.fillAmount;
        }

        debugText.text = currentValue + " / " + maxValue;
        //bar.fillAmount = (float) currentValue / (float) maxValue;
    }

    public IEnumerator checkForHealthBarComplete()
    {
        yield return new WaitUntil(() => bar.fillAmount == 0);
    }

    public void eventHandler(WaypointEvent wEvent, int index, int miscIndex = 0)
    {
        if (wEvent == WaypointEvent.cutsceneMode)
        {
            toggleVisible(false);
        }
        else if (wEvent == WaypointEvent.endCutsceneMode)
        {
            toggleVisible(true);
        }


    }

}
