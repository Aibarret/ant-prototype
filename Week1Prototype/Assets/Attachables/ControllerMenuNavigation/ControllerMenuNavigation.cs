using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ControllerMenuNavigation : MonoBehaviour
{

    public Selectables selectables;
    public bool isActive;
    public InputAction up;
    public InputAction down;
    public InputAction left;
    public InputAction right;

    public void Start()
    {
        selectables.currentSelection = selectables.menu[0][0];
        selectables.cursor.transform.position = selectables.currentSelection.transform.position;
    }

    public void OnEnable()
    {
        // Setting key bindings for keyboard and controller
        up = new InputAction(binding: "<Gamepad>/dpad/up");
        down = new InputAction(binding: "<Gamepad>/dpad/down");
        left = new InputAction(binding: "<Gamepad>/dpad/left");
        right = new InputAction(binding: "<Gamepad>/dpad/right");

        // Adding alternate key bindings for keyboard
        up.AddBinding("<Keyboard>/w");
        down.AddBinding("<Keyboard>/s");
        left.AddBinding("<Keyboard>/a");
        right.AddBinding("<Keyboard>/d");

        // Omg another set of alternate key bindings for joystick on controller
        up.AddBinding("<Gamepad>/leftStick/up");
        down.AddBinding("<Gamepad>/leftStick/down");
        left.AddBinding("<Gamepad>/leftStick/left");
        right.AddBinding("<Gamepad>/leftStick/right");

        /** idk which one to use

        up.AddBinding("<Joystick>/stick/up");
        down.AddBinding("<Joystick>/stick/down");
        left.AddBinding("<Joystick>/stick/left");
        right.AddBinding("<Joystick>/stick/right");

        **/

        up.Enable();
        down.Enable();
        left.Enable();
        right.Enable();

        // adding listeners to the input actions
        up.performed += ctx => selectables.MoveUp();
        down.performed += ctx => selectables.MoveDown();
        left.performed += ctx => selectables.MoveLeft();
        right.performed += ctx => selectables.MoveRight();
    }

    public void OnDisable()
    {
        up.Disable();
        down.Disable();
        left.Disable();
        right.Disable();
    }
}

[System.Serializable]
public class Selectables
{
    public List<List<GameObject>> menu = new List<List<GameObject>>();
    public GameObject currentSelection;
    public List<GameObject> Selectables0 = new List<GameObject>();
    public List<GameObject> Selectables1 = new List<GameObject>();
    public List<GameObject> Selectables2 = new List<GameObject>();
    public List<GameObject> Selectables3 = new List<GameObject>();
    public List<GameObject> Selectables4 = new List<GameObject>();
    public int x;
    public int y;
    public GameObject cursor;

    public Selectables()
    {
        x = 0;
        y = 0;

        // Adding all the selectables to the list
        if (Selectables0.Count > 0)
        {
            menu.Add(Selectables0);
        }
        if (Selectables1.Count > 0)
        {
            menu.Add(Selectables1);
        }
        if (Selectables2.Count > 0)
        {
            menu.Add(Selectables2);
        }
        if (Selectables3.Count > 0)
        {
            menu.Add(Selectables3);
        }
        if (Selectables4.Count > 0)
        {
            menu.Add(Selectables4);
        }
    }

    public void Select()
    {
        //
    }

    public void MoveUp()
    {
        while (y > 0 && menu[x][y] == null)
        {
            y--;
        }
        UpdateSelection();
    }

    public void MoveDown()
    {
        while (y < menu[x].Count - 1 && menu[x][y] == null)
        {
            y++;
        }
        UpdateSelection();
    }

    public void MoveLeft()
    {
        if (x > 0)
        {
            x--;
        }

        if (menu[x][y] == null) {
            MoveUp();
        }

        UpdateSelection();
    }

    public void MoveRight()
    {
        if (x < menu.Count - 1)
        {
            x++;
        }

        if (menu[x][y] == null) {
            MoveUp();
        }

        UpdateSelection();
    }

    private void UpdateSelection()
    {
        Debug.Log("UPDATING SELECTION");
        cursor.transform.position = menu[x][y].transform.position;
    }
}
