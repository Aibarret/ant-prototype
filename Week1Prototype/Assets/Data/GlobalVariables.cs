using SceneTransition;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class GlobalVariables : MonoBehaviour, IDataPersistence
{
    [SerializeField]
    public string levelName = "Tutorial1";

    [Header("The scene to go to after the level is completed or hit the continue button on the winscreen.")]
    public string sceneNameToGoToAfterCompletion;

    [SerializeField]
    public string unitToShowOnVictory;
    public static string unitToShowOnVictoryStatic;


    public static string currentSceneName;

    public string levelMusic;

    public static DrawingManager drawingManager;
    public static UIManager ui;
    public static Camera cam;
    public static CameraController camController;
    public static Queen queen; // When getting this one in particular, get the queen variable of the queen manager since that will always return
                               // the active queen
                               
    public static DialogueBox dialogue;
    public static int highScore;
    public static float timeScore;
    public static OptionsMenu optionsMenu;
    public static EnemyAlmanac guidebook;
    public static LevelManager levelManager;
    public static Dictionary<string, bool> checksToMarkAfterLevelCompletion = new Dictionary<string, bool>(); // For each level, add a check to this list
                                                                                                             // that should be marked as completed after
                                                                                                             // the level is completed #Omar
                                                                                                                
    public delegate void eventReached(WaypointEvent wEvent, int waypointIndex, int miscIndex);
    public delegate void onGamePaused(bool paused);
    public static eventReached callList;
    public static onGamePaused onPauseCallList;

    public static bool gamePaused = false;
    public static bool gameOver = false;
    public static bool tutorialDrawing = false;

    [Header("Check for debug so you can test without having to unlock flags.")]
    static public bool useTestingFromationUnits = false;
    static public GameObject[] overrideFormationUnits;

    [HideInInspector] public static float realSpeed = 1f; // Used to store the real speed of the game when paused

    public GameObject tempDialogueGameObject;
    public int levelHighScore;
    public float levelTimeScore;
    public static bool DEBUG_turnOffSpawning = false;

    public static List<FormationUnit> FormationUnitList { get; } = new List<FormationUnit>();

    // Start is called before the first frame update
    void Awake()
    {
        gameOver = false;
        currentSceneName = SceneManager.GetActiveScene().name;
        Application.targetFrameRate = 60;
        
        drawingManager = GameObject.Find("DrawingManager").GetComponent<DrawingManager>();
        optionsMenu = GameObject.Find("OptionMenu").GetComponent<OptionsMenu>();
        guidebook = GameObject.Find("EnemyAlmanac").GetComponent<EnemyAlmanac>();
        levelManager = LevelManager.instance;
        if (levelManager == null)
        {
            GameObject.Instantiate(Resources.Load("LevelManager"));
        }
        //queen = GameObject.Find("QueenManager").GetComponent<QueenManager>().queen;
        ui = gameObject.transform.Find("UICanvas").GetComponent<UIManager>();
        //cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        camController = GameObject.Find("Main Camera").GetComponent<CameraController>();
        dialogue = tempDialogueGameObject.GetComponent<DialogueBox>();
        timeScore = levelTimeScore;
        highScore = levelHighScore;
        unitToShowOnVictoryStatic = unitToShowOnVictory;
        toggleGamePause(false);
    }

    private void Start()
    {
        print("STARTING LEVEL: " + levelName);
        AddChecks();
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged += OnActiveSceneChanged;
        SFXManager.Instance.playMusic(levelMusic);
        OnLevelStart();
        LevelManager.instance.OnEnable();
    }
   

    public static void toggleGamePause(bool isPause)
    {
        
        gamePaused = isPause;
        if (isPause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = realSpeed;
        }

        if (ui)
        {
            ui.screenCursor.toggleCursor(!isPause);

        }


        /*if (onPauseCallList != null)
        {
            GlobalVariables.onPauseCallList(isPause);
        }*/

        /*if (drawingManager)
        {
            drawingManager.paused = isPause;
        }*/
    }

    public void nonStaticToLevelSelect()
    {
        SceneTransiter.Instance.TransitScene("MainMenu");
        LevelManager.instance.resetDeathCounter();
        /*        callList = null;
                onPauseCallList = null;*/
    }

    public static void ToLevelSelect()
    {
        SFXManager.Instance.stopMusic();
        LevelManager.instance.resetDeathCounter();
        SceneTransiter.Instance.TransitScene("MainMenu");

        callList = null;
        //Clear();
/*        callList = null;
        onPauseCallList = null;*/
    }

    public void RetryLevel()
    {
        LevelManager.instance.incrementDeathCounter();
        SceneTransiter.Instance.TransitScene(currentSceneName);
    }

    public void ToNextLevel()
    {
        LevelManager.instance.resetDeathCounter();
        if (sceneNameToGoToAfterCompletion == "")
        {
            Debug.LogError("No scene to go to after completion is set in the global variables");
            return;
        }
        SceneTransiter.Instance.TransitScene(sceneNameToGoToAfterCompletion);
        /*callList = null;
        onPauseCallList = null;*/
    }
    
    public static void eventCall(WaypointEvent wEvent, int waypointIndex, int miscIndex = -1)
    {
        //print(wEvent);
        if (!gameOver && callList != null)
        {
            callList(wEvent, waypointIndex, miscIndex);
        }
    }

    public static void addToCallList(eventReached callItem)
    {
       // print(callItem);
        callList += callItem;
    }

    public static void removeFromCallList(eventReached callItem)
    {
        //print(callItem);
        callList -= callItem;
    }

    public static void addToPauseCall(onGamePaused callItem)
    {
        onPauseCallList += callItem;
    }
    
    public static void removeFromPauseCall(onGamePaused callItem)
    {
        onPauseCallList -= callItem;
    }

    public static void Clear()
    {
        drawingManager = null;
        ui = null;
        cam = null;
        camController = null;
        queen = null;
        dialogue = null;
        //var list = callList.GetInvocationList();
        /*foreach (var item in list)
            callList -= (eventReached)item;*/
        callList = null;
        //gamePaused = false;
        /*list = onPauseCallList.GetInvocationList();
        foreach (var item in list)
            onPauseCallList -= (onGamePaused)item;*/
        DEBUG_turnOffSpawning = false;
    }


    //To save and load, add and other variables you need here and make it a class variable of GameData.cs
    public void LoadData(GameData gameData)
    {
        //Debug.Log("Load: " + highScore);
        highScore = gameData.highscore;
    }

    public void SaveData(ref GameData gameData)
    {
        //Debug.Log("Save: " + highScore);
        gameData.highscore = highScore;
    }

    public static void OnLevelStart()
    {
        FleaType.resetFleas();

        // Reset the time score
        timeScore = 0;

        // Load the formation units into the formation unit list based on flags in optionsMenu
        drawingManager.setFormRefs();

        // Calling this here so that the hud stuff starts after the scene is finished loading in
        ui.activateHUD();
        ui.fireSprayButton.setState(0);
        if (LevelManager.instance.getDeathCounter() >= 3)
        {
            drawingManager.setFiresprayState(1);
            ui.fireSprayButton.setState(1);
        }
        if (LevelManager.instance.getDeathCounter() >= 6)
        {
            drawingManager.setFiresprayState(2);
            ui.fireSprayButton.setState(2);
        }
        if (LevelManager.instance.getDeathCounter() >= 9)
        {
            drawingManager.setFiresprayState(3);
            ui.fireSprayButton.setState(3);
        }
        if (LevelManager.instance.getDeathCounter() >= 12)
        {
            drawingManager.setFiresprayState(4);
            ui.fireSprayButton.setState(4);
        }

        
    }

    public static void OnLevelEnd(bool winOrLose)
    {
        if (gameOver)
        {
            return;
        }
        gameOver = true;
        toggleGamePause(true);

        if (!winOrLose)
        {
            Instantiate(Resources.Load<GameObject>("LoseScreen"));
            return;
        }

        GameObject winScreen = Instantiate(Resources.Load<GameObject>("WinScreen"));
        

        // moved save data stuff to score results set

        // Save the data to system
    }

    /// <summary>
    /// Clean up resources & references
    /// </summary>
    /// 
    private void OnActiveSceneChanged(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.Scene currentScene)
    {
        //Debug.Log( "New scene loaded" ) ;
        //Saves Restarts from having no form refs
        //OnLevelStart();
    }
    private void OnDestroy()
    {
        FormationUnitList.Clear();
        Clear();
    }

    private void AddChecks()
    {
        TextAsset csvData = Resources.Load<TextAsset>("ChecksToMarkAfterLevelCompletion");
        string[] data = csvData.text.Split(new char[] { '\n' });
        int levelIndex = 0;
        foreach (string row in data)
        {
            string[] rowSplit = row.Split(new char[] { ',' });
            for (int level = 0; level < rowSplit.Length; level++) {
                if (rowSplit[level] == levelName)
                {
                    levelIndex = level;
                    break;
                }
            }
            //print("Checking Row: " + rowSplit[levelIndex]);
            if (rowSplit[levelIndex] == "TRUE")
            {
                
                if (checksToMarkAfterLevelCompletion.ContainsKey(rowSplit[0]))
                {
                    checksToMarkAfterLevelCompletion[rowSplit[0]] = true;
                    
                }
                else
                {
                    checksToMarkAfterLevelCompletion.Add(rowSplit[0], true);

                }
            }
            else
            {
                // TODO: add existing key error
                if (checksToMarkAfterLevelCompletion.ContainsKey(rowSplit[0]))
                {
                    checksToMarkAfterLevelCompletion[rowSplit[0]] = false;
                }
                else
                {
                    checksToMarkAfterLevelCompletion.Add(rowSplit[0], false);

                }
            }
        }

        
    }

}