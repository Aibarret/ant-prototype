
[System.Serializable]
public class StatModifier
{
    private string _tag;
    public string Tag {
        get { return _tag; }
        set { _tag = value; }
    }

    private float _amount;
    public float Amount {
        get { return _amount; }
        set { _amount = value; }
    }


    // Constructor
    public StatModifier(string tag, float amount)
    {
        Tag = tag;
        Amount = amount;
    }

    // Override GetHashCode and Equals for HashSet operations
    public override int GetHashCode()
    {
        return Tag.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        StatModifier other = (StatModifier)obj;
        return this.Tag == other.Tag;
    }
}

public enum StatType {
    Damage,
    Range,
    Speed,
    AttackRate
}