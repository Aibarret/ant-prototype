using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

/*public class BoostNegativeStatus : IStatusEffect
{

    private float _damageReductionAmount = 0.33f;
    private float _rangeReductionAmount = 0.33f;
    private float _speedReductionAmount = 0.33f;
    private float _attackRateReductionAmount = 0.33f;
    
    private static StatusEnum _statusEnum = StatusEnum.BoostNegative;
    private IEffectable _parent;
    private StatusValues _values;
    private bool _isImmortal;

    public void ApplyEffect(IEffectable parent, StatusValues values)
    {
        //Debug.Log("Applying Boost Negative with values " + values.StatusNumber + ", " + values.StatusTime);
        _parent = parent;
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
        _parent.AddModifier(StatType.Damage, "boostNegative", -_damageReductionAmount);
        _parent.AddModifier(StatType.Range, "boostNegative", -_rangeReductionAmount);
        _parent.AddModifier(StatType.Speed, "boostNegative", -_speedReductionAmount);
        _parent.AddModifier(StatType.AttackRate, "boostNegative", -_attackRateReductionAmount);
    }

    public void StackEffect(StatusValues values)
    {
        //Debug.Log("Stacking Boost Negative to values " + values.StatusNumber + ", " + values.StatusTime);

        // this stacks by just taking the values of the newest application of the status effect
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
    }
    public void HandleEffect()
    {
        float newTime = Time.deltaTime;
        _values.StatusTime -= newTime;
    }

    public StatusEnum GetStatusEnum()
    {
        return _statusEnum;
    }

    public int GetStatusNumber()
    {
        return _values.StatusNumber;
    }

    public float GetTimeRemaining()
    {
        if (_isImmortal) return 1;
        return _values.StatusTime;
    }

    public void RemoveEffect()
    {
        _parent.RemoveModifier(StatType.Damage, "boostNegative");
        _parent.RemoveModifier(StatType.Range, "boostNegative");
        _parent.RemoveModifier(StatType.Speed, "boostNegative");
        _parent.RemoveModifier(StatType.AttackRate, "boostNegative");
        //Debug.Log("Removing Boost Negative status...");
    }
}*/
