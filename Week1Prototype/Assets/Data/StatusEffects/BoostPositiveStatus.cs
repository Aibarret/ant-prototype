using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

/*public class BoostPositiveStatus : IStatusEffect
{

    private float _damageIncreaseAmount = 0.33f;
    private float _rangeIncreaseAmount = 0.33f;
    private float _speedIncreaseAmount = 0.33f;
    private float _attackRateIncreaseAmount = 0.33f;
    
    private static StatusEnum _statusEnum = StatusEnum.BoostPositive;
    private IEffectable _parent;
    private StatusValues _values;
    private bool _isImmortal;

    public void ApplyEffect(IEffectable parent, StatusValues values)
    {
        //Debug.Log("Applying Boost Positive with values " + values.StatusNumber + ", " + values.StatusTime);
        _parent = parent;
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
        _parent.AddModifier(StatType.Damage, "boostPositive", _damageIncreaseAmount);
        _parent.AddModifier(StatType.Range, "boostPositive", _rangeIncreaseAmount);
        _parent.AddModifier(StatType.Speed, "boostPositive", _speedIncreaseAmount);
        _parent.AddModifier(StatType.AttackRate, "boostPositive", _attackRateIncreaseAmount);
    }

    public void StackEffect(StatusValues values)
    {
        //Debug.Log("Stacking Boost Positive to values " + values.StatusNumber + ", " + values.StatusTime);

        // this stacks by just taking the values of the newest application of the status effect
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
    }
    public void HandleEffect()
    {
        float newTime = Time.deltaTime;
        _values.StatusTime -= newTime;
    }

    public StatusEnum GetStatusEnum()
    {
        return _statusEnum;
    }

    public int GetStatusNumber()
    {
        return _values.StatusNumber;
    }

    public float GetTimeRemaining()
    {
        if (_isImmortal) return 1;
        return _values.StatusTime;
    }

    public void RemoveEffect()
    {
        _parent.RemoveModifier(StatType.Damage, "boostPositive");
        _parent.RemoveModifier(StatType.Range, "boostPositive");
        _parent.RemoveModifier(StatType.Speed, "boostPositive");
        _parent.RemoveModifier(StatType.AttackRate, "boostPositive");
        //Debug.Log("Removing Boost Positive status...");
    }
}*/
