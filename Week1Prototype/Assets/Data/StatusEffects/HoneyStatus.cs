using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

/*public class HoneyStatus : IStatusEffect
{
    private float _speedReductionAmount = 0.5f;
    private float _attackRateReductionAmount = 0.5f;
    private static StatusEnum _statusEnum = StatusEnum.Honey;
    private IEffectable _parent;
    private StatusValues _values;
    private bool _isImmortal;

    public void ApplyEffect(IEffectable parent, StatusValues values)
    {
        //Debug.Log("Applying Honey with values " + values.StatusNumber + ", " + values.StatusTime);
        _parent = parent;
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
        _parent.AddModifier(StatType.Speed, "honey", -_speedReductionAmount);
        _parent.AddModifier(StatType.AttackRate, "honey", -_attackRateReductionAmount);
    }

    public void StackEffect(StatusValues values)
    {
        //Debug.Log("Stacking Honey to values " + values.StatusNumber + ", " + values.StatusTime);

        // this stacks by just taking the values of the newest application of the status effect
        _values = values;
        if (values.StatusTime == -1) _isImmortal = true;
    }
    public void HandleEffect()
    {
        float newTime = Time.deltaTime;
        _values.StatusTime -= newTime;
    }

    public StatusEnum GetStatusEnum()
    {
        return _statusEnum;
    }

    public int GetStatusNumber()
    {
        return _values.StatusNumber;
    }

    public float GetTimeRemaining()
    {
        if (_isImmortal) return 1;
        return _values.StatusTime;
    }

    public void RemoveEffect()
    {
        _parent.RemoveModifier(StatType.Speed, "honey");
         _parent.RemoveModifier(StatType.AttackRate, "honey");
        //Debug.Log("Removing Honey status...");
    }
}*/
