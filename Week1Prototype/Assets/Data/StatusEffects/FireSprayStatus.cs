using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class FireSprayStatus : StatusEffect
{
    public FireSprayStatus(int effectLevel, float duration = 0) : base(duration)
    {

    }

    public override void applyEffect(StatusManager manager)
    {
        this.manager = manager;
    }

    public void activate()
    {
        manager.playVFX(2, VFXManager.VFXControl.Play);
        affectedStats.Add(manager.statList[AffectableStat.AttackCooldown]);
        affectedStats.Add(manager.statList[AffectableStat.Damage]);
        manager.statList[AffectableStat.AttackCooldown].addEffect(Stat.AffectType.Multiply, .5f, this);
        manager.statList[AffectableStat.Damage].addEffect(Stat.AffectType.Multiply, 1.5f, this);
    }

    public void deactivate()
    {
        manager.playVFX(2, VFXManager.VFXControl.Stop);
        for (int i = affectedStats.Count; i > 0; i--)
        {

            affectedStats[0].removeEffect(this);
            affectedStats.RemoveAt(0);
        }
    }
}
