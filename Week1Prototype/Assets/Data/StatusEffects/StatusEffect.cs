using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public interface IStatusEffect
{

    // What happens when the status effect is applied
    public void ApplyEffect(IEffectable parent, StatusValues values);

    // What happens when the status effect is stacked
    public void StackEffect(StatusValues values);

    // What happens each 'tick' of the status effect (Usually an Update())
    public void HandleEffect();

    // What happens when a status effect is removed
    public void RemoveEffect();

    // Return time remaining on the status effect
    public float GetTimeRemaining();

    // Return StatusEnum that the status effect represents
    public StatusEnum GetStatusEnum();

    // Return Status Number (level or amount) of a status
    public int GetStatusNumber();
}


// The values of a status.
[System.Serializable]
public class StatusValues {
    //The level or amount of a status (Can Be used for either, depending on the status effect's goal)
    [SerializeField] private int _statusNumber = 1; 
    public int StatusNumber { 
        get { return _statusNumber; }
        set { _statusNumber = Mathf.Clamp(value,1,10); }
    }
    //Time remaining on a status. -1 means the status will last forever.
    [SerializeField] private float _statusTime = -1; 
    public float StatusTime { 
        get { return _statusTime; }
        set { _statusTime = Mathf.Clamp(value,-1,1000); }
    }
    //The most recent person who applied this status effect
    private IEffectable _applier = null; 
    public IEffectable Applier { 
        get { return _applier; }
        set { _applier = value; }
    }

    public StatusValues(int statusNumber, float statusTime) {
        StatusNumber = statusNumber;
        StatusTime = statusTime;
    }

    public StatusValues Clone() {
        return new StatusValues(StatusNumber, StatusTime); 
    }
}


// To make a new status effect, add it to StatusEnum here
public enum StatusEnum {
    FireSpray,
    Honey,
    BoostPositive,
    BoostNegative
}


// If you make a new status effect, add it to EnumToType here
public static class StatusUtil {
    public static Type EnumToType(StatusEnum statusEnum) {
        switch (statusEnum) {
        case StatusEnum.FireSpray:
            return typeof(FireSprayStatus);
        case StatusEnum.Honey:
            return typeof(HoneyStatus);
        case StatusEnum.BoostPositive:
            return typeof(BoostPositiveStatus);
        case StatusEnum.BoostNegative:
            return typeof(BoostNegativeStatus);
        }
        return null;
    }
}*/



