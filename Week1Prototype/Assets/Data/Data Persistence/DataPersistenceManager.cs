using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DataPersistenceManager : MonoBehaviour
{

    [Header("File Storage Config")]
    [SerializeField] private string fileName;

    public static DataPersistenceManager instance { get; private set; }

    private GameData gameData;

    private List<IDataPersistence> dataPersistenceObjects;

    private FileDataHandler dataHandler;

    public void Awake()
    {
        if (instance != null)
        {
            //Debug.LogError("There is more than one data persistence manager present! ");
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        
        DontDestroyOnLoad(this.gameObject);

        this.dataHandler = new FileDataHandler(Application.persistentDataPath, fileName);
        this.dataPersistenceObjects = FindAllDataPersistenceObjects();


        try
        {
            bool isEditor = false;

#if UNITY_EDITOR
            isEditor = true;
#endif
            if (isEditor)
            {
#if UNITY_EDITOR
                if (DebugLoader.instance.isDebugMode)
                {
                    DebugData(DebugLoader.instance.gameData);
                }
                else
                {
                    LoadGame();
                }
#endif
            }
            else
            {
                LoadGame();
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Debug Loader not present, unable to check if in debug mode: " + e);
            
            LoadGame();
        }

    }

    private void Start()
    {
        
    }

    public void NewGame()
    {
        print("New Game Data Created");
        this.gameData = new GameData();
    }

    public void LoadGame()
    {
        this.gameData = dataHandler.Load();

        // Make new game if no save data was found to load
        if (this.gameData == null ) {
            NewGame();
        }

        foreach(IDataPersistence obj in dataPersistenceObjects)
        {
            obj.LoadData(gameData);
        }
    }

    public void SaveGame()
    {
        foreach (IDataPersistence obj in dataPersistenceObjects)
        {
            obj.SaveData(ref gameData);
        }

        dataHandler.Save(this.gameData);
    }

    public void ResetData()
    {
        NewGame();
        SaveGame();
        LoadGame();
    }

    public void DebugData(GameData data)
    {
        Debug.Log("Debugging data");
        this.gameData = data;
        
        foreach(IDataPersistence obj in dataPersistenceObjects)
        {
            obj.LoadData(gameData);
        }
    }

    private void OnApplicationQuit()
    {
#if UNITY_EDITOR
        try
        {
            if (DebugLoader.instance.isDebugMode) {
                return;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("Debug Loader not present, unable to check if in debug mode. " + e);
        }
#endif
        
        SaveGame();
    }

    private List<IDataPersistence> FindAllDataPersistenceObjects()
    {
        IEnumerable<IDataPersistence> dataPersistenceObjects = FindObjectsOfType<MonoBehaviour>().OfType<IDataPersistence>();

        return new List<IDataPersistence>(dataPersistenceObjects);
    }
}
