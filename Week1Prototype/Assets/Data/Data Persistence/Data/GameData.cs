using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DictStorage
{
    public List<string> pbKeys = new List<string>();
    public List<int> pbValues = new List<int>();
}

[System.Serializable]
public class GameData
{
    public int highscore;

    // Difficulty settings
    public bool isEasyMode;

    // Music settings
    public float musicVolume;
    public float sfxVolume;
    public float masterVolume;

    // Video Settings
    public int resolution;
    public int quality;
    public bool fullscreen;
    public bool vSync;

    // Control settings
    public float cursorSpeed;
    public float cameraSpeed;

    // Checks
    public List<Check> checks;

    // Keybindings
    public string cameraUpRebind;
    public string cameraDownRebind;
    public string cameraLeftRebind;
    public string cameraRightRebind;
    public string toQueenRebind;
    public string playerSelectRebind;
    public string playerEraseRebind;
    
    public string switchDrawRebind;
    public string hotkey1Rebind;
    public string hotkey2Rebind;
    public string hotkey3Rebind;
    public string hotkey4Rebind;
    public string hotkey5Rebind;
    public string hotkey6Rebind;
    public string hotkey7Rebind;
    public string hotkey8Rebind;
    public string hotkey9Rebind;
    public string hotkey10Rebind;
    public string hotkey11Rebind;
    public string hotkey12Rebind;

    public string useFireSprayRebind;
    public string searchButtonRebind;

    // new rebindings
    public string cancelDrawRebind;
    public string buttonXRebind;
    public string buttonZRebind;
    public string buttonCRebind;
    public string switchEraseRebind;
    public string switchSearchRebind;
    public string pauseButtonRebind;
    public string selectLeftRebind;
    public string selectRightRebind;

    public string controllerCancelDrawRebind;
    public string controllerUseFiresprayRebind;
    public string controllerCenterControllerMouseRebind;
    public string controllerToQueenRebind;
    public string controllerButtonXRebind;
    public string controllerButtonZRebind;
    public string controllerButtonCRebind;
    public string controllerSwitchDraw;
    public string controllerSwitchErase;
    public string controllerSwitchSearch;
    public string controllerPauseButton;
    public string controllerSelectLeftRebind;
    public string controllerSelectRightRebind;
    public string controllerSelectSwapRowRebind;



    // controller bindings
    public string controllercursorUpRebind;
    public string controllercursorDownRebind;
    public string controllercursorLeftRebind;
    public string controllercursorRightRebind;

    public string controllercameraUpRebind;
    public string controllercameraDownRebind;
    public string controllercameraLeftRebind;
    public string controllercameraRightRebind;

    public string controllerplayerSelectRebind;
    

    // Scoring data
    public Dictionary<string, int> playerPersonalBests;
    public Dictionary<string, int> levelHighscoresToBeat;

    public DictStorage personalBests;

    // Initial values to start with when there is no data to load
    public GameData()
    {
        this.highscore = 0;

        // Difficulty settings
        this.isEasyMode = false;

        // Music settings
        this.musicVolume = 1.0f;
        this.sfxVolume = 1.0f;
        this.masterVolume = 1.0f;

        // Video Settings
        this.resolution = 1;
        this.quality = 2;
        this.fullscreen = false;
        this.vSync = true;

        // Control Settings
        this.cursorSpeed = 15f;
        this.cameraSpeed = 20f;

        // Checks
        this.checks = new List<Check>();
        #region Checks
        checks.Add(new Check("hasBeatenLevelTutorial1", false));
        checks.Add(new Check("hasBeatenLevelTutorial2", false));
        checks.Add(new Check("hasBeatenLevelTutorial3", false));
        checks.Add(new Check("hasBeatenLevelTutorial4", false));
        checks.Add(new Check("hasBeatenLevelTutorial5", false));
        checks.Add(new Check("hasBeatenLevelPark1", false));
        checks.Add(new Check("hasBeatenLevelPark2", false));
        checks.Add(new Check("hasBeatenLevelPark3", false));
        checks.Add(new Check("hasBeatenLevelPark4", false));
        checks.Add(new Check("hasBeatenLevelPark5", false));
        checks.Add(new Check("hasBeatenLevelCity1", false));
        checks.Add(new Check("hasBeatenLevelCity2", false));
        checks.Add(new Check("hasBeatenLevelCity3", false));
        checks.Add(new Check("hasBeatenLevelCity4", false));
        checks.Add(new Check("hasBeatenLevelCity5", false));
        checks.Add(new Check("hasBeatenLevelLab1", false));
        checks.Add(new Check("hasBeatenLevelLab2", false));
        checks.Add(new Check("hasBeatenLevelLab3", false));
        checks.Add(new Check("hasBeatenLevelLab4", false));
        checks.Add(new Check("hasBeatenLevelLab5", false));

        checks.Add(new Check("hasUnlockedUnitBase", false));
        checks.Add(new Check("hasUnlockedUnitWall", false));
        checks.Add(new Check("hasUnlockedUnitCracker", false));
        checks.Add(new Check("hasUnlockedUnitCharger", false));
        checks.Add(new Check("hasUnlockedUnitLance", false));
        checks.Add(new Check("hasUnlockedUnitMoving", false));
        checks.Add(new Check("hasUnlockedUnitBumper", false));
        checks.Add(new Check("hasUnlockedUnitPatrol", false));
        checks.Add(new Check("hasUnlockedUnitElectric", false));
        checks.Add(new Check("hasUnlockedUnitBoost", false));
        checks.Add(new Check("hasUnlockedUnitToy", false));
        checks.Add(new Check("hasUnlockedUnitPharaoh", false));
        checks.Add(new Check("hasUnlockedUnitSuper", false));

        checks.Add(new Check("hasSeenEnemySpider", false));
        checks.Add(new Check("hasSeenEnemyBeetle", false));
        checks.Add(new Check("hasSeenEnemyFlea", false));
        checks.Add(new Check("hasSeenEnemyTermite", false));
        checks.Add(new Check("hasSeenEnemyStink", false));
        checks.Add(new Check("hasSeenEnemyBee", false));
        checks.Add(new Check("hasSeenEnemyWasp", false));
        checks.Add(new Check("hasSeenEnemyMantis", false));
        checks.Add(new Check("hasSeenEnemySuicide", false));
        checks.Add(new Check("hasSeenEnemyDragon", false));
        checks.Add(new Check("hasSeenEnemyShield", false));
        checks.Add(new Check("hasSeenEnemyArmor", false));
        checks.Add(new Check("hasSeenEnemyCentipede", false));
        checks.Add(new Check("hasSeenEnemyCricket", false));
        checks.Add(new Check("hasSeenEnemyRoly", false));
        checks.Add(new Check("hasSeenEnemyBombardier", false));

        checks.Add(new Check("hasSeenObstacleBridge", false));
        checks.Add(new Check("hasSeenObstacleBattery", false));
        checks.Add(new Check("hasSeenObstacleDoor", false));
        checks.Add(new Check("hasSeenObstacleBall", false));
        checks.Add(new Check("hasSeenObstacleJogger", false));
        checks.Add(new Check("hasSeenObstacleGrill", false));
        checks.Add(new Check("hasSeenObstaclePie", false));
        checks.Add(new Check("hasSeenObstacleGravity", false));
        checks.Add(new Check("hasSeenObstacleSoldier", false));
        checks.Add(new Check("hasSeenObstacleUmbrella", false));
        checks.Add(new Check("hasSeenObstacleFan", false));
        checks.Add(new Check("hasSeenObstacleFireSpray", false));
        checks.Add(new Check("hasSeenObstacleFireSprayI", false));
        checks.Add(new Check("hasSeenObstacleFireSprayII", false));
        checks.Add(new Check("hasSeenObstacleFireSprayIII", false));
        checks.Add(new Check("hasSeenObstacleIncubator", false));
        checks.Add(new Check("hasSeenObstacleTires", false));
        checks.Add(new Check("hasSeenObstacleCar", false));
        checks.Add(new Check("hasSeenObstacleButtons", false));
        checks.Add(new Check("hasSeenObstacleTeleporter", false));
        checks.Add(new Check("hasSeenObstacleTrainGun", false));
        
        checks.Add(new Check("hasSeenQueenCorgi", false));
        checks.Add(new Check("hasSeenQueenDucklings", false));
        checks.Add(new Check("hasSeenQueenRoomba", false));

        checks.Add(new Check("hasSeenBossBee", false));
        checks.Add(new Check("hasSeenBossTermites", false));
        checks.Add(new Check("hasSeenBossWasp", false));
        #endregion

        // Keybindings
        this.cameraUpRebind = "<Keyboard>/w";
        this.cameraDownRebind = "<Keyboard>/s";
        this.cameraLeftRebind = "<Keyboard>/a";
        this.cameraRightRebind = "<Keyboard>/d";
        this.toQueenRebind = "<Keyboard>/q";
        this.playerSelectRebind = "<Mouse>/leftButton";
        this.playerEraseRebind = "<Mouse>/rightButton";
        this.switchDrawRebind = "<Keyboard>/v";
        this.useFireSprayRebind = "<Keyboard>/f";
        this.hotkey1Rebind = "<Keyboard>/1";
        this.hotkey2Rebind = "<Keyboard>/2";
        this.hotkey3Rebind = "<Keyboard>/3";
        this.hotkey4Rebind = "<Keyboard>/4";
        this.hotkey5Rebind = "<Keyboard>/5";
        this.hotkey6Rebind = "<Keyboard>/6";
        this.hotkey7Rebind = "<Keyboard>/7";
        this.hotkey8Rebind = "<Keyboard>/8";
        this.hotkey9Rebind = "<Keyboard>/9";
        this.hotkey10Rebind = "<Keyboard>/10";
        this.hotkey11Rebind = "<Keyboard>/11";
        this.hotkey12Rebind = "<Keyboard>/12";

        this.cancelDrawRebind = "<Mouse>/rightButton";
        this.buttonXRebind = "<Keyboard>/t";
        this.buttonZRebind = "<Keyboard>/g";
        this.buttonCRebind = "<Keyboard>/b";
        this.switchEraseRebind = "<Keyboard>/x";
        this.switchSearchRebind = "<Keyboard>/c";
        this.pauseButtonRebind = "<Keyboard>/escape";
        this.selectLeftRebind = "<Keyboard>/e";
        this.selectRightRebind = "<Keyboard>/r";

        // controller bindings
        this.controllercursorUpRebind = "<Gamepad>/leftStick/up";
        this.controllercursorDownRebind = "<Gamepad>/leftStick/down";
        this.controllercursorLeftRebind = "<Gamepad>/leftStick/left";
        this.controllercursorRightRebind = "<Gamepad>/leftStick/right";

        this.controllercameraUpRebind = "<Gamepad>/rightStick/up";
        this.controllercameraDownRebind = "<Gamepad>/rightStick/down";
        this.controllercameraLeftRebind = "<Gamepad>/rightStick/left";
        this.controllercameraRightRebind = "<Gamepad>/rightStick/right";

        this.controllerplayerSelectRebind = "<Gamepad>/rightTrigger";

        this.controllerCancelDrawRebind = "<Gamepad>/rightStickPress";
        this.controllerUseFiresprayRebind = "<Gamepad>/buttonSouth";
        this.controllerCenterControllerMouseRebind = "<Gamepad>/dpad/down";
        this.controllerToQueenRebind = "<Gamepad>/dpad/up";
        this.controllerButtonXRebind = "<Gamepad>/dpad/left";
        this.controllerButtonZRebind = "<Gamepad>/dpad/up";
        this.controllerButtonCRebind = "<Gamepad>/dpad/right";
        this.controllerSwitchDraw = "<Gamepad>/buttonNorth";
        this.controllerSwitchErase = "<Gamepad>/buttonWest";
        this.controllerSwitchSearch = "<Gamepad>/buttonEast";
        this.controllerPauseButton = "<Gamepad>/start";
        this.controllerSelectLeftRebind = "<Gamepad>/leftShoulder";
        this.controllerSelectRightRebind = "<Gamepad>/rightShoulder";
        this.controllerSelectSwapRowRebind = "<Gamepad>/leftTrigger";

        // Scoring data
        this.playerPersonalBests = new Dictionary<string, int>();
        this.levelHighscoresToBeat = new Dictionary<string, int>();

        personalBests = new DictStorage();
    }
}