using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataTestingScript : MonoBehaviour, IDataPersistence
{
    public int score = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        score++;
    }

    public void LoadData(GameData gameData)
    {
        this.score = gameData.highscore;
    }

    public void SaveData(ref GameData gameData)
    {
        gameData.highscore = this.score;
    }
}
