using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshController
{
    public static NavMeshPlus.Components.NavMeshSurface navMesh;

    void Awake()
    {
        NavMeshController.navMesh = GameObject.Find("NavMesh").GetComponent<NavMeshPlus.Components.NavMeshSurface>();
        
    }

    public static void buildNavmesh()
    {
        navMesh.BuildNavMesh();
    }
}
