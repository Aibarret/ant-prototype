using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeMaterialBlock : MonoBehaviour
{
    [Range(0,255)]
    public int StencilID;

    private void Start()
    {
        Renderer renderer = GetComponent<Renderer>();

        MaterialPropertyBlock newblock = new MaterialPropertyBlock();

        newblock.SetFloat("_StencilID", StencilID);

        renderer.SetPropertyBlock(newblock);
    }

    public void OnValidate()
    {

        Renderer renderer = GetComponent<Renderer>();

        MaterialPropertyBlock newBlock = new MaterialPropertyBlock();
        renderer.GetPropertyBlock(newBlock);

        newBlock.SetFloat("_StencilID", StencilID);

        renderer.SetPropertyBlock(newBlock);

        /*Renderer renderer = GetComponent<Renderer>();

        Material mat = renderer.sharedMaterial;
        mat.SetFloat("_StencilID", StencilID);

        renderer.material = mat;*/
    }
}
