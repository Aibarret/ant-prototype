using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOutlineColor : MonoBehaviour
{
    public bool active = true;
    public Color outlineColor;
    public float outlineSize = 1;

    public void OnValidate()
    {
        if (active)
        {
            Renderer renderer = GetComponent<Renderer>();

            Material mat = renderer.sharedMaterial;

            mat.SetColor("_OutlineColor", outlineColor);
            mat.SetFloat("_OutlineThickness", outlineSize);

            renderer.material = mat;

        }
        
    }
}
