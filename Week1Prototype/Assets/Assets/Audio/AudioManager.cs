using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class AudioManager : MonoBehaviour
{
    //Will look into point of the Queen to trigger the layer of music in FMOD.
    public int musicLayer = 0;

    //FMOD event emitter that will play the music
    FMODUnity.StudioEventEmitter gameMusic;

    public void IncreaseMusicLayer() 
    {
        musicLayer += 1;
        gameMusic.SetParameter("layer", musicLayer);
    }

    private void Awake()
    {
        gameMusic = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K)) 
        {
            IncreaseMusicLayer();
        }
    }
}
