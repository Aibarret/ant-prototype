Shader "TestRangeOutline"
{
    Properties
    {
        [NoScaleOffset] _MainTex("MainTex", 2D) = "white" {}
        _OutlineColor("OutlineColor", Color) = (1, 1, 1, 1)
        _OutlineThickness("OutlineThickness", Range(0, 2)) = 1
        [IntRange] _StencilID("Stencil ID", Range(0, 255)) = 0
        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
    }
        SubShader
    {
        Tags
        {
            "RenderPipeline" = "UniversalPipeline"
            "RenderType" = "Transparent"
            "UniversalMaterialType" = "Unlit"
            "Queue" = "Transparent"
            "ShaderGraphShader" = "true"
            "ShaderGraphTargetId" = ""
        }
        Pass
        {
            Name "Sprite Unlit"
            Tags
            {
                "LightMode" = "Universal2D"
            }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest Always
    ZWrite Off

        Stencil 
        {
            Ref [_StencilID]
            Comp NotEqual
            Pass Zero
        }

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEUNLIT
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.color = input.interp2.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Step_float(float Edge, float In, out float Out)
{
    Out = step(Edge, In);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float3 BaseColor;
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    float4 _Property_4ad5a69206b848378d0b7613383dae4c_Out_0 = _OutlineColor;
    UnityTexture2D _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0 = _OutlineThickness;
    float _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0 = 0.01;
    float _Multiply_a346da63396540da8178af2769947fcf_Out_2;
    Unity_Multiply_float_float(_Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0, _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0, _Multiply_a346da63396540da8178af2769947fcf_Out_2);
    float _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1);
    float4 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4;
    float3 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5;
    float2 _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1, 0, 0, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5, _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6);
    float2 _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6, _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3);
    float4 _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.tex, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.samplerstate, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.GetTransformedUV(_TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3));
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_R_4 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.r;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_G_5 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.g;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_B_6 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.b;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.a;
    UnityTexture2D _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4;
    float3 _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5;
    float2 _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4, _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5, _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6);
    float2 _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3);
    float4 _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.tex, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.samplerstate, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.GetTransformedUV(_TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3));
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_R_4 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.r;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_G_5 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.g;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_B_6 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.b;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.a;
    UnityTexture2D _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4;
    float3 _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5;
    float2 _Combine_46433adee1db401b9bdf58f77bf48387_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, 0, _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4, _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5, _Combine_46433adee1db401b9bdf58f77bf48387_RG_6);
    float2 _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3);
    float4 _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.tex, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.samplerstate, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.GetTransformedUV(_TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3));
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_R_4 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.r;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_G_5 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.g;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_B_6 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.b;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.a;
    UnityTexture2D _Property_81620ddf9cbe41009707ba4033b27108_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4;
    float3 _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5;
    float2 _Combine_7d28a82bc7034c21a814143960c392a7_RG_6;
    Unity_Combine_float(0, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4, _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5, _Combine_7d28a82bc7034c21a814143960c392a7_RG_6);
    float2 _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3);
    float4 _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0 = SAMPLE_TEXTURE2D(_Property_81620ddf9cbe41009707ba4033b27108_Out_0.tex, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.samplerstate, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.GetTransformedUV(_TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3));
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_R_4 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.r;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_G_5 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.g;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_B_6 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.b;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.a;
    float _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2;
    Unity_Add_float(_SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7, _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2);
    float _Add_859781eaba98497c92be230637c78b4d_Out_2;
    Unity_Add_float(_SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2, _Add_859781eaba98497c92be230637c78b4d_Out_2);
    float _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2;
    Unity_Add_float(_SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7, _Add_859781eaba98497c92be230637c78b4d_Out_2, _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2);
    UnityTexture2D _Property_e2b1f343cde140f294deb80a85d65c40_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_5c6d950f4874480d9052d989cc49c482_Out_1;
    Unity_Negate_float2(_Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _Negate_5c6d950f4874480d9052d989cc49c482_Out_1);
    float2 _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_5c6d950f4874480d9052d989cc49c482_Out_1, _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3);
    float4 _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e2b1f343cde140f294deb80a85d65c40_Out_0.tex, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.samplerstate, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.GetTransformedUV(_TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3));
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_R_4 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.r;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_G_5 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.g;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_B_6 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.b;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.a;
    UnityTexture2D _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1;
    Unity_Negate_float2(_Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1);
    float2 _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1, _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3);
    float4 _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.tex, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.samplerstate, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.GetTransformedUV(_TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3));
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_R_4 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.r;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_G_5 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.g;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_B_6 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.b;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.a;
    float _Add_ad07a1d312af4f3aad694749576f4494_Out_2;
    Unity_Add_float(_SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7, _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7, _Add_ad07a1d312af4f3aad694749576f4494_Out_2);
    UnityTexture2D _Property_24bbccd7230d47748675032307b4c60d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1;
    Unity_Negate_float2(_Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1);
    float2 _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1, _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3);
    float4 _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_24bbccd7230d47748675032307b4c60d_Out_0.tex, _Property_24bbccd7230d47748675032307b4c60d_Out_0.samplerstate, _Property_24bbccd7230d47748675032307b4c60d_Out_0.GetTransformedUV(_TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3));
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_R_4 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.r;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_G_5 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.g;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_B_6 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.b;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.a;
    float _Add_b81549f0d77345078204f8fa050851b8_Out_2;
    Unity_Add_float(_Add_ad07a1d312af4f3aad694749576f4494_Out_2, _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7, _Add_b81549f0d77345078204f8fa050851b8_Out_2);
    UnityTexture2D _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Negate_db49a4529aa84924a9083a840c017f16_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_db49a4529aa84924a9083a840c017f16_Out_1);
    float4 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4;
    float3 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5;
    float2 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6;
    Unity_Combine_float(_Negate_db49a4529aa84924a9083a840c017f16_Out_1, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6);
    float2 _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6, _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3);
    float4 _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.tex, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.samplerstate, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.GetTransformedUV(_TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3));
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_R_4 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.r;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_G_5 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.g;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_B_6 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.b;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.a;
    float _Add_31b5aa761598400080b71e8974a9bb21_Out_2;
    Unity_Add_float(_Add_b81549f0d77345078204f8fa050851b8_Out_2, _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7, _Add_31b5aa761598400080b71e8974a9bb21_Out_2);
    float _Add_82235cba335a47848d5d19be3720d472_Out_2;
    Unity_Add_float(_Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2, _Add_31b5aa761598400080b71e8974a9bb21_Out_2, _Add_82235cba335a47848d5d19be3720d472_Out_2);
    float _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2;
    Unity_Step_float(0.08, _Add_82235cba335a47848d5d19be3720d472_Out_2, _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2);
    UnityTexture2D _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.tex, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.samplerstate, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_R_4 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.r;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_G_5 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.g;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_B_6 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.b;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.a;
    float _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    Unity_Subtract_float(_Step_efcd30ef3bee43399eab3c99c39e738c_Out_2, _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7, _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2);
    surface.BaseColor = (_Property_4ad5a69206b848378d0b7613383dae4c_Out_0.xyz);
    surface.Alpha = _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
Pass
{
    Name "SceneSelectionPass"
    Tags
    {
        "LightMode" = "SceneSelectionPass"
    }

        // Render State
        Cull Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        // PassKeywords: <None>
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define VARYINGS_NEED_TEXCOORD0
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_DEPTHONLY
    #define SCENESELECTIONPASS 1

        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float4 texCoord0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float4 interp0 : INTERP0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyzw = input.texCoord0;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.texCoord0 = input.interp0.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Step_float(float Edge, float In, out float Out)
{
    Out = step(Edge, In);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0 = _OutlineThickness;
    float _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0 = 0.01;
    float _Multiply_a346da63396540da8178af2769947fcf_Out_2;
    Unity_Multiply_float_float(_Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0, _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0, _Multiply_a346da63396540da8178af2769947fcf_Out_2);
    float _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1);
    float4 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4;
    float3 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5;
    float2 _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1, 0, 0, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5, _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6);
    float2 _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6, _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3);
    float4 _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.tex, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.samplerstate, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.GetTransformedUV(_TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3));
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_R_4 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.r;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_G_5 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.g;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_B_6 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.b;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.a;
    UnityTexture2D _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4;
    float3 _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5;
    float2 _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4, _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5, _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6);
    float2 _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3);
    float4 _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.tex, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.samplerstate, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.GetTransformedUV(_TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3));
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_R_4 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.r;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_G_5 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.g;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_B_6 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.b;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.a;
    UnityTexture2D _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4;
    float3 _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5;
    float2 _Combine_46433adee1db401b9bdf58f77bf48387_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, 0, _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4, _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5, _Combine_46433adee1db401b9bdf58f77bf48387_RG_6);
    float2 _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3);
    float4 _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.tex, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.samplerstate, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.GetTransformedUV(_TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3));
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_R_4 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.r;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_G_5 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.g;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_B_6 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.b;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.a;
    UnityTexture2D _Property_81620ddf9cbe41009707ba4033b27108_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4;
    float3 _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5;
    float2 _Combine_7d28a82bc7034c21a814143960c392a7_RG_6;
    Unity_Combine_float(0, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4, _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5, _Combine_7d28a82bc7034c21a814143960c392a7_RG_6);
    float2 _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3);
    float4 _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0 = SAMPLE_TEXTURE2D(_Property_81620ddf9cbe41009707ba4033b27108_Out_0.tex, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.samplerstate, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.GetTransformedUV(_TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3));
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_R_4 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.r;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_G_5 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.g;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_B_6 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.b;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.a;
    float _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2;
    Unity_Add_float(_SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7, _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2);
    float _Add_859781eaba98497c92be230637c78b4d_Out_2;
    Unity_Add_float(_SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2, _Add_859781eaba98497c92be230637c78b4d_Out_2);
    float _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2;
    Unity_Add_float(_SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7, _Add_859781eaba98497c92be230637c78b4d_Out_2, _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2);
    UnityTexture2D _Property_e2b1f343cde140f294deb80a85d65c40_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_5c6d950f4874480d9052d989cc49c482_Out_1;
    Unity_Negate_float2(_Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _Negate_5c6d950f4874480d9052d989cc49c482_Out_1);
    float2 _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_5c6d950f4874480d9052d989cc49c482_Out_1, _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3);
    float4 _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e2b1f343cde140f294deb80a85d65c40_Out_0.tex, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.samplerstate, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.GetTransformedUV(_TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3));
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_R_4 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.r;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_G_5 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.g;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_B_6 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.b;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.a;
    UnityTexture2D _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1;
    Unity_Negate_float2(_Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1);
    float2 _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1, _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3);
    float4 _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.tex, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.samplerstate, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.GetTransformedUV(_TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3));
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_R_4 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.r;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_G_5 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.g;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_B_6 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.b;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.a;
    float _Add_ad07a1d312af4f3aad694749576f4494_Out_2;
    Unity_Add_float(_SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7, _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7, _Add_ad07a1d312af4f3aad694749576f4494_Out_2);
    UnityTexture2D _Property_24bbccd7230d47748675032307b4c60d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1;
    Unity_Negate_float2(_Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1);
    float2 _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1, _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3);
    float4 _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_24bbccd7230d47748675032307b4c60d_Out_0.tex, _Property_24bbccd7230d47748675032307b4c60d_Out_0.samplerstate, _Property_24bbccd7230d47748675032307b4c60d_Out_0.GetTransformedUV(_TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3));
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_R_4 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.r;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_G_5 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.g;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_B_6 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.b;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.a;
    float _Add_b81549f0d77345078204f8fa050851b8_Out_2;
    Unity_Add_float(_Add_ad07a1d312af4f3aad694749576f4494_Out_2, _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7, _Add_b81549f0d77345078204f8fa050851b8_Out_2);
    UnityTexture2D _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Negate_db49a4529aa84924a9083a840c017f16_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_db49a4529aa84924a9083a840c017f16_Out_1);
    float4 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4;
    float3 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5;
    float2 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6;
    Unity_Combine_float(_Negate_db49a4529aa84924a9083a840c017f16_Out_1, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6);
    float2 _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6, _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3);
    float4 _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.tex, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.samplerstate, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.GetTransformedUV(_TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3));
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_R_4 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.r;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_G_5 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.g;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_B_6 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.b;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.a;
    float _Add_31b5aa761598400080b71e8974a9bb21_Out_2;
    Unity_Add_float(_Add_b81549f0d77345078204f8fa050851b8_Out_2, _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7, _Add_31b5aa761598400080b71e8974a9bb21_Out_2);
    float _Add_82235cba335a47848d5d19be3720d472_Out_2;
    Unity_Add_float(_Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2, _Add_31b5aa761598400080b71e8974a9bb21_Out_2, _Add_82235cba335a47848d5d19be3720d472_Out_2);
    float _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2;
    Unity_Step_float(0.08, _Add_82235cba335a47848d5d19be3720d472_Out_2, _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2);
    UnityTexture2D _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.tex, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.samplerstate, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_R_4 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.r;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_G_5 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.g;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_B_6 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.b;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.a;
    float _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    Unity_Subtract_float(_Step_efcd30ef3bee43399eab3c99c39e738c_Out_2, _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7, _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2);
    surface.Alpha = _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SelectionPickingPass.hlsl"

    ENDHLSL
}
Pass
{
    Name "ScenePickingPass"
    Tags
    {
        "LightMode" = "Picking"
    }

        // Render State
        Cull Back

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        // PassKeywords: <None>
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define VARYINGS_NEED_TEXCOORD0
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_DEPTHONLY
    #define SCENEPICKINGPASS 1

        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float4 texCoord0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float4 interp0 : INTERP0;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyzw = input.texCoord0;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.texCoord0 = input.interp0.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Step_float(float Edge, float In, out float Out)
{
    Out = step(Edge, In);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    UnityTexture2D _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0 = _OutlineThickness;
    float _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0 = 0.01;
    float _Multiply_a346da63396540da8178af2769947fcf_Out_2;
    Unity_Multiply_float_float(_Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0, _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0, _Multiply_a346da63396540da8178af2769947fcf_Out_2);
    float _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1);
    float4 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4;
    float3 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5;
    float2 _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1, 0, 0, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5, _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6);
    float2 _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6, _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3);
    float4 _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.tex, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.samplerstate, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.GetTransformedUV(_TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3));
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_R_4 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.r;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_G_5 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.g;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_B_6 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.b;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.a;
    UnityTexture2D _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4;
    float3 _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5;
    float2 _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4, _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5, _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6);
    float2 _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3);
    float4 _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.tex, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.samplerstate, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.GetTransformedUV(_TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3));
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_R_4 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.r;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_G_5 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.g;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_B_6 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.b;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.a;
    UnityTexture2D _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4;
    float3 _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5;
    float2 _Combine_46433adee1db401b9bdf58f77bf48387_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, 0, _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4, _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5, _Combine_46433adee1db401b9bdf58f77bf48387_RG_6);
    float2 _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3);
    float4 _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.tex, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.samplerstate, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.GetTransformedUV(_TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3));
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_R_4 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.r;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_G_5 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.g;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_B_6 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.b;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.a;
    UnityTexture2D _Property_81620ddf9cbe41009707ba4033b27108_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4;
    float3 _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5;
    float2 _Combine_7d28a82bc7034c21a814143960c392a7_RG_6;
    Unity_Combine_float(0, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4, _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5, _Combine_7d28a82bc7034c21a814143960c392a7_RG_6);
    float2 _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3);
    float4 _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0 = SAMPLE_TEXTURE2D(_Property_81620ddf9cbe41009707ba4033b27108_Out_0.tex, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.samplerstate, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.GetTransformedUV(_TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3));
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_R_4 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.r;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_G_5 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.g;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_B_6 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.b;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.a;
    float _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2;
    Unity_Add_float(_SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7, _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2);
    float _Add_859781eaba98497c92be230637c78b4d_Out_2;
    Unity_Add_float(_SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2, _Add_859781eaba98497c92be230637c78b4d_Out_2);
    float _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2;
    Unity_Add_float(_SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7, _Add_859781eaba98497c92be230637c78b4d_Out_2, _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2);
    UnityTexture2D _Property_e2b1f343cde140f294deb80a85d65c40_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_5c6d950f4874480d9052d989cc49c482_Out_1;
    Unity_Negate_float2(_Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _Negate_5c6d950f4874480d9052d989cc49c482_Out_1);
    float2 _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_5c6d950f4874480d9052d989cc49c482_Out_1, _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3);
    float4 _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e2b1f343cde140f294deb80a85d65c40_Out_0.tex, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.samplerstate, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.GetTransformedUV(_TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3));
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_R_4 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.r;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_G_5 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.g;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_B_6 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.b;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.a;
    UnityTexture2D _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1;
    Unity_Negate_float2(_Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1);
    float2 _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1, _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3);
    float4 _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.tex, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.samplerstate, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.GetTransformedUV(_TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3));
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_R_4 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.r;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_G_5 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.g;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_B_6 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.b;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.a;
    float _Add_ad07a1d312af4f3aad694749576f4494_Out_2;
    Unity_Add_float(_SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7, _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7, _Add_ad07a1d312af4f3aad694749576f4494_Out_2);
    UnityTexture2D _Property_24bbccd7230d47748675032307b4c60d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1;
    Unity_Negate_float2(_Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1);
    float2 _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1, _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3);
    float4 _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_24bbccd7230d47748675032307b4c60d_Out_0.tex, _Property_24bbccd7230d47748675032307b4c60d_Out_0.samplerstate, _Property_24bbccd7230d47748675032307b4c60d_Out_0.GetTransformedUV(_TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3));
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_R_4 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.r;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_G_5 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.g;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_B_6 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.b;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.a;
    float _Add_b81549f0d77345078204f8fa050851b8_Out_2;
    Unity_Add_float(_Add_ad07a1d312af4f3aad694749576f4494_Out_2, _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7, _Add_b81549f0d77345078204f8fa050851b8_Out_2);
    UnityTexture2D _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Negate_db49a4529aa84924a9083a840c017f16_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_db49a4529aa84924a9083a840c017f16_Out_1);
    float4 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4;
    float3 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5;
    float2 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6;
    Unity_Combine_float(_Negate_db49a4529aa84924a9083a840c017f16_Out_1, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6);
    float2 _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6, _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3);
    float4 _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.tex, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.samplerstate, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.GetTransformedUV(_TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3));
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_R_4 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.r;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_G_5 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.g;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_B_6 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.b;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.a;
    float _Add_31b5aa761598400080b71e8974a9bb21_Out_2;
    Unity_Add_float(_Add_b81549f0d77345078204f8fa050851b8_Out_2, _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7, _Add_31b5aa761598400080b71e8974a9bb21_Out_2);
    float _Add_82235cba335a47848d5d19be3720d472_Out_2;
    Unity_Add_float(_Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2, _Add_31b5aa761598400080b71e8974a9bb21_Out_2, _Add_82235cba335a47848d5d19be3720d472_Out_2);
    float _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2;
    Unity_Step_float(0.08, _Add_82235cba335a47848d5d19be3720d472_Out_2, _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2);
    UnityTexture2D _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.tex, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.samplerstate, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_R_4 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.r;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_G_5 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.g;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_B_6 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.b;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.a;
    float _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    Unity_Subtract_float(_Step_efcd30ef3bee43399eab3c99c39e738c_Out_2, _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7, _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2);
    surface.Alpha = _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/SelectionPickingPass.hlsl"

    ENDHLSL
}
Pass
{
    Name "Sprite Unlit"
    Tags
    {
        "LightMode" = "UniversalForward"
    }

        // Render State
        Cull Off
    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
    ZTest LEqual
    ZWrite Off

        // Debug
        // <None>

        // --------------------------------------------------
        // Pass

        HLSLPROGRAM

        // Pragmas
        #pragma target 2.0
    #pragma exclude_renderers d3d11_9x
    #pragma vertex vert
    #pragma fragment frag

        // DotsInstancingOptions: <None>
        // HybridV1InjectedBuiltinProperties: <None>

        // Keywords
        #pragma multi_compile_fragment _ DEBUG_DISPLAY
        // GraphKeywords: <None>

        // Defines
        #define _SURFACE_TYPE_TRANSPARENT 1
        #define ATTRIBUTES_NEED_NORMAL
        #define ATTRIBUTES_NEED_TANGENT
        #define ATTRIBUTES_NEED_TEXCOORD0
        #define ATTRIBUTES_NEED_COLOR
        #define VARYINGS_NEED_POSITION_WS
        #define VARYINGS_NEED_TEXCOORD0
        #define VARYINGS_NEED_COLOR
        #define FEATURES_GRAPH_VERTEX
        /* WARNING: $splice Could not find named fragment 'PassInstancing' */
        #define SHADERPASS SHADERPASS_SPRITEFORWARD
        /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

        // Includes
        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreInclude' */

        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"

        // --------------------------------------------------
        // Structs and Packing

        /* WARNING: $splice Could not find named fragment 'CustomInterpolatorPrePacking' */

        struct Attributes
    {
         float3 positionOS : POSITION;
         float3 normalOS : NORMAL;
         float4 tangentOS : TANGENT;
         float4 uv0 : TEXCOORD0;
         float4 color : COLOR;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : INSTANCEID_SEMANTIC;
        #endif
    };
    struct Varyings
    {
         float4 positionCS : SV_POSITION;
         float3 positionWS;
         float4 texCoord0;
         float4 color;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };
    struct SurfaceDescriptionInputs
    {
         float4 uv0;
    };
    struct VertexDescriptionInputs
    {
         float3 ObjectSpaceNormal;
         float3 ObjectSpaceTangent;
         float3 ObjectSpacePosition;
    };
    struct PackedVaryings
    {
         float4 positionCS : SV_POSITION;
         float3 interp0 : INTERP0;
         float4 interp1 : INTERP1;
         float4 interp2 : INTERP2;
        #if UNITY_ANY_INSTANCING_ENABLED
         uint instanceID : CUSTOM_INSTANCE_ID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
         uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
         uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
         FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
        #endif
    };

        PackedVaryings PackVaryings(Varyings input)
    {
        PackedVaryings output;
        ZERO_INITIALIZE(PackedVaryings, output);
        output.positionCS = input.positionCS;
        output.interp0.xyz = input.positionWS;
        output.interp1.xyzw = input.texCoord0;
        output.interp2.xyzw = input.color;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }

    Varyings UnpackVaryings(PackedVaryings input)
    {
        Varyings output;
        output.positionCS = input.positionCS;
        output.positionWS = input.interp0.xyz;
        output.texCoord0 = input.interp1.xyzw;
        output.color = input.interp2.xyzw;
        #if UNITY_ANY_INSTANCING_ENABLED
        output.instanceID = input.instanceID;
        #endif
        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
        #endif
        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
        #endif
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        output.cullFace = input.cullFace;
        #endif
        return output;
    }


    // --------------------------------------------------
    // Graph

    // Graph Properties
    CBUFFER_START(UnityPerMaterial)
float4 _MainTex_TexelSize;
float4 _OutlineColor;
float _OutlineThickness;
CBUFFER_END

// Object and Global properties
SAMPLER(SamplerState_Linear_Repeat);
TEXTURE2D(_MainTex);
SAMPLER(sampler_MainTex);

// Graph Includes
// GraphIncludes: <None>

// -- Property used by ScenePickingPass
#ifdef SCENEPICKINGPASS
float4 _SelectionID;
#endif

// -- Properties used by SceneSelectionPass
#ifdef SCENESELECTIONPASS
int _ObjectId;
int _PassValue;
#endif

// Graph Functions

void Unity_Multiply_float_float(float A, float B, out float Out)
{
    Out = A * B;
}

void Unity_Negate_float(float In, out float Out)
{
    Out = -1 * In;
}

void Unity_Combine_float(float R, float G, float B, float A, out float4 RGBA, out float3 RGB, out float2 RG)
{
    RGBA = float4(R, G, B, A);
    RGB = float3(R, G, B);
    RG = float2(R, G);
}

void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
{
    Out = UV * Tiling + Offset;
}

void Unity_Add_float(float A, float B, out float Out)
{
    Out = A + B;
}

void Unity_Negate_float2(float2 In, out float2 Out)
{
    Out = -1 * In;
}

void Unity_Step_float(float Edge, float In, out float Out)
{
    Out = step(Edge, In);
}

void Unity_Subtract_float(float A, float B, out float Out)
{
    Out = A - B;
}

/* WARNING: $splice Could not find named fragment 'CustomInterpolatorPreVertex' */

// Graph Vertex
struct VertexDescription
{
    float3 Position;
    float3 Normal;
    float3 Tangent;
};

VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
{
    VertexDescription description = (VertexDescription)0;
    description.Position = IN.ObjectSpacePosition;
    description.Normal = IN.ObjectSpaceNormal;
    description.Tangent = IN.ObjectSpaceTangent;
    return description;
}

    #ifdef FEATURES_GRAPH_VERTEX
Varyings CustomInterpolatorPassThroughFunc(inout Varyings output, VertexDescription input)
{
return output;
}
#define CUSTOMINTERPOLATOR_VARYPASSTHROUGH_FUNC
#endif

// Graph Pixel
struct SurfaceDescription
{
    float3 BaseColor;
    float Alpha;
};

SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
{
    SurfaceDescription surface = (SurfaceDescription)0;
    float4 _Property_4ad5a69206b848378d0b7613383dae4c_Out_0 = _OutlineColor;
    UnityTexture2D _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0 = _OutlineThickness;
    float _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0 = 0.01;
    float _Multiply_a346da63396540da8178af2769947fcf_Out_2;
    Unity_Multiply_float_float(_Property_95e26bf7e1d3492c835de1a5ec6b4d24_Out_0, _Float_bd3c021add1c43c0a40a5f812456bb7f_Out_0, _Multiply_a346da63396540da8178af2769947fcf_Out_2);
    float _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1);
    float4 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4;
    float3 _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5;
    float2 _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_01f2887bc4bb4fadb3ff42517125fa1b_Out_1, 0, 0, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGBA_4, _Combine_c3b71f80e7644d2883839b97cbaa7022_RGB_5, _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6);
    float2 _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_c3b71f80e7644d2883839b97cbaa7022_RG_6, _TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3);
    float4 _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.tex, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.samplerstate, _Property_4d7d3f1a2dd343b09b24b9d83db84c24_Out_0.GetTransformedUV(_TilingAndOffset_f4a4e51bf69642a6a1435a44a431d785_Out_3));
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_R_4 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.r;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_G_5 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.g;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_B_6 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.b;
    float _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7 = _SampleTexture2D_947c182e55d04db8a6318c0692c3b909_RGBA_0.a;
    UnityTexture2D _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4;
    float3 _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5;
    float2 _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_d08de755417b4af0bd951bc57a4770d0_RGBA_4, _Combine_d08de755417b4af0bd951bc57a4770d0_RGB_5, _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6);
    float2 _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3);
    float4 _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0 = SAMPLE_TEXTURE2D(_Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.tex, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.samplerstate, _Property_4c219ab5b1ad44629247b9995ec8500e_Out_0.GetTransformedUV(_TilingAndOffset_110d22bf1872492abeaa6261311e9e18_Out_3));
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_R_4 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.r;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_G_5 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.g;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_B_6 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.b;
    float _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7 = _SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_RGBA_0.a;
    UnityTexture2D _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4;
    float3 _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5;
    float2 _Combine_46433adee1db401b9bdf58f77bf48387_RG_6;
    Unity_Combine_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, 0, _Combine_46433adee1db401b9bdf58f77bf48387_RGBA_4, _Combine_46433adee1db401b9bdf58f77bf48387_RGB_5, _Combine_46433adee1db401b9bdf58f77bf48387_RG_6);
    float2 _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3);
    float4 _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0 = SAMPLE_TEXTURE2D(_Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.tex, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.samplerstate, _Property_dfcb7eff42184583a0174a7d0fa52662_Out_0.GetTransformedUV(_TilingAndOffset_4cbfda89e5714bb68853075a83ca68d7_Out_3));
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_R_4 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.r;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_G_5 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.g;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_B_6 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.b;
    float _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7 = _SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_RGBA_0.a;
    UnityTexture2D _Property_81620ddf9cbe41009707ba4033b27108_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4;
    float3 _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5;
    float2 _Combine_7d28a82bc7034c21a814143960c392a7_RG_6;
    Unity_Combine_float(0, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_7d28a82bc7034c21a814143960c392a7_RGBA_4, _Combine_7d28a82bc7034c21a814143960c392a7_RGB_5, _Combine_7d28a82bc7034c21a814143960c392a7_RG_6);
    float2 _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3);
    float4 _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0 = SAMPLE_TEXTURE2D(_Property_81620ddf9cbe41009707ba4033b27108_Out_0.tex, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.samplerstate, _Property_81620ddf9cbe41009707ba4033b27108_Out_0.GetTransformedUV(_TilingAndOffset_1a351a61ec794afbb89f6b1605ec3699_Out_3));
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_R_4 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.r;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_G_5 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.g;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_B_6 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.b;
    float _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7 = _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_RGBA_0.a;
    float _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2;
    Unity_Add_float(_SampleTexture2D_ed85be9b1744487496ad49276ef30ccb_A_7, _SampleTexture2D_8625c0f313d045c3b8c4205163af7447_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2);
    float _Add_859781eaba98497c92be230637c78b4d_Out_2;
    Unity_Add_float(_SampleTexture2D_021f60af34d44a20a5c5881bad103ef2_A_7, _Add_bbaa74452dfe41df9624a63f2b04db24_Out_2, _Add_859781eaba98497c92be230637c78b4d_Out_2);
    float _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2;
    Unity_Add_float(_SampleTexture2D_947c182e55d04db8a6318c0692c3b909_A_7, _Add_859781eaba98497c92be230637c78b4d_Out_2, _Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2);
    UnityTexture2D _Property_e2b1f343cde140f294deb80a85d65c40_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_5c6d950f4874480d9052d989cc49c482_Out_1;
    Unity_Negate_float2(_Combine_46433adee1db401b9bdf58f77bf48387_RG_6, _Negate_5c6d950f4874480d9052d989cc49c482_Out_1);
    float2 _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_5c6d950f4874480d9052d989cc49c482_Out_1, _TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3);
    float4 _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e2b1f343cde140f294deb80a85d65c40_Out_0.tex, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.samplerstate, _Property_e2b1f343cde140f294deb80a85d65c40_Out_0.GetTransformedUV(_TilingAndOffset_bbf68b306ee3480784635ff5ff2139a7_Out_3));
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_R_4 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.r;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_G_5 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.g;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_B_6 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.b;
    float _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7 = _SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_RGBA_0.a;
    UnityTexture2D _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1;
    Unity_Negate_float2(_Combine_7d28a82bc7034c21a814143960c392a7_RG_6, _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1);
    float2 _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_7ea89ac996434ff2958a73af65c7cc64_Out_1, _TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3);
    float4 _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.tex, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.samplerstate, _Property_0e95c0cc91ca4dc28e4b86dca72a5c0e_Out_0.GetTransformedUV(_TilingAndOffset_10370e58354a4227840f795e3d0ca13d_Out_3));
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_R_4 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.r;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_G_5 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.g;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_B_6 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.b;
    float _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7 = _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_RGBA_0.a;
    float _Add_ad07a1d312af4f3aad694749576f4494_Out_2;
    Unity_Add_float(_SampleTexture2D_eba9f3b766484d2a9ffd5c54c9ec1d27_A_7, _SampleTexture2D_f9413578fa04493e83f96ae05033c11b_A_7, _Add_ad07a1d312af4f3aad694749576f4494_Out_2);
    UnityTexture2D _Property_24bbccd7230d47748675032307b4c60d_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float2 _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1;
    Unity_Negate_float2(_Combine_d08de755417b4af0bd951bc57a4770d0_RG_6, _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1);
    float2 _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Negate_61747ac7c60e4fd7a83ce49a584e5207_Out_1, _TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3);
    float4 _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0 = SAMPLE_TEXTURE2D(_Property_24bbccd7230d47748675032307b4c60d_Out_0.tex, _Property_24bbccd7230d47748675032307b4c60d_Out_0.samplerstate, _Property_24bbccd7230d47748675032307b4c60d_Out_0.GetTransformedUV(_TilingAndOffset_10ac7a419b4c47df94c9b6612557c77c_Out_3));
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_R_4 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.r;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_G_5 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.g;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_B_6 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.b;
    float _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7 = _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_RGBA_0.a;
    float _Add_b81549f0d77345078204f8fa050851b8_Out_2;
    Unity_Add_float(_Add_ad07a1d312af4f3aad694749576f4494_Out_2, _SampleTexture2D_abba1e2f9d8d4d348032de4fa779d17b_A_7, _Add_b81549f0d77345078204f8fa050851b8_Out_2);
    UnityTexture2D _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float _Negate_db49a4529aa84924a9083a840c017f16_Out_1;
    Unity_Negate_float(_Multiply_a346da63396540da8178af2769947fcf_Out_2, _Negate_db49a4529aa84924a9083a840c017f16_Out_1);
    float4 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4;
    float3 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5;
    float2 _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6;
    Unity_Combine_float(_Negate_db49a4529aa84924a9083a840c017f16_Out_1, _Multiply_a346da63396540da8178af2769947fcf_Out_2, 0, 0, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGBA_4, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RGB_5, _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6);
    float2 _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3;
    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1, 1), _Combine_f2b1e22e1bae4c9c81f0d4e0f79c1a94_RG_6, _TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3);
    float4 _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0 = SAMPLE_TEXTURE2D(_Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.tex, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.samplerstate, _Property_8aaf3fb21ee04939964b3b5bf32b63bd_Out_0.GetTransformedUV(_TilingAndOffset_10852f9f2de64d099ee389befc4bf029_Out_3));
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_R_4 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.r;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_G_5 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.g;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_B_6 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.b;
    float _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7 = _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_RGBA_0.a;
    float _Add_31b5aa761598400080b71e8974a9bb21_Out_2;
    Unity_Add_float(_Add_b81549f0d77345078204f8fa050851b8_Out_2, _SampleTexture2D_3a6fa061cee846e5ba9a86df495f2eae_A_7, _Add_31b5aa761598400080b71e8974a9bb21_Out_2);
    float _Add_82235cba335a47848d5d19be3720d472_Out_2;
    Unity_Add_float(_Add_dcb7642a253c49d1a6054d208bdcfa03_Out_2, _Add_31b5aa761598400080b71e8974a9bb21_Out_2, _Add_82235cba335a47848d5d19be3720d472_Out_2);
    float _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2;
    Unity_Step_float(0.08, _Add_82235cba335a47848d5d19be3720d472_Out_2, _Step_efcd30ef3bee43399eab3c99c39e738c_Out_2);
    UnityTexture2D _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0 = UnityBuildTexture2DStructNoScale(_MainTex);
    float4 _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0 = SAMPLE_TEXTURE2D(_Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.tex, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.samplerstate, _Property_6b1b519e01964f76bb6f4645031cd93e_Out_0.GetTransformedUV(IN.uv0.xy));
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_R_4 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.r;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_G_5 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.g;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_B_6 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.b;
    float _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7 = _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_RGBA_0.a;
    float _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    Unity_Subtract_float(_Step_efcd30ef3bee43399eab3c99c39e738c_Out_2, _SampleTexture2D_8cb8ada36f634778b833c8dae983ba0e_A_7, _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2);
    surface.BaseColor = (_Property_4ad5a69206b848378d0b7613383dae4c_Out_0.xyz);
    surface.Alpha = _Subtract_5066fc8e85f944f3b9808017c384ce6d_Out_2;
    return surface;
}

// --------------------------------------------------
// Build Graph Inputs

VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
{
    VertexDescriptionInputs output;
    ZERO_INITIALIZE(VertexDescriptionInputs, output);

    output.ObjectSpaceNormal = input.normalOS;
    output.ObjectSpaceTangent = input.tangentOS.xyz;
    output.ObjectSpacePosition = input.positionOS;

    return output;
}
    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
{
    SurfaceDescriptionInputs output;
    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);







    output.uv0 = input.texCoord0;
#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN                output.FaceSign =                                   IS_FRONT_VFACE(input.cullFace, true, false);
#else
#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
#endif
#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

    return output;
}

    // --------------------------------------------------
    // Main

    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
#include "Packages/com.unity.render-pipelines.universal/Editor/2D/ShaderGraph/Includes/SpriteUnlitPass.hlsl"

    ENDHLSL
}
    }
        CustomEditor "UnityEditor.ShaderGraph.GenericShaderGraphMaterialGUI"
        FallBack "Hidden/Shader Graph/FallbackError"
}