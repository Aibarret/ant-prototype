using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject spawnPointContainer;
    [SerializeField] private GameObject enemyContainer;

    

    private void Awake()
    {
        if (spawnPointContainer != null && enemyContainer != null)
        {
            for (int i = 0; i < spawnPointContainer.transform.childCount; i++)
            {
                spawnPointContainer.transform.GetChild(i).GetComponent<SpawnPoint>().enemyContainer = enemyContainer;
            }
        }
    }

    

    // Old Script that *probably* doesn't need to be used but like why not keep it anyway
    public static void spawnFromFile(string file, Vector3 origin)
    {
        PrefabPattern pattern = Resources.Load<PrefabPattern>(file);

        for (int i = 0; i < pattern.posns.Count; i++)
        {
            GameObject prefab = Resources.Load(pattern.prefabNames[i], typeof(GameObject)) as GameObject;

            GameObject newEnemy = Instantiate(prefab, pattern.posns[i], Quaternion.identity);
        }
    }
}
