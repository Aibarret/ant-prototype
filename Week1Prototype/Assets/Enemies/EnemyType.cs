using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyType : MonoBehaviour
{
    public Enemy enemy;
    public abstract string prefabName { get; }

    [Header("Base Stats")]
    public int damage;
     public int health;
    public int maxHealth;
    public float speed;
    public int weight;
    public int knockback;
    public float attackCooldown;
    public float attackRange;

    public bool CanMove { get; set; } = true;

    [HideInInspector] public float navTimer;

    [HideInInspector] public bool waitForAttack = false;

    [Header("Armor")]

    [SerializeField] public bool hasArmor = false;

    /* ===== Shield ===== */
    [Header("Shield")]
    
    public int shieldHealth;
    [SerializeField] public bool hasShield = false;
    [SerializeField] private float shieldStart = 60f;
    [SerializeField] private float shieldEnd = 120f;



    public virtual void enemyBehavior(Enemy enemy)
    {
        
        if (!CanMove)
            return;

        if (enemy.agent)
        {
            if (enemy.agent.enabled)
            {
                enemy.setNavDestination(enemy.queenObject.transform.position);
            }
        }
        

        if (Vector3.Distance(QueenManager.Instance.getQueen().transform.position, transform.position) <= attackRange)
        {
            if (!waitForAttack)
            {
                attackQueen();
            }
        }
    }

    /*public virtual void RandomizeArmor()
    {
        float randomChance = Random.Range(0f, 1f);

        if(randomChance < armorChance)
        {
            maxHealth *= 2;
            speed = armoredSpeed;
        }
    }*/
    
    

    public virtual void onSpawn() {}

    public virtual void onDie() {}

    public virtual float pushBehavior(Vector3 direction, float initialSpeed)
    {
        float speedDecay = 15f;

        if (initialSpeed > 0)
        {
            float speed = initialSpeed - (speedDecay * Time.deltaTime);
            enemy.rb.MovePosition(transform.position + direction * speed * Time.deltaTime);

            return speed;
        }
        else
        {
            enemy.setMoveMode(MoveMode.normal);
            return 0;
        }
    }

    public virtual void pullbehavior(Vector3 direction, float speed)
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    public virtual void carryBehavior(Transform carryingObject, Vector3 carryPosition)
    {
        transform.position = carryingObject.position + carryPosition;
    }

    public virtual void onPickUp()
    {
        enemy.animator.SetFloat("Speed", 0);
    }

    public virtual void onPutDown()
    {
        enemy.changeSpeed(speed);
    }

    public virtual void attackQueen()
    {
        enemy.queenObject.GetComponent<Queen>().takeDamage(damage);
        enemy.attackTimer.startTimer(attackCooldown, attackCooldownComplete);
        enemy.animator.SetTrigger("Attack");
        waitForAttack = true;

    }

    public void attackCooldownComplete()
    {
        waitForAttack = false;
    }

    public virtual bool takeDamage(int damage, bool isPush = false, float? direction = null)
    {
        if (hasShield && direction.HasValue && !enemy.shieldRemoved)
        {
            // Normalize angles to be between 0 and 360
            float dir = (direction.Value % 360f + 360f) % 360f;
            float start = (shieldStart % 360f + 360f) % 360f;
            float end = (shieldEnd % 360f + 360f) % 360f;

            // Check if the attack direction is within the shield range
            if (start > end)
            {
                if (dir >= start || dir <= end)
                {
                    shieldHealth -= damage;
                    if (shieldHealth <= 0)
                        enemy.removeShield();
                    return false;
                }
            }
            else
            {
                if (dir >= start && dir <= end)
                {
                    shieldHealth -= damage;
                    if (shieldHealth <= 0)
                        enemy.removeShield();
                    return false;
                }
            }
        }
        
        if (health >= 0)
        {

            health -= damage;

            if ((hasArmor == true) && (enemy.armorRemoved == false))
            {
                if (health <= enemy.normalMaxHealth)
                {
                    enemy.removeArmor();
                }
            }
            if (health <= 0)
            {
                health = 0;
                enemy.vfxManager.ManageObjects(4, .5f, transform.position);

                enemy.dieAndRespawn();
                return (true);
            }
            else if (health > maxHealth)
            {
                health = maxHealth;

            }
        }
   
        return false;
        
    }

    public virtual void changeDirection(bool isRight)
    {
        if (isRight)
        {
            shieldStart = 60f;
            shieldEnd = 120f;
        }
        else
        {
            shieldStart = 240f;
            shieldEnd = 300f;
        }
    }


    
}

