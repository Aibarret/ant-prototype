using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WallUnitFinder : MonoBehaviour
{
    public Collider2D collision;
    public GameObject enemyObject;

    private Enemy enemy;
    private int rangeCount = 0;

    private void Awake()
    {
        enemy = enemyObject.GetComponent<Enemy>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("FormationUnit") && collision.GetComponent<WallType>() != null)
        {
            rangeCount += 1;
            if (enemy.agent.enabled)
            {
                enemy.agent.enabled = false;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("FormationUnit") && collision.GetComponent<WallType>() != null)
        {
            rangeCount -= 1;
            if (!enemy.agent.enabled && rangeCount <= 0)
            {
                enemy.agent.enabled = true;
                
            }
        }
    }
}
