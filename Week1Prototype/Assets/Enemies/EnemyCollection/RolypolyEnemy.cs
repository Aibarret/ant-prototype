using UnityEngine;

public class RolypolyEnemy : EnemyType
{
    private enum RolypolyPhase {
        Rolling,
        Revving,
        Flattened
    }

    [Header("Object & Component Refs")]
    [SerializeField] GameObject hit;
    [SerializeField] Animator animator;
    public float hitWallSpeed;
    //public float revCooldown;
    public float flattenedTime;
    public int flattenedMultiplier;
    public override string prefabName => "RolypolyEnemy";

    private RolypolyPhase phase = RolypolyPhase.Rolling;
    private Rigidbody2D rb;
    private float currentTime;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        currentTime = attackCooldown;
        
    }

    public override void enemyBehavior(Enemy enemy)
    {
        switch (phase)
        {
            case RolypolyPhase.Rolling:
            {
                    animator.SetBool("IsMoving", true);
                    animator.SetBool("IsRevving", false);
                    //enemy.vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, gameObject);
                    base.enemyBehavior(enemy);   
            }
                break;

            case RolypolyPhase.Revving:
            {

                    animator.SetBool("IsRevving", true);
                    animator.SetBool("IsMoving", true);
                    enemy.setNavDestination(transform.position);
                    currentTime -= Time.deltaTime;
                if (currentTime <= 0)
                {
                    currentTime = attackCooldown;
                    phase = RolypolyPhase.Rolling;
                }
            }
                break;

            case RolypolyPhase.Flattened:
            {
                    
                    currentTime -= Time.deltaTime;
                if (currentTime <= 0) {
                    currentTime = attackCooldown;
                    phase = RolypolyPhase.Revving;
                }
            }
                break;
        }
    }

    public override bool takeDamage(int damage, bool isPush = false, float? direction = null)
    {
        if (phase == RolypolyPhase.Flattened)
            return base.takeDamage(damage * flattenedMultiplier, isPush, direction);
        
        return base.takeDamage(damage, isPush, direction);
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        /*Queen queen = collider.GetComponent<Queen>();
        if (queen != null)
        {
            RollOverQueen(queen);
            return;
        }*/

        WallType wall = collider.GetComponent<WallType>();
        if (wall != null)
        {
            enemy.push((enemy.queenObject.transform.position - enemy.transform.position).normalized, 9);
            phase = RolypolyPhase.Flattened;
            animator.SetBool("IsMoving", false);
        }
    }

    /*private void RollOverQueen(Queen queen) 
    {
        enemy.vfxManager.ManageObjects(0, .5f, transform.position);
        queen.takeDamage(damage);
        phase = RolypolyPhase.Revving;
        currentTime = attackCooldown;
    }*/

    public override void attackQueen()
    {
        base.attackQueen();
        enemy.vfxManager.ManageObjects(0, .5f, transform.position);
        phase = RolypolyPhase.Revving;
        animator.SetBool("IsRevving", true);
        currentTime = attackCooldown;

    }

    /*private void RollOverWall() 
    {
        gameObject.layer = 7;
        Vector3 direction = (enemy.queenObject.transform.position - enemy.transform.position).normalized;
        rb.AddForce(direction *(hitWallSpeed + 7.5f));
        gameObject.layer = 3;
        phase = RolypolyPhase.Flattened;
        currentTime = flattenedTime;
    }*/
    
    // Reset Enemy, remove comments and rename method once EnemyType has that
    // override void ResetOrWhateverItGetsCalled()
    // {
    //     private RolypolyPhase phase = RolypolyPhase.Rolling;
    //     healthValue = maxHealthValue;
    //     currentTime = revCooldown;
    // }
}
