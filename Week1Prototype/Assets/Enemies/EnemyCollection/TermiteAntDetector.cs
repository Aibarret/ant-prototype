using UnityEngine;

public sealed class TermiteAntDetector : MonoBehaviour
{
    [SerializeField] private TermiteType type;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("FormationUnit"))
        {
            if (collision.TryGetComponent(out WallType wall))
                type.DetectWallUnit(wall);

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("FormationUnit"))
        {
            if (collision.TryGetComponent(out WallType wall))
                type.DetectWallUnit(wall);

        }
    }
}
