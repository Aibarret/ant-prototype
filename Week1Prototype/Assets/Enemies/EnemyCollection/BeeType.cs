using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeType : EnemyType, Resetable
{
    public override string prefabName => "BeeEnemy";

    [Header("Bee Variables")]
    public GameObject beehive;
    public GameObject childColl;
    public bool recharging = false;
    public float honeyDuration;

    [HideInInspector] public int honeyBeforeRecharge;
    public ChildCollision cl;

    //Exists to help unit not spazz out with new system - Omar
    public float unitSwitch = 1;
    private int currentHoney = 0;

    //Moved here to help with new unit system - Omar
    FormationUnit nearestEnemy = null;

    [SerializeField] private Rigidbody2D rb;

#if UNITY_EDITOR
    // This field will automatically be left out when building
    [SerializeField] private string DEBUG_status = "Idle";
#endif
    
    public void resetObject()
    {
        health = maxHealth;
        recharging = false;
        currentHoney = 0;
        nearestEnemy = null;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        
        cl = childColl.GetComponent<ChildCollision>();
        cl.trigEnt = OnTriggerStay2D;
        //Fix for when their spawned in through a point - Omar
        if (beehive == null)
        {
            GameObject[] Obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

            foreach (GameObject obstacle in Obstacles)
            {
                if(obstacle.name.StartsWith("Beehive"))
                {
                    beehive = obstacle;
                    break;
                }
            }
        }
        //Help with unit finding system - Omar
        nearestEnemy = FindNearestUnit();
    }

    public override void enemyBehavior(Enemy enemy)
    {
        //So that findNearestUnit doesn't always happen - Omar
        if (unitSwitch <= 0)
        {
            nearestEnemy = FindNearestUnit();
            unitSwitch = 10;
        }

        const float minimumDistanceWithTheQueen = 30f;
        Vector2 queenPos = enemy.queenObject.transform.position;
        if (!recharging)
        {
            if (Vector2.Distance(transform.position, queenPos) > minimumDistanceWithTheQueen)
            {
#if UNITY_EDITOR
                DEBUG_status = "Approaching";
#endif
                
                // Try to get close to the queen
                enemy.setNavDestination(queenPos);
            }
            else if (nearestEnemy != null && !nearestEnemy.hasStatusEffectOfType("HoneyedStatus"))
            {
#if UNITY_EDITOR
                DEBUG_status = "Honeying";
#endif
                
                // Look for units to attack
            
                if (Vector2.Distance(transform.position, nearestEnemy.transform.position) < 1)
                {
                    if (!nearestEnemy.hasStatusEffectOfType("HoneyedStatus") && currentHoney <= 5)
                    {
                        currentHoney += 1;
                        nearestEnemy.applyStatusEffect(new HoneyedStatus(honeyDuration));
                        nearestEnemy.vfxManager.ManageParticles(3, VFXManager.VFXControl.Play, nearestEnemy.gameObject);
                        enemy.PlaySFX("Honey Drop");
                        //So it doesn't stay above the same unit
                        unitSwitch = 0;
                    }
                    if (currentHoney >= 5)
                    {
                        recharging = true;
                    }
                }
                else
                {
                    enemy.setNavDestination(nearestEnemy.transform.position);
                }
            }
        }
        else if (recharging)
        {
#if UNITY_EDITOR
            DEBUG_status = "Recharging";
#endif
            
            /* Find a new beehive */
            
            GameObject[] Obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

            foreach (GameObject obstacle in Obstacles)
            {
                if (obstacle.name.StartsWith("Beehive"))
                {
                    beehive = obstacle;
                    break;
                }
            }
            
            if (beehive == null && currentHoney == 0)
            {
                Destroy(gameObject);
                return;
            }
            
            
            
            /* Goes to assigned beehive */
            

            if (Vector2.Distance(transform.position, beehive.transform.position) < 2)
            {
                currentHoney = 0;
                recharging = false;
                unitSwitch = 0;
                enemy.vfxManager.ManageParticles(3, VFXManager.VFXControl.Play, gameObject);
            }
            else if (beehive != null)
            {
                enemy.setNavDestination(beehive.transform.position);
            }
        }
    }

    private void Update()
    {
        if (enemy != null)
        {
            enemyBehavior(enemy);
        }
        unitSwitch -= Time.deltaTime;
    }

    private FormationUnit FindNearestUnit()
    {
        // Had to simplfy this whole section, it was causing mass lag and made the game unplayable even with 1 bee - Omar
        List<FormationUnit> formUnits = GlobalVariables.FormationUnitList;
        if (formUnits.Count == 0)
        {
            unitSwitch = 0f;
            return null;
        }

        FormationUnit nearest = null;
        float distance = float.PositiveInfinity;
        foreach (FormationUnit unit in formUnits)
        {
            if (unit == null || !unit.gameObject.activeSelf)
                continue;
            
            float d = Vector2.Distance(enemy.queenObject.transform.position, unit.transform.position);
            if (d < distance)
            {
                if (unit.hasStatusEffectOfType("HoneyedStatus"))
                    continue;
                
                nearest = unit;
                distance = d;
            }
        }

        if (ReferenceEquals(nearest, null))
            unitSwitch = 0f;
        
        return nearest;
        // FormationUnit nearestUnit = formUnits[Random.Range(0,formUnits.Count)];
        // if (nearestUnit != null)
        // {
        //     foreach(FormationUnit unit in formUnits)
        //     {
        //         if (!unit.hasStatusEffectOfType("HoneyedStatus"))
        //         {
        //             nearestUnit = unit;
        //             break;
        //         }
        //     }
        // }
        // return nearestUnit;
        /*
                GameObject[] formUnits = GameObject.FindGameObjectsWithTag("FormationUnit");
                if (formUnits.Length == 0)
                {
                    return null;
                }

                FormationUnit nearestUnit = null;
                float closest = Mathf.Infinity;

                foreach (GameObject unit in formUnits)
                {
                    if (unit != null)
                    {
                        Debug.Log(unit.name);
                        FormationUnit currentUnit = unit.GetComponent<FormationUnit>();

                        if (currentUnit != null)
                        {
                            float dist = Vector3.Distance(transform.position, unit.transform.position);


                            if (dist < closest && !currentUnit.honeyed)
                            {
                                closest = dist;
                                nearestUnit = unit.GetComponent<FormationUnit>();
                            }
                        }
                    }
                }

                return nearestUnit;
        */
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("FormationUnit"))
        {
            
            FormationUnit unit = collision.gameObject.GetComponent<FormationUnit>();

            //Added these to fix errors for now, need a REAL fix - Omar
            if(collision.transform.Find("CursorCollision") == null)
            {
                return;
            }
            Transform cursorChild = collision.transform.Find("CursorCollision");
            if (cursorChild.gameObject.GetComponent<CircleCollider2D>() == null)
            {
                return;
            }
            CircleCollider2D collider = cursorChild.gameObject.GetComponent<CircleCollider2D>();
            float dist = Vector3.Distance(transform.position, cursorChild.transform.position);


            if (!unit.hasStatusEffectOfType("HoneyedStatus") && currentHoney < 5 && dist < (collider.radius / (collider.radius - 1)))
            {
                currentHoney += 1;
                unit.applyStatusEffect(new HoneyedStatus(honeyDuration));
                //So it doesn't stay above the same unit
                unitSwitch = 0;
            }
            if (currentHoney >= 5)
            {
                currentHoney = 0;
                recharging = true;
            }
        }
        if(collision.gameObject == beehive)
        {
            currentHoney = 0;
            recharging = false;
        }
    }
}
