using System;
using UnityEngine;

public class BombardierEnemy : EnemyType
{
    public override string prefabName => "BombardierEnemy";

    [SerializeField] private GameObject explosionObj; // Don't know what this is :O
    
    [SerializeField, Tooltip("The maximum distance between the queen that will trigger the explosion.")]
    private float triggerDistance = 3f;
    
    [SerializeField, Tooltip("The range of the actual explosion.")]
    private float explosionRadius = 5f;

    private bool isExploded;

    private void Start()
    {
        // Suppress the exceptions coming from the VFX manager bruh
        // The 'try-catch' block can be removed if VFX manager won't throw any exceptions anymore
        try
        {
            enemy.vfxManager.ManageParticles(5, VFXManager.VFXControl.Play, gameObject);
        }
        catch (Exception) { }
    }

    public override void onSpawn()
        => isExploded = false;
    
    public override void enemyBehavior(Enemy enemy)
    {
        base.enemyBehavior(enemy);
        
        if (Vector2.Distance(transform.position, enemy.queenObject.transform.position) <= triggerDistance)
            Explode();
    }

    public void Explode()
    {
        if (isExploded)
            return;
        isExploded = true;
        
        if (explosionObj != null)
            Instantiate(explosionObj, transform.position, Quaternion.identity);
        
        // Deal damage to all attackables and queen(s) in the circle range
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);
        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent(out Attackable a))
                a.takeDamage(damage);
            else if (collider.TryGetComponent(out Queen queen))
                queen.takeDamage(damage);
        }
        
        // Kill itself ;(
        takeDamage(int.MaxValue);
        gameObject.SetActive(false);

        // Suppress
        // The 'try-catch' block can be removed if VFX manager won't throw any exceptions anymore
        try
        {
            enemy.vfxManager.ManageParticles(13, VFXManager.VFXControl.Play, gameObject);
        }
        catch (Exception) { }
    }
    
    //Reset Enemy, remove comments and rename method once EnemyType has that
    // override void ResetOrWhateverItGetsCalled() {
    //     radiusSet = new HashSet<Attackable>();
    //     if (foundQueen != null) {
    //         speedValue /= 2;
    //         foundQueen = null;
    //     }
    //     explosionTimer = 0;
    // }
    
#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, triggerDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
#endif
}
