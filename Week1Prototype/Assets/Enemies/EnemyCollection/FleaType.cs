using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleaType : EnemyType, Resetable
{
    public static List<FleaType> fleasLatchedToQueen = new List<FleaType>();

    [HideInInspector]
    public GameObject queenObject;

    private bool isLatched;

    public override string prefabName => "FleaEnemy";

    public static void resetFleas()
    {
        fleasLatchedToQueen = new List<FleaType>();
    }

    public static void addToLatched(FleaType flea)
    {
        if (!fleasLatchedToQueen.Contains(flea))
        {
            fleasLatchedToQueen.Add(flea);
        }
    }

    public static void removeFromLatched(FleaType flea)
    {
        if (fleasLatchedToQueen.Contains(flea))
        {
            fleasLatchedToQueen.Remove(flea);
        }
    }

    public static Vector3 getLatchOffset(FleaType flea)
    {
        int index = fleasLatchedToQueen.FindIndex(a => a == flea);

        if (index != -1)
        {
            float yOffset = .25f + .25f * Mathf.Floor(index + 2  / 2f);
            float xOffset = .5f;

            if (index % 2 != 0)
            {
                xOffset *= -1;
            }

            return new Vector3(xOffset, yOffset);
        }
        else
        {
            return Vector3.zero;
        }

        

    }

    public void resetObject()
    {
        health = maxHealth;
    }

    public void Awake()
    {
        queenObject = GlobalVariables.queen.gameObject;
    }

    public override void onDie()
    {
        if (isLatched)
        {
            FleaType.removeFromLatched(this);
            isLatched = false;
        }
    }

    public override void enemyBehavior(Enemy enemy)
    {
        if (enemy.queenObject != null)
        {
            if (isLatched)
            {
                transform.position = enemy.queenObject.transform.position + FleaType.getLatchOffset(this);

                if (!waitForAttack)
                {
                    attackQueen();
                }
            }
            else
            {
                float distanceToQueen = (transform.position - queenObject.transform.position).magnitude;
                if (distanceToQueen > attackRange)
                {
                    enemy.setNavDestination(Vector3.zero);//queenObject.transform.position);
                }
                else
                {
                    isLatched = true;
                    weight = 10; // So it can't be pushed
                    enemy.toggleFlowField(false, false);
                    latchToQueen();
                }

            }

        } else
        {
            Debug.Log("Queen is NULL!!");
        }
    }
    
    public void latchToQueen()
    {
        enemy.toggleShadow(false);
        FleaType.addToLatched(this);
    }

    

    /*IEnumerator DamageQueen()
    {
        waitForAttack = true;
        queenObject.GetComponent<Queen>().takeDamage(damage);

        yield return new WaitForSeconds(damageInterval);
        waitForAttack = false;
    }*/
}
