using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class WaspType : EnemyType, Resetable
{
 
    private enum WaspState { SEARCHING, ATTACKING, RETREATING}

    public int fastSpeedValue;
    public int slowSpeedValue;

    
    public override string prefabName => "WaspEnemy";


    public bool drawAttackRange = false;

    [SerializeField] private int attackSpeed;

    private bool running = false;
    private Vector3 retreatLocation;
    private WaspState state = WaspState.SEARCHING;


    //Step 1) Find queen when spawning in




    //Step 2) Slowly move towards queen

    public override void enemyBehavior(Enemy enemy)
    {
        GameObject queen = enemy.queenObject;
        if (queen == null) return;

        switch (state)
        {
            case WaspState.SEARCHING:
                // Letting the flowfield control the wasp 
                float queenDistance = Vector3.Distance(transform.position, queen.transform.position);
                if (queenDistance <= attackRange)
                {
                    state = WaspState.ATTACKING;
                    enemy.changeSpeed(fastSpeedValue);
                    enemy.changeFlyHeight(FlyingState.HOVERING);
                    enemy.toggleFlowField(false);

                }
                else
                {
                    enemy.setNavDestination(queen.transform.position);
                }
                break;
            case WaspState.ATTACKING:
                enemy.vfxManager.ManageParticles(15, VFXManager.VFXControl.Play, gameObject);
                float currentDistance = Vector3.Distance(transform.position, queen.transform.position);

                enemy.setNavDestination(enemy.queenObject.transform.position);

                if (currentDistance <= 1)
                {
                    Vector3 direction = (queen.transform.position - enemy.transform.position).normalized;
                    retreatLocation = queen.transform.position + (direction * attackRange * 1.3f);
                    enemy.queenObject.GetComponent<Queen>().takeDamage(damage);
                    state = WaspState.RETREATING;
                    enemy.toggleFlowField(false);
                    enemy.changeFlyHeight(FlyingState.FLYING);
                }
                break;
            case WaspState.RETREATING:

                enemy.setNavDestination(retreatLocation);
                if (Vector3.Distance(transform.position, retreatLocation) <= 2)
                {   
                    enemy.changeSpeed(slowSpeedValue);
                    enemy.setNavDestination(enemy.queenObject.transform.position);
                    retreatLocation = Vector3.zero;
                    enemy.toggleFlowField(true);
                    state = WaspState.SEARCHING;
                }
                break;
        }

        //Step 3) Check if queen is in radius, if so start charge attack
        /*float queenDistance = Vector3.Distance(transform.position, queen.transform.position);
        if (queenDistance <= attackRange) 
        {
            if (!running) 
            {
                running = true;
                StartCoroutine(StartAttack(queen));
            }
        } 
        else if (!running)
        {
            enemy.setNavDestination(queen.transform.position);
        }*/
    
    }


    

    //Step 4) Charge attack - Speed up to 8 and descend
    //when first very close to queen, do damage attack
    //then keep flying past queen until queen is out of radius
    
    private IEnumerator StartAttack(GameObject queen) {
        //running = true;
        float timeSinceEnabled = 0;
        enemy.changeSpeed(fastSpeedValue);
        bool finished = false;
        enemy.changeFlyHeight(FlyingState.HOVERING);
        
        while (!finished) {
            enemy.vfxManager.ManageParticles(15, VFXManager.VFXControl.Play, gameObject);
            timeSinceEnabled += Time.deltaTime;
            float currentDistance = Vector3.Distance(transform.position, queen.transform.position);

            enemy.setNavDestination(enemy.queenObject.transform.position);


            if (currentDistance <= 1 && !finished) {
                finished = true;
                
                Vector3 direction = (queen.transform.position - enemy.transform.position).normalized;
                retreatLocation = queen.transform.position + (direction * attackRange);
                print("Wasp Attacked Queen");
                enemy.queenObject.GetComponent<Queen>().takeDamage(damage);
            }
            yield return null;
        }
        StartCoroutine(StartRetreat(queen));
    }

    private IEnumerator StartRetreat(GameObject queen) {
        //Debug.Log("start retreat!!!!!");
        float timeSinceEnabled = 0;
        bool finished = false;
        enemy.toggleFlowField(false);

        while (!finished) {
            timeSinceEnabled += Time.deltaTime;
            float currentDistance = Vector3.Distance(transform.position, queen.transform.position);

            enemy.setNavDestination(retreatLocation);
            if (Vector3.Distance(transform.position, retreatLocation) <= 2) 
            {
                finished = true;
                enemy.changeFlyHeight(FlyingState.FLYING);
                enemy.changeSpeed(speed);
                enemy.setNavDestination(enemy.queenObject.transform.position);
                retreatLocation = Vector3.zero;
            }
            yield return null;

        }
        enemy.toggleFlowField(true);
        running = false;
    }

    public void resetObject()
    {
        running = false;
        enemy.changeFlyHeight(FlyingState.FLYING);
    }


    //Step 5) Wait for cooldown to finish

    //Step 6) Repeat from step 2

    private void OnDrawGizmos()
    {
        if (drawAttackRange)
        {
            Gizmos.DrawWireSphere(transform.position, attackRange);
        }
    }
}
