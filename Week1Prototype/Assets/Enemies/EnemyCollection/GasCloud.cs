using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasCloud : MonoBehaviour
{
    public int damage;
    public float lifetime;
    public float timePerTick;

    private List<Collider2D> collidersInTrigger = new List<Collider2D>();
    private float lifeTimer = 0f;
    private float tickTimer = Mathf.Infinity;
    private Collider2D cloudCollider;

    public VFXManager vfxManager;

    private void Start()
    {
        cloudCollider = GetComponent<Collider2D>();

        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
        }

        vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, gameObject);
    }
    
    void Update()
    {
        ManageLife();
        DamageQueenAndRestrictDrawing();
    }

    private void ManageLife()
    {
        lifeTimer += Time.deltaTime;
        if (lifeTimer > lifetime)
        {
            Destroy(gameObject);
        }
    }

    private void DamageQueenAndRestrictDrawing()
    {
        tickTimer += Time.deltaTime;
        collidersInTrigger.Clear();
        Physics2D.OverlapCollider(cloudCollider, new ContactFilter2D().NoFilter(), collidersInTrigger);

        foreach (Collider2D collider in collidersInTrigger)
        {
            if (collider.CompareTag("Queen") && tickTimer > timePerTick)
            {
                tickTimer = 0f;
                collider.gameObject.GetComponent<Queen>().takeDamage(damage);
            }
            /* - Commenting this out because it is way to strong, It should just stop new units being spawned on it, not killing old ones - Omar
            if(collider.CompareTag("FormationUnit"))
            {
                UnitType formationUnitType = collider.gameObject.GetComponent<UnitType>();

                if(formationUnitType.GetCurrentLifeSpan() < lifetime)
                {
                    Destroy(collider.gameObject);
                }
            }
            */
        }
    }
}
