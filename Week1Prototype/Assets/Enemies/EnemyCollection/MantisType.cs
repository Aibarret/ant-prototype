using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor.Experimental;
using UnityEngine;

public class MantisType : EnemyType, Resetable
{
    private bool isHealing = false;
    private List<Collider2D> collidersInTrigger = new List<Collider2D>();
    public int healRate = 3;
    public Collider2D healDetector;
    public float healTimer = 2;
    private float timer;

    public override string prefabName => "MantisEnemy";


    [SerializeField] private GameObject healingEffect;

    public void resetObject()
    {
        health = maxHealth;
        isHealing = false;
    }

    public override void enemyBehavior(Enemy enemy)
    {
        Enemy nearestEnemy = FindNearestEnemy();
        if (nearestEnemy != null)
            enemy.setNavDestination(nearestEnemy.transform.position);
    }

    private void Update()
    {
        FindAndHealEnemiesInTrigger();
        
        timer -= Time.deltaTime;
        healingEffect.SetActive(isHealing);
    }

    private Enemy FindNearestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (enemies.Length == 0)
            return null;
    
        Enemy nearestEnemy = null;
        float closest = Mathf.Infinity;
        Vector2 mantisPos = transform.position;
    
        foreach (GameObject enemy in enemies)
        {
            // Ignore destroyed and inactive game objects
            if (enemy == null || !enemy.activeSelf)
                continue;
            
            // Ignore non enemies and self
            if (!enemy.CompareTag("Enemy") || enemy == this.gameObject)
                continue;
            
            // Ignore mantises and bees
            if (enemy.TryGetComponent<MantisType>(out _) || enemy.TryGetComponent<BeeType>(out _))
                continue;
            
            // Ignore instances without 'Enemy' component
            Enemy e = enemy.GetComponent<Enemy>();
            if (e == null)
                continue;
            
            // Ignore enemies with full health
            if (e.health >= e.maxHealth)
                continue;
            
            float dist = Vector2.Distance(mantisPos, enemy.transform.position);
            if (dist < closest)
            {
                closest = dist;
                nearestEnemy = enemy.GetComponent<Enemy>();
            }
        }
    
        return nearestEnemy;
    }
    
    private void FindAndHealEnemiesInTrigger()
    {
        collidersInTrigger.Clear();
        Physics2D.OverlapCollider(healDetector, new ContactFilter2D().NoFilter(), collidersInTrigger);
        isHealing = false;
        
        if (timer <= 0)
        {
            enemy.PlaySFX("Praying Mantis Heal");
            foreach (Collider2D collider in collidersInTrigger)
            {
                // Ignore destroyed and inactive game objects
                if (collider.gameObject == null || !gameObject.activeSelf)
                    continue;
                
                // Ignore non enemies and self
                if (!collider.CompareTag("Enemy") || collider.gameObject == this.gameObject)
                    continue;
                
                // Ignore mantises and bees
                if (collider.TryGetComponent<MantisType>(out _) || collider.TryGetComponent<BeeType>(out _))
                    continue;
                
                // Ignore instances without 'Enemy' component
                if (!enemy.TryGetComponent<Enemy>(out _))
                    continue;
                    
                enemy.takeDamage(0 - healRate);
                isHealing = true; 
            }
            timer = healTimer;
        }
    }
}
