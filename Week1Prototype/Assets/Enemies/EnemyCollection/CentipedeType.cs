using UnityEngine;

public class CentipedeType : EnemyType
{
    public override string prefabName => "CentipedeEnemy";

    [SerializeField] private float circleAroundDistance = 3f;

    private bool isCirclingAround;

    private float direction;

    private void Start()
        => isCirclingAround = false;

    private void OnDisable()
        => isCirclingAround = false;

    public override void enemyBehavior(Enemy enemy)
    {
        if (!CanMove)
            return;

        circleAround:
        if (isCirclingAround)
        {
            direction += Time.deltaTime * 50f;
            if (direction >= 360f)
                direction = 360f - direction;

            Vector3 pos = enemy.queenObject.transform.position;
            float rad = direction * Mathf.Deg2Rad;
            Vector2 dir = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
            Vector2 offset = dir * circleAroundDistance;
            pos.x += offset.x;
            pos.y += offset.y;
            //enemy.setNavDestination(pos);
            return;
        }
        
        if (Vector2.Distance(transform.position, enemy.queenObject.transform.position) <= circleAroundDistance)
        {
            // Start circling around the queen
            isCirclingAround = true;
            Vector2 dir = transform.position - enemy.queenObject.transform.position;
            dir.Normalize();
            direction = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            goto circleAround;
        }
        
        // Move towards the queen
        //enemy.setNavDestination(enemy.queenObject.transform.position);
    }
}
