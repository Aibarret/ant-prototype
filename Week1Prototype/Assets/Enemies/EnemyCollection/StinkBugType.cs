using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StinkBugType : EnemyType, Resetable
{
   
    [HideInInspector]
    public GameObject queenObject;
    public GameObject gasCloud;
    public float cloudCooldown;
    public float radiusToGas;

    public override string prefabName => "StinkBugEnemy";


    private float timer;
    private bool isCooldownActive = false;

    public void resetObject()
    {
        health = maxHealth;
        timer = cloudCooldown;
        isCooldownActive = false;
    }

    public void Awake()
    {
        queenObject = GlobalVariables.queen.gameObject;
        timer = cloudCooldown;
    }

    private void Update()
    {
        //print(transform.position + "          " + GetComponent<Rigidbody2D>().velocity);
        /*if (!isCooldownActive) {
          timer += Time.deltaTime;
            if (timer >= cloudCooldown)
            {
                float distanceToQueen = (transform.position - queenObject.transform.position).magnitude;

                if (distanceToQueen < radiusToGas)
                {
                    timer = 0f;
                    SpawnGasCloud();
                }
            }
        }*/
    }

    private void SpawnGasCloud()
    {
        enemy.PlaySFX("Stink Bug Gas Release");
        isCooldownActive = true;
        Instantiate(gasCloud, transform.position, Quaternion.identity);
        //StartCoroutine(PauseMovement());
    }

    private IEnumerator PauseMovement()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        Debug.Log("1st Test: " + rb.velocity);
        Vector2 previousVelocity = rb.velocity;
        

        rb.velocity = Vector2.zero;
        Debug.Log("2nd Test: " + rb.velocity);
        Debug.Log("3rd Test: " + previousVelocity);

        yield return new WaitForSeconds(cloudCooldown);

        GetComponent<Rigidbody2D>().velocity = previousVelocity;
        Debug.Log("4th Test: " + rb.velocity);
        isCooldownActive = false;
    }

    public override void enemyBehavior(Enemy enemy)
    {
        timer += Time.deltaTime;
        if (!isCooldownActive)
        {
            //enemy.setNavDestination(enemy.queenObject.transform.position);

            float distanceToQueen = (transform.position - queenObject.transform.position).magnitude;
            if (distanceToQueen < radiusToGas)
            {
                timer = 0f;
                SpawnGasCloud();
            }
        } 
        else
        {
            enemy.rb.velocity = Vector2.zero;
        }
        if(timer > cloudCooldown)
        {
            isCooldownActive = false;
        }
    }
}