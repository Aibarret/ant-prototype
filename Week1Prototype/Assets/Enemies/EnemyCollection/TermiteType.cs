using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

public class TermiteType : EnemyType, Resetable
{
    public override string prefabName => "TermiteEnemy";

    private List<WallType> unitsInRange = new List<WallType>();

    public override void enemyBehavior(Enemy enemy)
    {
        // if (foundWallAnt == null) {
        //     base.enemyBehavior(enemy);
        // } else {
        //     enemy.setNavDestination(foundWallAnt.transform.position);
        //     TryAttack();//
        // }

        if (targetWallUnit == null)
        {
            enemy.agent.enabled = true;
            base.enemyBehavior(enemy);
            return;
        }
        
        enemy.agent.enabled = true;
        enemy.setNavDestination(targetWallUnit.transform.position);
        TryAttack();
    }

    // Wall Ant
    [SerializeField] private float timeToAttack;
    [SerializeField] private int damageToWalls;
    private float currentAttackTime;
    private WallType targetWallUnit = null;


    private void TryAttack() {
        currentAttackTime -= Time.deltaTime;
        if (currentAttackTime <= 0)
        {
            if (Vector2.Distance(transform.position, targetWallUnit.transform.position) <= attackRange) { // Bruh
                try
                {
                    targetWallUnit.TakeDamage(damageToWalls);
                    enemy.PlaySFX("Termite Eating");
                    enemy.vfxManager.ManageParticles(14, VFXManager.VFXControl.Play, gameObject);
                } catch (Exception) { }
                currentAttackTime = timeToAttack;
                if (targetWallUnit.currentHealth <= 0)
                {
                    unitsInRange.Remove(targetWallUnit);
                    targetWallUnit = null;
                    decideTargetUnit();
                }
                   
            }
        }
    }

    public void resetObject()
    {
        // nearbyWallAnts = new();
        // currentAttackTime = timeToAttack;
        // foundWallAnt = null;
        currentAttackTime = timeToAttack;
        targetWallUnit = null;
    }

    public void DetectWallUnit([NotNull] WallType wall)
    {
        if (wall == null)
            throw new ArgumentNullException(nameof(wall));

        //if (targetWallUnit == null)

        if (!unitsInRange.Contains(wall))
        {
            unitsInRange.Add(wall);
        }

        if (targetWallUnit == null)
        {
            decideTargetUnit();
        }
    }

    public void undetectWallUnit([NotNull] WallType wall)
    {
        if (wall == null)
            throw new ArgumentNullException(nameof(wall));

        //if (targetWallUnit == null)

        if (unitsInRange.Contains(wall))
        {
            unitsInRange.Remove(wall);
            if (targetWallUnit == wall)
            {
                targetWallUnit = null;
                decideTargetUnit();
            }
        }
    }

    private void decideTargetUnit()
    {
        if (unitsInRange.Count == 0)
        {
            return;
        }

        float closestDistance = Vector3.Distance(transform.position, unitsInRange[0].transform.position);
        WallType closestUnit = unitsInRange[0];

        foreach (WallType unit in unitsInRange)
        {
            if (unit == null)
            {
                continue;
            }
            float distance = Vector3.Distance(transform.position, unit.transform.position);

            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestUnit = unit;
            }
        }

        targetWallUnit = closestUnit;
    }
}
