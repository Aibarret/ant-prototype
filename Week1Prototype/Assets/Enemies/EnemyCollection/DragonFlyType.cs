using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class DragonFlyType : EnemyType
{

    [HideInInspector] public GameObject queenObject;

    public override string prefabName => "DragonFlyEnemy";

    [SerializeField] private int attackSpeed;
    [SerializeField] private float maxHeight;
    [SerializeField] private SpriteRenderer flightShadow;
    [SerializeField] private CircleCollider2D circleColl2D;
    [SerializeField] private FlyingHeight flyingHeight;
    [SerializeField] private float cooldown;
    [SerializeField] private GameObject fireball;

    private bool cooling = false;

    private void Awake()
    {
        queenObject = GlobalVariables.queen.gameObject;
    }


    public override void enemyBehavior(Enemy enemy)
    {
        if (queenObject == null) return;

        enemy.setNavDestination(queenObject.transform.position);
        

        float queenDistance = Vector3.Distance(transform.position, queenObject.transform.position);
        if (queenDistance <= attackRange && !cooling)
        {
            StartCoroutine(StartAttack());
        }

    }

    /*public override void changeDirection(bool isRight)
    {
        if (isRight)
        {
            transform.localPosition = new Vector3(-1.7687f, 1.15799999f, -0.0069207456f);
        }
        else
        {
            transform.localPosition = new Vector3(1.7687f, 1.15799999f, -0.0069207456f);
        }
    }*/

    private IEnumerator StartAttack()
    {
        cooling = true;
        GameObject newFireball = Instantiate(fireball, transform.position, Quaternion.identity);
        Homing ball = newFireball.GetComponent<Homing>();
        ball.target = queenObject;
        enemy.vfxManager.ManageParticles(6, VFXManager.VFXControl.Play, ball.gameObject);

        queenObject.GetComponent<Queen>().takeDamage(damage);
        yield return new WaitForSeconds(cooldown);
        cooling = false;
    }
}
