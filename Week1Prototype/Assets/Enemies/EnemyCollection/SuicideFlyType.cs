using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuicideFlyType : EnemyType, Resetable
{
    [HideInInspector]
    public GameObject pieSprite;
    public float radius;

    [SerializeField] GameObject explosionObj;
    
    

    public override string prefabName => "SuicideFlyEnemy";


    private bool pied = false;
    private bool exploding = false;

    public void resetObject()
    {
        health = maxHealth;
        pied = false;
        exploding = false;
    }


    private void Start()
    {
        enemy.vfxManager.ManageParticles(5, VFXManager.VFXControl.Play, gameObject);
    }

    public override void enemyBehavior(Enemy enemy)
    {
        Obstacle nearestPie = FindNearestPie();

        if (pied)
        {
            enemy.setNavDestination(enemy.queenObject.transform.position);
        }
        else if (nearestPie != null)
        {
            enemy.setNavDestination(nearestPie.transform.position);
        }
        else
        {
            enemy.setNavDestination(enemy.queenObject.transform.position);
        }
    }

    private Obstacle FindNearestPie()
    {
        GameObject[] units = GameObject.FindGameObjectsWithTag("Power-Up Obstacle");
        if (units.Length == 0)
        {
            return null;
        }

        Obstacle nearestPie = null;
        float closest = Mathf.Infinity;
        Vector3 flyPos = transform.position;

        foreach (GameObject unit in units)
        {
            PieType pie = unit.GetComponent<PieType>();
            if (pie != null)
            {
                float dist = Vector3.Distance(flyPos, unit.transform.position);

                if (dist < closest)
                {
                    closest = dist;
                    nearestPie = unit.GetComponent<Obstacle>();
                }
            }
        }
        return nearestPie;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PieType pie = collision.gameObject.GetComponent<PieType>();
        if(pie != null)
        {
            pied = true;
            pieSprite.SetActive(true);          
        }
        if(collision.CompareTag("Queen"))
        {
            exploding = true;
            VFXManager.Instance.ManageObjects(1, 1f, transform.position);
            SFXManager.Instance.playSFX("event:/FX/Explosion");
            collision.GetComponent<Queen>().takeDamage(damage);
            enemy.takeDamage(health);
        }
    }

    public override void attackQueen()
    {
        if (exploding)
        {
            enemy.vfxManager.ManageParticles(13, VFXManager.VFXControl.Play, gameObject);
            CircleCollider2D collider = gameObject.GetComponent<CircleCollider2D>();
            collider.radius = radius;

            List<Collider2D> collidersInTrigger = new List<Collider2D>();
            collidersInTrigger.Clear();

            Physics2D.OverlapCollider(collider, new ContactFilter2D().NoFilter(), collidersInTrigger);

            foreach (Collider2D coll in collidersInTrigger)
            {
                if (coll.CompareTag("Queen"))
                {
                    collider.gameObject.GetComponent<Queen>().takeDamage(damage);
                }
                if (collider.CompareTag("Enemy"))
                {
                    collider.gameObject.GetComponent<Enemy>().takeDamage(damage);
                }
            }
            enemy.dieAndRespawn();
        }
    }

    /*private void OnTriggerStay2D(Collider2D collision)
    {
        if(exploding)
        {
            CircleCollider2D collider = gameObject.GetComponent<CircleCollider2D>();
            collider.radius = radius;

            List<Collider2D> collidersInTrigger = new List<Collider2D>();
            collidersInTrigger.Clear();

            Physics2D.OverlapCollider(collider, new ContactFilter2D().NoFilter(), collidersInTrigger);

            foreach (Collider2D coll in collidersInTrigger)
            {
                if (coll.CompareTag("Queen"))
                {
                    coll.gameObject.GetComponent<Queen>().takeDamage(damage);
                }
                if (coll.CompareTag("Enemy"))
                {
                    coll.gameObject.GetComponent<Enemy>().takeDamage(damage);
                }
            }
            Destroy(gameObject);
        }
    }*/
}
