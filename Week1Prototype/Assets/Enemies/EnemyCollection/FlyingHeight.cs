using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class FlyingHeight : MonoBehaviour
{

    private float _height;
    public float Height {
        get => _height;
        set { 
            _height = value;
            UpdateHeight();
        }
    }

    private void UpdateHeight() {
        transform.position = new Vector3(transform.parent.position.x,
        transform.parent.position.y + _height, transform.parent.position.z);
    }


}
