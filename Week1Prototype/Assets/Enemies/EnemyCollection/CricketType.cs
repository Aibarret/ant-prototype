using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class CricketType : EnemyType, Resetable
{
    private enum CricketState
    {
        SEARCHING_QUEEN,
        SEARCHING_UNITS,
        FLEEING
    }

    public override string prefabName => "Cricket";

    [Header("Object References")]
    [SerializeField] private ChildCollision unitSearcher;
    [SerializeField] private ParticleSystem musicVFX;

    [Header("Cricket Variables")]
    [SerializeField] private int minimumUnitAmount;
    [SerializeField] private float maxQueenDistance;
    [Range(0, 1)] [SerializeField] private float nearbyQueenPercentage;
    [SerializeField] private float musicFrequency;

    private List<FormationUnit> unitsInRange = new List<FormationUnit>();
    private List<FormationUnit> pullUnits = new List<FormationUnit>();

    private const float unitSearchInterval = 3f;
    private static readonly WaitForSeconds waitForUnitSearchInterval = new WaitForSeconds(unitSearchInterval);
    private const float minimumDistanceBetweenTarget = 3f;
    private const float minimumDistanceBetweenQueen = 10f;
    private CricketState state = CricketState.SEARCHING_QUEEN;
    private float soundEffectTimer = 0f;

    private Transform targetUnit = null;

    private Coroutine searchCoroutine = null;
    private Coroutine singCoroutine = null;

    private void Start()
    {
        unitSearcher.trigEnt += unitEnterSearchRange;
        unitSearcher.trigExt += unitExitSearchRange;
    }


    public override void enemyBehavior(Enemy enemy)
    {
        // General thought process is the the cricket has three main things to do. It's going to try to get close to the queen, find units in range, then pull them away.
        // it will likely keep trying to do that in short bursts due to the queens speed, causing it to quickly switch between forms.

        if (state == CricketState.SEARCHING_QUEEN)
        {
            base.enemyBehavior(enemy);

            if (Vector3.Distance(transform.position, enemy.queenObject.transform.position) < maxQueenDistance * nearbyQueenPercentage)
            {
                state = CricketState.SEARCHING_UNITS;
            }
        }
        else if (state == CricketState.SEARCHING_UNITS)
        {
            if (unitsInRange.Count >= minimumUnitAmount)
            {
                pullUnits = new List<FormationUnit>(unitsInRange);
                state = CricketState.FLEEING;
                enemy.PlaySFX("Cricket Lure");
            }
            else
            {
                base.enemyBehavior(enemy);
            }
        } 
        else if (state == CricketState.FLEEING)
        {
            if (!musicVFX.isPlaying)
            {
                musicVFX.Play();
            }
            MoveAwayFromTheQueen();
            

            bool checkNoUnits = true;
            foreach (FormationUnit unit in pullUnits)
            {
                if (unit.gameObject.activeInHierarchy)
                {
                    unit.pull((transform.position - unit.transform.position).normalized, 5);
                    checkNoUnits = false;
                }
            }

            if (Vector3.Distance(transform.position, enemy.queenObject.transform.position) > maxQueenDistance || checkNoUnits)
            {
                state = CricketState.SEARCHING_QUEEN;
                pullUnits = new List<FormationUnit>();
                soundEffectTimer = 0;
                musicVFX.Stop();
            }

            soundEffectTimer += Time.deltaTime;

            if (soundEffectTimer >= musicFrequency)
            {
                enemy.PlaySFX("Cricket Lure");
                soundEffectTimer = 0;

            }
        }

        #region old code
        /*if (Vector2.Distance(transform.position, enemy.queenObject.transform.position) < minimumDistanceBetweenQueen)
            MoveAwayFromTheQueen();

        if (targetUnit != null)
        {
            // Move to the target
            float distanceBetweenTarget = Vector2.Distance(transform.position, targetUnit.position);
            if (distanceBetweenTarget > minimumDistanceBetweenTarget)
            {
                // Move to the target unit
                enemy.setNavDestination(targetUnit.position);
                //transform.position = Vector2.MoveTowards(transform.position, targetUnit.position, _speed * Time.deltaTime);
            }
        }*/
        #endregion
    }

    private void MoveAwayFromTheQueen()
    {
        Vector2 direction = enemy.queenObject.transform.position - transform.position;
        direction.Normalize();
        direction = -direction;
        Vector2 pos = (Vector2)(transform.position) + (direction * knockback);
        enemy.setNavDestination(pos);
        
        //transform.Translate(direction * (Time.deltaTime * (_speed / 2f)));
    }

    #region old Coroutines
    private IEnumerator SearchNearestUnitRoutine()
    {
        while (true)
        {
            List<FormationUnit> unitList = new List<FormationUnit>();
            const float range = 5f;
            
            // Find the closest unit
            float closestDistance = Mathf.Infinity;
            GameObject closest = null;
            GameObject[] units = GameObject.FindGameObjectsWithTag("FormationUnit");
            foreach (GameObject unit in units)
            {
                float distance = Vector2.Distance(transform.position, unit.transform.position);

                if (distance <= range)
                {
                    FormationUnit formationUnit = unit.GetComponent<FormationUnit>();
                    if (formationUnit != null)
                    {
                        unitList.Add(formationUnit);

                    }
                }
                    
                
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closest = unit;
                }
            }
            
            // Found an unit, update target
            if (closest != null)
                targetUnit = closest.transform;

            if (singCoroutine != null)
                StopCoroutine(singCoroutine);
            if (unitList.Count >= 5)
                singCoroutine = StartCoroutine(SingRoutine(unitList));
            
            if (unitList.Count >= 5)
            {
                enemy.PlaySFX("Cricket Lure");
            }
            yield return waitForUnitSearchInterval;
        }
        
    }

    private IEnumerator SingRoutine(List<FormationUnit> unitList)
    {
        //const float unitSpeed = 3f;
        
        while (true)
        {
            enemy.vfxManager.ManageParticles(17, VFXManager.VFXControl.Play, gameObject);
            MoveAwayFromTheQueen();
            foreach (FormationUnit unit in unitList)
            {
                print(unit);
                unit.vfxManager.ManageParticles(17, VFXManager.VFXControl.Play, unit.gameObject);
                unit.pull((transform.position - unit.transform.position).normalized, knockback);
            }
               

            yield return null;
        }
    }
    #endregion

    public void resetObject()
    {
        // Start nearest unit searching coroutine
        /*if (searchCoroutine != null)
            StopCoroutine(searchCoroutine);
        searchCoroutine = StartCoroutine(SearchNearestUnitRoutine());*/
    }

    public void unitEnterSearchRange(Collider2D collider)
    {
        FormationUnit unit = collider.gameObject.GetComponent<FormationUnit>();

        if (unit != null && collider.gameObject.activeInHierarchy)
        {
            if (!unitsInRange.Contains(unit))
            {
                unitsInRange.Add(unit);
            }
        }
    }

    public void unitExitSearchRange(Collider2D collider)
    {
        FormationUnit unit = collider.gameObject.GetComponent<FormationUnit>();

        if (unit != null && collider.gameObject.activeInHierarchy)
        {
            if (unitsInRange.Contains(unit))
            {
                unitsInRange.Remove(unit);
            }
        }
    }
}
