﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

namespace Boss
{
    public sealed class QueenScuffle : MonoBehaviour
    {
        [SerializeField] private ParticleSystem ps;

        [SerializeField] private TextMeshProUGUI text;

        private new Camera camera;
        public VFXManager vfxManager;
        private void ClearEffects() => ps.Clear();
        
        private readonly HashSet<Enemy> enemySet = new HashSet<Enemy>();
        private readonly HashSet<FormationUnit> unitSet = new HashSet<FormationUnit>();

        private int enemyInScuffleCount;
        public int EnemyInScuffleCount
        { 
            get => enemyInScuffleCount;
            set
            {
                enemyInScuffleCount = value;
                UpdateText(AllyInScuffleCount, EnemyInScuffleCount);
            }
            
        }

        private int allyInScuffleCount;
        public int AllyInScuffleCount
        {
            get => allyInScuffleCount;
            set
            {
                allyInScuffleCount = value;
                UpdateText(AllyInScuffleCount, EnemyInScuffleCount);
            }
        }

        public bool IsStarted { get; private set; } = false;

        private void Awake()
        {
            camera = Camera.main;
            ClearEffects();
            text.gameObject.SetActive(false);
        }

        private void Start()
        {
            vfxManager = VFXManager.Instance;

            if (vfxManager == null)
            {
                Debug.Log("There is no VFXManager Instance");
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!IsStarted)
                return;
            
            if (other.CompareTag("Enemy"))
            {
                Enemy enemy = other.GetComponent<Enemy>();
                if (enemy.IsBoss)
                    return;
                enemy.setNavDestination(transform.position);
                enemySet.Add(enemy);
            }
            else if (other.CompareTag("FormationUnit"))
            {
                FormationUnit unit = other.GetComponent<FormationUnit>();
                /*unit.type.enabled = false;
                unit.enabled = false;*/
                unitSet.Add(unit);
            }
        }

        private void Update()
        {
            if (!IsStarted)
                return;
            
            text.rectTransform.position = camera.WorldToScreenPoint(transform.position);
            
            foreach (Enemy enemy in enemySet)
            {
                if (enemy.gameObject.activeSelf)
                    enemy.setNavDestination(transform.position);
            }

            foreach (FormationUnit unit in unitSet)
            {
                const float speed = 5f;
                //unit.transform.position = Vector2.MoveTowards(unit.transform.position, transform.position, speed * Time.deltaTime);
                if (unit)
                {
                    unit.pull((transform.position - unit.transform.position).normalized, 9);

                }
            }
        }
        
        public void StartScuffle(Vector2 position, [NotNull] Queen queen, [NotNull] BossType boss)
        {
            transform.position = position;
            
            StartCoroutine(ScuffleRoutine(queen, boss));
        }

        private IEnumerator ScuffleRoutine([NotNull] Queen queen, [NotNull] BossType boss)
        {
            print("Starting Scuffle");
            NullUtilities.ThrowIfNullArgument(queen, nameof(queen));
            NullUtilities.ThrowIfNullArgument(boss, nameof(boss));

            EnemyInScuffleCount = 50;
            AllyInScuffleCount = 50;
            
            // Initialize text content
            UpdateText(50, 50);
            
            // Start
            queen.paused = true;
            queen.CanTakeDamage = false;
            boss.CanBossMove = false;
            boss.enemy.CanTakeDamage = false;
            vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, gameObject);
            ps.Play();
            IsStarted = true;
            text.gameObject.SetActive(true);

            bool allyWins;
            while (true)
            {
                if (AllyInScuffleCount < 50 || EnemyInScuffleCount < 50)
                {
                    yield return null;
                    continue;
                }

                if (AllyInScuffleCount >= 1.33 * EnemyInScuffleCount)
                {
                    allyWins = true;
                    boss.scuffleTimer = boss.scuffleTimerTime;
                    break;
                }
                if (EnemyInScuffleCount >= 1.33 * AllyInScuffleCount)
                {
                    allyWins = false;
                    boss.scuffleTimer = boss.scuffleTimerTime;
                    break;
                }
                
                yield return null;
            }
            
            // End
            queen.paused = false;
            boss.CanBossMove = true;
            boss.CanBossMove = true;
            queen.CanTakeDamage = true;
            boss.enemy.CanTakeDamage = true;
            ps.Stop();
            IsStarted = false;
            text.gameObject.SetActive(false);

            // Deal damage to the loser side
            print("taking damage in scuffle");
            if (allyWins)
                boss.takeDamage(AllyInScuffleCount * 10);
            else
                queen.takeDamage(EnemyInScuffleCount * 2);
            
            // Send units and enemies away
            // foreach (Enemy enemy in enemySet)
            // {
            //     
            // }
            foreach (FormationUnit unit in unitSet)
            {
                unit.enabled = true;
                unit.type.enabled = true;
            }

            //GameObject.Destroy(gameObject);
        }

        private void UpdateText(int allyCount, int enemyCount)
            => text.text = $"ally count: {allyCount}\nenemy count: {enemyCount}";
    }
}
