using System;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    [Serializable]
    public sealed class BezierCurve
    {
        // Control Point List
        [SerializeField] private List<Vector2> pointList = new List<Vector2>();
        public List<Vector2> PointList => pointList;

        // Point Count
        public int PointCount => pointList.Count;



        /* Constructors */
            
        public BezierCurve(IEnumerable<Vector2> points)
            => pointList.AddRange(points);

        public BezierCurve() { }



        public Vector2 GetPoint(float t)
        {
            // Has no control point
            if (pointList.Count == 0)
                throw new InvalidOperationException("Cannot get point since the bezier curve has no control point.");
                
            // Clamp t between 0 and 1
            if (t < 0f)
                t = 0f;
            else if (t > 1f)
                t = 1f;

            // Calculate the point
            int n = pointList.Count - 1;
            Vector2 result = Vector2.zero;
            for (int i = 0; i <= n; i++)
            {
                result += BinomialCoefficient(n, i) * Mathf.Pow(1f - t, n - i) * Mathf.Pow(t, i) 
                    * pointList[i];
            }
            return result;
        }

        public void AddPoint(Vector2 newPoint)
            => pointList.Add(newPoint);

        public void RemovePointAt(int index)
            => pointList.RemoveAt(index);

        public float GetApproximateLength(int segments)
        {
            if (segments <= 0)
                throw new ArgumentException("Number of segments must be greater than zero.", nameof(segments));

            float length = 0f;
            Vector2 prevPoint = GetPoint(0f);

            for (int i = 1; i <= segments; i++)
            {
                Vector2 currentPoint = GetPoint(i / (float)segments);
                length += Vector2.Distance(prevPoint, currentPoint);
                prevPoint = currentPoint;
            }

            return length;
        }
        
        private static int BinomialCoefficient(int n, int k)
        {
            // Invalid arguments
            if (k > n)
                throw new ArgumentException("n must be greater or equal to k.", nameof(n));
                
            // If n = k, the result is 1
            if (k == 0 || k == n)
                return 1;

            const int m = 1000000007;

            Span<int> inv = stackalloc int[k + 1];
            inv[0] = 1;
            if (k + 1 >= 2)
                inv[1] = 1;
                
            for (int i = 2; i <= k; i++)
                inv[i] = m - (m / i) * inv[m % i] % m;

            int result = 1;
     
            // 1 / (r!)
            for (int i = 2; i <= k; i++)
                result = (result % m) * (inv[i] % m) % m;

            // n * (n - 1) * (n - 2) * ... * (n - r + 1)
            for (int i = n; i >= n - k + 1; i--)
                result = (result % m) * (i % m) % m;

            return result;
        }
    }
}
