﻿using System;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Boss
{
    public sealed class PathNode : MonoBehaviour
    {
        private void Awake()
            => SetUpCurve(Curve, next, useNextNodeAsControlPoint);

        private void SetUpCurve([NotNull] BezierCurve curve, PathNode next, bool useNextNodeAsControlPoint)
        {
            if (curve is null)
                throw new ArgumentNullException(nameof(curve));
            
            curve.PointList.Clear();
            
            // Add self position as the first control point
            curve.PointList.Add(transform.position);
            
            // Add middle control points
            foreach (Transform point in controlPoints)
            {
                if (point != null)
                    curve.PointList.Add(point.position);
            }
            
            // Add the position of the next node as the last control point if feasible
            if (useNextNodeAsControlPoint && next != null)
                curve.PointList.Add(next.transform.position);   
        }
        
        
        
        /* ===== Path ===== */
        [Header("Path")]
        
        [SerializeField] private Transform[] controlPoints;
        [SerializeField] private PathNode next;
        public PathNode Next => next;
        
        [SerializeField] private bool useNextNodeAsControlPoint = true;

        public BezierCurve Curve { get; } = new BezierCurve();

        
        
        /* ===== Events ===== */
        [Header("Events")]
        
        [SerializeField] private UnityEvent enterEvent;
        [SerializeField] private UnityEvent exitEvent;



        /* ===== Visiting Methods ===== */
        
        /// <summary>
        /// This method will be called by 'BossPathController' when the boss visit this node.
        /// </summary>
        public void OnVisit()
            => enterEvent?.Invoke();

        /// <summary>
        /// This method will be called by 'BossPathController' when the boss leave this node.
        /// </summary>
        public void OnLeave()
            => exitEvent?.Invoke();
        
        
#if UNITY_EDITOR
        /* ===== Debug ===== */

        private static readonly BezierCurve editorCurveCache = new BezierCurve();

        private static readonly Color pathColor = Color.red;

        private const int pathThickness = 3;

        private const int pathResolution = 10;
        
        private void OnDrawGizmos()
        {
            BezierCurve curve;
            if (Application.isPlaying)
                curve = Curve;
            else
            {
                SetUpCurve(editorCurveCache, next, useNextNodeAsControlPoint);
                curve = editorCurveCache;
            }

            if (curve.PointList.Count < 2)
                return;
            
            Handles.color = pathColor;
            
            Vector2 previousPoint = curve.GetPoint(0);
            for (int i = 1; i <= pathResolution; i++)
            {
                float t = i / (float)pathResolution;
                Vector2 currentPoint = curve.GetPoint(t);
                Handles.DrawLine(previousPoint, currentPoint, pathThickness);
                previousPoint = currentPoint;
            }
        }
#endif
    }
}