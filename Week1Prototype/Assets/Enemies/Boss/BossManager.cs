﻿using JetBrains.Annotations;
using UnityEngine;

namespace Boss
{
    public sealed class BossManager : MonoBehaviour
    {
        /* ===== Singleton ===== */

        [CanBeNull] public static BossManager Instance { get; private set; } = null;

        

        /* ===== Path ===== */
        [Header("Path")]
        
        [SerializeField] private PathNode pathBeginNode;

        private PathNode currentPathNode = null;
        private float t = 0f;
        private float tIncrement;
        
        
        
        /* ===== Boss ===== */
        [Header("Boss")]
        
        [SerializeField] private GameObject bossPrefab;
        [SerializeField] private Transform bossContainer;
        
        private BossType boss;

        private QueenScuffle queenScuffle;



        private void Awake()
        {
            Instance = this;
            queenScuffle = Instantiate(Resources.Load<QueenScuffle>("QueenScuffle"), transform);
        }

        private void OnDestroy()
            => Instance = null;
        
        private void Start()
        {
            boss = Instantiate(bossPrefab, bossContainer).GetComponent<BossType>();
            currentPathNode = pathBeginNode;
            pathBeginNode.OnVisit();

            UpdateTIncrement();
            boss.scuffleTimerTime = boss.scuffleTimer;
        }

        private void UpdateTIncrement()
            => tIncrement = (boss.speed / currentPathNode.Curve.GetApproximateLength(10)) * .02f;

        private void FixedUpdate()
        {
            boss.scuffleTimer -= Time.deltaTime;
            if (!boss.CanBossMove)
                return;
            
            // Get path node if needed
            if (currentPathNode == null)
                return;

            // Get next position
            boss.transform.position = currentPathNode.Curve.GetPoint(t);
            
            // Reset if reached the end of the current path node
            t += tIncrement;
            if (t >= 1f)
            {
                currentPathNode.OnLeave();
                currentPathNode = currentPathNode.Next;
                t = 0f;
                UpdateTIncrement();
            }
        }

        public void StartQueenScuffle([NotNull] Queen queen, [NotNull] BossType boss)
            => queenScuffle.StartScuffle(queen.transform.position, queen, boss);
    }
}