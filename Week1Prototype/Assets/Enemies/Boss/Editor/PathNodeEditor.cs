﻿using UnityEditor;
using UnityEngine;

namespace Boss
{
    [CustomEditor(typeof(PathNode))]
    public sealed class PathNodeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (Application.isPlaying)
                EditorGUILayout.HelpBox("Editing is disabled in play mode.", MessageType.Info);
            EditorGUI.BeginDisabledGroup(Application.isPlaying);
            DrawDefaultInspector();
            EditorGUI.EndDisabledGroup();
        }
    }
}
