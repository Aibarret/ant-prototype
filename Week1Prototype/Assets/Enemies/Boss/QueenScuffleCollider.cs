﻿using System;
using UnityEngine;

namespace Boss
{
    public sealed class QueenScuffleCollider : MonoBehaviour
    {
        [SerializeField] private QueenScuffle queenScuffle;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!queenScuffle.IsStarted)
                return;
            
            if (other.CompareTag("Enemy"))
                queenScuffle.EnemyInScuffleCount++;
            else if (other.CompareTag("FormationUnit"))
                queenScuffle.AllyInScuffleCount++;
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            if (!queenScuffle.IsStarted)
                return;
            
            if (other.CompareTag("Enemy"))
                queenScuffle.EnemyInScuffleCount--;
            else if (other.CompareTag("FormationUnit"))
                queenScuffle.AllyInScuffleCount--;
        }
    }
}
