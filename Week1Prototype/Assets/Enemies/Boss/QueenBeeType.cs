﻿using System.Collections;
using UnityEngine;

namespace Boss
{
    public sealed class QueenBeeType : BossType
    {
        [SerializeField] private GameObject sprite;
        //[SerializeField] private StatusValues statusValues;
        [SerializeField] private float honeyAttackRadius = 20f;
        [SerializeField] private float stringerAttackRadius = 12f;

        private bool isEnraged;
        private SpriteRenderer sr;

        private float honeyAttackCooldown;
        private  WaitForSeconds waitForHoneyAttackCooldown;

        private const float stingerAttackCooldown = 15f;
        private static readonly WaitForSeconds waitForStingerAttackCooldown = new WaitForSeconds(stingerAttackCooldown);

        private const float missedAttackCooldown = 2f;
        private static readonly WaitForSeconds waitForMissedAttackCooldown = new WaitForSeconds(missedAttackCooldown);
        
        private void Start()
        {
            GlobalVariables.ui.queenHUD.addToBossArray(this);
            isEnraged = false;
            sr = GetComponentInChildren<SpriteRenderer>();
            sr.color = new Color(1f, 1f, 1f, 1f);
            honeyAttackCooldown = 10f;
            waitForHoneyAttackCooldown = new WaitForSeconds(honeyAttackCooldown);
            
            StartCoroutine(HoneyAttackRoutine());
            StartCoroutine(StingerAttackRoutine());
        }

        private IEnumerator HoneyAttackRoutine()
        {
            // Wait for initial cooldown
            yield return waitForHoneyAttackCooldown;
            
            while (true)
            {
                // Honey attack
                bool hasAttackedSomething = false;
                foreach (FormationUnit unit in GlobalVariables.FormationUnitList)
                {
                    if (unit == null || !unit.gameObject.activeSelf)
                        continue;
                    
                    float distance = Vector2.Distance(transform.position, unit.transform.position);
                    
                    if (distance <= honeyAttackRadius)
                    {
                        unit.applyStatusEffect(new HoneyedStatus(15));
                        hasAttackedSomething = true;
                    }
                }

                // Wait for cooldown
                if (hasAttackedSomething)
                    yield return waitForHoneyAttackCooldown;
                else
                    yield return waitForMissedAttackCooldown;
            }
        }

        private IEnumerator StingerAttackRoutine()
        {
            // Wait for initial cooldown
            yield return waitForStingerAttackCooldown;
            
            while (true)
            {
                // Do nothing when the queen bee is not enraged
                if (!isEnraged)
                {
                    sr.color = new Color(1f, 1f, 1f, 1f);
                    yield return null;
                    continue;
                }
                
                // Stinger attack
                bool hasAttackedSomething = false;
                foreach (FormationUnit unit in GlobalVariables.FormationUnitList)
                {
                    if (unit == null || !unit.gameObject.activeSelf)
                        continue;
                    
                    float distance = Vector2.Distance(transform.position, unit.transform.position);

                    if (distance <= stringerAttackRadius)
                    {
                        enemy.vfxManager.ManageParticles(16, VFXManager.VFXControl.Play, gameObject);
                        unit.KillUnit();
                        hasAttackedSomething = true;
                    }
                }
                
                // Wait for cooldown
                if (hasAttackedSomething)
                    yield return waitForStingerAttackCooldown;
                else
                    yield return waitForMissedAttackCooldown;
            }
        }

        private void Update()
        {
            // Facing the player queen
            Vector3 scale = sprite.transform.localScale;
            float scaleX = Mathf.Abs(scale.x);
            scale.x = (enemy.queenObject.transform.position.x <= transform.position.x) ? 1f * scaleX : -1f * scaleX;
            sprite.transform.localScale = scale;
            
            // Enraged state check
            if (!isEnraged)
            {
                if (_health <= (_maxHealth / 2))
                {
                    // Become enraged
                    isEnraged = true;
                    sr.color = new Color(1f, .5f, .5f, 1f);
                    enemy.vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
                    _speed = (int)(_speed * 1.5f);
                    honeyAttackCooldown = 5f;
                    waitForHoneyAttackCooldown = new WaitForSeconds(honeyAttackCooldown);
                }
            }
        }
    }
}