﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Boss
{
    public sealed class RoyalTermiteType : BossType
    {
        public bool debut_die = false;

        private static bool isAnyRoyalTermiteEnraged = false;
        private static bool isAnyRoyalTermiteDied = false;

        [SerializeField] private GameObject wingGameObject;
        
        //[SerializeField] private float attackRange = 10f;
        
        [Tooltip("Note: changing this field during runtime has no effect.")]
        [SerializeField] private float attackCoolDown = 30f;

        private WaitForSeconds waitForAttackCoolDown;
        private SpriteRenderer sr;

        private bool isEnraged;
        private bool isEvenAngrier; 

        [SerializeField] private int healAmountPerUnit = 5;

        public bool IsDead { get; private set; }

        private int originalSpeed;

        public override void enemyBehavior(Enemy enemy)
        {
            
        }

        private void Start()
        {
            isAnyRoyalTermiteEnraged = false;
            isAnyRoyalTermiteDied = false;
            sr = GetComponentInChildren<SpriteRenderer>();
            sr.color = new Color(1f, 1f, 1f, 1f);

            // Make sure the wing is hidden by default
            //wingGameObject.SetActive(false);

            GlobalVariables.ui.queenHUD.addToBossArray(this);

            waitForAttackCoolDown = new WaitForSeconds(attackCoolDown);

            isEnraged = false;
            isEvenAngrier = false;
            originalSpeed = _speed;

            StartCoroutine(AttackRoutine());
        }

        private IEnumerator AttackRoutine()
        {
            List<(FormationUnit unit, Vector2 pos)> unitToAttackList = new List<(FormationUnit unit, Vector2 pos)>();
            const float attackSpeed = 50f;
            
            while (true)
            {
                CanBossMove = true;
                try {
                    enemy.vfxManager.ManageParticles(12, VFXManager.VFXControl.Play, gameObject);
                }
                catch (Exception) { }
                
                yield return waitForAttackCoolDown;

                print("Royal Termite Attacking");
                CanBossMove = false;


                Vector2 startPos = transform.position;
                
                // Add units in range to a list
                foreach (FormationUnit unit in GlobalVariables.FormationUnitList)
                {
                    if (Vector2.Distance(transform.position, unit.transform.position) < attackRange)
                        unitToAttackList.Add((unit, unit.transform.position));
                }

                // Attack all units in sequence
                foreach ((FormationUnit unit, Vector2 pos) in unitToAttackList)
                {
                    while ((Vector2)(transform.position) != pos)
                    {
                        print("moving towards unit");
                        transform.position = Vector2.MoveTowards(transform.position, pos, (attackSpeed) * Time.deltaTime);//attackSpeed * .25f

                        // Wait for the next frame
                        yield return null;
                    }

                    _health += healAmountPerUnit;
                    unit.KillUnit();
                }
                
                // Clear the attack list
                unitToAttackList.Clear();
                
                // Return to the start position
                while ((Vector2)(transform.position) != startPos)
                {
                    print("return behavior");
                    transform.position = Vector2.MoveTowards(transform.position, startPos, attackSpeed * Time.deltaTime);
                    
                    // Wait for the next frame
                    yield return null;
                }
            }
        }

        private void Update()
        {
            if (debut_die)
            {
                debut_die = false;
                Die();
            }
            // Rage check
            if (!isAnyRoyalTermiteEnraged && !isEnraged && (_health <= (_maxHealth / 2) || isAnyRoyalTermiteDied))
            {
                // Disable the enraging ability for the other royal termite
                isAnyRoyalTermiteEnraged = true;
                
                // Update fields after being enraged
                isEnraged = true;
                sr.color = new Color(1f, .5f, .5f, 1f);
                enemy.vfxManager.ManageParticles(0, VFXManager.VFXControl.Play, gameObject);
                attackCoolDown /= 2f;
                waitForAttackCoolDown = new WaitForSeconds(attackCoolDown);
                _speed = (int)(originalSpeed * 1.5f);
                if (isAnyRoyalTermiteDied)
                {
                    _speed = originalSpeed * 2;
                    isEvenAngrier = true;
                }
                healAmountPerUnit = (int)(healAmountPerUnit * 1.5f);
                
                // Show the wing game object
                //wingGameObject.SetActive(true);
            }

            if (!isEvenAngrier)
            {
                if (isAnyRoyalTermiteDied)
                {
                    _speed = originalSpeed * 2;
                    isEvenAngrier = true;
                }
            }
        }

        protected override void Die()
        {
            if (isAnyRoyalTermiteDied)
            {
                base.Die();
                return;
            }
            gameObject.SetActive(false);

            IsDead = true;
            isAnyRoyalTermiteDied = true;
        }
    }
}
