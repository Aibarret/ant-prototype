﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Boss
{
    public sealed class WaspQueenType : BossType
    {
        [SerializeField] private DropshadowController dropshadowController;
        [SerializeField] private GameObject paperBall;
        [SerializeField] private GameObject head;
        
        private bool isEnraged = false;
        private bool isEvenMoreEnraged = false;

        private int originalSpeed;

        private QueenManager qm;

        private const float hoveringDistance = 15f;

        private int groundPoundUnitCount = 20;
        private float groundPoundRange = 10f;

        private int stingerUnitCount = 10;
        //private float stingerRange = 5f;
        private float stingerCooldown = 20f;

        private bool isHovering = false;

        [SerializeField] private float coneRadius = 10f;
        [SerializeField] private float coneAngle = 45f;

        private float currentConeRadius;
        
        private Vector2 coneDirection;

        private float spitCooldown = 5f;

        private void Start()
        {
            qm = QueenManager.Instance;
            
            originalSpeed = _speed;

            StartCoroutine(GroundPoundRoutine());
            StartCoroutine(StingerRoutine());
            StartCoroutine(SpitRoutine());
        }

        private IEnumerator GroundPoundRoutine()
        {
            int layerMask = 1 << LayerMask.NameToLayer("Ground");

            Collider2D[] results = new Collider2D[groundPoundUnitCount];
            
            while (true)
            {
                int count = Physics2D.OverlapCircleNonAlloc(transform.position, groundPoundRange, results, layerMask);
                if (count >= groundPoundUnitCount)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (results[i].TryGetComponent(out FormationUnit unit))
                            unit.KillUnit();
                    }
                    enemy.vfxManager.ManageParticles(20, VFXManager.VFXControl.Play, gameObject);
                    enemy.vfxManager.ManageParticles(21, VFXManager.VFXControl.Play, gameObject);
                }
                
                yield return null;
            }
        }

        private IEnumerator StingerRoutine()
        {
            int layerMask = (1 << LayerMask.NameToLayer("Ground")) | (1 << LayerMask.NameToLayer("Flying"));

            Collider2D[] results = new Collider2D[stingerUnitCount];
            
            while (true)
            {
                int count = Physics2D.OverlapCircleNonAlloc(transform.position, groundPoundRange, results, layerMask);
                if (count >= stingerUnitCount)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (results[i].TryGetComponent(out FormationUnit unit))
                            unit.KillUnit();
                    }
                    
                    yield return new WaitForSeconds(stingerCooldown);
                }
                
                yield return new WaitForSeconds(3f); // Retry after 3 seconds if missed
            }
        }

        private IEnumerator SpitRoutine()
        {
            int layerMask = (1 << LayerMask.NameToLayer("Ground")) | (1 << LayerMask.NameToLayer("Flying"));
            
            Collider2D[] results = new Collider2D[20];
            
            while (true)
            {
                Vector2 origin = transform.position;
                coneDirection = -transform.right; // TODO: Change to the direction of the boss sprite
                Collider2D target = null;
                
                int count = Physics2D.OverlapCircleNonAlloc(origin, currentConeRadius, results, layerMask);
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        Collider2D collider = results[i];
                        Vector2 toCollider = (Vector2)(collider.transform.position) - origin;
                        float angleToCollider = Vector2.Angle(coneDirection, toCollider);

                        if (angleToCollider <= coneAngle / 2f)
                        {
                            target = collider;
                            break;
                        }
                    }

                    if (target != null)
                    {
                        if (target.TryGetComponent(out FormationUnit unit))
                        {
                            GameObject paper = Instantiate(paperBall, head.transform.position, Quaternion.identity);
                            paper.GetComponent<Projectile>().SetDirection(unit.transform.position);

                            yield return new WaitForSeconds(spitCooldown);
                        }
                    }
                }
                
                yield return null;
            }
        }

        private void Update()
        {
            // Check enrage and even more enrage
            if (!isEnraged)
            {
                float percentage = _health / (float)_maxHealth;

                if (percentage <= 0.66f)
                {
                    isEnraged = true;
                    
                    _speed = (int)(originalSpeed * 1.5f);
                    groundPoundUnitCount = 15;
                    groundPoundRange = 15f;
                    stingerCooldown = 15f;
                    stingerUnitCount = 8;
                    spitCooldown = 3f;
                }
            }
            else if (!isEvenMoreEnraged)
            {
                float percentage = _health / (float)_maxHealth;

                if (percentage <= 0.33f)
                {
                    isEvenMoreEnraged = true;
                    
                    _speed = originalSpeed * 2;
                    groundPoundUnitCount = 10;
                    groundPoundRange = 20f;
                    spitCooldown = 1f;
                }
            }
            
            // Get down if near a player queen
            bool shouldHover = false;
            foreach (GameObject q in qm.queenList)
            {
                if (Vector2.Distance(transform.position, q.transform.position) <= hoveringDistance)
                {
                    shouldHover = true;
                    break;
                }
            }
            if (shouldHover != isHovering)
            {
                isHovering = shouldHover;
                dropshadowController.changeState(shouldHover ? FlyingState.HOVERING : FlyingState.FLYING);
                currentConeRadius = shouldHover ? (coneRadius / 2f) : coneRadius;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Vector2 origin = transform.position;
            
            float halfConeAngle = coneAngle / 2;
            Quaternion leftRotation = Quaternion.AngleAxis(-halfConeAngle, Vector3.forward);
            Quaternion rightRotation = Quaternion.AngleAxis(halfConeAngle, Vector3.forward);

            Vector2 leftDirection = leftRotation * coneDirection.normalized;
            Vector2 rightDirection = rightRotation * coneDirection.normalized;
            
            Gizmos.DrawLine(origin, origin + leftDirection * currentConeRadius);
            Gizmos.DrawLine(origin, origin + rightDirection * currentConeRadius);
            
            int segments = 10;
            float segmentAngle = coneAngle / segments;
            for (int i = 0; i <= segments; i++)
            {
                Quaternion segmentRotation = Quaternion.AngleAxis(-halfConeAngle + segmentAngle * i, Vector3.forward);
                Vector2 segmentDirection = segmentRotation * coneDirection.normalized;
                Vector2 previousPoint = origin + (Vector2)(i == 0 ? leftDirection : segmentRotation * Quaternion.AngleAxis(-segmentAngle, Vector3.forward) * coneDirection.normalized) * currentConeRadius;
                Vector2 nextPoint = origin + segmentDirection * currentConeRadius;
                Gizmos.DrawLine(previousPoint, nextPoint);
            }
        }
#endif
    }
}
