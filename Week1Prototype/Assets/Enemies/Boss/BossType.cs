using System;
using UnityEngine;

namespace Boss
{
    public class BossType : EnemyType
    {
        [SerializeField] protected Enemy _enemy;
        public new Enemy enemy { get => _enemy; set => _enemy = value; }
        
        [SerializeField] protected int _health = 1000;
        public new int health { get => _health; set => _health = value; }
        
        [SerializeField] protected int _maxHealth = 1000;
        public new int maxHealth { get => _maxHealth; set => _maxHealth = value; }

        [SerializeField] protected int _speed = 6;
        public new int speed { get => _speed; set => _speed = value; }

        public float scuffleTimer = 10f;
        public float scuffleTimerTime;

        public QueenTypeIcons icon;
        public string bossDisplayName;

        public override string prefabName { get => throw new NotImplementedException("Boss types has no field 'prefabName'."); }

        public bool CanBossMove { get; set; } = true;

        private void Start()
        {
            GlobalVariables.ui.queenHUD.addToBossArray(this);
        }

        public override void enemyBehavior(Enemy enemy) { }

        public override bool takeDamage(int damage, bool isPush = false, float? direction = null)
        {
            if (health > 0)
            {
                health -= damage;
                if (health <= 0)
                {
                    Die();
                    return true;
                }
                
                if (health > maxHealth)
                    health = maxHealth;

                GlobalVariables.ui.queenHUD.updateBossHealth(this);
            }
            
            return false;
        }

        protected virtual void Die()
        {
            // Boss defeat logic
            print("Boss Died");
            gameObject.SetActive(false);
            print("calling win con reached");
            for (int i = 0; i < QueenManager.Instance.queenWinCons.Count; i++)
            {
                QueenManager.Instance.queenWinCons[i] = true;
            }
            GlobalVariables.eventCall(WaypointEvent.queenWinConReached, -1, 0);
            //enemy.queenObject.GetComponent<Queen>().die(true);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Queen"))
                return;
            if (other.gameObject != QueenManager.Instance.queen)
            {
                return;
            }
            // Queen scuffle
            Queen queen = other.GetComponent<Queen>();
            //if((scuffleTimer <= 0) && (queen.queenManager.activeQueenIndex == queen.queenIndex))
            if(scuffleTimer <= 0)
            {
                BossManager.Instance!.StartQueenScuffle(queen, this);
            }
            else
            {
                print("OH GOD OH FUCK OH MY GOOOODDDDDDDDDDDDDDDDDDD");
            }
        }
    }
}
