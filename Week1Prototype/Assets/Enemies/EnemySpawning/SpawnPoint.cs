using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public enum SpawnType
    {
        CallInCode,
        Proximity,
        CallOnWaypoint,
        CallOnStart
    }

    public enum SpawnFrequency
    {
        OneOff,
        ContinuousTimer
    }

    [HideInInspector] public GameObject enemyContainer;
    [Header("Debug")]
    public bool printWaypointAtCheck = false;
    public bool DEBUG_TurnOffSpawning = false;
    public bool activelySpawningBundles;

    public SpawnType spawningMethod;
    public int assignedQueenIndex;
    [Header("Proximity Spawning")]
    public string tagOfProximityObject;
    public bool stopAtExitProximity;
    [Header("Waypoint Spawning")]
    public bool findWaypointRefs;
    public bool writeToIndex;
    public Vector2 spawnAtWaypoint;
    public QueenPoint spawnWaypointReference;
    public Vector2 stopAtWaypoint;
    public QueenPoint stopWaypointReference;
    [Header("Frequency Settings")]
    public SpawnFrequency spawningFrequency;
    public float timerSeconds;
    public bool waitOneSec;
    [Header("Spawn Modifiers")]
    public bool armorEnemies = false;
    public bool shieldEnemies = false;
    [Header("Pool Settings")]
    public List<PrefabPattern> spawnableBundles = new List<PrefabPattern>();
    public bool spawnAllBundles = false;
    public int poolAmount = 0;
    [Header("Object Refs")]
    public Timer timer;
    public Collider2D collision;
    public GameObject deadzoneObject;

    private ChildCollision deadzone;
    private bool neverEntered = true;

    private Dictionary<string, List<GameObject>> activeEnemyPools = new Dictionary<string, List<GameObject>>();
    private Dictionary<string, List<GameObject>> inactiveEnemyPools = new Dictionary<string, List<GameObject>>();

    private void Awake()
    {
        //DEBUG_TurnOffSpawning = GlobalVariables.DEBUG_turnOffSpawning;
        deadzone = deadzoneObject.GetComponent<ChildCollision>();
    }

    private void Start()
    {
        setUpEnemyPools();

        if (!DEBUG_TurnOffSpawning)
        {
            switch (spawningMethod)
            {
                case SpawnType.CallOnStart:
                    spawnBundle();
                    break;
                case SpawnType.CallOnWaypoint:
                    GlobalVariables.addToCallList(eventHandlerSpawnPoint);
                    DestroyImmediate(collision, true);
                    break;
                case SpawnType.Proximity:
                    deadzone.trigEnt += onDeadzoneTriggerEnter;
                    deadzone.trigExt += onDeadzoneTriggerExit;
                    break;
            }
        }
    }

    private void Update()
    {
        //print(activelySpawningBundles + "         " + gameObject.name);
        if (activelySpawningBundles && !DEBUG_TurnOffSpawning)
        {
            switch (spawningFrequency)
            {
                case SpawnFrequency.OneOff:
                    toggleSpawning(false);
                    if(neverEntered == true)
                    {
                        spawnBundle();
                    }
                    break;
                case SpawnFrequency.ContinuousTimer:

                    if (!timer.isTiming())
                    {
                        timer.startTimer(timerSeconds, timeOut);
                    }

                    break;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (spawningMethod == SpawnType.Proximity)
        {
            if ((collision.tag == tagOfProximityObject) && (QueenManager.Instance.activeQueenIndex == assignedQueenIndex) && (DEBUG_TurnOffSpawning != true))
            {
                toggleSpawning(true);
                if (neverEntered == true)
                {
                    spawnBundle();
                    timer.startTimer(timerSeconds, timeOut, true);
                    neverEntered = false;
                }
                else
                {
                    timer.pauseTimer();
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (spawningMethod == SpawnType.Proximity && stopAtExitProximity && collision.tag.Equals(tagOfProximityObject) && QueenManager.Instance.activeQueenIndex == assignedQueenIndex)
        {
            toggleSpawning(false);
            timer.pauseTimer();
        }
    }

    public void onDeadzoneTriggerEnter(Collider2D collision)
    {
        if (spawningFrequency == SpawnFrequency.ContinuousTimer && collision.tag.Equals(tagOfProximityObject))
        {
            DEBUG_TurnOffSpawning = true;
            timer.stopTimer();
        }
    }

    public void onDeadzoneTriggerExit(Collider2D collision)
    {
        if (spawningFrequency == SpawnFrequency.ContinuousTimer)
        {
            DEBUG_TurnOffSpawning = false;
            timer.startTimer(timerSeconds, timeOut, true);
        }
    }

    public void spawnBundle()
    {
        //No longer just instantiates, will now draw from a pool.
        int bundleIndex;
        
        if (spawnAllBundles)
        {
            bundleIndex = 0;
        }
        else
        {
            bundleIndex = (int)Random.Range(0, spawnableBundles.Count - 1);
        }

        while (bundleIndex < spawnableBundles.Count)
        {
            for (int i = 0; i < spawnableBundles[bundleIndex].posns.Count; i++)
            {
                GameObject newEnemyObject = getInactiveEnemyFromPool(spawnableBundles[bundleIndex].prefabNames[i]);
                
                if (newEnemyObject != null)
                {
                    newEnemyObject.transform.position = transform.position + spawnableBundles[bundleIndex].posns[i];
                    newEnemyObject.SetActive(true);
                    if(armorEnemies == true)
                    {
                        newEnemyObject.GetComponent<EnemyType>().hasArmor = true;
                    }
                    if(shieldEnemies == true)
                    {
                        newEnemyObject.GetComponent<EnemyType>().hasShield = true;
                    }
                    newEnemyObject.GetComponent<Enemy>().onSpawnEnemy(assignedQueenIndex);
                }
                else
                {
                    GameObject prefab = Resources.Load(spawnableBundles[bundleIndex].prefabNames[i], typeof(GameObject)) as GameObject;
                    prefab.GetComponent<Enemy>().assignedSpawnpoint = this;
                    GameObject newEnemy = Instantiate(prefab, transform.position + spawnableBundles[bundleIndex].posns[i], Quaternion.identity);
                    if (armorEnemies == true)
                    {
                        newEnemyObject.GetComponent<EnemyType>().hasArmor = true;
                    }
                    if (shieldEnemies == true)
                    {
                        newEnemyObject.GetComponent<EnemyType>().hasShield = true;
                    }
                    newEnemy.GetComponent<Enemy>().onSpawnEnemy(assignedQueenIndex);
                    if (enemyContainer != null)
                    {
                        newEnemy.transform.parent = enemyContainer.transform;
                    }
                    
                    addToActivePool(newEnemy.GetComponent<Enemy>());
                }

            }

            if (spawnAllBundles)
            {
                bundleIndex++;
            }
            else
            {
                break;
            }
        }
       
    }

    private void setUpEnemyPools()
    {
        Dictionary<string, int> prefabCounts = new Dictionary<string, int>();

        foreach (PrefabPattern bundle in spawnableBundles)
        {
            for (int i = 0; i < bundle.prefabNames.Count; i++)
            {
                if (inactiveEnemyPools.ContainsKey(bundle.prefabNames[i]))
                {
                    if (prefabCounts.ContainsKey(bundle.prefabNames[i]))
                    {
                        prefabCounts[bundle.prefabNames[i]] += 1;
                    }
                    else
                    {
                        prefabCounts.Add(bundle.prefabNames[i], 1);
                    }
                }
                else
                {
                    inactiveEnemyPools.Add(bundle.prefabNames[i], new List<GameObject>());

                    if (prefabCounts.ContainsKey(bundle.prefabNames[i]))
                    {
                        prefabCounts[bundle.prefabNames[i]] += 1;
                    }
                    else
                    {
                        prefabCounts.Add(bundle.prefabNames[i], 1);
                    }
                }
            }
        }


       
        foreach (var prefabDictEntry in prefabCounts)
        {
            int pool = prefabDictEntry.Value * 5;

            if (poolAmount > 0)
            {
                pool = poolAmount * prefabDictEntry.Value;
            }

            for (int i = 0; i < pool; i++)
            {
                GameObject prefab = Resources.Load(prefabDictEntry.Key, typeof(GameObject)) as GameObject;
                prefab.SetActive(false);
                prefab.GetComponent<Enemy>().assignedSpawnpoint = this;
                GameObject newEnemy = Instantiate(prefab, new Vector3(), Quaternion.identity);
                if (enemyContainer != null)
                {
                    newEnemy.transform.parent = enemyContainer.transform;
                }
                addToInactivePool(newEnemy.GetComponent<Enemy>());

            }
        }
    }

    public void addToInactivePool(Enemy enemy)
    {
        if (inactiveEnemyPools.ContainsKey(enemy.type.prefabName))
        {
            List<GameObject> inactivePools = inactiveEnemyPools[enemy.type.prefabName];
            
            if (!inactivePools.Contains(enemy.gameObject))
            {
                if (checkIfInPool(1, enemy))
                {
                    activeEnemyPools[enemy.type.prefabName].Remove(enemy.gameObject);
                }

                enemy.gameObject.SetActive(false);
                inactivePools.Add(enemy.gameObject);
            }
            else
            {
                print("HEY, THE INACTIVE ENEMY POOL JUST TRIED TO ADD AN ENEMY TO A POOL IT'S ALREADY ON");
            }
        }
        else
        {
            if (checkIfInPool(1, enemy))
            {
                activeEnemyPools[enemy.type.prefabName].Remove(enemy.gameObject);
            }

            inactiveEnemyPools.Add(enemy.type.prefabName, new List<GameObject>());
            enemy.gameObject.SetActive(false);
            inactiveEnemyPools[enemy.type.prefabName].Add(enemy.gameObject);
        }
    }


    public void addToActivePool(Enemy enemy)
    {
        if (activeEnemyPools.ContainsKey(enemy.type.prefabName))
        {
            List<GameObject> activePools = activeEnemyPools[enemy.type.prefabName];

            if (!activePools.Contains(enemy.gameObject))
            {
                if (checkIfInPool(0, enemy))
                {
                    inactiveEnemyPools[enemy.type.prefabName].Remove(enemy.gameObject);
                }

                activePools.Add(enemy.gameObject);
            }
            else
            {
                print("HEY, THE ACTIVE ENEMY POOL JUST TRIED TO ADD AN ENEMY TO A POOL IT'S ALREADY ON");
            }
        }
        else
        {
            if (checkIfInPool(0, enemy))
            {
                inactiveEnemyPools[enemy.type.prefabName].Remove(enemy.gameObject);
            }

            activeEnemyPools.Add(enemy.type.prefabName, new List<GameObject>());
            activeEnemyPools[enemy.type.prefabName].Add(enemy.gameObject);
        }
    }


    public GameObject getInactiveEnemyFromPool(string prefabName)
    {
        if (inactiveEnemyPools.ContainsKey(prefabName))
        {
            foreach(GameObject enemy in inactiveEnemyPools[prefabName])
            {
                if (!enemy.activeInHierarchy)
                {
                    GameObject foundEnemy = enemy;
                    addToActivePool(foundEnemy.GetComponent<Enemy>());
                    //inactiveEnemyPools[prefabName].Remove(enemy);
                    return foundEnemy;
                }
                

                
            }

            return null;
        }
        else
        {
            print("HEY, THE POOL JUST TRIED TO GET AN ENEMY FROM A POOL THAT DOESN'T EXIST");
            return null;
        }
    }


    /// <summary>
    /// Pool: 0 = inactiveEnemyPool and 1 = activeEnemyPool
    /// </summary>
    /// <param name="pool"></param>
    /// <param name="enemy"></param>
    /// <returns></returns>
    private bool checkIfInPool(int pool, Enemy enemy)
    {
        if (pool == 0)
        {
            if (inactiveEnemyPools.ContainsKey(enemy.type.prefabName))
            {
                if (inactiveEnemyPools[enemy.type.prefabName].Contains(enemy.gameObject))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else if (pool == 1)
        {
            if (activeEnemyPools.ContainsKey(enemy.type.prefabName))
            {
                if (activeEnemyPools[enemy.type.prefabName].Contains(enemy.gameObject))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            print("checkIfInPool attempted to check for an invalid pool");
            return false;
        }
    }

    public void timeOut()
    {
        if (activelySpawningBundles && QueenManager.Instance.activeQueenIndex == assignedQueenIndex)
        {
            spawnBundle();
        }
    }

    public void toggleSpawning(bool isSpawning)
    {
        activelySpawningBundles = isSpawning;
    }

    public void eventHandlerSpawnPoint(WaypointEvent wEvent, int index, int miscIndex)
    {
        int startPathIndex = 0;
        int startWaypointIndex = 0;
        int stopPathIndex = 0;
        int stopWaypointIndex = 0;

        if (spawnWaypointReference)
        {
            startPathIndex = spawnWaypointReference.pathNumber;
            startWaypointIndex = spawnWaypointReference.indexNumber;
        }
        else
        {
            startPathIndex = (int)spawnAtWaypoint.x;
            startWaypointIndex = (int)spawnAtWaypoint.y;
        }

        if (stopWaypointReference)
        {
            stopPathIndex = stopWaypointReference.pathNumber;
            stopWaypointIndex = stopWaypointReference.indexNumber;
        }
        else
        {
            stopPathIndex = (int)stopAtWaypoint.x;
            stopWaypointIndex = (int)stopAtWaypoint.y;
        }

        if (printWaypointAtCheck && wEvent == WaypointEvent.queenAtXPosition)
        {
            print("Comparing Assigned Waypoint " + startPathIndex + " - " + startWaypointIndex + ": Queen " + assignedQueenIndex + " to " + miscIndex + " - " + index + ": Queen " + QueenManager.Instance.activeQueenIndex);
        }
        
        if (wEvent == WaypointEvent.queenAtXPosition && index == startWaypointIndex && miscIndex == startPathIndex && QueenManager.Instance.activeQueenIndex == assignedQueenIndex)
        {
            if (printWaypointAtCheck)
            {
                print("Spawning");
                //print("Comparing Assigned Waypoint " + startPathIndex + " - " + startWaypointIndex + ": Queen " + assignedQueenIndex + " to " + miscIndex + " - " + index + ": Queen " + QueenManager.Instance.activeQueenIndex);
            }
            toggleSpawning(true);

            if (spawningFrequency == SpawnFrequency.ContinuousTimer)
            {
                if(waitOneSec == true)
                {
                    float x = 0;
                    while(x < 1)
                    {
                        x += Time.deltaTime;
                    }
                }
                spawnBundle();
                timer.startTimer(timerSeconds, timeOut, true);
            }
            
        }

        if (wEvent == WaypointEvent.queenAtXPosition && index == stopWaypointIndex && miscIndex == stopPathIndex && QueenManager.Instance.activeQueenIndex == assignedQueenIndex)
        {
            toggleSpawning(false);
            if (spawningFrequency == SpawnFrequency.ContinuousTimer)
            {
                timer.stopTimer();
            }
        }

        if (wEvent == WaypointEvent.queenAtXPosition && index > stopWaypointIndex && miscIndex == stopPathIndex && QueenManager.Instance.activeQueenIndex == assignedQueenIndex)
        {
            toggleSpawning(false);
            if (spawningFrequency == SpawnFrequency.ContinuousTimer)
            {
                timer.stopTimer();
            }
        }

        if (wEvent == WaypointEvent.queenDied)
        {
            DEBUG_TurnOffSpawning = true;
        }
    }

    private void OnDrawGizmos()
    {
        if (findWaypointRefs)
        {
            findWaypointRefs = false;

            QueenManager qManager = GameObject.Find("QueenManager").GetComponent<QueenManager>();
            QueenWaypoints relevantWaypointContainer = qManager.queenList[assignedQueenIndex].GetComponent<Queen>().queenPointContainer.GetComponent<QueenWaypoints>();

            spawnWaypointReference = relevantWaypointContainer.queenPaths[(int)spawnAtWaypoint.x].GetChild((int)spawnAtWaypoint.y).GetComponent<QueenPoint>();
            stopWaypointReference = relevantWaypointContainer.queenPaths[(int) stopAtWaypoint.x].GetChild((int) stopAtWaypoint.y).GetComponent<QueenPoint>();

            Debug.ClearDeveloperConsole();
        }

        if (writeToIndex)
        {
            writeToIndex = false;

            QueenManager qManager = GameObject.Find("QueenManager").GetComponent<QueenManager>();
            QueenWaypoints relevantWaypointContainer = qManager.queenList[assignedQueenIndex].GetComponent<Queen>().queenPointContainer.GetComponent<QueenWaypoints>();

            for (int pathindex = 0; pathindex < relevantWaypointContainer.queenPaths.Count; pathindex ++)
            {
                for (int waypointIndex = 0; waypointIndex < relevantWaypointContainer.queenPaths[pathindex].childCount; waypointIndex++)
                {
                    GameObject checkingPoint = relevantWaypointContainer.queenPaths[pathindex].GetChild(waypointIndex).gameObject;
                    if (checkingPoint == spawnWaypointReference.gameObject)
                    {
                        spawnAtWaypoint = new Vector2(pathindex, waypointIndex);
                    }

                    if (checkingPoint == stopWaypointReference.gameObject)
                    {
                        stopAtWaypoint = new Vector2(pathindex, waypointIndex);
                    }
                }
            }
        }
    }

}
