using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PrefabPattern", menuName = "ScriptableObjects/EnemyPatterns", order = 1)]
public class PrefabPattern : ScriptableObject
{
    public List<string> prefabNames;
    public List<Vector3> posns;

    public void clear()
    {
        prefabNames = new List<string>();
        posns = new List<Vector3>();
    }
}
