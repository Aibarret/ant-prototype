using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EnemyPatternMaker : MonoBehaviour
{
    public PrefabPattern prefabPatter;
    public bool writeToFile = false;
    public bool clearAll;

    private void OnDrawGizmosSelected()
    {
        if (writeToFile)
        {
            writeToFile = false;
            prefabPatter.clear();
            for (int i = 0; i < transform.childCount; i++)
            {
                print(i + transform.GetChild(i).name);
                
                Enemy enemy = transform.GetChild(i).GetComponent<Enemy>();
                prefabPatter.prefabNames.Add(enemy.type.prefabName);
                prefabPatter.posns.Add(enemy.transform.position);
            }

            prefabPatter.SetDirty();

            print("Done!");
        }

        if (clearAll)
        {
            clearAll = false;

            print(transform.childCount);


            while (transform.childCount > 0)
            {
                GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
            }

            prefabPatter = null;
        }

    }

}
