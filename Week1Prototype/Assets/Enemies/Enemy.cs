using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using FlowFieldNavigation;
using System.Linq;
using FMODUnity;
using UnityEngine.Rendering;

public static class NavMeshAgentType
{
    public const int WALKING_ID = 0;
    public const int HOVERING_ID = -137265422;
    public const int FLYING_ID = -334000983;
}


public class Enemy : MonoBehaviour, Attackable, Moveable, DropShadowCastable, StatusEffectable
{
    public bool drawNavPath = false;

    /* ===== References ===== */
    [Header("Object and Component Refs")]
    public EnemyType type;
    public SpriteRenderer sprite;
    public Collider2D Hitbox;
    public Collider2D movementCollider;
    public Rigidbody2D rb;
    public NavMeshAgent agent;
    public NavigationAgent flowFieldAgent;
    public Animator animator;
    public GameObject hpbar;
    public GameObject DropShadow;
    public VFXManager vfxManager;
    public Timer attackTimer;

    /* ===== Function Variables ===== */
    [SerializeField] private bool isBoss = false;
    public bool IsBoss => isBoss;

    public bool CanTakeDamage { get; set; } = true;

    // Script Variables
    [HideInInspector] public SpawnPoint assignedSpawnpoint;
    [HideInInspector] public GameObject queenObject { get => QueenManager.Instance.getQueenAtIndex(assignedQueenIndex).gameObject; }

    [HideInInspector] public int assignedQueenIndex;
    private MovingHealthBar healthBar;
    private DropshadowController dropshadowController;
    private StatusManager statusEffectManager;
    private bool isDead = false;

    // Push and Pull Variables
    [HideInInspector] public float pushDistance;
    private float pushSpeed;
    private GameObject carryObject;
    private Vector2 pushDirection;
    private MoveMode moveMode = MoveMode.normal;
    private bool pushedThisFrame = false;


    private bool usingFlowField = true;
    private Vector3 navDestination;
    private Vector3 lastPosn;

    public event Action<Enemy> EventEnemyKilled;

    public int health => type.health;
    public int maxHealth => type.maxHealth;

    public int normalMaxHealth;

    private bool armorAddWeight = true;
    public bool armorRemoved = false;
    private bool shieldAddWeight = true;
    public bool shieldRemoved = false;


    private IncreaseScore scoreIncrease;

    private void Awake()
    {
        type.health = type.maxHealth;
        healthBar = hpbar.GetComponent<MovingHealthBar>();
        scoreIncrease = gameObject.GetComponent<IncreaseScore>();
        dropshadowController = DropShadow.GetComponent<DropshadowController>();

        

        float[] stats = new float[7];
        stats[0] = type.maxHealth;
        stats[1] = type.damage;
        stats[2] = type.attackCooldown;
        stats[3] = type.attackRange;
        stats[4] = type.speed;
        stats[5] = type.weight;
        stats[6] = type.knockback;
        statusEffectManager = new EnemyStatusManager(this, stats);
    }

    private void Start()
    {
        vfxManager = VFXManager.Instance;

        if (vfxManager == null)
        {
            Debug.Log("There is no VFXManager Instance");
        }

        if (!isBoss)
        {
            if (agent != null)
            {
                agent.updateRotation = false;
                agent.updateUpAxis = false;   
            }
            changeSpeed(type.speed);
        }

        toggleFlowField(flowFieldAgent != null);

        //queenObject = QueenManager.Instance.queen.gameObject;
        type.onSpawn();
    }

    private void Update()
    {
        /*
        if (!isBoss)
        {
            takeDamage(500);
        }
        */
        //agent.destination;
        /*if (Input.GetKeyDown("k"))
        {
            applyStatusEffect(new TestEffect(3));
        }*/
    }

    private void FixedUpdate()
    {
        type.navTimer -= Time.deltaTime;

        if (isDead)
        {
            return;
        }

        if (isBoss)
            goto afterMoveMode;
        
        if (transform.position.x > lastPosn.x)
        {
            setSpriteDirection(true);
        }
        else if (transform.position.x < lastPosn.x)
        {
            setSpriteDirection(false);
        }
        
        switch (moveMode)
        {
            case MoveMode.normal:
                if (movementCollider.enabled)
                {
                    movementCollider.enabled = false;

                    if (agent != null)
                        agent.enabled = !usingFlowField;
                    if (flowFieldAgent != null)
                        flowFieldAgent.enabled = usingFlowField;
                }
                type.enemyBehavior(this);
                break;
            case MoveMode.push:
                if (!movementCollider.enabled)
                {
                    movementCollider.enabled = true;
                    if (agent != null)
                        agent.enabled = false;
                    if (flowFieldAgent != null)
                        flowFieldAgent.enabled = false;
                }
                
                pushSpeed = type.pushBehavior(pushDirection, pushSpeed);
                break;
            case MoveMode.pull:
                if (movementCollider.enabled || (agent != null && agent.enabled))
                {
                    movementCollider.enabled = false;
                    if (agent != null)
                        agent.enabled = false;
                }
                if (lastPosn == transform.position)
                {
                    moveMode = MoveMode.normal;
                }
                break;
            case MoveMode.carry:
                if (movementCollider.enabled || (agent != null && agent.enabled))
                {
                    movementCollider.enabled = false;
                    if (agent != null)
                        agent.enabled = false;
                }
                type.carryBehavior(carryObject.transform, pushDirection);
                break;
        }

    afterMoveMode:
        lastPosn.x = transform.position.x;
    }

    private void LateUpdate()
    {
        statusEffectManager.tick();
        pushedThisFrame = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((moveMode == MoveMode.push && !pushedThisFrame) && collision.tag.Equals("Enemy"))
        {
            Moveable moveObject = collision.GetComponent<Moveable>();
            if (moveObject != null)
            {
                //print("Collided and pushing at speed " + (int)(pushSpeed * .5f));
                moveObject.push(pushDirection, (int)(pushSpeed * .5f));
                pushDirection = Vector3.Reflect(pushDirection, collision.ClosestPoint(transform.position).normalized);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //print("Colliding with " + collision.gameObject.name);
        if (moveMode == MoveMode.push && !pushedThisFrame)
        {
            
            pushDirection = Vector3.Reflect(pushDirection, collision.contacts[0].normal);
        }
    }

    public void onSpawnEnemy(int assignedQueenIndex)
    {
        this.assignedQueenIndex = assignedQueenIndex;

        if (type.hasShield)
        {
            addShield();
        }
        if (type.hasArmor)
        {
            addArmor();
        }
        // TODO: null ref error
        healthBar.updateBar(type.health, type.maxHealth);
        type.onSpawn();
    }

    public void setNavDestination(Vector3 posn)
    {
        if (isBoss)
            return;

        navDestination = posn;

        if (agent == null && flowFieldAgent == null)
        {
            useDirect();
        }

        if (agent != null)
        {
            if (agent.enabled)
            {
                useNav();
            }
        }
        else
        {
            if (!flowFieldAgent.enabled)
            {
                useDirect();
            }
        }

        void useNav()
        {
            if (agent.destination != posn)
            {
                agent.SetDestination(navDestination);
            }
        }

        void useDirect()
        {
            Vector3 dir = (navDestination - transform.position).normalized;
            transform.position += (dir * type.speed) * Time.deltaTime;
        }
    }

    public void toggleFlowField(bool isActive, bool isTurnOnAgent = true)
    {
        if (flowFieldAgent)
        {
            usingFlowField = isActive;
            flowFieldAgent.enabled = isActive;
        }
        else
        {
            usingFlowField = false;
        }

        if (agent && isTurnOnAgent && !isActive)
        {
            agent.enabled = !isActive;

            if (agent.enabled)
            {
                agent.SetDestination(navDestination);
            }
        }
    }

    public void setSpriteDirection(bool isRight)
    {
        sprite.flipX = isRight;
        dropshadowController.updateDummySprite();
        type.changeDirection(isRight);
    }

    // STATUS EFFECT SYSTEM ===========================================================================================================================

    public void applyStatusEffect(StatusEffect effect)
    {
        statusEffectManager.applyStatusEffect(effect);
    }

    public void removeStatusEffect(StatusEffect effect)
    {
        statusEffectManager.removeStatusEffect(effect);
    }

    public void removeStatusEffect(string effectName)
    {
        statusEffectManager.removeStatusEffect(effectName);
    }

    public bool hasStatusEffectReference(StatusEffect effect)
    {
        return statusEffectManager.hasStatusEffect(effect);
    }

    public bool hasStatusEffectOfType(string typeName)
    {
        return statusEffectManager.hasSatusEffectOfType(typeName);
    }

    public void changeSpeed(float speed)
    {
        type.speed = speed;
        if (agent != null)
            agent.speed = speed;
        if (animator)
        {
            animator.SetFloat("Speed", speed * .25f);

        }
    }


    // HEALTH AND DEATH SYSTEM ===================================================================================================================
    
    public bool takeDamage(int damage, float? direction = null)
    {
        if (!CanTakeDamage)
            return false;

        bool isDead = type.takeDamage(damage, direction: direction);
        updateUI();
        return isDead;
    }
    public void updateUI()
    {
        healthBar.updateBar(type.health, type.maxHealth);
    }

    public void dieAndRespawn()
    {
        //TODO: Implement a reset function in enemy type, for now I'm just manually resetting health values.
        if (isDead == false)
        {
            isDead = true;
            if (flowFieldAgent)
            {
                flowFieldAgent.enabled = false;

            }
        }
        else
        {
            //StartCoroutine(waitForHealthBarDrop(true));
            return;
        }

        armorRemoved = false;
        shieldRemoved = false;
        if (hpbar != null)
        {
            // TODO: coroutine error. seems to be caused by attacking dead enemies?
            StartCoroutine(waitForHealthBarDrop());
        }
        else
        {
            StartCoroutine(waitForHealthBarDrop(true));
        }
    }

    public IEnumerator waitForHealthBarDrop(bool skip = false)
    {

        if (!skip)
        {
            yield return StartCoroutine(healthBar.checkForHealthBarComplete());

        }
        if (EventEnemyKilled != null)
        {
            EventEnemyKilled.Invoke(this);
        }
        type.onDie();
        gameObject.SetActive(false);
        type.health = type.maxHealth;
        isDead = false;
        if (flowFieldAgent)
        {
            flowFieldAgent.enabled = true;

        }
        statusEffectManager.reset();
        assignedSpawnpoint.addToInactivePool(this);
        if (GlobalVariables.ui.score != null)
            GlobalVariables.ui.score.IncreaseScore(scoreIncrease.amountToGain);
        
        yield break;
    }

    // DROP SHADOW SYSTEM =====================================================================================================================

    public SpriteRenderer getSpriteRenderer()
    {
        return sprite;
    }

    public void toggleShadow(bool isActive)
    {
        dropshadowController.toggleShadow(isActive);
    }

    public void changeFlyHeight(FlyingState state)
    {
        dropshadowController.changeState(state);
    }

    public void setCollision(FlyingState flyingState)
    {
        if (isBoss)
            return;
        
        switch (flyingState)
        {
            case FlyingState.GROUND:
                if (agent != null)
                {
                    agent.agentTypeID = NavMeshAgentType.WALKING_ID;
                    //agent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
                    agent.areaMask = 0001;   
                }
                Hitbox.enabled = true;
                break;
            case FlyingState.HOVERING:
                if (agent != null)
                {
                    agent.agentTypeID = NavMeshAgentType.FLYING_ID;
                    //agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
                    agent.areaMask = 1111;   
                }
                Hitbox.enabled = true;
                break;
            case FlyingState.FLYING:
                if (agent != null)
                {
                    agent.agentTypeID = NavMeshAgentType.FLYING_ID;
                    //agent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
                    agent.areaMask = 1111;   
                }
                Hitbox.enabled = false;
                break;
        }
    }

    // PUSH AND PULL SYSTEM =====================================================================================================================
    public void push(Vector3 direction, int force)
    {
        pushedThisFrame = true;

        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = (force - type.weight);

        pushSpeed = appliedForce + 10;

        if (pushSpeed <= 0)
        {
            return;
        }

        pushDirection = direction;
        setMoveMode(MoveMode.push);

    }

    public void pull(Vector3 direction, int force)
    {
        if (type.weight == 10 || force == 0)
        {
            return;
        }

        float appliedForce = force - type.weight;

        if (appliedForce > 0)
        {
            pushSpeed = appliedForce + 7.5f;
        }
        else
        {
            pushSpeed = 3;
        }

        pushDirection = direction;
        setMoveMode(MoveMode.pull);
        type.pullbehavior(pushDirection, pushSpeed);
    }

    public void pickUp(GameObject carryingObject, Vector3 carryPosition)
    {
        if (type.weight == 10)
        {
            return;
        }

        type.onPickUp();
        dropshadowController.toggleShadow(false);
        carryObject = carryingObject;
        pushDirection = carryPosition;
        setMoveMode(MoveMode.carry);
    }
    public void putDown(Vector3 placePosition)
    {
        type.onPutDown();
        dropshadowController.toggleShadow(true);
        transform.position = placePosition;
        setMoveMode(MoveMode.normal);
    }

    public bool isCarryable()
    {
        return type.weight < 10;
    }

    public void setMoveMode(MoveMode mm)
    {
        moveMode = mm;
    }



    // ARMOR CONTROLS =========================================================================================================================

    private void addArmor()
    {
        normalMaxHealth = type.maxHealth;

        type.maxHealth *= 2;
        type.health = type.maxHealth;

        if(type.weight == 10)
        {
            armorAddWeight = false;
        }
        else
        {
            type.weight += 1;
        }
    }

    private void addShield()
    {
        type.shieldHealth = type.maxHealth * 3;
        if (type.weight == 10)
        {
            shieldAddWeight = false;
        }
        else
        {
            type.weight += 1;
        }
    }

    public void removeArmor()
    {
        type.maxHealth = normalMaxHealth;

        if (armorAddWeight)
        {
            type.weight -= 1;
        }
        armorRemoved = true;
    }

    public void removeShield()
    {
        if (shieldAddWeight)
        {
            type.weight -= 1;
        }
        shieldRemoved = true;
    }

    private void OnDrawGizmos()
    {
        if (drawNavPath)
        {
            if (agent != null)
            {
                if (agent.enabled)
                {
                    for (int i = 1; i < agent.path.corners.Length; i++)
                    {
                        Gizmos.DrawLine(agent.path.corners[i - 1], agent.path.corners[i]);
                    }

                }
                else
                {
                    Gizmos.DrawLine(transform.position, navDestination);
                }   
            }
        }
    }

    public void PlaySFX(string effectName, bool useWorldSpace = true)
    {
        if (SFXManager.Instance)
        {
            if (useWorldSpace)
            {
                SFXManager.Instance.playSFX("event:/Enemies/" + effectName, transform.position);

            }
            else
            {
                SFXManager.Instance.playSFX("event:/Enemies/" + effectName);
            }
        }
    }

    public void PlayDirectSFX(string fullPathName)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName);
        }
    }

    public void PlayDirectSFX(string fullPathName, Vector3 worldSpace)
    {
        if (SFXManager.Instance)
        {
            SFXManager.Instance.playSFX(fullPathName, worldSpace);
        }
    }


}